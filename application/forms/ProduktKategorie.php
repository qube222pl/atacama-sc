<?php

class Application_Form_ProduktKategorie extends Moyoki_Form {

    public $entityManager;
    public $checkboxKategoriaPoziom0 = array(
        'FormElements',
        'Fieldset'
    );
    public $checkboxKategoriaPoziom1 = array(
        'ViewHelper',
        array('Label', array('class' => 'dupa', 'placement' => 'append', 'escape' => false)),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkbox col-12', 'style' => 'display: none;'))
    );
    public $checkboxKategoriaPoziom2 = array(
        'ViewHelper',
        array('Label', array('class' => 'dupa', 'placement' => 'append', 'escape' => false)),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkbox col-12 offset-1', 'style' => 'display: none;'))
    );
    public $checkboxKategoriaPoziom3 = array(
        'ViewHelper',
        array('Label', array('class' => 'dupa', 'placement' => 'append', 'escape' => false)),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkbox col-12 offset-2', 'style' => 'display: none;'))
    );
    public $checkboxKategoriaPoziom4 = array(
        'ViewHelper',
        array('Label', array('class' => 'dupa', 'placement' => 'append', 'escape' => false)),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkbox col-12 offset-3', 'style' => 'display: none;'))
    );

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-produkt-kategorie')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal')
                ->setDecorators($this->formDecorator);


        $kategorie = $this->entityManager->getRepository('Entities\Kategoria')->orderByNazwa();

        $fieldsetElementy = array();
        $fieldsetNazwa = null;
        foreach ($kategorie as $k) {
            if ($k->getPoziom() == 0) {
                if (count($fieldsetElementy) > 0) {
                    $this->addDisplayGroup($fieldsetElementy, $fieldsetNazwa, array('legend' => $fieldsetNazwa));
                    $fd = $this->getDisplayGroup($fieldsetNazwa);
                    $fd->setDecorators($this->checkboxKategoriaPoziom0);
                }
                $fieldsetNazwa = $k->getKategorieI18n()->first()->getNazwa();

                unset($fieldsetElementy);
                $fieldsetElementy = array();
            } else {
                $kategoria = $this->createElement('checkbox', 'kat_' . $k->getId());
                $kategoria->setLabel($k->getKategorieI18n()->first()->getNazwa())
                        ->setDecorators($this->{'checkboxKategoriaPoziom' . $k->getPoziom()});
                $this->addElement($kategoria);
                $fieldsetElementy[] = 'kat_' . $k->getId();
            }
        }

        /**
         * Dodanie ostatniej listy, bo taka sie sama z siebie nie pojawia
         */
        if (count($fieldsetElementy) > 0) {
            $this->addDisplayGroup($fieldsetElementy, $fieldsetNazwa, array('legend' => $fieldsetNazwa));
            $fd = $this->getDisplayGroup($fieldsetNazwa);
            $fd->setDecorators($this->checkboxKategoriaPoziom0);
        }

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));


        $submit = $this->createElement('submit', 'produkt');
        $submit->setLabel('ustaw')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
