<?php

class Application_Form_Banner extends Moyoki_Form {

    private $akcja = 'dodanie';

    public function __construct($options = null) {
        $this->akcja = $options;
        parent::__construct($options);
    }

    public function init() {
        $config = Atacama_Config::getInstance();

        $this->setMethod('post')
                ->setAttrib('id', 'form-banner')
                ->setAttrib('class', 'form-horizontal');

        $link = $this->createElement('text', 'link')
                ->setLabel('link (dla IMG):')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm')
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines()
                ))
                ->setAttrib('maxlength', 250)
                ->setAttrib('size', 60);

        $nazwa = $this->createElement('text', 'nazwa')
                ->setLabel('Nazwa:')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines()
                ))
                ->setAttrib('maxlength', 150)
                ->setAttrib('size', 60);

        $opis = $this->createElement('textarea', 'opis')
                ->setLabel('Opis')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm')
                ->setAttrib('cols', '30')
                ->setAttrib('rows', '5')
                ->setAttrib('style', 'width: 520px;')
                ->addFilters(array(
            new Zend_Filter_StringTrim(),
            new Zend_Filter_StripNewlines()
        ));

        if ($this->akcja != 'edycja') {
            $upload = new Zend_Validate_File_Upload();
            $upload->setMessages(array(
                Zend_Validate_File_Upload::NO_FILE => 'Plik nie był wysłany',
            ));

            $notExists = new Zend_Validate_File_NotExists($config->bannery->path);
            $notExists->setMessages(array(
                Zend_Validate_File_NotExists::DOES_EXIST => "Plik '%value%' istnieje już na serwerze. Zmień nazwę i wgraj ponownie.",
            ));

            $plik = $this->createElement('file', 'plik');
            $plik->setLabel('Banner:')
                    ->setAttrib('class', 'form-control sm')
                    ->setAttrib('style', 'width: 510px;')
                    ->setDecorators($this->fileDecorators)
                    ->setDestination($config->bannery->path)
                    ->setRequired(FALSE)
                    ->addValidators(array(
                        array('count', false, 1),
                        array('size', false, 5000000),
                        $notExists,
                        $upload
            ));
        }

        $widoczny = $this->createElement('checkbox', 'widoczny');
        $widoczny->setLabel('widoczny')
                ->setDecorators($this->checkboxElementDecorators);
        new Zend_Form_Decorator_HtmlTag();
        $this->addElement($widoczny);

        $this->addElement('hidden', 'id');
        $this->getElement('id')->removeDecorator('HtmlTag')->removeDecorator('label');

        $submit = $this->createElement('submit', 'submit');
        $submit->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-warning')
                ->setLabel('Dodaj');


        if ($this->akcja != 'edycja') {
            $tablica_elementow = array($nazwa, $link, $opis,  $plik, $widoczny, $submit);
        } else {
            $tablica_elementow = array($nazwa, $link, $opis, $widoczny, $submit);
        }

        $this->addElements($tablica_elementow);

        $this->addDisplayGroup($tablica_elementow, 'banner', array('class' => 'no-border'));
    }

}
