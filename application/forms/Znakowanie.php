<?php

class Application_Form_Znakowanie extends Moyoki_Form {

    public function init() {
        $this->setAttrib('id', 'form-znakowanie')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 100)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwa);

        $kod = $this->createElement('text', 'kod');
        $kod->setLabel('kod')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setDescription('Przyda się do łączenia z importami')
                ->setAttrib('class', 'form-control sm')
                ->setAttrib('maxlength', 10)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($kod);

        $kolory = $this->createElement('text', 'kolory');
        $kolory->setLabel('Ilość kolorów')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm text-right')
                ->setAttrib('maxlength', 10)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($kolory);

        $ryczalt = $this->createElement('text', 'ryczalt');
        $ryczalt->setLabel('Kwota ryczałtu')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm text-right')
                ->setAttrib('maxlength', 10)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
        ));
        $this->addElement($ryczalt);


        $przygotowalnia = $this->createElement('text', 'przygotowalnia');
        $przygotowalnia->setLabel('Koszt przygotowalni')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm text-right')
                ->setAttrib('maxlength', 10)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ));
        $this->addElement($przygotowalnia);

        $powtorzenie = $this->createElement('text', 'powtorzenie');
        $powtorzenie->setLabel('Koszt powtórzenia')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm text-right')
                ->setAttrib('maxlength', 10)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ));
        $this->addElement($powtorzenie);

        
        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));


        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
