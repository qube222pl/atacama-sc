<?php

class Application_Form_Validate_Nip extends Zend_Validate_Abstract
{
    const NIP_FORMAT = 'msgNipFormat';
    const NIP_CHECKSUM = 'msgNipChecksum';
 
    protected $_messageTemplates = array(
        self::NIP_FORMAT => "numer NIP ma niepoprawny format",
        self::NIP_CHECKSUM => "podano bledny numer NIP"
    );
 
    public function isValid($value)
    {
        $this->_setValue($value);
        $value = trim($value);
        
        
        if(strlen($value) < 2) {
            $this->_error(self::NIP_FORMAT);
            return false;
        }

        // sprawdzamy czy mamy polski NIP
        if(substr_compare($value, "PL", 0, 2, true) == 0 || is_numeric($value{0})) {

            // polski NIP
            if(!preg_match('/^(PL){0,1}((\d{3}\-\d{2}\-\d{2}\-\d{3})|(\d{3}\-\d{3}\-\d{2}\-\d{2})|(\d{10}))$/', $value)) {
                $this->_error(self::NIP_FORMAT);
                return false;
            }

            if (!$this->_checksum($value)) {
                $this->_error(self::NIP_CHECKSUM);
                return false;
            }
            
        } else {
            
            // zagraniczny NIP
            if($this->_foreignNumber($value)) {
                return true;
            } else {
                $this->_error(self::NIP_FORMAT);
                return false;
            }
            
        } 
        
        return true;
    }
    
    private function _checksum($value)
    {
        
        $NIP = str_replace('-', '', $value);
        $NIP = str_replace('PL', '', $NIP);
        
        if(strlen($NIP) == 10) {
            $sum  = 0;
            for($i = 0; $i < 9; ++$i)
                $sum += $NIP{$i} * substr('657234567', $i, 1);
                
            if ($sum % 11 == $NIP{9} )
                return true;
        }
        
        return false;
    }
    
    private function _foreignNumber($value)
    {
        
        $formats = array(
            'AT' => '/^ATU\d{8}$/',
            'BE' => '/^BE\d{9}$/',
            'CY' => '/^CY\d{8}[A-Z]{1}$/',
            'CZ' => '/^CZ\d{8,10}$/',
            'DK' => '/^DK\d{8}$/',
            'EE' => '/^EE\d{9}$/',
            'FI' => '/^FI\d{8}$/',
            'FR' => '/^FR[0-9A-HJ-NP-Z]{2}\d{9}$/',
            'EL' => '/^EL\d{9}$/',
            'ES' => '/^ES[0-9A-Z]\d{7}[0-9A-Z]$/',
            'NL' => '/^NL\d{9}B\d{2}$/',
            'IE' => '/^IE\d[0-9A-Z\+\*]\d{5}[A-Z]$/',
            'LT' => '/^LT((\d{9})|(\d{12}))$/',
            'LU' => '/^LU\d{8}$/',
            'LV' => '/^LV\d{11}$/',
            'MT' => '/^MT\d{8}$/',
            'DE' => '/^DE\d{9}$/',
            'PT' => '/^PT\d{9}$/',
            'SK' => '/^SK\d{9,10}$/',
            'SI' => '/^SI\d{8}$/',
            'SE' => '/^SE\d{12}$/',
            'HU' => '/^HU\d{8}$/',
            'GB' => '/^GB((\d{9})|(\d{12}))$/',
            'IT' => '/^IT\d{11}$/'
        );
        
        $country = substr($value, 0, 2);
        if(isset($formats[$country])) {
            return preg_match($formats[$country], $value);
        } else {
            return false;
        }
        
    }
    
    
}