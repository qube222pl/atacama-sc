<?php

class Application_Form_HasloPrzypomnij extends Moyoki_Form {

    public function init() {
        $this->setAttrib('class', 'form-horizontal')
                ->setAttrib('id', 'form-przypomnij');

        $email = $this->createElement('text', 'email');
        $email->setLabel('E-mail')
                ->setRequired(TRUE)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 80)
                ->setAttrib('required', 'true')
                ->setDecorators($this->divElementDecorators)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS)
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($email);

        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('wyslij')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');
        $this->addElement($submit);
    }

}
