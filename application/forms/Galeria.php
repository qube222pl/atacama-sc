<?php

class Application_Form_Galeria extends Moyoki_Form {

    public function init() {

        $view = Zend_Layout::getMvcInstance()->getView();

        $this->setIsArray(false) //Jakby parametry musialy byc tablica np jezyki
                ->setMethod('post')
                ->setAttrib('id', 'form-galeria')
                ->setAttrib('class', 'form-horizontal');

        $this->addElement($this->createElement('text', 'nazwa')
                        ->setLabel('Nazwa:')
                        ->setAttrib('class', 'form-control')
                        ->setDecorators($this->divElementDecorators)
                        ->setRequired(TRUE)
                        ->setAttrib('maxlength', 150)
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));

        $this->addElement($this->createElement('textarea', 'opis')
                        ->setLabel('Opis')
                        ->setDecorators($this->divTextareaDecorators)
                        ->setAttrib('class', 'form-control')
                        ->setAttrib('cols', '45')
                        ->setAttrib('rows', '3')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim()
        )));

        $this->addElement($this->createElement('checkbox', 'widoczna')
                        ->setLabel('Galeria widoczna')
                ->setDecorators($this->checkboxElementDecorators));


        $this->addDisplayGroup(array('nazwa', 'opis', 'widoczna'), 'Galeria', array('class' => 'no-border'));


        $this->addElement($this->createElement('hidden', 'id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));


        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('zmień')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
