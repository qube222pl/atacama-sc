<?php

class Application_Form_ZamowienieZmienStatus extends Moyoki_Form {

    public function __construct() {
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-zamowienie-status')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $status = $this->createElement('select', 'status');

        $status->setLabel('zmien status')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $conf = Atacama_Config::getInstance();

        foreach ($conf->zamowienia->statusy as $id => $nazwa) {
            // Bez koszyka
            if ($id > 0) {
                $status->addMultiOption($id, $nazwa);
            }
        }

        $this->addElement($status);

        $this->addElement($this->createElement('textarea', 'uwagi')
                        ->setLabel('uwagi')
                        ->setAttrib('class', 'form-control sm')
                        ->setDecorators($this->divTextareaDecorators)
                        ->setAttrib('rows', '3')
                        ->setAttrib('cols', '10')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim()
        )));

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $this->addElement($this->createElement('hidden', 'redirect')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'zmien');
        $submit->setValue('zmien')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
