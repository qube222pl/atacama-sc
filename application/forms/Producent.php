<?php

class Application_Form_Producent extends Moyoki_Form {

    public $entityManager;
    private $_akcja;

    public function __construct($entityManager, $akcja = 'zmien') {
        $this->entityManager = $entityManager;

        $this->_akcja = $akcja;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-producent')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 100)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwa);

        $this->addElement($this->createElement('file', 'logotyp')
                ->setDecorators($this->divElementFileDecorators)
                        ->setLabel('Logotyp')
                        ->setAttrib('title', 'Logotyp'));
        if($this->_akcja == 'zmien') {
            $this->getElement('logotyp')->setDescription('Dodanie nowego pliku nadpisze obecny, jeżeli jest.');
        }

         $widoczny = $this->createElement('checkbox', 'widoczny');
        $widoczny->setLabel('widoczny')
                ->setDecorators($this->checkboxElementDecorators);
                new Zend_Form_Decorator_HtmlTag();
        $this->addElement($widoczny);
        
        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'firma');
        $submit->setLabel('wykonaj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
