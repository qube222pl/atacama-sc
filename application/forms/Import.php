<?php

class Application_Form_Import extends Moyoki_Form {

    public $entityManager;

//    private $_akcja;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
//        $this->_akcja = $akcja;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-import')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');


        $dystrybutor = $this->createElement('select', 'dystrybutor');
        $dystrybutor->setLabel('Dystrybutor')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $dystrybutor->addMultiOption('null', '-- Proszę wybrać --');
        $dystrybutorzy = $this->entityManager->getRepository('Entities\Dystrybutor')->orderByNazwa(1);
        foreach ($dystrybutorzy as $dystr) {
            $dystrybutor->addMultiOption($dystr->getId(), $dystr->getNazwa());
        }
        $this->addElement($dystrybutor);

        $placer = $this->createElement('hidden', 'placer');
        $placer->setDecorators(array(
            array('Description', array('class' => 'help-block', 'placement' => 'appand')),
            array('Label', array('placement' => 'prepand', 'class' => 'control-label')),
            array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'id' => 'placer'))
        ));

        $this->addElement($placer);

        $upload = new Zend_Validate_File_Upload();
        $upload->setMessages(array(Zend_Validate_File_Upload::NO_FILE => 'Plik nie był wysłany'));
        $ext = new Zend_Validate_File_Extension('xml, xlsx, xls, csv, zip');
        $ext->setMessages(array(Zend_Validate_File_Extension::FALSE_EXTENSION => "Plik '%value%' ma niepoprawne rozszerzenie"));
        $size = new Zend_Validate_File_Size(20 * 1024 * 1024);
        $size->setMessages(array(Zend_Validate_File_Size::TOO_BIG => "Maksymalna dopuszczalna wielkość pliku '%value%' wynosi '%max%' a wykryto '%size%'"));

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);


        $plik = new Zend_Form_Element_File('plik');

        $plik->setLabel('Plik z danymi')
                ->setDecorators($this->fileDecorators)
                ->setDestination($path)
                ->setRequired(FALSE)
                ->addValidators(array(
                    array('count', false, 1),
                    $size,
                    $upload,
                    $ext
        ));
        $this->addElement($plik);

        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
