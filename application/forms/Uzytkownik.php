<?php

class Application_Form_Uzytkownik extends Moyoki_Form {

    public $entityManager;
    private $_akcja;

    public function __construct($entityManager, $akcja = 'edycja') {
        $this->entityManager = $entityManager;

        $this->_akcja = $akcja;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-uzytkownik')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal')
                ->setDecorators($this->formDecorator);


        $imie = $this->createElement('text', 'imie');
        $imie->setLabel('imie')
                ->setRequired(TRUE)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 50)
                ->setDecorators($this->divElementDecorators)
                ->addFilters(array(
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($imie);

        $nazwisko = $this->createElement('text', 'nazwisko');
        $nazwisko->setLabel('nazwisko')
                ->setRequired(TRUE)
                ->setAttrib('class', 'form-control')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('maxlength', 50)
                ->addFilters(array(
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwisko);

        $email = $this->createElement('text', 'email');
        $email->setLabel('login (e-mail)')
                ->setRequired(TRUE)
                ->setAttrib('class', 'form-control')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('maxlength', 80)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS)
                ))
                ->addErrorMessage('Login (e-mail) jest niepoprawny');
        $this->addElement($email);

        $haslo1 = $this->createElement('password', 'haslo1');
        $haslo1->setLabel('haslo')
                ->setAttrib('class', 'form-control')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('maxlength', 40)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_StringLength(8, 30, 'UTF-8')
                ))
                ->addErrorMessage('Hasło jest wymagane')
                ->setDescription('haslo sklada sie z 8 do 30 znakow');
        $this->addElement($haslo1);

        $haslo2 = $this->createElement('password', 'haslo2');
        $haslo2->setLabel('powtorz haslo')
                ->setAttrib('class', 'form-control')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('maxlength', 40)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    array(new Zend_Validate_StringLength(8, 30, 'UTF-8'), 'breakChainOnFailure'),
                    new Zend_Validate_Identical(array('token' => 'haslo1')
                    )
                ))
                ->addErrorMessage('haslo musi byc zgodne z polem haslo');
        $this->addElement($haslo2);

        if ($this->_akcja == 'edycja') {
            $haslo1->setRequired(FALSE);
            $haslo2->setRequired(FALSE);
        } else {
            $haslo1->setRequired(TRUE);
            $haslo2->setRequired(TRUE);
        }

        if (Atacama_Acl::getInstance()->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_UZYTKOWNICY)) {

            $rola = $this->createElement('select', 'rola');

            $rola->setLabel('rola')
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control sm');

            foreach (Atacama_Acl::$_wszystkieRole as $rolaID => $rolaNazwa) {
                if ($rolaID != Atacama_Acl::ROLA_NIEZALOGOWANY)
                    $rola->addMultiOption($rolaID, $rolaNazwa);
            }

            $this->addElement($rola);

            $firma = $this->createElement('select', 'firmy_id');

            $firma->setLabel('przypisany do firmy')
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control md');

            $firmy = $this->entityManager->getRepository('Entities\Firma')->pobierzWszystkieOrderBy();

            foreach ($firmy as $f) {
                $firma->addMultiOption($f->getId(), $f->getNazwa());
            }

            $this->addElement($firma);


            $telefon = $this->createElement('text', 'telefon');
            $telefon->setLabel('telefon kontaktowy')
                    ->setRequired(FALSE)
                    ->setAttrib('class', 'form-control')
                    ->setDescription('Jeżeli użytkownik jest Opiekunem, to ten numer będzie prezentowany jego klientom')
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('maxlength', 50)
                    ->addFilters(array(
                        new Zend_Filter_HtmlEntities(),
                        new Zend_Filter_StringTrim()
                    ))
                    ->addErrorMessage('pole jest wymagane');
            $this->addElement($telefon);

            $aktywny = $this->createElement('checkbox', 'active');
            $aktywny->setLabel('aktywny')
                    //->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control11');

            $this->addElement($aktywny);
        }

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'uzytkownik');
        $submit->setLabel('wykonaj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
