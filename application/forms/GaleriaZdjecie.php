<?php

class Application_Form_GaleriaZdjecie extends Moyoki_Form {

    private $_galeria_id;

    public function init() {
        $this->setAttrib('class', 'form-inline');

        $this->setMethod('post')->setAttrib('id', 'fileupload');

        $this->addElement($this->createElement('file', 'galeriazdjecie')
                        //->setAttrib('class', 'form-control btn btn-primary')
                        ->setDecorators($this->fileDecorators)
                        ->setLabel('Dodaj zdjęcie')
                        ->setAttrib('title', 'Nowe zdjęcie')
        );

        $this->addElement($this->createElement('hidden', 'galeria_id')
                        ->setValue($this->_galeria_id)
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));
    }

    public function __construct($options = null) {
        $this->_galeria_id = $options;
        parent::__construct($options);
    }

}
