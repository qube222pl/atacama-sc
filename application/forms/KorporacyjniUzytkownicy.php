<?php

class Application_Form_KorporacyjniUzytkownicy extends Moyoki_Form {

    public function init() {
        $this->setAttrib('id', 'form-korporacyjni-uzytkownicy')
                ->setMethod('post')
                ->setAttrib('class', 'form-inline');

        $uzytkownik = $this->createElement('select', 'uzytkownik')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setLabel('Dodaj użytkownika');
        $this->addElement($uzytkownik);

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'Dodaj');
        $submit->setValue('Wybierz')
                ->setAttrib('class', 'btn btn-primary btn-raised')
                ->setValue('Wybierz')
                ->setDecorators($this->inlineSubmitDivElementDecorators);

        $this->addElement($submit);
    }

}
