<?php

class Application_Form_AdminWyszukiwarka extends Moyoki_Form {

    public function init() {
        $this->setAttrib('id', 'form-admin-wyszukiwarka')
                ->setMethod('get')
                ->setAttrib('class', 'form-inline');

        $firma = $this->createElement('text', 'f');
        $firma->setLabel('firma')
                ->setRequired(FALSE)
                ->setAttrib('maxlength', 60)
                ->setAttrib('placeholder', 'imie lub nazwisko')
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_StringLength(2, 60, 'UTF-8')
                ))
                ->setDecorators(array(
                    'ViewHelper',
                    array('Errors', array('tag' => 'div', 'class' => 'form-error label label-warning')),
                    array('Description', array('class' => 'help-block', 'placement' => 'appand')),
                    array('Label', array('placement' => 'prepand', 'class' => 'sr-only')),
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
                ))
                ->setAttrib('class', 'form-control');
        
        $this->addElement($firma);

        $submit = $this->createElement('submit', 'szukaj');
        $submit->setValue('Szukaj')
                ->setAttrib('class', 'btn btn-primary btn-raised')
                ->setDecorators(array(
                    'ViewHelper',
                    'Description',
                    'Errors',
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
        ));

        $this->addElement($submit);
    }

}
