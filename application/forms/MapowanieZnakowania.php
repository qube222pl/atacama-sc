<?php

class Application_Form_MapowanieZnakowania extends Moyoki_Form {

    private $_entityManager;
    private $_dystrybutorId;

    public function __construct($entityManager, $dystrybutorId) {
        $this->_entityManager = $entityManager;
        $this->_dystrybutorId = $dystrybutorId;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-znakowania')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $importZnakowania = $this->_entityManager->getRepository('Entities\Import_Znakowanie')->filtrujPoDystrybutorze($this->_dystrybutorId, 1);
        $znakowania = $this->_entityManager->getRepository('Entities\Znakowanie')->getOrderByNazwa();

        $importParams = $this->_entityManager->getRepository('Entities\Import_Parametr')->filtrujPoDystrybutorze($this->_dystrybutorId);
        $showId = (bool)(isset($importParams['pokazujIdZnakowan']) && $importParams['pokazujIdZnakowan'] == 'tak');

        $znakowaniaSelect = array();
        foreach ($znakowania as $znakowanie) {
            $znakowaniaSelect[$znakowanie->getId()] = $znakowanie->getNazwa();
        }
        
        $populate = array();

        foreach ($importZnakowania as $importZnakowanie) {

            $znakowanie = $this->createElement('select', 'znakowanie_' . $importZnakowanie->getId());
            $znakowanieId = ($showId) ? (' [id:' . $importZnakowanie->getZnakowanieDystrId() . ']') : '';
            $znakowanie->setLabel($importZnakowanie->getZnakowanieDystrNazwa() . $znakowanieId)
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control sm');

            $znakowanie->addMultiOptions($znakowaniaSelect);

            $populate[('znakowanie_' . $importZnakowanie->getId())] = $importZnakowanie->getZnakowanieId();

            $this->addElement($znakowanie);
            unset($znakowanie);
            
        }
        
        $this->populate($populate);
        
        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

        $this->getView()->parametry = (count($importZnakowania) != 0 ? true : false);

    }

}
