<?php

class Application_Form_KoszykProduktZnakowanie extends Moyoki_Form {

    public $entityManager;
    private $_prdId = null;
    private $_detalId = null;
    private $_zam_id = null;

//    private $_akcja;

    public function __construct($entityManager, $prodId, $detal_id, $edycja) {
        $this->entityManager = $entityManager;
        $this->_prdId = $prodId;
        $this->_detalId = $detal_id;
        $this->_zam_id = $edycja;
        parent::__construct();
    }

    public function init() {
        $view = Zend_Layout::getMvcInstance()->getView();

        $this->setAttrib('id', 'form-znakowanie-dodawanie')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');
        if ($this->_zam_id > 0) {
            $this->setAction($view->baseUrl('/pl/zamowienia/dodaj-znakowanie/id/' . $this->_zam_id));
            $this->addElement($this->createElement('hidden', 'zam_id')->removeDecorator('HtmlTag')
                            ->removeDecorator('label')
                            ->setValue($this->_zam_id));
        } else {
            $this->setAction($view->baseUrl('/pl/koszyk/dodaj-znakowanie/'));
        }




        $metoda = $this->createElement('select', 'metoda');
        $metoda->setLabel('metoda znakowania')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $metoda->addMultiOption('null', '-- Proszę wybrać --');
        $metody = $this->entityManager->getRepository('Entities\Produkt_Has_Znakowanie')->getByProduktId($this->_prdId);
        foreach ($metody as $met) {
            $metoda->addMultiOption($met->getZnakowanie()->getId(), $met->getZnakowanie()->getnazwa());
        }
        $this->addElement($metoda);

        $kolory = $this->createElement('select', 'kolory');
        $kolory->setLabel('ilosc kolorow')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $kolory->addMultiOption('null', '-- Proszę wybrać --');
        $this->addElement($kolory);

        $powtorzenie = $this->createElement('checkbox', 'powtorzenie');
        $powtorzenie->setLabel('powtorzenie wzoru')
                ->setDecorators($this->checkboxElementDecorators);
        new Zend_Form_Decorator_HtmlTag();
        $this->addElement($powtorzenie);

        $this->addElement($this->createElement('hidden', 'detal_id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label')
                        ->setValue($this->_detalId));

        $this->addElement($this->createElement('hidden', 'prod_id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label')
                        ->setValue($this->_prdId));

        $submit = $this->createElement('submit', 'dodaj');
        $submit->setValue('dodaj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
