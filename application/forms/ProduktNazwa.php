<?php

class Application_Form_ProduktNazwa extends Moyoki_Form {

    public function __construct() {
        parent::__construct();
    }

    public function init() {
        $this->setIsArray(TRUE) //Jakby parametry musialy byc tablica np jezyki
                ->setMethod('post')
                ->setAttrib('id', 'form-produkt-opis')
                ->setAttrib('class', 'form-horizontal');

        $view = Zend_Layout::getMvcInstance()->getView();

        $langs = Atacama_Config::getInstance()->resources->translate->languages->toArray();

        foreach ($langs as $lang => $langNazwa) {
            //nazwy
            $this->addElement($this->createElement('text', 'nazwa_' . $lang)
                            ->setLabel('Nazwa:')
                            ->setAttrib('maxlength', 100)
                            ->setDecorators($this->divElementDecorators)
                            ->setAttrib('class', 'form-control')
                            ->setBelongsTo($lang)
                            ->addFilters(array(
                                new Zend_Filter_StringTrim(),
                                new Zend_Filter_StripNewlines()
            )));

            $this->addElement($this->createElement('textarea', 'opis_' . $lang)
                            ->setBelongsTo($lang)
                            ->addFilters(array(
                                new Zend_Filter_StringTrim(),
                                new Zend_Filter_StripNewlines()
            )));


            $cke = new Moyoki_CKEditor($view->baseUrl('/js/libs/ckeditor/'));
            $cke->replace($lang . '[opis_' . $lang . ']');

            $this->addElement($this->createElement('textarea', 'seo_opis_' . $lang)
                            ->setBelongsTo($lang)
                            ->setLabel('Opis do google')
                            ->setDecorators($this->divTextareaDecorators)
                            ->setAttrib('class', 'form-control')
                            ->setDescription('Przydatne do pozycjonowania produktu w google - czysty opis bez znaczników')
                            ->setAttrib('rows', 3)
                            ->addFilters(array(
                                new Zend_Filter_StringTrim(),
                                new Zend_Filter_StripNewlines()
            )));


            $this->addDisplayGroup(array('nazwa_' . $lang, 'opis_' . $lang, 'seo_opis_' . $lang), 'grupa ' . $lang, array('legend' => 'Grupa ' . $langNazwa));
        }


        $this->addElement($this->createElement('hidden', 'id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('Zapisz')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
