<?php

class Application_Form_Firma extends Moyoki_Form {

    public $entityManager;
    private $_akcja;

    public function __construct($entityManager, $akcja = 'zmien') {
        $this->entityManager = $entityManager;

        $this->_akcja = $akcja;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-firma')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 200)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwa);

        $nip = $this->createElement('text', 'nip');
        $nip->setLabel('NIP')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 14)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->addFilters(array(
                    new Zend_Filter_Alnum(),
                    new Zend_Filter_StringTrim()
                ))
                ->addValidator(new Application_Form_Validate_Nip());

        if ($this->_akcja == 'zmien') {
            $nip->setAttrib('readonly', 'readonly');
        }

        $this->addElement($nip);

        $regon = $this->createElement('text', 'regon');
        $regon->setLabel('REGON')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 10)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm')
                ->addFilters(array(
                    new Zend_Filter_Digits(),
                    new Zend_Filter_StripTags(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($regon);


        if (Atacama_Acl::getInstance()->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_FIRMY)) {

            $upust = $this->createElement('text', 'upust');
            $upust->setLabel('% upustu')
                    ->setRequired(TRUE)
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control sm text-right')
                    ->setAttrib('maxlength', 2)
                    ->addFilters(array(
                        new Zend_Filter_Digits(),
                        new Zend_Filter_StripTags(),
                        new Zend_Filter_StringTrim()
                    ))
                    ->addErrorMessage('pole jest wymagane');
            $this->addElement($upust);

            $ph = $this->createElement('select', 'ph_id');

            $ph->setLabel('przestawiciel')
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control sm');

            $uzytkownicy = $this->entityManager->getRepository('Entities\Uzytkownik')->phOrderByNazwisko();

            foreach ($uzytkownicy as $u) {
                $ph->addMultiOption($u->getId(), $u->getNazwisko() . ' ' . $u->getImie());
            }

            $this->addElement($ph);
        }

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'firma');
        $submit->setLabel('wykonaj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }
}
