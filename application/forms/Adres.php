<?php

class Application_Form_Adres extends Moyoki_Form {

    // wartość stałej musi być 2 do N-tej potęgi
    // ponieważ opcje można dodawać w granicach logiki
    const DODAJ = 1;
    const ZMIEN = 2;
    const ADRES_FAKTURA = 4; // adres fakturowy
    const ADRES_DOSTAWA = 8; // adres dostawy
//    const ZAMKNIJ = 16; // z klawiszem zamknij
    const ADRES_CONTROLLER = 32; // przekierowanie do adres/dodaj

    public $entityManager;
    private $_opcje;
    private $_lang;
    private $_powrot;

    public function __construct($entityManager, $opcje = self::ZMIEN, $lang = 'pl', $powrot = null) {
        $this->entityManager = $entityManager;
        $this->_lang = $lang;
        $this->_opcje = $opcje;
        $this->_powrot = $powrot;
        parent::__construct();
    }

    public function init() {

        $view = Zend_Layout::getMvcInstance()->getView();

        $this->setAttrib('id', 'form-adres')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        if ($this->_opcje & self::ADRES_CONTROLLER) {
            $view = Zend_Layout::getMvcInstance()->getView();
            $this->setAction($view->baseUrl('/' . $this->_lang . '/adres/dodaj'));
        }

        if ($this->_powrot != null) {
            $powrot = $this->createElement('hidden', 'powrot')
                    ->removeDecorator('HtmlTag')
                    ->removeDecorator('label');
            $powrot->setValue($this->_powrot);
            $this->addElement($powrot);
        }

        $adres = $this->createElement('hidden', 'typ_adresu');
        $adres->removeDecorator('HtmlTag')
                ->removeDecorator('label');

        if ($this->_opcje & self::ADRES_DOSTAWA) {
            $adres->setValue(2);
            $this->addElement($adres);
        } else if ($this->_opcje & self::ADRES_FAKTURA) {
            $adres->setValue(1);
            $this->addElement($adres);
        } else {
            $typ = $this->createElement('select', 'typ_adresu');

            $typ->setLabel('typ adresu')
                    ->setDecorators($this->divTextareaDecorators)
                    ->setAttrib('class', 'form-control sm')
                    ->addMultiOption(1, $view->translate('adres fakturowy'))
                    ->addMultiOption(2, $view->translate('adres dostawy'));

            $this->addElement($typ);
        }

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 200)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwa);

        $ulica = $this->createElement('text', 'ulica');
        $ulica->setLabel('adres')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 200)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($ulica);

        $kod = $this->createElement('text', 'kod_pocztowy');
        $kod->setLabel('kod pocztowy')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm')
                ->setAttrib('maxlength', 6)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($kod);

        $miasto = $this->createElement('text', 'miasto');
        $miasto->setLabel('miejscowosc')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 100)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($miasto);

        $kraj = $this->createElement('text', 'kraj');
        $kraj->setLabel('kraj')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 100)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($kraj);

        $telefon = $this->createElement('text', 'telefon');
        $telefon->setLabel('nr kontaktowy')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 40)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
        ));
        $this->addElement($telefon);

        $this->addElement($this->createElement('hidden', 'firmy_id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        if ($this->_opcje & self::DODAJ) {
            $label = 'dodaj';
        } else if ($this->_opcje & self::ZMIEN) {
            $label = 'zmien';
        } else {
            $label = 'wykonaj';
        }

        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setLabel($label)
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
