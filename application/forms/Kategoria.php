<?php

class Application_Form_Kategoria extends Moyoki_Form {

    public $entityManager;
    private $_akcja;

    public function __construct($entityManager, $akcja = 'zmien') {
        $this->entityManager = $entityManager;
        $this->_akcja = $akcja;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-kategoria')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 60)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_StringLength(2, 60, 'UTF-8')
                ))
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwa);

        $kat = $this->createElement('select', 'kategoria');

        $kat->setLabel('Kategoria nadrzędna')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $kat->addMultiOption(0, 'Nowa kategoria główna');

        $kategorie = $this->entityManager->getRepository('Entities\Kategoria')->orderByNazwa('pl');

        foreach ($kategorie as $kategoria) {
            $kat->addMultiOption($kategoria->getId(), str_repeat('-', $kategoria->getPoziom() * 3) . $kategoria->getKategorieI18n()->first()->getNazwa());
        }

        $this->addElement($kat);

        $konfekcjonowanie = $this->createElement('text', 'konfekcjonowanie');
        $konfekcjonowanie->setLabel('Konfekcjonowanie')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm text-right')
                ->setAttrib('maxlength', 10)
                ->setValue('0,00')
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_Float('pl_PL')
                ))
                ->addErrorMessage('Niepoprawna liczba!');
        ;
        $this->addElement($konfekcjonowanie);


        $opcje = $this->createElement('select', 'opcje');

        $opcje->setLabel('Pozycja na stronie')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $opcje->addMultiOption(0, 'Lewe menu')
                ->addMultiOption(1, 'Górne menu')
                ->addMultiOption(2, 'Ukryta');

        $this->addElement($opcje);
        
        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));



        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

    }

}
