<?php

class Application_Form_MapowanieProducenci extends Moyoki_Form {

    private $_entityManager;
    private $_dystrybutorId;

    public function __construct($entityManager, $dystrybutorId) {
        $this->_entityManager = $entityManager;
        $this->_dystrybutorId = $dystrybutorId;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-producenci')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal')
                ->setDecorators(array(
                    array('viewScript', array('viewScript' => 'import/mapowanieProducenci.phtml'))
        ));


        $importProducenci = $this->_entityManager->getRepository('Entities\Import_Producent')->filtrujPoDystrybutorze($this->_dystrybutorId, 1);
        $producenci = $this->_entityManager->getRepository('Entities\Producent')->orderByNazwa();

        $importParams = $this->_entityManager->getRepository('Entities\Import_Parametr')->filtrujPoDystrybutorze($this->_dystrybutorId);
        $showId = (bool) (isset($importParams['pokazujIdProducentow']) && $importParams['pokazujIdProducentow'] == 'tak');
        $showDiscount = (bool)(isset($importParams['pokazujRabaty']) && $importParams['pokazujRabaty'] == 'tak');
        $this->setAttrib('showDiscount', $showDiscount);

        $producenciSelect = array();

        foreach ($producenci as $producent) {
            $producenciSelect[$producent->getId()] = $producent->getNazwa();
        }

        $populate = array();

        $i=0;
        foreach ($importProducenci as $importProducent) {

            $i++;
            $producent = $this->createElement('select', 'producent_' . $importProducent->getId());
            $producentId = ($showId) ? (' [id:' . $importProducent->getProducentDystrId() . ']') : '';
            $producent->setLabel($importProducent->getProducentDystrNazwa() . $producentId)
                    ->setAttrib('class', 'form-control sm')
                    ->removeDecorator('label');

            $producent->addMultiOptions($producenciSelect);

            $populate[('producent_' . $importProducent->getId())] = $importProducent->getProducenciId();

            $this->addElement($producent);
            unset($producent);

            if($showDiscount) {
                $rabat = $this->createElement('text', 'rabat_' . $importProducent->getId());
                $rabat //->setLabel($name)
                        ->setRequired(TRUE)
                        ->setAttrib('maxlength', 6)
                        ->setAttrib('class', 'form-control xs text-right')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_HtmlEntities(),
                            new Zend_Filter_StripNewlines(),
                            new Zend_Filter_StripTags()
                        ))
                        ->addValidators(array(
                            new Zend_Validate_StringLength(1, 6, 'UTF-8')
                        ))
                        ->addErrorMessage('pole jest wymagane')
                        ->removeDecorator('label');
                $this->addElement($rabat);
                unset($rabat);
                $populate[('rabat_' . $importProducent->getId())] = $importProducent->getRabat();
            }            
        }

        $this->populate($populate);
//        Moyoki_Debug::debug($this->getElements());
        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

        $this->getView()->parametry = (count($importProducenci) != 0 ? true : false);
    }

}
