<?php

class Application_Form_Rejestracja extends Moyoki_Form {

    public function init() {
        $this->setAttrib('id', 'form-rejestracja')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $email = $this->createElement('text', 'email');
        $email->setLabel('login (e-mail)')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setAttrib('maxlength', 80)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS)
                ))
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addErrorMessage('Login (e-mail) jest niepoprawny');
        $this->addElement($email);

        $haslo1 = $this->createElement('password', 'haslo1');
        $haslo1->setLabel('haslo')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 40)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_StringLength(8, 30, 'UTF-8')
                ))
                ->addErrorMessage('Hasło jest wymagane')
                ->setDescription('haslo sklada sie z 8 do 30 znakow');
        $this->addElement($haslo1);


        $haslo2 = $this->createElement('password', 'haslo2');
        $haslo2->setLabel('powtorz haslo')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 40)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    array(new Zend_Validate_StringLength(8, 30, 'UTF-8'), 'breakChainOnFailure'),
                    new Zend_Validate_Identical(array('token' => 'haslo1')
                    )
                ))
                ->addErrorMessage('haslo musi byc zgodne z polem haslo');
        $this->addElement($haslo2);

        $imie = $this->createElement('text', 'imie');
        $imie->setLabel('imie')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 50)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addErrorMessage('brak wpisu');
        $this->addElement($imie);

        $nazwisko = $this->createElement('text', 'nazwisko');
        $nazwisko->setLabel('nazwisko')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setAttrib('maxlength', 100)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addErrorMessage('brak wpisu');
        $this->addElement($nazwisko);

        $this->addDisplayGroup(array('email', 'haslo1', 'haslo2', 'imie', 'nazwisko'), 'user', array('class' => 'no-border'));

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa firmy')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('maxlength', 200)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addErrorMessage('brak wpisu');
        $this->addElement($nazwa);

        $nip = $this->createElement('text', 'nip');
        $nip->setLabel('NIP')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 10)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidator(new Application_Form_Validate_Nip());

        $this->addElement($nip);

        $regon = $this->createElement('text', 'regon');
        $regon->setLabel('regon')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm')
                ->setAttrib('maxlength', 12)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addErrorMessage('brak wpisu');
        $this->addElement($regon);

        $regulamin = $this->createElement('checkbox', 'regulamin');
        $regulamin->setLabel('zatwierdz regulamin')
                ->setRequired(TRUE)
                ->setAttrib('required', 'true')
                ->setDecorators($this->checkboxElementDecorators)
                ->addValidator(new Zend_Validate_InArray(array(1)), false)
                ->addErrorMessage('akceptacja jest wymagana');

        $this->addElement($regulamin);


        $submit = $this->createElement('submit', 'rejestruj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

    }

}
