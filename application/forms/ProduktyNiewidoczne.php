<?php

class Application_Form_ProduktyNiewidoczne extends Moyoki_Form {

    public $entityManager;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    public function init() {
        $this
                ->setMethod('get')
                ->setAttrib('id', 'form-produkty-niewidoczne')
                ->setAttrib('class', 'form-inline');

        $fraza = $this->createElement('text', 'szukane');
        $fraza
                ->setRequired(FALSE)
                ->setAttrib('placeholder', 'szukane')
                ->setDecorators($this->inlineDivElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 100)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
        ));
        $this->addElement($fraza);

        $dystrybutor = $this->createElement('select', 'dystrybutor');
        $dystrybutor->setLabel('Dystrybutor')
                ->setDecorators($this->inlineDivElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $dystrybutor->addMultiOption('null', '-- Dystrybutor --');
        $dystrybutorzy = $this->entityManager->getRepository('Entities\Dystrybutor')->orderByNazwa(1);
        foreach ($dystrybutorzy as $dystr) {
            $dystrybutor->addMultiOption($dystr->getId(), $dystr->getNazwa());
        }
        $this->addElement($dystrybutor);

        $iloscNaStronie = $this->createElement('select', 'ilosc');
        $iloscNaStronie->setLabel('na stronie')
                ->setDecorators($this->inlineDivElementDecorators)
                ->setAttrib('class', 'form-control sm');

        $iloscNaStronie->addMultiOptions([100 => '100', 500 => '500', 1000 => '1000', 2000 => '2000']);

        $this->addElement($iloscNaStronie);

        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('Wybierz')
                ->setDecorators($this->inlineSubmitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
