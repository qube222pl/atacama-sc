<?php

class Application_Form_ProduktZdjecieZmienKolor extends Moyoki_Form {

    private $_produkt_id;

    public function __construct($options = null) {
        $this->_produkt_id = $options;
        parent::__construct($options);
    }

    public function init() {
        $this->setAttrib('class', 'form-inline');

        $this->setMethod('post')->setAttrib('id', 'form-zmien-kolor');

        $kolor = $this->createElement('select', 'nowykolor')
                ->setLabel('Wybierz kolor:')
                ->addMultiOption('0', '-- bez koloru --')
                ->setDecorators($this->inlineDivElementDecorators)
                ->setAttrib('class', 'form-control md');
        $this->addElement($kolor);

        $this->addElement($this->createElement('hidden', 'produkty_zdjecia_id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'zmien');
        $submit->setLabel('zmien')
                ->setDecorators($this->inlineSubmitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');
        
        $this->addElement($submit);
    }

}
