<?php

class Application_Form_MapowanieKolory extends Moyoki_Form {

    private $_entityManager;
    private $_dystrybutorId;

    public function __construct($entityManager, $dystrybutorId) {
        $this->_entityManager = $entityManager;
        $this->_dystrybutorId = $dystrybutorId;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-kolory')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $importKolory = $this->_entityManager->getRepository('Entities\Import_Kolor')->filtrujPoDystrybutorze($this->_dystrybutorId, 1);
        $kolory = $this->_entityManager->getRepository('Entities\Kolor')->getOrderByNazwa();
        
        $importParams = $this->_entityManager->getRepository('Entities\Import_Parametr')->filtrujPoDystrybutorze($this->_dystrybutorId);
        $showId = (bool)(isset($importParams['pokazujIdKolorow']) && $importParams['pokazujIdKolorow'] == 'tak');
        
        
        $kolorySelect = array();
        $kolorySelect[-1] = 'USUŃ!!!';
        foreach ($kolory as $kolor) {
            $kolorySelect[$kolor->getId()] = '[' . $kolor->getKod() . '] ' . $kolor->getNazwa();
        }
        
        $populate = array();

        foreach ($importKolory as $importKolor) {

            $kolor = $this->createElement('select', 'kolor_' . $importKolor->getId());
            $colorId = ($showId) ? (' [id:' . $importKolor->getKolorDystrId() . ']') : '';
            $kolor->setLabel($importKolor->getKolorDystrNazwa() . $colorId)
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control sm');

            $kolor->addMultiOptions($kolorySelect);

            $populate[('kolor_' . $importKolor->getId())] = $importKolor->getKoloryId();

            $this->addElement($kolor);
            unset($kolor);
            
        }
        
        $this->populate($populate);
        
        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

        $this->getView()->parametry = (count($importKolory) != 0 ? true : false);

    }

}
