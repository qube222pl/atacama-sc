<?php

class Application_Form_ProducentProdukt extends Moyoki_Form {

    public $entityManager;
    public $lang;
    private $_ID;

    public function __construct($entityManager, $producentId, $lang) {
        $this->entityManager = $entityManager;

        $this->_ID = $producentId;
        $this->lang = $lang;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-producent-produkt')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $produkty = $this->entityManager->getRepository('Entities\Produkt')->produktyByProducentIdOrderByNazwa($this->_ID);
        $produkt = $this->createElement('select', 'produkty_id');
        $produkt->setLabel('Dodaj nowy produkt')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md');

        foreach ($produkty as $p) {
            foreach ($p->getProduktyI18n() as $i18n) {
                if ($i18n->getJezyk() == $this->lang) {
                    $nazwa = $i18n->getNazwa();
                }
            }
            $produkt->addMultiOption($p->getId(), htmlspecialchars_decode($nazwa));
        }
        $this->addElement($produkt);

        $this->addElement($this->createElement('hidden', 'producenci_id')
                        ->setValue($this->_ID)
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'produkt');
        $submit->setLabel('dodaj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
