<?php

class Application_Form_Strona extends Moyoki_Form {

    public function init() {
        $view = Zend_Layout::getMvcInstance()->getView();

        $this->setIsArray(false) //Jakby parametry musialy byc tablica np jezyki
                ->setMethod('post')
                ->setAttrib('id', 'form-strona')
                ->setAttrib('class', 'form-horizontal')
                ->setDecorators($this->formDecorator);

        $this->addElement($this->createElement('text', 'tytul')
                        ->setLabel('Tytuł:')
                        ->setRequired(TRUE)
                        ->setDecorators($this->divElementDecorators)
                        ->setAttrib('class', 'form-control md')
                        ->setAttrib('maxlength', 150)
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));

        //Opis
        $this->addElement($this->createElement('textarea', 'opis')
                        ->setLabel('Opis:')
                        ->setDecorators($this->divTextareaDecorators)
                        ->setAttrib('class', 'form-control')
                        ->setAttrib('cols', '45')
                        ->setAttrib('rows', '3')
                        ->setDescription('<b>Ważne</B> Treść tu wpisana pojawia się jako opis stronyw wynikach wyszukiwania<br>w googlach dlatego dobrze jest wypełniać to pole.')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim()
        )));

        $this->getElement('opis')->getDecorator('Description')->setOption('escape', FALSE);


        $this->addElement($this->createElement('textarea', 'tresc')
                        ->setAttrib('height', '450px')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim()
        )));


        $cke = new Moyoki_CKEditor($view->baseUrl('/js/libs/ckeditor/'));
        $cke->replace('tresc');
        
        $this->addElement($this->createElement('hidden', 'kod')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $this->addElement($this->createElement('hidden', 'lang')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('zmień')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
