<?php

class Application_Form_ProduktZnakowanie extends Moyoki_Form {

    public $entityManager;

    
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-produkt-znakowanie')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal')
                ->setDecorators($this->formDecorator);


        $znakowanie = $this->entityManager->getRepository('Entities\Znakowanie')->getOrderByNazwa();
        
        foreach ($znakowanie as $z) {
            //$z = new Entities\Znakowanie;
            $znak = $this->createElement('checkbox', 'znak_' . $z->getId());
            $znak->setLabel($z->getNazwa())
                    ->setDecorators($this->checkboxcol4ElementDecorators);
            
            $this->addElement($znak);
        }
        
         $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));


        $submit = $this->createElement('submit', 'produkt');
        $submit->setLabel('ustaw')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
