<?php

class Application_Form_ZnakowanieNazwa extends Moyoki_Form {

    public function __construct() {
        parent::__construct();
    }

    public function init() {
        $this->setIsArray(TRUE) //Jakby parametry musialy byc tablica np jezyki
                ->setMethod('post')
                ->setAttrib('id', 'form-znakowanie-nazwa')
                ->setAttrib('class', 'form-horizontal');

        $view = Zend_Layout::getMvcInstance()->getView();

        $langs = Atacama_Config::getInstance()->resources->translate->languages->toArray();

        foreach ($langs as $lang => $langNazwa) {
            //nazwy
            $this->addElement($this->createElement('text', 'nazwa_' . $lang)
                            ->setLabel('Nazwa [' . $langNazwa . ']:')
                            ->setAttrib('maxlength', 100)
                            ->setDecorators($this->divElementDecorators)
                            ->setAttrib('class', 'form-control')
                            ->setBelongsTo($lang)
                            ->addFilters(array(
                                new Zend_Filter_StringTrim(),
                                new Zend_Filter_StripNewlines()
            )));

            $this->addDisplayGroup(array('nazwa_' . $lang), 'grupa ' . $lang, array('legend' => 'Grupa ' . $langNazwa));
        }


        $this->addElement($this->createElement('hidden', 'id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('Zapisz')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
