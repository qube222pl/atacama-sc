<?php

class Application_Form_HasloZmien extends Moyoki_Form {

    private $_options;

    const FORM_PRZYPOMNIENIE = 1;
    const FORM_ZMIANA_HASLA = 2;

    public $fieldsetDecorator = array(
        'FormElements',
        'Fieldset'
    );

    public function __construct($options = null) {
        $this->_options = $options;
        parent::__construct($options);
    }

    public function init() {
        $this->setAttrib('class', 'form-horizontal');
        
        if ($this->_options == self::FORM_ZMIANA_HASLA) {
            $shaslo = $this->createElement('password', 'stareHaslo');
            $shaslo->setLabel('aktualne haslo')
                    ->setRequired(TRUE)
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control md')
                    ->setAttrib('maxlength', 30)
                    ->setAttrib('size', 30)
                    ->addFilters(array(
                        new Zend_Filter_StringTrim(),
                        new Zend_Filter_StripNewlines(),
                        new Zend_Filter_StripTags()
                    ))
                    ->addValidators(array(
                        new Zend_Validate_StringLength(1, 30, 'UTF-8')
                    ))
                    ->addErrorMessage('pole jest wymagane');
            $this->addElement($shaslo);
        }

        if ($this->_options == self::FORM_PRZYPOMNIENIE) {
            $shaslo = $this->createElement('password', 'tymczasowe');
            $shaslo->setLabel('haslo tymczasowe')
                    ->setRequired(TRUE)
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control md')
                    ->setAttrib('maxlength', 30)
                    ->setAttrib('size', 30)
                    ->addFilters(array(
                        new Zend_Filter_StringTrim(),
                        new Zend_Filter_StripNewlines(),
                        new Zend_Filter_StripTags()
                    ))
                    ->addValidators(array(
                        new Zend_Validate_StringLength(8, 30, 'UTF-8')
                    ))
                    ->addErrorMessage('pole jest wymagane');
            $this->addElement($shaslo);
        }

        $haslo1 = $this->createElement('password', 'haslo1');
        $haslo1->setLabel('nowe haslo')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 30)
                ->setAttrib('size', 30)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_StringLength(8, 30, 'UTF-8')
                ))
                ->addErrorMessage('haslo musi posiadac od 8 do 30 znakow');
        $this->addElement($haslo1);

        $haslo2 = $this->createElement('password', 'haslo2');
        $haslo2->setLabel('powtorzenie hasla')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 30)
                ->setAttrib('size', 30)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    array(new Zend_Validate_StringLength(8, 30, 'UTF-8'), 'breakChainOnFailure'),
                    new Zend_Validate_Identical(array('token' => 'haslo1'))
                ))
                ->addErrorMessage('bledne powtorzenie hasla')
        ;
        $this->addElement($haslo2);

        $id = $this->createElement('hidden', 'id');
        $id->removeDecorator('HtmlTag')
                ->removeDecorator('label');

        $this->addElement($id);

        $kod = $this->createElement('hidden', 'kod');
        $kod->removeDecorator('HtmlTag')
                ->removeDecorator('label');

        $this->addElement($id);
        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('zmien')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-warning');
        $this->addElement($submit);

        $legend = '';



        $this->addDisplayGroup(array($shaslo, $haslo1, $haslo2, $id, $kod, $submit), 'haslo', array('legend' => $legend));
        $kolor = $this->getDisplayGroup('haslo');
        $kolor->setDecorators($this->fieldsetDecorator);
    }

}
