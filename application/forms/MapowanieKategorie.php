<?php

class Application_Form_MapowanieKategorie extends Moyoki_Form {

    private $_entityManager;
    private $_dystrybutorId;

    public function __construct($entityManager, $dystrybutorId) {
        $this->_entityManager = $entityManager;
        $this->_dystrybutorId = $dystrybutorId;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-kategorie')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $importKategorie = $this->_entityManager->getRepository('Entities\Import_Kategoria')->filtrujPoDystrybutorze($this->_dystrybutorId, 1);
        $kategorie = $this->_entityManager->getRepository('Entities\Kategoria')->orderByNazwa();

        $importParams = $this->_entityManager->getRepository('Entities\Import_Parametr')->filtrujPoDystrybutorze($this->_dystrybutorId);
        $showId = (bool)(isset($importParams['pokazujIdKategorii']) && $importParams['pokazujIdKategorii'] == 'tak');

        $kategorieSelect = array();
        $disabledSelect = array();

        foreach ($kategorie as $kategoria) {
            $kategorieSelect[$kategoria->getId()] = str_repeat('-', $kategoria->getPoziom() * 3) . $kategoria->getKategorieI18n()->first()->getNazwa();
            if($kategoria->getPoziom() == 0 && $kategoria->getId() != 1) {
                $disabledSelect[] = $kategoria->getId();
            }
        }
        
        $populate = array();

        foreach ($importKategorie as $importKategoria) {

            $kategoria = $this->createElement('select', 'kategoria_' . $importKategoria->getId());
            $kategoriaId = ($showId) ? (' [id:' . $importKategoria->getKategoriaDystrId() . ']') : '';
            $kategoria->setLabel($importKategoria->getKategoriaDystrNazwa() . $kategoriaId)
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control sm');

            $kategoria->addMultiOptions($kategorieSelect);
            $kategoria->setAttrib('disable', $disabledSelect);            
            
            $populate[('kategoria_' . $importKategoria->getId())] = $importKategoria->getKategorieId();

            $this->addElement($kategoria);
            unset($kategoria);
            
        }
        
        $this->populate($populate);
        
        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

        $this->getView()->parametry = (count($importKategorie) != 0 ? true : false);

    }

}
