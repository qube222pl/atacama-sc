<?php

class Application_Form_KoszykPodsumowanie extends Moyoki_Form {

    public $entityManager;
    public $acl;

    public function __construct($entityManager, $acl) {
        $this->entityManager = $entityManager;
        $this->acl = $acl;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-koszyk-podsumowanie')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $uwagi = $this->createElement('textarea', 'uwagi')
                ->setLabel('uwagi')
                ->setDecorators($this->divTextareaDecorators)
                ->setAttrib('class', 'form-control')
                ->setAttrib('cols', '45')
                ->setAttrib('rows', '3')
                ->addFilters(array(
            new Zend_Filter_StringTrim()
        ));
        $this->addElement($uwagi);

        $data = $this->createElement('text', 'data_dostawy')
                ->setLabel('termin dostawy')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm')
                ->setAttrib('readonly', 'readonly')
                ->setDescription('termin zostanie potwierdzony przez ph');
        $this->addElement($data);


        $submit = $this->createElement('submit', 'wstecz');
        $submit->setLabel('wroc do adresow')
                ->setDecorators(array(
                    'ViewHelper',
                    'Description',
                    'Errors',
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit-wstecz'))
                ))
                ->setAttrib('class', 'btn btn-info')
                ->setAttrib('id', 'btn-wstecz');
        $this->addElement($submit);

        $submit = $this->createElement('submit', 'dalej');
        $submit->setLabel('zatwierdz zamowienie')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');
        $this->addElement($submit);
    }

}
