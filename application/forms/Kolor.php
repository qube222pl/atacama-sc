<?php

class Application_Form_Kolor extends Moyoki_Form {

    public $entityManager;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-kolory')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 60)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_StringLength(2, 60, 'UTF-8')
                ))
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwa);

        $kod = $this->createElement('text', 'kod');
        $kod->setLabel('kod')
                ->setRequired(FALSE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 5)
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->setDescription('kod koloru');
        $this->addElement($kod);

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));


        $grupa = $this->createElement('select', 'grupa_id');
        $grupa->setLabel('Grupa')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md');
        $grupa->addMultiOption(null, '------');
        $grupy = $this->entityManager->getRepository('Entities\Kolor_Grupa')->getWszystkie();
        foreach ($grupy as $g) {
            $grupa->addMultiOption($g->getId(), $g->getNazwa());
        }
        $this->addElement($grupa);

        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

    }

}
