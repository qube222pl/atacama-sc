<?php

class Application_Form_KoszykAdresy extends Moyoki_Form {

    public $entityManager;
    protected $_adresy;

    public function __construct($entityManager, $adresy) {
        $this->entityManager = $entityManager;
        $this->_adresy = $adresy;
        parent::__construct();
    }

    public function init() {

        $view = Zend_Layout::getMvcInstance()->getView();

        $this->setAttrib('id', 'form-zamowienie-adresy')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $adresFaktura = new Zend_Form_Element_Radio('adresFaktura');
        $adresFaktura->setLabel('adres fakturowy')
                ->setRequired(TRUE)
                ->addErrorMessage('nie wybrano adresu')
                ->setOptions(array('escape' => false, 'separator' => '&nbsp;'));

        foreach ($this->_adresy as $adres) {
            if ($adres->getTypAdresu() == 1) {
                $adresSzczegoly = '<p>' . $adres->getUlica() . '<br>' . $adres->getKodPocztowy() . ' ' . $adres->getMiasto() . '</p>';
                $adresFaktura->addMultiOption($adres->getId(), "<div class='adres'>" . $adres->getNazwa() . $adresSzczegoly . "</div>");
            }
        }

        $this->addElement($adresFaktura);


        $nowyAdres = $this->createElement('submit', 'adres');
        $nowyAdres->setLabel('dodaj nowy adres dostawy')
                ->setDecorators(array(
                    'ViewHelper',
                    'Description',
                    'Errors',
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'nowy-adres'))
                ))
                ->setAttrib('class', 'btn btn-primary btn-raised')
                ->setAttrib('id', 'btn-adres')
                ->setAttrib('onClick', 'javascript:return false;')
                ->setAttrib('data-target', '#metodaModal')
                ->setAttrib('data-toggle', 'modal');
        $this->addElement($nowyAdres);


        $adresWysylka = new Zend_Form_Element_Radio('adresWysylka');
        $adresWysylka->setLabel('adres dostawy')
                ->setRequired(TRUE)
                ->addErrorMessage('nie wybrano adresu')
                ->setOptions(array('escape' => false, 'separator' => '&nbsp;'));

        foreach ($this->_adresy as $adres) {
            $adresSzczegoly = '<p>' . $adres->getUlica() . '<br>' . $adres->getKodPocztowy() . ' ' . $adres->getMiasto() . '</p>';
            $adresWysylka->addMultiOption($adres->getId(), "<div class='adres'>" . $adres->getNazwa() . $adresSzczegoly . "</div>");
        }

        $this->addElement($adresWysylka);



        $submitWstecz = $this->createElement('submit', 'wstecz');
        $submitWstecz->setLabel('wroc do koszyka')
                ->setDecorators(array(
                    'ViewHelper',
                    'Description',
                    'Errors',
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit-wstecz'))
                ))
                ->setAttrib('class', 'btn btn-info')
                ->setAttrib('id', 'btn-wstecz');
        $this->addElement($submitWstecz);


        $submitDalej = $this->createElement('submit', 'dalej');
        $submitDalej->setLabel('przejdz do podsumowania')
                ->setDecorators(array(
                    'ViewHelper',
                    'Description',
                    'Errors',
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit-dalej'))
                        )
                )
                ->setAttrib('class', 'btn btn-primary btn-raised');
        $this->addElement($submitDalej);
    }

}
