<?php

class Application_Form_Logowanie extends Moyoki_Form {

    public function init() {
        $view = Zend_Layout::getMvcInstance()->getView();

        $this->setAttrib('id', 'form-logowanie')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $nazwa = $this->createElement('text', 'email');
        $nazwa->setLabel('login')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 80)
                ->setAttrib('required', 'true')
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addValidators(array(
                    new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS)
                ))
                ->addErrorMessage('Login jest wymagany');
        $this->addElement($nazwa);

        $haslo = $this->createElement('password', 'haslo');
        $haslo->setLabel('haslo')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 40)
                ->setAttrib('required', 'true')
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StripNewlines(),
                    new Zend_Filter_StripTags()
                ))
                ->addErrorMessage('Hasło jest wymagane');
        $this->addElement($haslo);

        $submit = $this->createElement('submit', 'logowanie1');
        $submit->setLabel('logowanie')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

        $this->addDisplayGroup(array('email', 'haslo', 'logowanie1'), 'logowanie', array('class' => 'no-border'));
    }

}
