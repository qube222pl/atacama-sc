<?php

class Application_Form_KorporacyjniFirmyDlaPh extends Moyoki_Form {

    public function init() {
        $this->setAttrib('id', 'form-korporacyjni-firmy')
                ->setMethod('get')
                ->setAttrib('class', 'form-inline');

        $firma = $this->createElement('select', 'firma')
                                ->setDecorators(array(
                    'ViewHelper',
                    array('Errors', array('tag' => 'div', 'class' => 'form-error label label-warning')),
                    array('Description', array('class' => 'help-block', 'placement' => 'appand')),
                    array('Label', array('placement' => 'prepand', 'class' => 'sr-only')),
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
                ))
                ->setAttrib('class', 'form-control');


            $firma->setLabel('')
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control sm');        
        $this->addElement($firma);

        $submit = $this->createElement('submit', 'wybierz');
        $submit->setValue('Wybierz')
                ->setAttrib('class', 'btn btn-primary btn-raised')
                ->setValue('Wybierz')
                ->setDecorators(array(
                    'ViewHelper',
                    'Description',
                    'Errors',
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
        ));

        $this->addElement($submit);
    }

}
