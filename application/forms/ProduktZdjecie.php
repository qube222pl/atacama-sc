<?php

class Application_Form_ProduktZdjecie extends Moyoki_Form {

    private $_produkt_id;

    public function __construct($options = null) {
        $this->_produkt_id = $options;
        parent::__construct($options);
    }

    public function init() {
        $this->setAttrib('class', 'form-inline');

        $this->setMethod('post')->setAttrib('id', 'fileupload');

        $kolor = $this->createElement('select', 'kolor')
                ->setLabel('Wybierz kolor:')
                ->addMultiOption('0', '-- bez koloru --')
                ->setDecorators($this->inlineDivElementDecorators)
                ->setAttrib('class', 'form-control md');
        $this->addElement($kolor);

        $this->addElement($this->createElement('hidden', 'produkt_id')
                        ->setValue($this->_galeria_id)
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $this->addElement($this->createElement('file', 'produktzdjecie')
                        ->setAttrib('class', 'form-control btn btn-primary')
                        ->setDecorators($this->fileDecorators)
                        ->setLabel('dodaj zdjęcie')
                        ->setAttrib('data-input', "true")
                        ->setAttrib('title', 'Nowe zdjęcie')
        );
    }

}
