<?php

class Application_Form_Dystrybutor extends Moyoki_Form {

    public $entityManager;
    private $_akcja;

    public function __construct($entityManager, $akcja = 'zmien') {
        $this->entityManager = $entityManager;

        $this->_akcja = $akcja;
        
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-producent')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $nazwa = $this->createElement('text', 'nazwa');
        $nazwa->setLabel('nazwa')
                ->setRequired(TRUE)
                ->setAttrib('maxlength', 100)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nazwa);

        $opis = $this->createElement('text', 'opis');
        $opis->setLabel('opis')
                ->setRequired(FALSE)
                ->setAttrib('maxlength', 100)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control')
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($opis);


        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'firma');
        $submit->setLabel('wykonaj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
        
    }

}
