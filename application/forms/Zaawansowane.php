<?php

class Application_Form_Zaawansowane extends Moyoki_Form {

    public $entityManager;
    private $radioDecorator = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elementy')),
        array('Label', array('tag' => 'span', 'class' => 'tytul')),
        array('description', array('tag' => 'span')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-xs-12 col-sm-4'))
    );
    private $kolorDecorator = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'kolory')),
        array('Label', array('tag' => 'span', 'class' => 'tytul')),
        array('description', array('tag' => 'span')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-xs-12 col-sm-4'))
    );

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-zaawansowane')
                ->setMethod('get')
                ->setAttrib('class', 'form-horizontal row')
                ->setDecorators($this->formDecorator);

        $fraza = $this->createElement('text', 'fraza');
        $fraza->setLabel('fraza')
                ->setRequired(FALSE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 100)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
        ));
        $this->addElement($fraza);

        $ceny = new Zend_Form_Element_Radio('ceny');
        $ceny->setRequired(false)
                ->setDecorators($this->radioDecorator)
                ->setAttrib('class', 'form-control md')
                ->setSeparator('')
                ->setAttrib('class', 'radio')
                ->setLabel('cena');
        $ceny->addMultiOption(0, 'dowolna cena')
                ->addMultiOption(1, '0,1 zł - 10 zł')
                ->addMultiOption(2, '11 zł - 30 zł')
                ->addMultiOption(3, '31 zł - 50 zł')
                ->addMultiOption(4, '51 zł - 100 zł')
                ->addMultiOption(5, '101 zł - wiecej');

        $this->addElement($ceny);


        $kolory = new Zend_Form_Element_Radio('kolory');
        $kolory->setRequired(false)
                ->setSeparator('')
                ->setAttrib('escape', FALSE)
                ->setDecorators($this->kolorDecorator)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('class', 'radio')
                ->setLabel('kolory');

        $view = Zend_Layout::getMvcInstance()->getView();
        $grupy = $this->entityManager->getRepository('Entities\Kolor_Grupa')->getWszystkie();
        foreach ($grupy as $g) {
            if (NULL != $g->getPlik() && strlen($g->getPlik()) > 2) {
                $kolory->addMultiOption($g->getId(), '<div class="thumbnail"><div style="background-image: url(' . $view->baseUrl('images/'.$g->getPlik()) . ')" class="kolor-element tooltip-init" title="' . $g->getNazwa() . '"></div></div>');
            } else {
                $kolory->addMultiOption($g->getId(), '<div class="thumbnail"><div style="background-color: ' . $g->getHex() . '" class="kolor-element tooltip-init" title="' . $g->getNazwa() . '"></div></div>');
            }
        }

        $this->addElement($kolory);

        $stany = new Zend_Form_Element_Radio('stany');
        $stany->setRequired(false)
                ->setSeparator('')
                ->setDecorators($this->radioDecorator)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('class', 'radio')
                ->setLabel('stany');

        $stany->addMultiOption(0, 'dowolna ilosc')
                ->addMultiOption(1, 'mniej niz 10')
                ->addMultiOption(2, '11 - 100')
                ->addMultiOption(3, '101 - 500')
                ->addMultiOption(4, '500 - 1000');

        $this->addElement($stany);

        $submit = $this->createElement('submit', 'zaawansowany');
        $submit->setLabel('szukaj')
                ->setAttrib('class', 'btn btn-primary btn-raised')
                ->setDecorators(array(
                    'ViewHelper',
                    'Description',
                    'Errors',
                    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit col-xs-12 text-center'))
        ));

        $this->addElement($submit);
    }

}
