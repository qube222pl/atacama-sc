<?php

class Application_Form_KorporacyjnyProdukt extends Moyoki_Form {

    public function __construct() {
        parent::__construct();
    }

    public function init() {
        $this->setMethod('post')
                ->setAttrib('id', '')
                ->setAttrib('class', 'form-horizontal');

        $this->addElement($this->createElement('file', 'zdjecie')
                        //->setAttrib('class', 'form-control btn btn-primary')
                        ->setDecorators($this->fileDecorators)
                        ->setLabel('Dodaj zdjęcie')
                        ->setAttrib('title', 'Nowe zdjęcie')
        );

        $this->addElement($this->createElement('text', 'cena')
                        ->setLabel('Cena:')
                        ->setAttrib('maxlength', 100)
                        ->setDecorators($this->divElementDecorators)
                        ->setAttrib('class', 'form-control')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));

        $this->addElement($this->createElement('text', 'stock')
                        ->setLabel('Stock:')
                        ->setAttrib('maxlength', 100)
                        ->setDecorators($this->divElementDecorators)
                        ->setAttrib('class', 'form-control')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));

        $this->addElement($this->createElement('text', 'min_stock')
                        ->setLabel('Min stock:')
                        ->setDescription('Poniżej tego poziomu ma przyjść powiadomienie')
                        ->setAttrib('maxlength', 100)
                        ->setDecorators($this->divElementDecorators)
                        ->setAttrib('class', 'form-control')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));


        $this->addElement($this->createElement('text', 'czas_dodruku')
                        ->setLabel('Dodruk dni:')
                        ->setDescription('Ilość dni potrzebnych na dodruk')
                        ->setAttrib('maxlength', 100)
                        ->setDecorators($this->divElementDecorators)
                        ->setAttrib('class', 'form-control')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));

        $this->addElement($this->createElement('hidden', 'id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $this->addElement($this->createElement('hidden', 'firma_id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $this->addElement($this->createElement('hidden', 'detal_id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));


        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('Zapisz')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
