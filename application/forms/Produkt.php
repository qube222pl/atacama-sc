<?php

class Application_Form_Produkt extends Moyoki_Form {

    public $entityManager;
    private $_akcja;

    public function __construct($entityManager, $akcja = 'edycja') {
        $this->entityManager = $entityManager;

        $this->_akcja = $akcja;

        parent::__construct();
    }

    public function init() {
        $this->setAttrib('id', 'form-produkt')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal')
                ->setDecorators($this->formDecorator);

        if ($this->_akcja == 'dodaj') {
            $nazwa = $this->createElement('text', 'nazwa');
            $nazwa->setLabel('nazwa')
                    ->setRequired(FALSE)
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control md')
                    ->setAttrib('maxlength', 100)
                    ->addFilters(array(
                        new Zend_Filter_HtmlEntities(),
                        new Zend_Filter_StringTrim()
            ));
            $this->addElement($nazwa);
        }

        $producent = $this->createElement('select', 'producenci_id');
        $producent->setLabel('producent')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md');

        $producenci = $this->entityManager->getRepository('Entities\Producent')->orderByNazwa();
        foreach ($producenci as $p) {
            $producent->addMultiOption($p->getId(), $p->getNazwa());
        }
        $this->addElement($producent);


        $dystrybutor = $this->createElement('select', 'dystrybutorzy_id');
        $dystrybutor->setLabel('dystrybutor')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md');
                
        $dystrybutorzy = $this->entityManager->getRepository('Entities\Dystrybutor')->orderByNazwa();
        foreach ($dystrybutorzy as $d) {
            $dystrybutor->addMultiOption($d->getId(), $d->getNazwa());
        }
        $this->addElement($dystrybutor);


        $kategoria_glowna = $this->createElement('select', 'kategoria_glowna');
        $kategoria_glowna->setLabel('kategoria glowna')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md');
                
        $this->addElement($kategoria_glowna);
        
        $nr_kat = $this->createElement('text', 'nr_katalogowy');
        $nr_kat->setLabel('nr katalogowy')
                ->setRequired(TRUE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 45)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
                ))
                ->addErrorMessage('pole jest wymagane');
        $this->addElement($nr_kat);

        $nr_prod = $this->createElement('text', 'nr_producenta');
        $nr_prod->setLabel('nr producenta')
                ->setRequired(FALSE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 45)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
        ));
        $this->addElement($nr_prod);

        $ident_prod = $this->createElement('text', 'identyfikator_producenta');
        $ident_prod->setLabel('identyfikator producenta')
                ->setRequired(FALSE)
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                ->setAttrib('maxlength', 45)
                ->addFilters(array(
                    new Zend_Filter_HtmlEntities(),
                    new Zend_Filter_StringTrim()
        ));
        $this->addElement($ident_prod);

        $model = $this->createElement('select', 'model');
        $model->setLabel('model')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control sm')
                ->addMultiOption(NULL, '-- nie dotyczy --')
                ->addMultiOption(1, 'damski')
                ->addMultiOption(2, 'męski')
                ->addMultiOption(3, 'unisex');

        $this->addElement($model);

        $widoczny = $this->createElement('checkbox', 'widoczny');
        $widoczny->setLabel('widoczny')
                ->setDecorators($this->checkboxcol4ElementDecorators);
                new Zend_Form_Decorator_HtmlTag();
        $this->addElement($widoczny);

        $bestseller = $this->createElement('checkbox', 'bestseller');
        $bestseller->setLabel('bestseller')
                ->setDecorators($this->checkboxcol4ElementDecorators);
        $this->addElement($bestseller);

        $nowosc = $this->createElement('checkbox', 'nowosc');
        $nowosc->setLabel('nowosc')
                ->setDecorators($this->checkboxcol4ElementDecorators);
        $this->addElement($nowosc);

        $promocja = $this->createElement('checkbox', 'promocja');
        $promocja->setLabel('promocja')
                ->setDecorators($this->checkboxcol4ElementDecorators);
        $this->addElement($promocja);

        $this->addElement($this->createElement('hidden', 'id')
                        ->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'produkt');
        $submit->setLabel('wykonaj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
