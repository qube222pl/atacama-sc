<?php

class Application_Form_ProducentNazwa extends Moyoki_Form {

    public function __construct() {
        parent::__construct();
    }

    public function init() {
        $this->setIsArray(TRUE) //Jakby parametry musialy byc tablica np jezyki
                ->setMethod('post')
                ->setAttrib('id', 'form-producent-opis')
                ->setAttrib('class', 'form-horizontal');

        $view = Zend_Layout::getMvcInstance()->getView();

        $langs = Atacama_Config::getInstance()->resources->translate->languages->toArray();

        foreach ($langs as $lang => $langNazwa) {
            $this->addElement($this->createElement('textarea', 'opis_' . $lang)
                            ->setBelongsTo($lang)
                            ->addFilters(array(
                                new Zend_Filter_StringTrim(),
                                new Zend_Filter_StripNewlines()
            )));


            $cke = new Moyoki_CKEditor($view->baseUrl('/js/libs/ckeditor/'));
            $cke->replace($lang . '[opis_' . $lang . ']');

            $this->addElement($this->createElement('textarea', 'seo_opis_' . $lang)
                            ->setBelongsTo($lang)
                            ->setLabel('Opis do google')
                            ->setDecorators($this->divTextareaDecorators)
                            ->setAttrib('class', 'form-control')
                            ->setDescription('Przydatne do pozycjonowania produktu w google - czysty opis bez znaczników')
                            ->setAttrib('rows', 3)
                            ->addFilters(array(
                                new Zend_Filter_StringTrim(),
                                new Zend_Filter_StripNewlines()
            )));


            $this->addDisplayGroup(array('nazwa_' . $lang, 'opis_' . $lang, 'seo_opis_' . $lang), 'grupa ' . $lang, array('legend' => 'Grupa ' . $langNazwa));
        }


        $this->addElement($this->createElement('hidden', 'id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('Zapisz')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);
    }

}
