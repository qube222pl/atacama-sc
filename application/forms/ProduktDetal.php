<?php

class Application_Form_ProduktDetal extends Moyoki_Form {

    public $em;
    public $akcja;
    public function __construct($em, $akcja = 'dodaj') {
        $this->em = $em;
        $this->akcja = $akcja;
        parent::__construct();
        
    }

    public function init() {
        $this->setMethod('post')
                ->setAttrib('id', 'form-produkt-detal');

        $kolor = $this->createElement('select', 'kolor_id');
        $kolor->setLabel('Kolor')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md');
        
        $kolor->addMultiOption('', '! brak koloru');
        
        $kolory = $this->em->getRepository('Entities\Kolor')->getOrderByNazwa();
        foreach ($kolory as $k) {
            $kolor->addMultiOption($k->getId(), $k->getNazwa());
        }
        $this->addElement($kolor);
        

        $rozmiar = $this->createElement('select', 'rozmiar')
                        ->setLabel('Rozmiar:')
                ->setDecorators($this->divElementDecorators)
                        ->setAttrib('class', 'form-control md');
        
        foreach (Atacama_Produkt_Rozmiar::tablica() as $id => $nazwa) {
            $rozmiar->addMultiOption($id, $nazwa);
        }
        $this->addElement($rozmiar);

        $this->addElement($this->createElement('text', 'cena')
                ->setLabel('cena')
                ->setDecorators($this->divElementDecorators)
                ->setAttrib('class', 'form-control md')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));
        $this->addElement($this->createElement('text', 'wyp')
                ->setLabel('cena wyprzedaży')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));
        
        $this->addElement($this->createElement('text', 'promo')
                ->setLabel('Cena promocyjna')
                        ->addFilters(array(
                            new Zend_Filter_StringTrim(),
                            new Zend_Filter_StripNewlines()
        )));

        $this->addElement($this->createElement('hidden', 'id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $this->addElement($this->createElement('hidden', 'produkty_id')->removeDecorator('HtmlTag')
                        ->removeDecorator('label'));

        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel('Zapisz');

        $this->addElement($submit);
    }

}
