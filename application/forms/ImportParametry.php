<?php

class Application_Form_ImportParametry extends Moyoki_Form {

    private $_entityManager;
    private $_dystrybutorId;

    public function __construct($entityManager, $dystrybutorId) {
        $this->_entityManager = $entityManager;
        $this->_dystrybutorId = $dystrybutorId;
        parent::__construct();
    }

    public function init() {
        
        $this->setAttrib('id', 'form-kolory')
                ->setMethod('post')
                ->setAttrib('class', 'form-horizontal');

        $importParams = $this->_entityManager->getRepository('Entities\Import_Parametr')->filtrujPoDystrybutorze($this->_dystrybutorId, 1);

        $populate = array();

        foreach ($importParams as $name => $value) {
            
            $parametr = $this->createElement('text', $name);
            $parametr->setLabel($name)
                    ->setRequired(TRUE)
                    ->setAttrib('maxlength', 200)
                    ->addFilters(array(
                        new Zend_Filter_StringTrim(),
//                        new Zend_Filter_HtmlEntities(),
                        new Zend_Filter_StripNewlines(),
                        new Zend_Filter_StripTags()
                    ))
                    ->addValidators(array(
                        new Zend_Validate_StringLength(1, 200, 'UTF-8')
                    ))
                    ->setDecorators($this->divElementDecorators)
                    ->setAttrib('class', 'form-control')
                    ->addErrorMessage('pole jest wymagane');
            $this->addElement($parametr);
            $populate[$name] = $value;
            unset($parametr);
        }

        $this->populate($populate);

        $submit = $this->createElement('submit', 'wykonaj');
        $submit->setValue('Zarejestruj')
                ->setDecorators($this->submitDivElementDecorators)
                ->setAttrib('class', 'btn btn-primary btn-raised');

        $this->addElement($submit);

        $this->getView()->parametry = (count($importParams) != 0 ? true : false);

    }

}
