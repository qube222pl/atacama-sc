<?php

/**
 * Description of GornyKoszyk
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_Gornykoszyk extends Zend_View_Helper_Abstract {

    public function Gornykoszyk() {
        $acl = Atacama_Acl::getInstance();
        $return = "";
        $razemIlosci = 0;
        if (Zend_Auth::getInstance()->hasIdentity() && $acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $em = $bootstrap->getResource('Entitymanagerfactory');

            $user = Zend_Auth::getInstance()->getIdentity();
            $return = "<a class=\"btn btn-dark btn-sm\" "
                    . "href=\"".$this->view->url(array(
                        'controller' => 'Koszyk',
                        'action' => 'index'
                            ), 'default', true)."\">\n";

            if ($acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
                $koszyk = $em->getRepository('Entities\Koszyk')->getKoszykUzytkownika($user['id']);
                if ($koszyk instanceof Entities\Koszyk && count($koszyk->getKoszykiProdukty()) > 0) {
                    foreach ($koszyk->getKoszykiProdukty() as $kp) {
                        $razemIlosci += $kp->getIlosc();
                    }
                }
            }
            $return .= '<span class="fa-stack fa-2x has-badge" data-count="' . $razemIlosci . ' szt.">
                                        <i class="fa fa fa-shopping-basket fa-stack-1x"></i>
                                    </span>';
            $return .= "\n</a>\n";
        }
        return $return;
    }
}
