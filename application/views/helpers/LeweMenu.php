<?php

/**
 * Description of LeweMenu
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_LeweMenu extends Zend_View_Helper_Abstract {

    public function LeweMenu() {

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');

        $view = Zend_Layout::getMvcInstance()->getView();
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        ?>

        <div class="panel-group" id="accordion">
            <?php
            $pierwszy = NULL;
            $kategorie = $em->getRepository('Entities\Kategoria')->orderByNazwaOpcja($lang, 0);
            foreach ($kategorie as $kategoria) :
                //$kategoria = new Entities\Kategoria();
                if ($kategoria->getPoziom() == 0) :
                    if (NULL != $pierwszy) {
                        echo '</div></div></div>';
                    } else {
                        $pierwszy = TRUE;
                    }
                    ?>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" 
                               href="#collapse-<?php echo $kategoria->getId(); ?>"><?php
                                   foreach ($kategoria->getKategorieI18n() as $i18n) {
                                       if ($i18n->getJezyk() == $lang) {
                                           echo $i18n->getNazwa();
                                       }
                                   }
                                   ?>
                            </a>
                        </div>
                        <div id="collapse-<?php echo $kategoria->getId(); ?>" 
                        <?php if ($view->rozwin == $kategoria->getId()) : ?>
                                 class="panel-collapse in">
                                 <?php else : ?>
                                class="panel-collapse collapse">
                            <?php endif; ?>
                            <div class="panel-body">
                            <?php
                            else :
                                foreach ($kategoria->getKategorieI18n() as $i18n) {
                                    if ($i18n->getJezyk() == $lang) {
                                        $nazwa = $i18n->getNazwa();
                                    }
                                }
                                ?>
                                <div class="poziom-<?php echo $kategoria->getPoziom(); ?>">
                                    <a href="<?php
                                    echo $this->view->url(
                                            array('language' => $lang, 'controller' => 'Produkt',
                                        'action' => 'lista',
                                        'kat' => $kategoria->getId(),
                                        'nazwa' => System_Slug::string2slug($nazwa)
                                            ), 'kategorie', true);
                                    ?>" title="<?php echo $nazwa; ?>"><?php echo $nazwa; ?></a></div>
                            <?php endif; ?>

                        <?php endforeach;
                        ?>
                    </div>
                </div><!-- OSTATNI panel-collapse //-->
            </div> <!-- OSTATNI panel panel-default //-->
        </div> <!-- //accordion //-->

        <?php
    }

}
