<?php

/**
 * Helper oferty do pdfa
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_OfertaTabelaPodsumowanie extends Zend_View_Helper_Abstract {

    public function OfertaTabelaPodsumowanie(Doctrine\ORM\EntityManager $em, $koszyk_id, $pdf = FALSE) {

        $acl = Atacama_Acl::getInstance();
        $view = Zend_Layout::getMvcInstance()->getView();
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $user = Zend_Auth::getInstance()->getIdentity();

        $config = Atacama_Config::getInstance();
        $vat = $config->importVat;
        $sWidth = $config->zdjecia->rozmiary->l->w;
        $sHeight = $config->zdjecia->rozmiary->l->h;

        $poprzedniProduktID = NULL;
        if ($koszyk_id > 0) {
            if ($pdf || $acl->sprawdzDostep(Atacama_Acl::ZAS_GENERUJ_OFERTE)) :

                $koszyk = $em->getRepository('Entities\Koszyk')->getById($koszyk_id);

                $return = '';
                foreach ($koszyk->getKoszykiProdukty() as $koszykProdukt) :
                    $razemNetto = 0;
                    $znakowaniaSuma = 0;
                    $return .= '<div>';

                    //$koszykProdukt = new Entities\Koszyk_Produkt;
                    $produkt = $koszykProdukt->getProduktyDetale()->getProdukty();

                    /**
                     * Jezeli ten produkt juz jest, tylko tylko tabelka bez zdjecia i opisu
                     */
                    if (NULL === $poprzedniProduktID || $poprzedniProduktID != $produkt->getId()) {

                        if (NULL != $poprzedniProduktID) {
                            $return .= '<p>&nbsp;</p>';
                            $return .= '<p>&nbsp;</p>';
                            $return .= '<p>&nbsp;</p>';
                            $cenaSztuka = 0;
                        }
                        foreach ($koszykProdukt->getProduktyDetale()->getProdukty()->getProduktyI18n() as $i18n) {
                            if ($i18n->getJezyk() == $lang) {
                                $nazwaProduktu = $i18n->getNazwa();
                                $opisProduktu = $i18n->getOpis();
                            }
                        }
                        try {

                            $zdjecie = $em->getRepository('Entities\Produkt_Zdjecie')->getZdjecieKolor($produkt->getId(), $koszykProdukt->getProduktyDetale()->getKoloryId());

                            if ($zdjecie && strlen($zdjecie->getPlik()) > 4) {

                                $foto = new Moyoki_File($config->zdjecia->path . $zdjecie->getPlik());
                                $thumbSrc = $view->baseUrl($config->zdjecia->urlbase . $produkt->getDystrybutorzyId() . '/' . $foto->getThumbnailFile($sWidth, $sHeight));
                            } else {
                                $thumbSrc = $view->baseUrl('images/nophoto_' . $sWidth . 'x' . $sHeight . '.png');
                            }
                        } catch (Exception $e) {
                            $thumbSrc = $view->baseUrl('images/nophoto_' . $sWidth . 'x' . $sHeight . '.png');
                        }

                        $return .= '<table class="table table-striped">
                            <tr>
                                <td valign="top" class="no-border produkt-opis"><h2>' . $nazwaProduktu . '</h2>' . $opisProduktu . '</td>
                                <td class="no-border produkt-zdjecie"><img src="' . substr($config->appUrl, 0, -1) . $thumbSrc . '" alt="" /></td>
                             </tr>
                             </table>';
                    }

                    /**
                     * Tu juz mozemy ustawic produkt jako poprzedni
                     */
                    $poprzedniProduktID = $produkt->getId();

                    $return .= '<table class="table table-striped oferta-lista">
                <thead>
                    <tr>
                        <th class="row-nazwa produkt">' . $view->translate('nazwa') . '</th>
                        <th class="row-dodatki produkt">&nbsp;</th>
                        <th class="row-cena produkt">' . $view->translate('cena j netto') . '</th>
                        <th class="row-ilosc produkt">' . $view->translate('ilosc') . '</th>
                        <th class="row-cena produkt">' . $view->translate('kwota netto') . '</th>
                            </tr>
                </thead>

                <tbody>';

                    $return .= '<tr>
                    <td class="produkt nazwa">' . $nazwaProduktu . '</td>
                        <td class="produkt dodatki">
                                ' . $view->translate('kolor') . ': ' . $koszykProdukt->getProduktyDetale()->getKolory()->getNazwa() . ', ';

                    if ($koszykProdukt->getProduktyDetale()->getRozmiar() != 0) {
                        $return .= $view->translate('rozmiar') . ': ' . Atacama_Produkt_Rozmiar::pokaz($koszykProdukt->getProduktyDetale()->getRozmiar()) . ', ';
                    }

                    $cenaSztuka = $koszykProdukt->getCenaZakupu();
                 
                if (count($koszykProdukt->getKoszykiProduktyZnakowania()) > 0) {
                    $cenaSztuka += $koszykProdukt->getCenaKonfekcjonowanie();
                }
                    $return .= $view->translate('nr kat') . ': ' . $koszykProdukt->getProduktyDetale()->getProdukty()->getNrKatalogowy()
                            . '</td>
                            <td class="text-right produkt">' . Atacama_Produkt_Kwota::odczyt($cenaSztuka, 'zł') . '</td>
                            <td class="text-center produkt">' . $koszykProdukt->getIlosc() . '</td>
                            <td class="text-right produkt">';
                    $razemNetto = $cenaSztuka * $koszykProdukt->getIlosc();

                    $return .= Atacama_Produkt_Kwota::odczyt($razemNetto, 'zł');
                    $return .= '</td>';

                    $cmd = new Application_Model_Commands_KoszykPokazZnakowaniaProduktu($em, $koszykProdukt);
                    $result = $cmd->execute();
                    if (!key_exists('brak', $result)) {
                        $return .= '
                            <tr>
                                            <th class="row-nazwa">' . $view->translate('nazwa') . '</th>
                                            <th class="row-dodatki">' . $view->translate('ilosc kolorow') . '</th>
                                            <th class="row-cena">' . $view->translate('cena j netto') . '</th>
                                            <th class="row-ilosc">' . $view->translate('ilosc') . '</th>
                                            <th class="row-cena">' . $view->translate('kwota netto') . '</th>';
                        $return .= '</tr>
                                    </thead>';
                        foreach ($result['znakowania'] as $znakowanie) {
                            $znakowaniaSuma = $znakowaniaSuma + $znakowanie['suma'] + $znakowanie['przygotowalnia'];
                            $return .= '<tr>
                                            <td>' . $znakowanie['nazwa'] . '</td>
                                            <td class="text-center">' . $znakowanie['ilosc_kolorow'] . '</td>
                                            <td class="text-right">' . Atacama_Produkt_Kwota::odczyt($znakowanie['suma'] / $znakowanie['naklad'], 'zł') . '</td>
                                            <td class="text-center">' . $znakowanie['naklad'] . '</td>
                                            <td class="text-right">' . Atacama_Produkt_Kwota::odczyt($znakowanie['suma'], 'zł') . '</td>
                                        </tr>';
                            $return .= ' <tr>
                                            <td class="text-right">' . $view->translate('przygotowalnia') . '</td>
                                            <td class="text-center">' . $znakowanie['ilosc_kolorow'] . '</td>
                                            <td class="text-right">' . Atacama_Produkt_Kwota::odczyt(($znakowanie['przygotowalnia'] / $znakowanie['ilosc_kolorow']), 'zł') . '</td>
                                            <td>&nbsp;</td>
                                            <td class="text-right">' . Atacama_Produkt_Kwota::odczyt($znakowanie['przygotowalnia'], 'zł') . '</td>
                                        </tr>';
                        }
                    }

                    $return .= '<tr>';
                    $return .= '<th class="no-border" colspan="2"></th>';
                    $return .= '<th class="podsumowanie">' . $view->translate('wartosc netto') . '</th>
                        <th class="podsumowanie">' . $view->translate('vat') . '</th>
                        <th class="podsumowanie">' . $view->translate('razem brutto') . '</th>
                    </tr>';

                    $return .= '<tr>';
                    $return .= '<td class="no-border" colspan="2"></td>';
                    $razemNetto = $razemNetto + $znakowaniaSuma;
                    $return .= '<td class="text-right podsumowanie">' . Atacama_Produkt_Kwota::odczyt($razemNetto, 'zł') . '</td>
                        <td class="text-right podsumowanie">' . $vat . '</td>
                        <td class="text-right podsumowanie">' . Atacama_Produkt_Kwota::odczyt($razemNetto + ($razemNetto * $vat / 100), 'zł') . '</td>
                    </tr>';
                    $return .= '</tbody></table></div>';

                endforeach;

                return $return;
            endif;
        }
    }

}
