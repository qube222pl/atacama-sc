<?php

/**
 * Description of Miniaturkaadmin
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_MiniaturkaGaleria extends Zend_View_Helper_Abstract {

    public function miniaturkaGaleria($thumbnail, Entities\Galeria_Zdjecie $zdjecie) {
        $string = '<div class="col-xs-12 col-sm-6 col-md-3" id="z' . $zdjecie->getId() . '">
           
                    <div class="thumbnail text-center">
			<p><img src="' . $this->view->baseUrl($thumbnail) . '" /></p>';

// Przyciski
        $string .= "<p>";
        $string .= "<a onclick=\"javascript:kasujZdjecie('" . $this->view->url(array(
                    'language' => 'pl',
                    'controller' => 'ajax',
                    'action' => 'galeriausunzdjecie',
                    'zdjecie_id' => $zdjecie->getId(),
                        ), 'default', true) . "', '#z" . $zdjecie->getId() . "');\"  class=\"btn btn-primary\">Usuń</a>";
        $string .= "</p>";
        $string .= "\n</div>\n</div>\n";

        return $string;
    }

}
