<?php

/**
 * Description of CategoryAndSub
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_CategoryAndSub extends Zend_View_Helper_Abstract {

    public function CategoryAndSub($catId) {
        ?>
        <div class="col-md-6 col-xl-3 sub-menu my-xl-3 mt-4 mb-5">
            <?php
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $em = $bootstrap->getResource('Entitymanagerfactory');

            $kategorie = $em->getRepository('Entities\Kategoria')->pobierzDzieci($catId);
            
            if (count($kategorie) > 0 && $kategorie[0] instanceof Entities\Kategoria) {
                /**
                 * Kategoria ma pod soba inne kategorie
                 */
                $glowna = $kategorie[0]->getKategorie();
                foreach ($glowna->getKategorieI18n() as $i18n) {
                    if ($i18n->getJezyk() == 'pl') {
                        $nazwa = $i18n->getNazwa();
                    }
                }
                ?>
                <h2><a href="<?php echo $this->_getUrl($glowna, $nazwa); ?>"><?php echo $nazwa ?></a></h2>

                <ul class="caret-style pl-0">
                    <?php
                    foreach ($kategorie as $kategoria) :
                        if ($kategoria instanceof Entities\Kategoria) :
                            foreach ($kategoria->getKategorieI18n() as $i18n) {
                                if ($i18n->getJezyk() == 'pl') {
                                    $nazwa = $i18n->getNazwa();
                                }
                            }
                            ?>
                            <li class="level-<?php echo $kategoria->getPoziom(); ?>">
                                <a class="menu-item mb-0" href="<?php echo $this->_getUrl($kategoria, $nazwa); ?>"><?php echo $nazwa; ?></a>
                            </li>
                            <?php
                        endif;
                    endforeach;
                    ?>
                </ul>
                <?php
            } else {
                /**
                 * Kategoria nie ma podleglych kategorii
                 */
                $glowna = $em->getRepository('Entities\Kategoria')->getById($catId);
                if ($glowna instanceof Entities\Kategoria) {
                    foreach ($glowna->getKategorieI18n() as $i18n) {
                        if ($i18n->getJezyk() == 'pl') {
                            $nazwa = $i18n->getNazwa();
                        }
                    }
                    ?>
                    <h2><a href="<?php echo $this->_getUrl($glowna, $nazwa); ?>"><?php echo $nazwa ?></a></h2>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    }

    private function _getUrl(Entities\Kategoria $kategoria, $nazwa) {
        $url = $this->view->url(
                array('controller' => 'Produkt',
            'action' => 'lista',
            'kat' => $kategoria->getId(),
            'nazwa' => System_Slug::string2slug($nazwa)
                ), 'kategorie', true);
        return $url;
    }

}
