<?php

/**
 * Menu widoczne przez admina przy produktach
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_AdminImportMenu extends Zend_View_Helper_Abstract {

    public function AdminImportMenu($dystrybutorId, $active = 'ogolne') {
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $acl = Atacama_Acl::getInstance();
        $returnString = '';

        if ((int) $dystrybutorId > 0) {
            if ($acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
                $returnString = "\n<li ".($active == 'ogolne'?"class=\"active\"":"").">"
                        . "<a href=\"" . $this->view->url(
                                array('controller' => 'Import', 'action' => 'zmien', 'id' => $dystrybutorId), 'default', true) . "\" "
                        . " class=\" ml-4 btn btn-default\">Ogólne</a></li>\n"
                        . "\n<li ".($active == 'kolory'?"class=\"active\"":"").">"
                        . "<a href=\"" . $this->view->url(
                                array('controller' => 'Mapowanie', 'action' => 'kolory', 'id' => $dystrybutorId), 'default', true) . "\" "
                        . " class=\" ml-4 btn btn-default\">Mapowanie kolorów</a></li>\n"
                        . "\n<li ".($active == 'kategorie'?"class=\"active\"":"").">"
                        . "<a href=\"" . $this->view->url(
                                array('controller' => 'Mapowanie', 'action' => 'kategorie', 'id' => $dystrybutorId), 'default', true) . "\" "
                        . " class=\" ml-4 btn btn-default\">Mapowanie kategorii</a></li>\n"
                        . "\n<li ".($active == 'znakowania'?"class=\"active\"":"").">"
                        . "<a href=\"" . $this->view->url(
                                array('controller' => 'Mapowanie', 'action' => 'znakowania', 'id' => $dystrybutorId), 'default', true) . "\" "
                        . " class=\" ml-4 btn btn-default\">Mapowanie znakowania</a></li>\n"
                        . "\n<li ".($active == 'producenci'?"class=\"active\"":"").">"
                        . "<a href=\"" . $this->view->url(
                                array('controller' => 'Mapowanie', 'action' => 'producenci', 'id' => $dystrybutorId), 'default', true) . "\" "
                        . " class=\" ml-4 btn btn-default\">Mapowanie producentów</a></li>\n"
                    ;
            }
        }

        return "\n<ul class=\"nav nav-tabs\">".$returnString."\n</ul><p>&nbsp;</p>";
    }
}
