<?php

/**
 * Generowanie listy uzytkownikow dla firm korporacyjnych
 * Jest to lista do dodawania nowych uzytkownikow
 * Na liscie nie ma juz istniejacych osob, nawet nieaktywnych
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_KorporacyjniMenu extends Zend_View_Helper_Abstract {

    public function KorporacyjniMenu() {
        $view = Zend_Layout::getMvcInstance()->getView();
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');
        $user = Zend_Auth::getInstance()->getIdentity();
        $return = '';
        $zwroc = '';

        $firmy = $em->getRepository('Entities\Korporacyjny_Uzytkownik')->getWszystkichByUserId($user['id']);

        $klient = FALSE;
        $koordynator = FALSE;

        /**
         * Sprawdzamy czy uzytkownik jest gdzies przypisany
         */
        foreach ($firmy as $firma) {
            if ($firma instanceof Entities\Korporacyjny_Uzytkownik) {
                /**
                 * Jezeli uzytkownik jest przysany gdziej jako klient
                 */
                if ($firma->getRola() == Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KLIENT) {
                    $klient = TRUE;
                }
                /**
                 * Jezeli uzytkownik jest przysany gdziej jako koordynator
                 */
                if ($firma->getRola() == Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KOORDYNATOR) {
                    $koordynator = TRUE;
                }
            }
        }

        /**
         * Jezeli chociaz w 1 firmie uzytkownik jest przypisany jako koordynator
         */
        if ($koordynator) {
            $return .= '<a href="' . $view->url([
                        'controller' => 'zamowienia-specjalne',
                        'action' => 'uzytkownicy'
                            ], 'default', true) . '" class="btn btn-default btn-raised mr-2">Zarządzaj budżetami</a>';

            $return .= '<a href="' . $view->url([
                        'controller' => 'zamowienia-specjalne',
                        'action' => 'koszyk'
                            ], 'default', true) . '" class="btn btn-default btn-raised mr-2">Sprawdź produkty</a>';
        }

        /**
         * Jezeli chociaz w 1 firmie jest przypisany jako Klient
         */
        if ($klient) {
            $return .= '<a href="' . $view->url([
                        'controller' => 'zamowienia-specjalne',
                        'action' => 'index'
                            ], 'default', true) . '" class="btn btn-default btn-raised mr-2"><i class="fa fa-sun-o" aria-hidden="true" style="color: green"></i> Moje zamówienia</a>';
            $return .= '<a href="' . $view->url([
                        'controller' => 'zamowienia-specjalne',
                        'action' => 'koszyk'
                            ], 'default', true) . '" class="btn btn-default btn-raised mr-2"><i class="fa fa-sun-o" aria-hidden="true" style="color: green"></i> Moje produkty</a>';
        }

        if (strlen($return) > 5) {
            $zwroc = '<div class="btn-group" role="group" aria-label="Menu ph dla zamowien specjalnych">';
            $zwroc .= $return;
            $zwroc .= "\n\n</div>
<p>&nbsp;</p>\n";
        }


        return $zwroc;
    }

}
