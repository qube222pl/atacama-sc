<?php

/**
 * Description of KoszykAdresy
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_KoszykTabelaPodsumowanie extends Zend_View_Helper_Abstract {

    public function KoszykTabelaPodsumowanie($koszyk_id = NULL, $pdf = FALSE, $marza = FALSE) {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');

        $acl = Atacama_Acl::getInstance();
        $view = Zend_Layout::getMvcInstance()->getView();
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $user = Zend_Auth::getInstance()->getIdentity();

        $config = Atacama_Config::getInstance();
        $sWidth = $config->zdjecia->rozmiary->ms->w;
        $sHeight = $config->zdjecia->rozmiary->ms->h;

        if ($pdf || $acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) :

            if (is_numeric($koszyk_id) && $koszyk_id > 0) {
                $koszyk = $em->getRepository('Entities\Koszyk')->getById($koszyk_id);
            } else {
                $koszyk = $em->getRepository('Entities\Koszyk')->getKoszykUzytkownika($user['id']);
            }
            $return = '<div id="koszyk-tabela-podsumowanie">
                <div id="koszyk-lista">
                <div class="row" id="naglowek">
                        <div class="col row-img">&nbsp;</div>
                        <div class="col row-nazwa">' . $view->translate('nazwa produktu') . '</div>
                        <div class="col row-cena">' . $view->translate('cena j netto') . '</div>
                        <div class="col row-ilosc">' . $view->translate('ilosc') . '</div>
                        <div class="col row-cena">' . $view->translate('wartosc netto') . '</div>';
            if ($marza == TRUE && $acl->sprawdzDostep(Atacama_Acl::ZAS_ZAMOWIENIA_MARZA)) {
                $return .= '<div class="col row-marza">' . $view->translate('marza') . '</div>';
            }
            $return .= "\n</div>\n";

            $razemNetto = 0;
            $razemProduktow = 0;
            $znakowaniaSuma = 0;
            foreach ($koszyk->getKoszykiProdukty() as $koszykProdukt) :

                $dodajKonfekcjonowanie = FALSE;

                if (count($koszykProdukt->getKoszykiProduktyZnakowania()) > 0) {
                    $dodajKonfekcjonowanie = TRUE;
                }
                //$koszykProdukt = new Entities\Koszyk_Produkt;
                $produkt = $koszykProdukt->getProduktyDetale()->getProdukty();

                foreach ($produkt->getProduktyI18n() as $i18n) {
                    if ($i18n->getJezyk() == $lang) {
                        $nazwaProduktu = $i18n->getNazwa();
                    }
                }
                try {
                    $zdjecie = $em->getRepository('Entities\Produkt_Zdjecie')->getZdjecieKolor($produkt->getId(), $koszykProdukt->getProduktyDetale()->getKoloryId());

                    if ($zdjecie && strlen($zdjecie->getPlik()) > 4) {

                        $foto = new Moyoki_File($config->zdjecia->path . $zdjecie->getPlik());
                        $thumbSrc = $view->baseUrl($config->zdjecia->urlbase . $produkt->getDystrybutorzyId() . '/' . $foto->getThumbnailFile($sWidth, $sHeight));
                    } else {
                        $thumbSrc = $view->baseUrl('images/nophoto_' . $sWidth . 'x' . $sHeight . '.png');
                    }
                } catch (Exception $e) {
                    $thumbSrc = $view->baseUrl('images/nophoto_' . $sWidth . 'x' . $sHeight . '.png');
                }

                $return .= '<div class="produkt" id="pozycja-' . $koszykProdukt->getProduktyDetaleId() . '">
                    <div class="row">
                    <div class="col row-img">
                        <div class="holder exposed-box text-center"><img src="' . substr($config->appUrl, 0, -1) . $thumbSrc . '" alt="" /></div>
                    </div>
                    <div class="col row-nazwa">' . $nazwaProduktu . '<small>
                                ' . $koszykProdukt->getProduktyDetale()->getKolory()->getNazwa();

                if ($koszykProdukt->getProduktyDetale()->getRozmiar() != 0) {
                    $return .= ' ' . Atacama_Produkt_Rozmiar::pokaz($koszykProdukt->getProduktyDetale()->getRozmiar());
                }

                $return .= '</small><br/><small>' . $view->translate('nr kat') . ': ' . $koszykProdukt->getProduktyDetale()->getProdukty()->getNrKatalogowy() . '</small>
                        </div>';

                $cenaSzt = $koszykProdukt->getCenaZakupu();
                if ($dodajKonfekcjonowanie) {
                    $cenaSzt += $koszykProdukt->getCenaKonfekcjonowanie();
                }
                $return .= '<div class="col row-cena">' . Atacama_Produkt_Kwota::odczyt($cenaSzt, 'zł') . '</div>';

                $return .= '<div class="col  row-ilosc">';
                $razemProduktow += $koszykProdukt->getIlosc();
                $return .= $koszykProdukt->getIlosc();
                $return .= "\n</div>\n";

                $return .= '<div class="col row-cena">';
                $netto = $cenaSzt * $koszykProdukt->getIlosc();
                $razemNetto += $netto;
                $return .= Atacama_Produkt_Kwota::odczyt($netto, 'zł');
                $return .= "\n</div>\n";

                if ($marza == TRUE && $acl->sprawdzDostep(Atacama_Acl::ZAS_ZAMOWIENIA_MARZA)) {
                    $return .= '<div class="col row-marza">' . Atacama_Produkt_Kwota::odczyt(($koszykProdukt->getCenaZakupu() - $koszykProdukt->getCenaSprzedazy()) * $koszykProdukt->getIlosc(), 'zł') . '</div>';
                }
                $return .= "\n\n\t\t\t\t</div>\n"; // /row

                $cmd = new Application_Model_Commands_KoszykPokazZnakowaniaProduktu($em, $koszykProdukt);
                $result = $cmd->execute();
                if (!key_exists('brak', $result)) {
                    $return .= '<div class="znakowanie">
                        <div class="naglowek row">
                                            <div class="col row-nazwa">' . $view->translate('nazwa') . '</div>
                                            <div class="col row-kolor">' . $view->translate('ilosc kolorow') . '</div>
                                            <div class="col row-cena">' . $view->translate('cena j netto') . '</div>
                                            <div class="col row-ilosc">' . $view->translate('ilosc') . '</div>
                                            <div class="col row-cena">' . $view->translate('wartosc netto') . '</div>';
                    $return .= '</div>'; // /naglowek
                    foreach ($result['znakowania'] as $znakowanie) {
                        $znakowaniaSuma = $znakowaniaSuma + $znakowanie['suma'] + $znakowanie['przygotowalnia'];
                        $return .= '<div class="znakowanie row">
                                            <div class="col row-nazwa">' . $znakowanie['nazwa'] . '</div>
                                            <div class="col text-center row-kolor">' . $znakowanie['ilosc_kolorow'] . '</div>
                                            <div class="col row-cena">' . Atacama_Produkt_Kwota::odczyt($znakowanie['suma'] / $znakowanie['naklad'], 'zł') . '</div>
                                            <div class="col text-center row-ilosc">' . $znakowanie['naklad'] . '</div>
                                            <div class="col row-cena">' . Atacama_Produkt_Kwota::odczyt($znakowanie['suma'], 'zł') . '</div>
                                        </div>';
                        $return .= '<div class="przygotowalnia row">
                                            <div class="col text-right row-nazwa">' . $view->translate('przygotowalnia') . ':</div>
                                            <div class="col text-center row-kolor">' . $znakowanie['ilosc_kolorow'] . '</div>
                                            <div class="col row-cena">';

                        /**
                         * Jezeli znakowanie nie ma mnozenia, to nie dzielimy na kolory
                         */
                        if ($znakowanie['przygotowalnia_mnozenie'] > 0) {
                            $return .= Atacama_Produkt_Kwota::odczyt(($znakowanie['przygotowalnia'] / $znakowanie['ilosc_kolorow']), 'zł');
                        } else {
                            $return .= Atacama_Produkt_Kwota::odczyt(($znakowanie['przygotowalnia']), 'zł');
                        }

                        $return .= '</div>'; //col text-right hidden-sm-down

                        $return .= '<div class="col row-ilosc">&nbsp;</div>
                                            <div class="col row-cena">' . Atacama_Produkt_Kwota::odczyt($znakowanie['przygotowalnia'], 'zł') . '</div>
                                        </div>';
                    }
                    $return .= '</div>'; // /przygotowalnia
                }
                $return .= '</div>'; // /znakowanie

            endforeach;
            $return .= '</div>';

            $return .= '<p>&nbsp;</p>';
            $return .= '<div class="col-12 col-md-6">';
            $return .= '<h2>' . $view->translate('wartosc zamowienia') . '</h2>
            <table id="koszyk-podsumowanie" class="table table-striped">
                <thead>
                    <tr>
                        <th>' . $view->translate('netto') . '</th>
                        <th>' . $view->translate('vat') . '</th>
                        <th>' . $view->translate('brutto') . '</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>';
            $kosztDostawy = $config->kosztDostawyKarton * $koszykProdukt->getKoszyki()->getIloscKartonow();
            if ($kosztDostawy > 0) {
                $razemNetto = $razemNetto + $kosztDostawy;
            } else {
                //$return .= $view->translate('nie dotyczy');
            }
            /*
             * Jeszcze dodajemy koszt znakowania
             */
            $razemNetto = $razemNetto + $znakowaniaSuma;
            $return .= '<td class="text-center"><span id="razemNetto">' . Atacama_Produkt_Kwota::odczyt($razemNetto, 'zł') . '</span></td>
                        <td class="text-center">';

            if ($koszykProdukt->getKoszyki()->getVies() == 1) {
                $vat = 0;
            } else {
                $vat = $config->importVat;
            }
            $return .= $vat . '</td>
                        <td class="text-center"><span id="razemBrutto">' . Atacama_Produkt_Kwota::odczyt($razemNetto + ($razemNetto * $vat / 100), 'zł') . '</span></td>
                    </tr>
            </table>';
            $return .= '</div></div>';

            return $return;
        endif;
    }

}
