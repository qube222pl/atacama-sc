<?php

/**
 * Description of Miniaturkaadmin
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_MiniaturkaAdmin extends Zend_View_Helper_Abstract {

    public function miniaturkaAdmin($thumbnail, Entities\Produkt_Zdjecie $zdjecie) {
        $string = "\n<div class=\"col-xs-12 col-sm-6 col-md-3\">";
                
        $string .= '<div class="thumbnail exposed-box p-2 mb-4 bg-white text-center ' . ($zdjecie->getGlowne() == '1' ? ' glowne' : '') . '" id="z' . $zdjecie->getId() . '">
			<p class="img"><img src="' . $thumbnail . '" /></p>';

        $string.= '<p class="kolor"><span id="'.$zdjecie->getId().'-kolor">';
        $kolor = $zdjecie->getKolory();
        if ($kolor instanceof Entities\Kolor)
            $string .= $kolor->getNazwa() . ' [' . $kolor->getKod() . ']';
        else
            $string .= "Zdjęcie detal";
        
        $string .= '</span>&nbsp;<button class="btn btn-primary btn-xs kolor-zmien" data-toggle="modal" id="'.$zdjecie->getId().'" data-target="#ModalKolor">Zmień</button>';
        $string .= "\n</p>\n";
// Przyciski
        $string .= "<p class=\"przyciski\">";
// Glowny
        if ($zdjecie->getGlowne() != '1') {
            $string .= "<a href=\"" . $this->view->url(array(
                        'language' => 'pl',
                        'controller' => 'Produkt',
                        'action' => 'zdjecia',
                        'id' => $zdjecie->getProduktyId(),
                        'glowne' => $zdjecie->getId()
                            ), 'default', true) . "\" class=\"btn btn-warning btn-sm\">Ustaw główne</a>";
            $string .= "<a onclick=\"javascript:kasujZdjecie('" . $this->view->url(array(
                        'language' => 'pl',
                        'controller' => 'ajax',
                        'action' => 'usunzdjecie',
                        'zdjecie_id' => $zdjecie->getId(),
                            ), 'default', true) . "', '#z" . $zdjecie->getId() . "');\" class=\"btn btn-info btn-sm\">Usuń</a>";
        } else {
            $string .= "<div class='alert alert-warning'>Zdjęcie główne</div>";
        }
        $string .= "</p>";
        $string .= "\n</div>\n";
        $string .= "\n</div>\n";

        return $string;
    }

}
