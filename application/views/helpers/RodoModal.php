<?php

/**
 * Description of RodoModal
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_RodoModal extends Zend_View_Helper_Abstract {

    public function RodoModal() {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $view = Zend_Layout::getMvcInstance()->getView();
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $em = $bootstrap->getResource('Entitymanagerfactory');
            $auth = Zend_Auth::getInstance()->getIdentity();
            $user = $em->getRepository('Entities\Uzytkownik')->getById($auth['id']);
            if ($user instanceof Entities\Uzytkownik && null === $user->getDataRodo()) {
                ?>

                <div class="modal fade show" tabindex="-1" role="dialog" id="modal-rodo">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Obowiązek informacyjny - RODO</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Drogi Kliencie<br>
                                    Dbając o Twoje dane osobowe i w oparciu o wewnętrzne procedury w związku z wejściem w życie od dnia 25 maja 2018 r. RODO (Ogólne Rozporządzenie o Ochronie Danych Osobowych), prowadzimy weryfikację wyrażonych przez Ciebie zgód. Pragniemy spełnić względem Ciebie ponowny obowiązek informacyjny.</p>
                                <p>Po więcej szczegółów zapraszamy na stronę <a href="<?php
                                    echo $view->url(array(
                                        'language' => 'pl',
                                        'controller' => 'strona',
                                        'action' => 'podglad',
                                        'kod' => 'polityka-prywatnosci',
                                        'nazwa' => ''
                                            ), 'strony', true);
                                    ?>"><?php echo $view->translate('polityka prywatnosci'); ?></a>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary btn-raised" id="rodo-potwiedzam">Potwierdzam zgodę</button>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
            }
        }
    }

}
