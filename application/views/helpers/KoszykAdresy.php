<?php

/**
 * Description of KoszykAdresy
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_KoszykAdresy extends Zend_View_Helper_Abstract {

    public function KoszykAdresy($koszyk_id = NULL, $pdf = FALSE) {
        $acl = Atacama_Acl::getInstance();

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');

        $view = Zend_Layout::getMvcInstance()->getView();
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $user = Zend_Auth::getInstance()->getIdentity();


        if ($pdf || $acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {

            $return = "<div id=\"koszyk-adresy\" class=\"row exposed-box pt-3 mb-4 bg-white\">\n";
//          $return .= "\n<div id=\"zawartosc\">";

            if (is_numeric($koszyk_id) && $koszyk_id > 0) {
                $koszyk = $em->getRepository('Entities\Koszyk')->getById($koszyk_id);
            } else {
                $koszyk = $em->getRepository('Entities\Koszyk')->getKoszykUzytkownika($user['id']);
            }
            if ($koszyk instanceof Entities\Koszyk) {
                //$koszyk = new Entities\Koszyk;

                $return .= '<div class="col-xs-12 col-md-4 adres"><h3>' . $view->translate('nabywca') . '</h3>';
                $return .= '<p class="nazwa">' . $koszyk->getFirmy()->getNazwa() . '</p>';
                $return .= '<p>' . $view->translate('nip') . ': ' . $koszyk->getFirmy()->getNip() . '</p>';
                $return .= '<p>' . $view->translate('regon') . ': ' . $koszyk->getFirmy()->getRegon() . '</p>';
                $return .= '</div>';

                $adres = $em->getRepository('Entities\Adres')->getById($koszyk->getAdresFvId());
                if ($adres instanceof Entities\Adres) {
                    $return .= '<div class="col-xs-12 col-md-4 adres"><h3>' . $view->translate('adres fakturowy') . '</h3>'
                            . '<p class="nazwa">' . $adres->getNazwa() . '</p>'
                            . '<p>' . $adres->getUlica() . '<br>' . $adres->getKodPocztowy() . ' ' . $adres->getMiasto() . '</p></div>';

                    $adres = $em->getRepository('Entities\Adres')->getById($koszyk->getAdresyDostawyId());
                    if ($adres !== null) {
                        $return .= '<div class="col-xs-12 col-md-4 adres"><h3>' . $view->translate('adres dostawy') . '</h3><p>' . $adres->getUlica() . '<br>' . $adres->getKodPocztowy() . ' ' . $adres->getMiasto() . '</p></div>';
                    } else {
                        $return .= '<div class="col-xs-12 col-md-4 adres"><h3>' . $view->translate('adres dostawy') . '</h3><p>' . $view->translate('odbior wlasny') . '</p></div>';
                    }
                }
            }

//          $return .= "\n</div>\n";
            $return .= "</div>\n";
            $crypt = Moyoki_Crypt::getInstance();
            $return .= '<p class="zamawiajacy">' . $view->translate('osoba zamawiajaca') . ': <b>' . $koszyk->getUzytkownicy()->getImie() . ' ' . $koszyk->getUzytkownicy()->getNazwisko() . '</b>, '.$crypt->decrypt($koszyk->getUzytkownicy()->getEmail()).'</p>';


            return $return;
        }
    }

}
