<?php

/**
 * Generowanie listy uzytkownikow dla firm korporacyjnych
 * Jest to lista do dodawania nowych uzytkownikow
 * Na liscie nie ma juz istniejacych osob, nawet nieaktywnych
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_KorporacyjniUzytkownicyLista extends Zend_View_Helper_Abstract {

    public function KorporacyjniUzytkownicyLista($select, $firma_id) {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');

        $obecni = $em->getRepository('Entities\Korporacyjny_Uzytkownik')->getWszystkichDlaFirmy($firma_id);
        $obecni_pracownicy = [];
        foreach ($obecni as $op) {
            if ($op instanceof Entities\Korporacyjny_Uzytkownik) {
                $obecni_pracownicy[$op->getUzytkownicyId()] = $op->getUzytkownicyId();
            }
        }
        $wszyscy = $em->getRepository('Entities\Uzytkownik')->allActiveOrderByNazwisko();

        foreach ($wszyscy as $u) {
            if ($u instanceof Entities\Uzytkownik && !key_exists($u->getId(), $obecni_pracownicy)) {
                $select->addMultiOption($u->getId(), $u->getNazwisko() . ' ' . $u->getImie() . ' - ' . $u->getFirmy()->getNazwa());
            }
        }
        return $select;
    }
}
