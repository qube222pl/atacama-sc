<?php

/**
 * Description of LeweMenu
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_ProductView extends Zend_View_Helper_Abstract {

    public function ProductView(Entities\Produkt $produkt, $sell = FALSE, $adminView = FALSE) {
        $config = Atacama_Config::getInstance();

        $LogotypMWidth = $config->logotypy->rozmiary->s->w;
        $logotypMHeight = $config->logotypy->rozmiary->s->h;

        $acl = Atacama_Acl::getInstance();

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');

        $view = Zend_Layout::getMvcInstance()->getView();
        ?>
        <div class="product-card-box hvr-grow py-3 col-sm-6 col-lg-3">
            <div class="product-card">
                <div class="card exposed-box">
                    <?php
                    foreach ($produkt->getProduktyI18n() as $i18n) {
                        if ($i18n->getJezyk() == 'pl') {
                            $nazwa = $i18n->getNazwa();
                        }
                    }

                    $url = $view->url(array(
                        'controller' => 'Produkt',
                        'action' => 'podglad',
                        'id' => $produkt->getId(),
                        'nazwa' => Moyoki_Friendly::name($nazwa)), 'produkty', true);

                    // Ikony promocja nowosc
                    $ikony = '';

                    if ($produkt->getNowosc() == 1) {
                        $ikony .= '<span class="badge badge-new">Nowość</span>';
                    }

                    if ($produkt->getPromocja() == 1) {
                        $ikony .= '<span class="badge badge-promo">Promocja</span>';
                    }

                    if ($produkt->getBestseller() == 1) {
                        $ikony .= '<span class="badge badge-bestseller">Bestseller</span>';
                    }

                    $url_array = array('controller' => 'Produkt',
                        'action' => 'widok', 'id' => $produkt->getId(), 'nazwa' => Moyoki_Friendly::name($nazwa));

                    if ($sell) {
                        $url_array['sell'] = 'true';
                    }

                    /**
                     * Logotyp
                     */
                    if (null != $produkt->getProducenci()->getLogo() && strlen($produkt->getProducenci()->getLogo()) > 4) :
                        ?>
                        <div class="logo">
                            <?php
                            try {
                                $foto = new Moyoki_File($config->logotypy->path . $produkt->getProducenci()->getLogo());
                                $thumbSrc = $this->view->baseUrl($config->logotypy->urlbase . $foto->getThumbnailFile($LogotypMWidth, $logotypMHeight));
                                ?>
                                <a href="">
                                    <img src="<?php echo $thumbSrc; ?>" alt="<?php echo strtolower($produkt->getProducenci()->getNazwa()); ?>" />
                                </a>

                                <?php
                            } catch (Exception $e) {
                                echo $e->getMessage();
                            }
                            ?>
                        </div>
                        <?php
                    endif;
                    ?>
                    <div class="product-img">
                        <?php
                        $zdjecia = $this->_pobierzZdjecia($em, $view, $produkt, $nazwa);
                        foreach ($zdjecia as $zdjecie) :
                            ?>
                            <div>
                                <a href="<?php echo $url; ?>"><img src="<?php echo $zdjecie['foto']; ?>" class="img img-responsive" alt="<?php echo \Moyoki_Friendly::name($nazwa); ?>" /></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="card-body">
                        <div class="card-badge">
                            <?php echo $ikony; ?>
                        </div>
                        <h6 class="card-title">
                            <a href="<?php echo $url; ?>" title="<?php echo \Moyoki_Friendly::name($nazwa); ?>"><?php echo $nazwa; ?></a>
                        </h6>
                        <?php
                        $srp = NULL;
                        foreach ($produkt->getProduktyDetale() as $detal):
                            if ($detal instanceof Entities\Produkt_Detal && NULL != $detal->getSrp()) :
                                $srp = $detal->getSrp();
                            endif;
                        endforeach;
                        if ($srp != NULL):
                            ?>
                            <p class="card-text text-right"><small class="text-muted"><?php echo $this->view->translate('cena od'); ?>: <?php echo (NULL != $srp ? $srp : 'brak'); ?> zł</small></p>

                        <?php endif;
                        ?>
                        <div class="card-btn-container text-center">
                            <a href="<?php echo $url; ?>" class="btn btn-primary btn-raised">Szczegóły</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function _pobierzZdjecia($em, $view, Entities\Produkt $produkt, $nazwaProduktu) {
        $config = Atacama_Config::getInstance();
        $mWidth = $config->zdjecia->rozmiary->m->w;
        $mHeight = $config->zdjecia->rozmiary->m->h;

        $zdjeciaArray = [];
        if (count($produkt->getProduktyZdjecia())) {

            foreach ($produkt->getProduktyZdjecia() as $zdjecie) :
                if ($zdjecie instanceof Entities\Produkt_Zdjecie && NULL != $zdjecie->getKoloryId() && $zdjecie->getGlowne() == 1 && $zdjecie->getWidoczne() == 1):
                    Atacama_Produkt_ZdjecieNazwaSeo::zmien($em, $zdjecie, $nazwaProduktu);
                    try {
                        if ($zdjecie instanceof Entities\Produkt_Zdjecie) {
                            try {
                                $plik = new Moyoki_File($config->zdjecia->path . $zdjecie->getPlik());
                                $foto = $view->baseUrl($config->zdjecia->urlbase . $produkt->getDystrybutorzyId() . '/' . $plik->getThumbnailFile($mWidth, $mHeight));
                            } catch (Exception $e) {
                                $plik = new Moyoki_File($config->zdjecia->path . 'nophoto.png');
                                $foto = $view->baseUrl($config->zdjecia->urlbase . $plik->getThumbnailFile($mWidth, $mHeight));
                            }
                        }
                    } catch (Exception $e) {
                        Moyoki_Debug::debug('zdjecia brak', __METHOD__);
                        $plik = new Moyoki_File($config->zdjecia->path . 'nophoto.png');
                        $foto = $view->baseUrl($config->zdjecia->urlbase . $plik->getThumbnailFile($mWidth, $mHeight));
                    }

                    $zdjeciaArray[] = [
                        'foto' => $foto,
                        'kolor-nazwa' => $zdjecie->getKolory()->getNazwa()
                    ];

                endif;
            endforeach;
        }
        /**
         * Sytuacja gdy produkt nie ma zdjec
         * Pewnie sie nie zdarza, ale kto wie
         */
        if (count($zdjeciaArray) == 0) {
            $plik = new Moyoki_File($config->zdjecia->path . 'nophoto.png');
            $foto = $view->baseUrl($config->zdjecia->urlbase . $plik->getThumbnailFile($mWidth, $mHeight));
            $zdjeciaArray[] = [
                'foto' => $foto,
                'kolor-nazwa' => 'Brak'
            ];
        }
        return $zdjeciaArray;
    }

}
