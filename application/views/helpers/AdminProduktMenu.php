<?php

/**
 * Menu widoczne przez admina przy produktach
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_AdminProduktMenu extends Zend_View_Helper_Abstract {

    public function AdminProduktMenu($produktID, $active = 'podglad') {
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $acl = Atacama_Acl::getInstance();
        $returnString = '';

        if ((int) $produktID > 0) {
            if ($acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
                $returnString = "\n<a href=\"" . $this->view->url(
                                array('language' => $lang, 'controller' => 'Produkty', 'action' => 'podglad', 'id' => $produktID), 'produkty', true) . "\" "
                        . " class=\"btn btn-default btn-raised mr-2 ".($active == 'podglad'?"active":"")."\">Podgląd</a>\n"
                        . "\n<a href=\"" . $this->view->url(
                                array('language' => $lang, 'controller' => 'Produkt', 'action' => 'zmien', 'id' => $produktID), 'default', true) . "\" "
                        . " class=\"btn btn-default btn-raised mr-2 ".($active == 'edycja'?"active":"")."\">Edycja</a>\n"
                        . "\n<a href=\"" . $this->view->url(
                                array('language' => $lang, 'controller' => 'Produkt', 'action' => 'tlumaczenia', 'id' => $produktID), 'default', true) . "\" "
                        . " class=\"btn btn-default btn-raised mr-2 ".($active == 'tlumaczenia'?"active":"")."\">Tłumaczenia</a>\n"
                        . "\n<a href=\"" . $this->view->url(
                                array('language' => $lang, 'controller' => 'Produkt', 'action' => 'detale', 'id' => $produktID), 'default', true) . "\" "
                        . " class=\"btn btn-default btn-raised mr-2 ".($active == 'detale'?"active":"")."\">Detale</a>\n"
                        . "\n<a href=\"" . $this->view->url(
                                array('language' => $lang, 'controller' => 'Produkt', 'action' => 'zdjecia', 'id' => $produktID), 'default', true) . "\" "
                        . " class=\"btn btn-default btn-raised mr-2 ".($active == 'zdjecia'?"active":"")."\">Zdjęcia</a>\n"
                        . "\n<a href=\"" . $this->view->url(
                                array('language' => $lang, 'controller' => 'Produkt', 'action' => 'kategorie', 'id' => $produktID), 'default', true) . "\" "
                        . " class=\"btn btn-default btn-raised mr-2 ".($active == 'kategorie'?"active":"")."\">Kategorie</a>\n"
                        . "\n<a href=\"" . $this->view->url(
                                array('language' => $lang, 'controller' => 'Produkt', 'action' => 'znakowanie', 'id' => $produktID), 'default', true) . "\" "
                        . " class=\"btn btn-default btn-raised mr-2 ".($active == 'znakowanie'?"active":"")."\">Metody znakowania</a>\n";
            }
        }

        $result = "\n<div class=\"btn-group mr-2\" role=\"group\">".$returnString."\n</div>\n";
        return "\n<div class=\"btn-toolbar\">".$result."</div><div class=\"clearfix\"></div>";
    }
}
