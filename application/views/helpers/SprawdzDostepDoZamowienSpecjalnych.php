<?php

/**
 * Sprawdza czy uzytkownik jest w jakiejs firmie zapisany jako Klient specjalny
 * 
 * @author Studio Moyoki
 */
class Zend_View_Helper_SprawdzDostepDoZamowienSpecjalnych extends Zend_View_Helper_Abstract {

    public function SprawdzDostepDoZamowienSpecjalnych() {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');
        $user = Zend_Auth::getInstance()->getIdentity();

        $firmy = $em->getRepository('Entities\Korporacyjny_Uzytkownik')->getWszystkichByUserId($user['id']);

        /**
         * Sprawdzamy czy uzytkownik jest gdzies przypisany
         */
        foreach ($firmy as $firma) {
            if ($firma instanceof Entities\Korporacyjny_Uzytkownik) {
                /**
                 * Jezeli uzytkownik jest przysany gdziej jako klient
                 */
                if ($firma->getRola() == Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KLIENT) {
                    return TRUE;
                }
            }
            /**
             * Jezeli uzytkownik jest przysany gdziej jako koordynator
             */
            if ($firma->getRola() == Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KOORDYNATOR) {
                return TRUE;
            }
        }
        return FALSE;
    }

}
