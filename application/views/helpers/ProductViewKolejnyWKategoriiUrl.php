<?php

/**
 * Description of ProductViewKolejnyWKategoriiUrl
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_ProductViewKolejnyWKategoriiUrl extends Zend_View_Helper_Abstract {

    public function ProductViewKolejnyWKategoriiUrl(Entities\Produkt $produkt) {
        $start = microtime(TRUE);

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('Entitymanagerfactory');

        $view = Zend_Layout::getMvcInstance()->getView();

        $i = 0;
        $produktObecny = FALSE;
        $produktNastepny = NULL;
        $produktPierwszy = NULL;
        $produkty = $em->getRepository('Entities\Produkt')->paginatorByKategoriaIdOrderByNazwa([$produkt->getKategoriaGlowna()], 'pl', TRUE);
        foreach ($produkty as $p) {
            if ($p instanceof Entities\Produkt) {
                $i++;
                
                if ($i == 1) {
                    $produktPierwszy = $p;
                }
                
                /**
                 * To musi byc wyzej, bo we wrzesniejszej iteracji tablicy
                 * powinno sie dodac
                 */
                if ($produktObecny) {
                    $produktNastepny = $p;
                    $produktObecny = FALSE;
                    break;
                }
                /**
                 * Jezeli jest ten sam, to nastepna iteracja pobieze produkt
                 */
                if ($p->getId() == $produkt->getId()) {
                    $produktObecny = TRUE;
                }
                
            }
        }

        if (NULL === $produktNastepny) {
            $produktNastepny = $produktPierwszy;
        }

        if ($produktNastepny instanceof Entities\Produkt) {
            foreach ($produktNastepny->getProduktyI18n() as $i18n) {
                if ($i18n instanceof Entities\Produkt_i18n && $i18n->getJezyk() == 'pl') {
                    $nazwa = $i18n->getNazwa();
                }
            }
            $url = $view->url(array(
                'controller' => 'Produkt',
                'action' => 'podglad',
                'id' => $produktNastepny->getId(),
                'nazwa' => Moyoki_Friendly::name($nazwa)), 'produkty', true);
        }

        /**
         * Statystyki
         */
        $total_time = round((microtime(TRUE) - $start), 4);
        $wynik = [
            'kategoria_id' => 14,
            'ilosc_w_kategorii' => count($produkty),
            'w_kolejnosci' => $i,
            'czas' => $total_time
        ];

        Atacama_Log::dodaj($em, Atacama_Log::WYDAJNOSC_POBIERANIE_NASTEPNEGO_PRODUKTU_W_KATEGORII, serialize($wynik));

        return $url;
    }

}
