<?php

/**
 * Menu widoczne przez admina przy producentach
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_AdminProducentMenu extends Zend_View_Helper_Abstract {

    public function AdminProducentMenu($producentID, $active = 'podglad') {
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $acl = Atacama_Acl::getInstance();
        $returnString = '';

        if ((int) $producentID > 0) {
            if ($acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
                $returnString = "\n<a class='btn btn-default btn-raised mr-2 " . ($active == 'podglad' ? "active" : "") . "' href=\"" . $this->view->url(array(
                            'controller' => 'Marka', 'action' => 'widok', 'id' => $producentID), 'marki', true) . "\" "
                        . ">Podgląd</a>\n"
                        . "<a class='btn btn-default btn-raised mr-2 " . ($active == 'edycja' ? "active" : "") . "' href=\"" . $this->view->url(
                                array('controller' => 'Producent', 'action' => 'zmien', 'id' => $producentID), 'default', true) . "\" "
                        . ">Edycja</a>\n"
                        . "<a class='btn btn-default btn-raised mr-2 " . ($active == 'produkty' ? "active" : "") . "' href=\"" . $this->view->url(
                                array('controller' => 'Producent', 'action' => 'produkty', 'id' => $producentID), 'default', true) . "\" "
                        . ">Produkty producenta</a>\n";
            }
        }

        return "\n<ul class=\"nav nav-tabs\">" . $returnString . "\n</ul><p>&nbsp;</p>";
    }

}
