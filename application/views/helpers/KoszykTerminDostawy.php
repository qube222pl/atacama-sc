<?php

/**
 * Wizualna prezentacja terminu dostawy
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_KoszykTerminDostawy extends Zend_View_Helper_Abstract {

    public function KoszykTerminDostawy($koszyk) {
        if ($koszyk instanceof Entities\Koszyk) {

            $view = Zend_Layout::getMvcInstance()->getView();
            
            $return = "<div id=\"termin-dostawy\" class=\"row\">\n";
            $return .= '<div class="col-xs-12"><h2>' . $view->translate('planowany termin dostawy') . ':</h2></div>';
            if (null === $koszyk->getDataDostawy()) {
                $return .= '<div class="data-opis col-xs-12 col-md-6 text-center">' . $view->translate('10-15 dni od akceptacji wizualizacji lub prototypu');
            } else {
                $return .= '<div class="data col-xs-12 col-md-4 text-center">' . date_format($koszyk->getDataDostawy(), 'd.m.Y') . 'r';
            }
            $return .= '</div>';

            $return .= "</div>\n";


            return $return;
        }
    }

}
