<?php

/**
 * Wyswietlenie zdjecia produktu korporacyjnego
 *
 * @author Studio Moyoki
 */
class Zend_View_Helper_KorporacyjniProduktZdjecie extends Zend_View_Helper_Abstract {

    private $_width;
    private $_height;

    public function KorporacyjniProduktZdjecie(Entities\Korporacyjny_Produkt $produkt, $width = NULL, $height = NULL) {
        $config = Atacama_Config::getInstance();
        $this->_width = (NULL != $width ? $width : $config->zdjecia->rozmiary->xl->w);
        $this->_height = (NULL != $height ? $height : $config->zdjecia->rozmiary->xl->h);


        try {
            $photo = new Moyoki_File($config->zdjeciaspecjalne->path . $produkt->getZdjecie());
            $image = $config->zdjeciaspecjalne->urlbase . $photo->getThumbnailFile($this->_width, $this->_height);
        } catch (Exception $e) {
            $image = $this->_getZdjecieProduktu($produkt->getProduktyDetale()->getProdukty());
        }
        return $config->resources->frontController->baseUrl . $image;
    }

    /**
     * Pobieranie zdjecia glownego produktu
     * @return string path
     */
    private function _getZdjecieProduktu(Entities\Produkt $produkt) {
        $config = Atacama_Config::getInstance();
        foreach ($produkt->getProduktyZdjecia() as $zdjecie) {
            if ($zdjecie instanceof Entities\Produkt_Zdjecie && $zdjecie->getGlowne() == 1) {
                try {

                    $photo = new Moyoki_File($config->zdjecia->path . $zdjecie->getPlik());
                    $image = $config->zdjecia->urlbase . $produkt->getDystrybutorzyId() . '/' . $photo->getThumbnailFile($this->_width, $this->_height);
                } catch (Exception $e) {
                    $image = $this->_getBrakZdjecia();
                }
            }
        }
        return $image;
    }

    private function _getBrakZdjecia() {
        $config = Atacama_Config::getInstance();
        $photo = new Moyoki_File($config->zdjecia->path . 'nophoto.png');
        $image = $config->zdjecia->urlbase . $photo->getThumbnailFile($this->_width, $this->_height);
        return $image;
    }

}
