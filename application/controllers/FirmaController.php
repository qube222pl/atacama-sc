<?php

class FirmaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_FIRMY)) {
            $this->addMessageError('brak uprawnien', TRUE);
            exit;
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = true;
    }

    public function indexAction() {


        $strona = (int) $this->getRequest()->getParam('s');
        $config = Atacama_Config::getInstance();
        $f = trim($this->getRequest()->getParam('f'));

        $form = new Application_Form_AdminWyszukiwarka();
        $this->view->form = $form;

        if (strlen($f) > 2) {
            $fraza = $f;
            $form->getElement('f')->setValue($f);
        } else {
            $fraza = NULL;
        }

        $paginator = $this->_entityManager->getRepository('Entities\Firma')->paginatorByKategoriaIdOrderByNazwa($fraza, 'nazwa');

        $paginatorIter = $paginator->getIterator();

        $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

        $zend_paginator = new \Zend_Paginator($adapter);
        if (!isset($strona))
            $strona = 1;

        $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
        $zend_paginator->setItemCountPerPage($iloscNaStronie)
                ->setCurrentPageNumber($strona);

        $this->view->lang = $this->_lang;
        $this->view->iter = $iloscNaStronie * ($strona - 1);

        $this->view->paginator = $zend_paginator;
        $this->view->em = $this->_entityManager;
    }

    public function dodajAction() {
        $form = new Application_Form_Firma($this->_entityManager, 'dodaj');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                
                $firma = $this->_entityManager->getRepository('Entities\Firma')->getByNip($form->getValue('nip'));

                if ($firma instanceof Entities\Firma) {
                    $this->addMessageError('Firma o numerze nip <b>' . $form->getValue('nip') . '</b> już jest w systemie', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Firma');
                } else {
                    $ph = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($form->getValue('ph_id'));

                    if ($ph instanceof Entities\Uzytkownik) {
                        $firma = new Entities\Firma();
                        $firma->setNazwa($form->getValue('nazwa'))
                                ->setNip($form->getValue('nip'))
                                ->setRegon($form->getValue('regon'))
                                ->setUpust(($form->getValue('upust') > 0 ? (int) $form->getValue('upust') : 0))
                                ->setPhId($ph->getId())
                                ->setPh($ph);

                        $this->_entityManager->persist($firma);
                        $this->_entityManager->flush();
                    }
                }

                $this->addMessageSuccess('Nowa firma została dodana pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Firma');
            }

            if ($form->isErrors()) {
                $form->populate($this->getRequest()->getPost());
            }
        }
    }

    public function zmienAction() {

        $firmaID = (int) $this->getRequest()->getParam('id');

        if ($firmaID > 0) {
            $form = new Application_Form_Firma($this->_entityManager, 'zmien');
            $this->view->form = $form;

            $firma = $this->_entityManager->getRepository('Entities\Firma')->getById($firmaID);

            $tablica_firma = array('nazwa' => $firma->getNazwa(),
                'nip' => $firma->getNip(),
                'id' => $firma->getId(),
                'upust' => $firma->getUpust(),
                'regon' => $firma->getRegon()
            );

            $form->populate($tablica_firma);
            $form->getElement('ph_id')->setValue($firma->getPhId());

            if ($this->getRequest()->isPost()) {
                if ($form->isValid($_POST)) {
                    try {
                        $cmd = new Application_Model_Commands_FirmaZmien($this->_entityManager, $_POST);
                        $cmd->execute();
                    } catch (Exception $exc) {
                        $this->addMessageError($exc->getMessage(), TRUE);
                        $this->_redirect('/' . $this->_lang . '/Firma');
                    }

                    $this->addMessageSuccess('Dane firmy zostały zmienione', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Firma');
                }

                if ($form->isErrors()) {
                    $form->populate($_POST);
                }
            }
        } else {
            $this->addMessageError('Nie ma firmy o podanym ID', TRUE);
            $this->_redirect('/' . $this->_lang . '/Firma');
        }
    }

    public function pracownicyAction() {
        $firmaID = (int) $this->getRequest()->getParam('id');

        if ($firmaID > 0) {
            $this->view->firma = $this->_entityManager->getRepository('Entities\Firma')->getById($firmaID);

            $this->view->pracownicy = $this->_entityManager->getRepository('Entities\Uzytkownik')->getByFirmaId($firmaID);
        }
    }

}
