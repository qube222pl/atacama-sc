<?php

class KoszykController extends Atacama_Controller_Action {

    private $koszykDostep;

    public function init() {
        parent::init();
        $this->koszykDostep = Atacama_Koszyk_Dostep::getInstance();
    }

    public function indexAction() {
        /**
         * Zatwierdzanie ofert przez osoby niezalogowane
         */
        $zatwierdz = (int) $this->getRequest()->getParam('zatwierdz');
        if (isset($zatwierdz) && is_numeric($zatwierdz)) {
            if (!Zend_Auth::getInstance()->hasIdentity()) {
                $this->_redirect('/' . $this->_lang . '/Uzytkownik/logowanie?redirect=' . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());
            }
        }

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $id = (int) $this->getRequest()->getParam('id');

        if ($id > 0) {
            $this->view->dodajId = $id;
            $params[Atacama_Koszyk_Dostep::PARAM_KOSZYK_ID] = $id;
            if (!$this->koszykDostep->sprawdzDostep($params, $this->_entityManager)) {
                /**
                 * Odmowa dostepu do koszyka z ID
                 */
                $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
                $this->_redirect('/' . $this->_lang . '/index/');
            } else {
                /**
                 * Koszyk specyficzny: ofertowy / przetargowy
                 */
                $koszyk = $this->koszykDostep->getKoszyk();
            }
        } else {
            /**
             * Zwykly koszyk klienta
             */
            $koszyk = $this->_entityManager->getRepository('Entities\Koszyk')->getKoszykUzytkownika($this->_user['id']);
        }

        if ($this->getRequest()->isPost()) {
            if ($koszyk instanceof Entities\Koszyk) {
                $cmd = new Application_Model_Commands_KoszykAktualizuj($this->_entityManager, $_POST);
                $koszyk = $cmd->execute();
                if ($koszyk->getStatus() > 0) {
                    $this->_redirect('/' . $this->_lang . '/Koszyk/index/id/' . $koszyk->getId());
                }
            }
        }

        $this->view->acl = $this->_acl;
        $this->view->em = $this->_entityManager;
        $this->view->lang = $this->_lang;
        $this->view->tytul = 'moj koszyk';
        $this->view->headScript()->appendFile($this->view->baseURL('/js/koszykIndex.js'));

        if ($koszyk instanceof Entities\Koszyk) {

            switch ($koszyk->getStatus()) {
                case 0: $this->view->tytul = 'moj koszyk';
                    break;
                case 10: $this->view->tytul = 'oferta dla klienta';
                    break;
                case 11: $this->view->tytul = 'oferta dla ciebie';
                    break;
            }

            $this->view->koszyk = $koszyk;
            if (count($koszyk->getKoszykiProdukty()) == 0) {
                $this->view->koszykPusty = TRUE;
            }
        }
    }

    public function adresyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $id = (int) $this->getRequest()->getParam('id');

        if ($id > 0) {
            $this->view->dodajId = $id;

            $params[Atacama_Koszyk_Dostep::PARAM_KOSZYK_ID] = $id;
            if (!$this->koszykDostep->sprawdzDostep($params, $this->_entityManager)) {
                /**
                 * Odmowa dostepu do koszyka z ID
                 */
                $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
                $this->_redirect('/' . $this->_lang . '/index/');
            } else {
                /**
                 * Koszyk specyficzny: ofertowy / przetargowy
                 */
                $koszyk = $this->koszykDostep->getKoszyk();
                $dodajAdresPowrot = '/koszyk/adresy/id/' . $koszyk->getId();
                $koszykZID = TRUE;
            }
        } else {
            /**
             * Zwykly koszyk klienta
             */
            $koszyk = $this->_entityManager->getRepository('Entities\Koszyk')->getKoszykUzytkownika($this->_user['id']);
            $dodajAdresPowrot = '/koszyk/adresy';
        }
        if ($koszyk instanceof Entities\Koszyk && count($koszyk) == 1) {

            $adresy = $this->_entityManager->getRepository('Entities\Adres')->wszystkieAdresyFirmy($koszyk->getFirmyId());

            $form = new Application_Form_KoszykAdresy($this->_entityManager, $adresy);
            $this->view->form = $form;


            $formNowyAdres = new Application_Form_Adres($this->_entityManager, Application_Form_Adres::DODAJ + Application_Form_Adres::ADRES_DOSTAWA + Application_Form_Adres::ADRES_CONTROLLER, $this->_lang, $dodajAdresPowrot);

            $formNowyAdres->getElement('firmy_id')->setValue($koszyk->getFirmyId());

            $this->view->formNowyAdres = $formNowyAdres;

            if ($this->getRequest()->isPost()) {

                if (isset($_POST['wstecz'])) {
                    if (isset($koszykZID)) {
                        $this->_redirect('/' . $this->_lang . '/Koszyk/index/id/' . $koszyk->getId());
                    } else {
                        $this->_redirect('/' . $this->_lang . '/Koszyk');
                    }
                }

//                if (isset($_POST['wykonaj'])) {
//                    $this->_dodajAdres($formNowyAdres);
//                }

                if ($form->isValid($this->getRequest()->getPost())) {

                    if (isset($_POST['dalej'])) {
                        $koszyk->setAdresFvId($form->getValue('adresFaktura'))
                                ->setAdresyDostawyId($form->getValue('adresWysylka'))
                        ;
                        $this->_entityManager->persist($koszyk);
                        $this->_entityManager->flush();
                        if (isset($koszykZID)) {
                            $this->_redirect('/' . $this->_lang . '/Koszyk/podsumowanie/id/' . $koszyk->getId());
                        } else {
                            $this->_redirect('/' . $this->_lang . '/Koszyk/podsumowanie');
                        }
                    }
                }
            } else {
                $form->populate(array(
                    'adresFaktura' => $koszyk->getAdresFvId(),
                    'adresWysylka' => $koszyk->getAdresyDostawyId()
                ));
            }

            $this->view->adresy = true;
        }
    }

    public function podsumowanieAction() {

        $this->view->headLink()
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css',
                    'href' => 'https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css'));
        $this->view->headScript()->appendFile('https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js')
                ->appendFile($this->view->baseUrl('js/koszykPodsumowanie.js'));

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $id = (int) $this->getRequest()->getParam('id');

        if ($id > 0) {
            $this->view->dodajId = $id;
            $params[Atacama_Koszyk_Dostep::PARAM_KOSZYK_ID] = $id;
            if (!$this->koszykDostep->sprawdzDostep($params, $this->_entityManager)) {
                /**
                 * Odmowa dostepu do koszyka z ID
                 */
                $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
                $this->_redirect('/' . $this->_lang . '/index/');
            } else {
                /**
                 * Koszyk specyficzny: ofertowy / przetargowy
                 */
                $koszyk = $this->koszykDostep->getKoszyk();
                $koszykZID = TRUE;
            }
        } else {
            /**
             * Zwykly koszyk klienta
             */
            $koszyk = $this->_entityManager->getRepository('Entities\Koszyk')->getKoszykUzytkownika($this->_user['id']);
        }
        if ($koszyk instanceof Entities\Koszyk && count($koszyk) == 1) {
            $form = new Application_Form_KoszykPodsumowanie($this->_entityManager, $this->_acl);

            if (NULL === $koszyk->getDataDostawy()) {
                $form->getElement('data_dostawy')->setValue(Atacama_Data::getDataZaDniRobocze(15));
            }

            if ($koszyk->getStatus() == Atacama_acl::STATUS_OFERTA_TWORZENIE) {
                $form->getElement('dalej')->setLabel('zatwierdz oferte');
            }

            $this->view->form = $form;


            $uwagi = $this->_entityManager->getRepository('Entities\Koszyk_Uwaga')->getByUserKoszykId($this->_user['id'], $koszyk->getId());
            if (isset($uwagi[0])) {
                $uwaga = $uwagi[0];
            } else {
                $uwaga = new Entities\Koszyk_Uwaga();
            }

            if ($this->getRequest()->isPost()) {
                if ($form->isValid($_POST)) {


                    if (isset($_POST['wstecz'])) {
                        if ($koszykZID) {
                            $this->_redirect('/' . $this->_lang . '/Koszyk/adresy/id/' . $koszyk->getId());
                        } else {
                            $this->_redirect('/' . $this->_lang . '/Koszyk/adresy');
                        }
                    }

                    if (isset($_POST['dalej'])) {
                        $uzytkownikObj = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($this->_user['id']);
                        if ($uzytkownikObj instanceof Entities\Uzytkownik) {
                            $uwaga->setUzytkownicyId($uzytkownikObj->getId())
                                    ->setUzytkownicy($uzytkownikObj)
                                    ->setKoszykiId($koszyk->getId())
                                    ->setTresc($form->getValue('uwagi'))
                                    ->setStatus($koszyk->getStatus())
                                    ->setKoszyki($koszyk);
                            $this->_entityManager->persist($uwaga);
                            $this->_entityManager->flush();

                            $koszyk->setDataDostawy($form->getValue('data_dostawy'))
                                    ->setDataZamowienia(date('Y-m-d G:i:s'));

                            $this->_entityManager->getRepository('Entities\Koszyk')->zmien($koszyk);
                        }
                        if ($koszykZID) {
                            $this->_redirect('/' . $this->_lang . '/Koszyk/potwierdzenie/id/' . $koszyk->getId());
                        } else {
                            $this->_redirect('/' . $this->_lang . '/Koszyk/potwierdzenie');
                        }
                    }
                }
            } else {
                $form->populate(array('uwagi' => $uwaga->getTresc()));
            }
            $this->view->koszyk = $koszyk;
            $this->view->podsumowanie = true;
        }
    }

    public function potwierdzenieAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $id = (int) $this->getRequest()->getParam('id');

        if ($id > 0) {
            $this->view->dodajId = $id;

            $params[Atacama_Koszyk_Dostep::PARAM_KOSZYK_ID] = $id;
            if (!$this->koszykDostep->sprawdzDostep($params, $this->_entityManager)) {
                /**
                 * Odmowa dostepu do koszyka z ID
                 */
                $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
                $this->_redirect('/' . $this->_lang . '/index/');
            } else {
                /**
                 * Koszyk specyficzny: ofertowy / przetargowy
                 */
                $koszyk = $this->koszykDostep->getKoszyk();
                $koszykZID = TRUE;
            }
        } else {
            /**
             * Zwykly koszyk klienta
             */
            $koszyk = $this->_entityManager->getRepository('Entities\Koszyk')->getKoszykUzytkownika($this->_user['id']);
        }
        if ($koszyk instanceof Entities\Koszyk && count($koszyk) == 1) {

            if ($koszyk->getStatus() == Atacama_Acl::STATUS_KOSZYK || $koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA || $koszyk->getStatus() == Atacama_Acl::STATUS_WYSLANE) {
                $koszykPodsumowanie = $this->view->KoszykTabelaPodsumowanie($koszyk->getId()) . $this->view->KoszykTerminDostawy($koszyk) . $this->view->KoszykAdresy($koszyk->getId(), FALSE, TRUE);

                // zmiana statusu koszyka i nadanie nr zamowienia
                $najwyzszyNrZamObj = $this->_entityManager->getRepository('Entities\Koszyk')->getNajwyzszyNrZamowienia();

                $najwyzszyNrZam = $najwyzszyNrZamObj->getIdZamowienia() + 1;

                $koszyk->setIdZamowienia($najwyzszyNrZam)
                        ->setNrZamowienia($najwyzszyNrZam . '/' . date('Y'))
                        ->setStatus(Atacama_Acl::STATUS_PRZYJETE);
                $this->_entityManager->getRepository('Entities\Koszyk')->zmien($koszyk);

                /*
                 *  Wysyłamy  e-maia do użytkownika
                 */
                $widget = $this->_entityManager->getRepository('Entities\Widget')->getByKod('MAIL_KOSZYK_UZYTKOWNIK');
                $email = new Atacama_Mail($this->_entityManager, $this->_lang);
                $temat = $this->view->translate('atacama gifts zamowienie nr') . ' ' . $koszyk->getNrZamowienia();

                $tresc = str_replace('##KOSZYK##', $koszykPodsumowanie, $widget->getTresc());
                $wynikWysylki = $email->klientNoweZamowienie($this->_user['mail'], $temat, $tresc);

                if (!$wynikWysylki) {
                    $this->addMessageSuccess('Zamówienie zostało przyjęte, ale jest bląd wysyłki maila', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Koszyk');
                }

                /*
                 *  pobieramy adres email PH
                 */

                $ph = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($koszyk->getFirmy()->getPhId());
                $adresEmail = Moyoki_Crypt::getInstance()->decrypt($ph->getEmail());

                /*
                 *  wysyłamy maila do PH
                 */
                $widgetph = $this->_entityManager->getRepository('Entities\Widget')->getByKod('MAIL_KOSZYK_PH');

                $trescPh = str_replace('##KOSZYK##', $koszykPodsumowanie, $widgetph->getTresc());

                $emailPh = new Atacama_Mail($this->_entityManager, $this->_lang);
                $wynikWysylkidoPH = $emailPh->phNoweZamowienie($adresEmail, $temat, $trescPh);

                $widgetPotwierdzenie = $this->_entityManager->getRepository('Entities\Widget')->getByKod('WWW_POTWIERDZENIE_ZAMOWIENIA');
                if ($widgetPotwierdzenie instanceof Entities\Widget) {
                    $this->view->widget = $widgetPotwierdzenie->getTresc();
                }
            } else {
                $this->view->oferta = $koszyk;
            }
        }
    }

    public function dodajZnakowanieAction() {

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $cmd = new Application_Model_Commands_ZamowienieDodajZnakowanie($this->_entityManager, $_POST);
        $result = $cmd->execute();
        if ($result instanceof Entities\Koszyk_Produkt_Znakowanie) {
            $this->addMessageSuccess('Znakowanie dodane', TRUE);
            if ($result->getKoszykiProdukty()->getKoszyki()->getStatus() > 0) {
                $this->_redirect('/' . $this->_lang . '/Koszyk/index/id/' . $result->getKoszykiProdukty()->getKoszyki()->getId());
            }
        } else {
            $this->addMessageError('Coś nie poszło dobrze.', TRUE);
        }

        $this->_redirect('/' . $this->_lang . '/Koszyk');
    }

    public function usunProduktAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->_helper->layout->disableLayout();
        $id = (int) $this->getRequest()->getParam('id');

        $auth = Zend_Auth::getInstance()->getIdentity();

        $kp = $this->_entityManager->getRepository('Entities\Koszyk_Produkt')->getById($id);
        $koszyk = $kp->getKoszyki();
        // 1. Koszyk jest USERa
        // 2. Oferta jest USERa
        // 3. Oferta w trakcie tworzenia przez HP
        if (($koszyk->getStatus() == Atacama_Acl::STATUS_KOSZYK && $koszyk->getUzytkownicyId() == $auth['id']) || ($koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA && $koszyk->getUzytkownicyId() == $auth['id']) || ($koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA_TWORZENIE && $koszyk->getFirmy()->getPhId() == $auth['id'])) {

            $kpusuniety = $this->_entityManager->getRepository('Entities\Koszyk_Produkt')->deleteById($kp->getId());

            if ($kpusuniety == 1) {
                /*                 * Produkt zostal usuniety - przeliczamy podsumowanie */
            } else {
                $this->addMessageError('Pozycja nie została usunięta', TRUE);
            }
        } else {
            $this->addMessageError('Nie ma takiej pozycji', TRUE);
        }
        $this->_redirect('/' . $this->_lang . '/Koszyk');
    }

    public function usunZnakowanieAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $id = (int) $this->getRequest()->getParam('id');

        $auth = Zend_Auth::getInstance()->getIdentity();

        $kpz = $this->_entityManager->getRepository('Entities\Koszyk_Produkt_Znakowanie')->getById($id);
        if ($kpz instanceof Entities\Koszyk_Produkt_Znakowanie) {
            //$kpz = new Entities\Koszyk_Produkt_Znakowanie;
            /**
             * Sprawdzamy czy znakowanie ma konfekcjonowanie. 
             * Jezeli ma, to musi przekazac dalej
             */
            $koszyk = $kpz->getKoszykiProdukty()->getKoszyki();
            // 1. Koszyk jest USERa
            // 2. Oferta jest USERa
            // 3. Oferta w trakcie tworzenia przez HP
            if (($koszyk->getStatus() == Atacama_Acl::STATUS_KOSZYK && $koszyk->getUzytkownicyId() == $auth['id']) || ($koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA && $koszyk->getUzytkownicyId() == $auth['id']) || ($koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA_TWORZENIE && $koszyk->getFirmy()->getPhId() == $auth['id'])) {
                $kpusuniety = $this->_entityManager->getRepository('Entities\Koszyk_Produkt_Znakowanie')->deleteById($kpz->getId());
                if ($kpusuniety == 1) {
                    /**
                     * Produkt zostal usuniety - przeliczamy podsumowanie
                     */
                } else {
                    $this->addMessageError('Pozycja nie została usunięta', TRUE);
                }
            } else {
                $this->addMessageError('Nie ma takiego znakowania w koszyku', TRUE);
            }
        } else {
            $this->addMessageError('Nie ma takiej pozycji', TRUE);
        }

        $this->_redirect('/' . $this->_lang . '/Koszyk');
    }

    /**
     * Uzywane przy pokazywaniu okna modalnego
     * @return content
     */
    public function ajaxDodajZnakowanieAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();

            return false;
        } else {
            $this->_helper->layout->disableLayout();
            $prod_id = (int) $this->getRequest()->getParam('prod_id');
            $detal_id = (int) $this->getRequest()->getParam('detal_id');
            $edycja = (int) $this->getRequest()->getParam('edycja');

            $this->view->form = new Application_Form_KoszykProduktZnakowanie($this->_entityManager, $prod_id, $detal_id, $edycja);
        }
    }

    public function ajaxIloscKolorowAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();

            return false;
        } else {
            $this->_helper->layout->disableLayout();
            $znakowanie_id = $this->getRequest()->getParam('znakowanie_id');
            $znakowanie = $this->_entityManager->getRepository('Entities\Znakowanie')->getById($znakowanie_id);
            //$znakowanie = new Entities\Znakowanie;
            //$this->view->form = new Application_Form_ZnakowanieProdukt($this->_entityManager, $prod_id);
            echo $znakowanie->getKolory();
        }
    }

    /**
     * Zatwierdzenie oferty z pliku PDF
     * Dosc ryzykowne, bo jest to akcja bez logowania
     */
    public function zatwierdzAction() {
        $zamID = (int) $this->getRequest()->getParam('id');
        if ($zamID == 0) {
            $this->addMessageError($this->view->translate('niepoprawny identyfikator'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }

        /**
         * Sprawdzane po parametrze weryfikacji
         * W celu zwiekszenia bezpieczensta
         */
        $ver = $this->getRequest()->getParam('ver');
        $crypt = Moyoki_Crypt::getInstance();

        /*
          if ($crypt->decrypt($ver) != 'oferta-nr-' . $zamID) {
          $this->addMessageError($this->view->translate('niepoprawny identyfikator'), TRUE);
          $this->_redirect('/' . $this->_lang . '/Index');
          }

         */
        $koszyk = $this->_entityManager->getRepository('Entities\Koszyk')->getById($zamID);
        if (!$koszyk instanceof Entities\Koszyk) {
            $this->addMessageError($this->view->translate('brak oferty'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }

        if ($koszyk->getStatus() != Atacama_Acl::STATUS_WYSLANE) {
            $this->addMessageError($this->view->translate('brak oferty'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }

        $uwagi = '<p>Akceptacja automatyczna z pliku oferty</p>'
                . 'IP: ' . $_SERVER['REMOTE_ADDR'] . '<br/>'
                . 'Przegladarka: ' . $_SERVER['HTTP_USER_AGENT'] . '<br/>'
                . 'Jezyk: ' . $_SERVER['HTTP_ACCEPT_LANGUAGE'] . '<br/>';

        $post = array('id' => $zamID,
            'status' => Atacama_acl::STATUS_PRZYJETE,
            'uwagi' => $uwagi);

        $cmd = new Application_Model_Commands_ZamowienieZmienStatus($this->_entityManager, $post);
        $zamowienieZmienione = $cmd->execute();

        if ($zamowienieZmienione instanceof Entities\Koszyk) {

            /**
             * Oferte zatwierdza osoba niezalogowana
             */
            if (!Zend_Auth::getInstance()->hasIdentity()) {

                $crypt = Moyoki_Crypt::getInstance();
                $zatwierdzacz = $this->_entityManager->getRepository('Entities\Uzytkownik')->getByEmail($crypt->encrypt('zatwierdzacz@atacama.pl'));
                if (!$zatwierdzacz instanceof Entities\Uzytkownik) {
                    Atacama_Powiadomienie::dodaj($this->_entityManager, Atacama_Powiadomienie::BLAD_SYSTEMOWY, json_encode(array('opis' => 'Brak użytkownika zatwierdzacz@atacama.pl To jest użytkownik systemowy i jest potrzebny!!!')));
                } else {
                    $uwaga = new Entities\Koszyk_Uwaga();

                    $uwaga->setUzytkownicy($zatwierdzacz)
                            ->setUzytkownicyId($zatwierdzacz->getId())
                            ->setTresc($uwagi)
                            ->setStatus(Atacama_acl::STATUS_PRZYJETE)
                            ->setKoszykiId($zamowienieZmienione->getId())
                            ->setKoszyki($zamowienieZmienione);

                    $this->_entityManager->persist($uwaga);
                    $this->_entityManager->flush();
                }

                /*
                 *  Wysyłamy  e-maia do użytkownika
                 */

                $widget = $this->_entityManager->getRepository('Entities\Widget')->getByKod('MAIL_OFERTA_POTW_UZYTKOWNIK');
                $email = new Atacama_Mail($this->_entityManager, $this->_lang);
                $temat = $this->view->translate('atacama gifts zamowienie nr') . ' ' . $zamowienieZmienione->getNrZamowienia();

                $tresc = $widget->getTresc();
                $wynikWysylki = $email->klientNoweZamowienie($crypt->decrypt($zamowienieZmienione->getUzytkownicy()->getEmail()), $temat, $tresc);
                if (!$wynikWysylki) {
                    $this->addMessageSuccess('zamowienie zostalo przyjete, ale jest blad wysylki maila', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Index');
                }

                /*
                 *  pobieramy adres email PH
                 */

                $ph = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($zamowienieZmienione->getFirmy()->getPhId());
                $adresEmail = Moyoki_Crypt::getInstance()->decrypt($ph->getEmail());

                /*
                 *  wysyłamy maila do PH
                 */
                $widgetph = $this->_entityManager->getRepository('Entities\Widget')->getByKod('MAIL_OFERTA_POTW_PH');

                $trescPh = $widgetph->getTresc();

                $emailPh = new Atacama_Mail($this->_entityManager, $this->_lang);
                $wynikWysylkidoPH = $emailPh->phNoweZamowienie($adresEmail, $temat, $trescPh);
            }
            $this->addMessageSuccess($this->view->translate('oferta zatwierdzona') . '.', TRUE);
        } else {
            $this->addMessageError($this->view->translate('blad zatwiedzenia oferty') . '.', TRUE);
        }
        $this->_redirect('/' . $this->_lang . '/Index');
    }

}
