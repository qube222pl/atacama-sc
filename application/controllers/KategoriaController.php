<?php

class KategoriaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = TRUE;
    }

    public function indexAction() {
        $this->view->lang = $this->_lang;
        $this->view->kategorie = $this->_entityManager->getRepository('Entities\Kategoria')->orderByNazwa($this->_lang);
    }

    public function dodajAction() {

        $form = new Application_Form_Kategoria($this->_entityManager, 'dodaj');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $cmd = new Application_Model_Commands_KategoriaDodaj($this->_entityManager, $this->_lang, $_POST);
                $cmd->execute();

                $this->addMessageSuccess('Nowa kategoria została dodana pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Kategoria');
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function zmienAction() {
        $kategoriaID = (int) $this->getRequest()->getParam('id');

        if ($kategoriaID > 0) {
            $form = new Application_Form_Kategoria($this->_entityManager, 'zmien');
            $this->view->form = $form;

            $kategoria = $this->_entityManager->getRepository('Entities\Kategoria')->getByid($kategoriaID);

            $tablica_kategoria = array(
                'nazwa' => $kategoria->getKategorieI18n()->first()->getNazwa(),
                'konfekcjonowanie' => str_replace('.', ',', $kategoria->getKonfekcjonowanie()),
                'id' => $kategoria->getId(),
                'kategoria' => $kategoria->getRodzic(),
                'opcje' => $kategoria->getOpcje()
            );

            $form->populate($tablica_kategoria);

            if ($this->getRequest()->isPost()) {
                if ($form->isValid($_POST)) {
                    try {
                        $cmd = new Application_Model_Commands_KategoriaZmien($this->_entityManager, $_POST);
                        $cmd->execute();
                    } catch (Exception $exc) {
                        $this->addMessageError($exc->getMessage(), TRUE);
                        $this->_redirect('/' . $this->_lang . '/Kategoria');
                    }

                    $this->addMessageSuccess('Dane kategorii zostały zmienione', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Kategoria');
                }

                if ($form->isErrors()) {
                    $form->populate($_POST);
                }
            }
        } else {
            $this->addMessageError('Nie ma kategorii o podanym ID', TRUE);
            $this->_redirect('/' . $this->_lang . '/Kategoria');
        }
    }

    public function tlumaczeniaAction() {
        $kategoriaID = (int) $this->getRequest()->getParam('id');

        if ($kategoriaID > 0) {
            $form = new Application_Form_KategoriaNazwa();
            $this->view->form = $form;

            $kategoria = $this->_entityManager->getRepository('Entities\Kategoria')->getById($kategoriaID);

            $nazwyTablica = array('id' => $kategoria->getId());
            foreach ($kategoria->getKategorieI18n() as $i18n) {
                $nazwyTablica[$i18n->getJezyk()] = array(
                    'nazwa_' . $i18n->getJezyk() => $i18n->getNazwa(),
                    'opis_' . $i18n->getJezyk() => $i18n->getOpis(),
                );
            }

            $form->populate($nazwyTablica);

            if ($this->getRequest()->isPost()) {
                if ($form->isValid($_POST)) {
                    try {
                        $cmd = new Application_Model_Commands_KategoriaNazwy($this->_entityManager, $_POST);
                        $cmd->execute();
                    } catch (Exception $exc) {
                        $this->addMessageError($exc->getMessage(), TRUE);
                        $this->_redirect('/' . $this->_lang . '/Kategoria');
                    }

                    $this->addMessageSuccess('Dane kategorii zostały zmienione', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Kategoria');
                }

                if ($form->isErrors()) {
                    $form->populate($_POST);
                }
            }
        } else {
            $this->addMessageError('Nie ma kategorii o podanym ID', TRUE);
            $this->_redirect('/' . $this->_lang . '/Kategoria');
        }
    }

    public function usunAction() {

        $id = (int) $this->_getParam('id');
        if ($id > 0) {

            $kategoria = $this->_entityManager->getRepository('Entities\Kategoria')->getByid($id);
            $nazwaKategorii = $kategoria->getKategorieI18n()->first()->getNazwa();
            $this->view->kategoria = $nazwaKategorii;

            if ($this->getRequest()->isPost()) {
                $del = $this->getRequest()->getPost('del');
                if ($del == 'Tak') {

                    $result = $this->_entityManager->getRepository('Entities\Kategoria')->usun($id);

                    if ($result == 1) {
                        $this->addMessageSuccess('Kategoria ' . $this->view->kategoria . ' została prawidłowo usunięta.', true);
                        $this->_redirect('/' . $this->_lang . '/Kategoria');
                    } else {
                        $this->addMessageError('Wystąpił błąd podczas usuwania kategorii \'' . $nazwaKategorii . '\'. Prawdopodobnie istnieją produkty przypisane do tej kategorii.', true);
                        $this->_redirect('/' . $this->_lang . '/Kategoria');
                    }
                } else {
                    $this->_helper->redirector('index');
                }
            }
        } else {
            $this->addMessageInfo('Brak podanej kategorii', true);
            $this->_redirect('/' . $this->_lang . '/Kategoria');
        }
    }

    public function cenyAction() {

        $id = (int) $this->_request->getParam('id');

        if ($id < 1) {
            $this->addMessageError('Brak identyfikatora', TRUE);
            $this->_redirect('/' . $this->_lang . '/Kategoria/');
        }

        $kategoria = $this->_entityManager->getRepository('Entities\Kategoria')->getById($id);

        if (null === $kategoria) {
            $this->addMessageError('Nie ma takiej kategorii', TRUE);
            $this->_redirect('/' . $this->_lang . '/Kategoria/');
        }

        $this->view->kategoriaId = $kategoria->getId();

        $this->view->headScript()->appendFile($this->view->baseURL('/js/kategorieCeny.js'));

        $this->view->kategoriaNazwa = $kategoria->getKategorieI18n()->first()->getNazwa();

        $this->view->rodzaje = $this->_entityManager->getRepository('Entities\Kategoria_Cena')->getByKategoriaId($kategoria->getId());


        if ($this->getRequest()->isPost()) {
            try {
                $cmd = new Application_Model_Commands_KategoriaCeny($this->_entityManager, $_POST);
                $cmd->execute();

                $this->addMessageSuccess('Ceny dla kategorii <b>' . $this->view->znakowanieNazwa . '</b> zmienione pomyślnie.', TRUE);
                $this->_redirect('/' . $this->_lang . '/Kategoria');
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
    }

    public function wdolAction() {

        $id = (int) $this->_getParam('id');
        if ($id > 0) {

            $kategoria = $this->_entityManager->getRepository('Entities\Kategoria')->getByid($id);
            $this->_entityManager->getRepository('Entities\Kategoria')->przesunPozycje($id, -1);
            $this->_redirect('/' . $this->_lang . '/Kategoria');
        }
    }

    public function wgoreAction() {

        $id = (int) $this->_getParam('id');
        if ($id > 0) {

            $kategoria = $this->_entityManager->getRepository('Entities\Kategoria')->getByid($id);
            $this->_entityManager->getRepository('Entities\Kategoria')->przesunPozycje($id, 1);
            $this->_redirect('/' . $this->_lang . '/Kategoria');
        }
    }

}
