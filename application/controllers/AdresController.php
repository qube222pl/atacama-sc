<?php

class AdresController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    public function indexAction() {
        $this->view->firmy = $this->_entityManager->getRepository('Entities\Firma')->pobierzWszystkieOrderBy();
    }

    public function dodajAction() {

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_FIRMY)) {
            $firmaID = $this->_user['firma_id'];
            $this->view->uzytkownik = TRUE;
        } else {
            $firmaID = (int) $this->getRequest()->getParam('firma_id');

            if ($firmaID == 0) {
                $firmaID = $this->_user['firma_id'];
            }
        }
        $this->view->firma = $this->_entityManager->getRepository('Entities\Firma')->getById($firmaID);

        $powrot = isset($_POST['powrot']) ? $_POST['powrot'] : null;
        $form = new Application_Form_Adres($this->_entityManager, Application_Form_Adres::DODAJ, $this->_lang, $powrot);
        $this->view->form = $form;

        $formTab = array('firmy_id' => $this->view->firma->getId());

        $form->populate($formTab);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {

                if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_FIRMY)) {
                    $firma = $this->_entityManager->getRepository('Entities\Firma')->getById($this->_user['firma_id']);
                } else {
                    $firma = $this->_entityManager->getRepository('Entities\Firma')->getById($_POST['firmy_id']);
                }


                if ($firma instanceof Entities\Firma) {
                    $adres = new Entities\Adres();
                    $adres->setNazwa($_POST['nazwa'])
                            ->setTypAdresu($_POST['typ_adresu'])
                            ->setUlica($_POST['ulica'])
                            ->setFirmy($firma)
                            ->setFirmyId($firma->getId())
                            ->setKodPocztowy($_POST['kod_pocztowy'])
                            ->setMiasto($_POST['miasto'])
                            ->setKraj($_POST['kraj'])
                            ->setNrTelefonu($_POST['telefon'])
                            ->setWidoczny(1);

                    $this->_entityManager->persist($adres);
                    $this->_entityManager->flush();
                } else {
                    $this->addMessageError('Błąd. Brakuje informacji dla jakiej firmy przypisać adres.', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Adres/firmy/id/' . $firmaID);
                }

                if (isset($_POST['powrot'])) {
                    $this->_redirect('/' . $this->_lang . $_POST['powrot']);
                } else {
                    $this->addMessageSuccess('Nowy adres został dodany pomyślnie', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Adres/firmy/id/' . $firmaID);
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function zmienAction() {

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_FIRMY)) {
            $firmaID = $this->_user['firma_id'];
            $this->view->uzytkownik = TRUE;
        } else {
            $firmaID = (int) $this->getRequest()->getParam('firma_id');
            if ($firmaID == 0) {
                $firmaID = $this->_user['firma_id'];
            }
        }

        $adresID = (int) $this->getRequest()->getParam('id');



        if ($adresID > 0) {
            $this->view->firma = $this->_entityManager->getRepository('Entities\Firma')->getById($firmaID);

            $form = new Application_Form_Adres($this->_entityManager, Application_Form_Adres::ZMIEN);
            $this->view->form = $form;

            $adres = $this->_entityManager->getRepository('Entities\Adres')->getById($adresID);

            $formTab = array('firmy_id' => $firmaID,
                'nazwa' => $adres->getNazwa(),
                'ulica' => $adres->getUlica(),
                'miasto' => $adres->getMiasto(),
                'kod_pocztowy' => $adres->getKodPocztowy(),
                'typ_adresu' => $adres->getTypAdresu(),
                'id' => $adres->getId(),
                'kraj' => $adres->getKraj(),
                'telefon' => $adres->getNrTelefonu()
            );

            $form->populate($formTab);

            if ($this->getRequest()->isPost()) {
                if ($form->isValid($_POST)) {
                    try {
                        $cmd = new Application_Model_Commands_AdresZmien($this->_entityManager, $_POST);
                        $cmd->execute();
                    } catch (Exception $exc) {
                        $this->addMessageError($exc->getMessage(), TRUE);
                        $this->_redirect('/' . $this->_lang . '/Adres/firmy/id/' . $firmaID);
                    }

                    $this->addMessageSuccess('Dane adresowe zostały zmienione', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Adres/firmy/id/' . $firmaID);
                }

                if ($form->isErrors()) {
                    $form->populate($_POST);
                }
            }
        } else {
            $this->addMessageError('Niepoprawny identyfikator adresu do zmiany', TRUE);
            $this->_redirect('/' . $this->_lang . '/Adres/firmy/id/' . $firmaID);
        }
    }

    public function firmyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_FIRMY)) {
            $firmaID = $this->_user['firma_id'];
            $this->view->uzytkownik = TRUE;
        } else {
            $this->view->adminView = true;
            $firmaID = (int) $this->getRequest()->getParam('id');
        }
        if ($firmaID > 0) {
            $this->view->adresy = $this->_entityManager->getRepository('Entities\Adres')->wszystkieAdresyFirmy($firmaID);
            $this->view->firma = $this->_entityManager->getRepository('Entities\Firma')->getById($firmaID);
        } else {
            $this->addMessageError('Brak firmy o podanym identyfikatorze', TRUE);
            $this->_redirect('/' . $this->_lang . '/index');
        }
    }

}
