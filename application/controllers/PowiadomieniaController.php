<?php

class PowiadomieniaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_OGOLNA)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }
        $this->view->adminView = TRUE;
    }

    public function indexAction() {
        $this->view->lang = $this->_lang;
        $this->view->headScript()->appendFile($this->view->baseURL('/js/PowiadomieniaIndex.js'));

        if (isset($_POST) && count($_POST) > 0) {
            $this->_entityManager->beginTransaction();
            foreach ($_POST as $k => $v) {
                if (is_numeric($k)) {
                    $powiadomienie = $this->_entityManager->getRepository('Entities\Powiadomienie')->getById($k);

                    if ($powiadomienie instanceof Entities\Powiadomienie) {
                        $powiadomienie->setWidoczne(0);
                        $this->_entityManager->getRepository('Entities\Powiadomienie')->Zmien($powiadomienie);
                        $this->addMessageSuccess('Powiadomienie usuniete pomyślnie', TRUE);
                    }
                }
            }
            $this->_entityManager->commit();
        }
        $this->view->powiadomienia = $this->_entityManager->getRepository('Entities\Powiadomienie')->getWszystkieOrdeByDataDesc();
    }

    public function podgladAction() {
        $this->view->lang = $this->_lang;
        $id = (int) $this->getRequest()->getParam('id');
        if ($id > 0) {
            $powiadomienie = $this->_entityManager->getRepository('Entities\Powiadomienie')->getById($id);
            if (NULL === $powiadomienie) {
                $this->addMessageError('Brak powiadomienia', TRUE);
                $this->_redirect('/' . $this->_lang . '/Powiadomienia');
            } else {
                $this->view->powiadomienie = $powiadomienie;

                $powiadomienie->setPrzeczytane(1);
                $this->_entityManager->getRepository('Entities\Powiadomienie')->Zmien($powiadomienie);
            }
        }
    }

    public function usunAction() {
        $this->view->lang = $this->_lang;
        $id = (int) $this->getRequest()->getParam('id');
        if ($id > 0) {
            $powiadomienie = $this->_entityManager->getRepository('Entities\Powiadomienie')->getById($id);
            if (NULL === $powiadomienie) {
                $this->addMessageError('Brak powiadomienia', TRUE);
                $this->_redirect('/' . $this->_lang . '/Powiadomienia');
            } else {
                $powiadomienie->setWidoczne(0);
                $this->_entityManager->getRepository('Entities\Powiadomienie')->Zmien($powiadomienie);
                $this->addMessageSuccess('Powiadomienie usuniete pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Powiadomienia');
            }
        }
    }

}
