<?php

class ImportController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = TRUE;
    }

    public function indexAction() {

        $this->view->lang = $this->_lang;
        $this->view->headScript()->appendFile($this->view->baseURL('/js/formImport.js'));

        $form = new Application_Form_Import($this->_entityManager);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

//            Moyoki_Debug::debug($_POST);
//            exit;
            if ($_POST['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_PLIK) {
                $plik = $form->getElement('plik');
                $plik->setRequired(true);
            }

//            Moyoki_Debug::debug($_POST);
//            exit;
            if ($form->isValid($_POST)) {

                $cmd = new Application_Model_Commands_Import($this->_entityManager, array_merge($_POST, $form->getValues() ));
                $result = $cmd->execute();

                if ($result === true) {
                    $this->addMessageSuccess('Import przeprowadzony pomyślnie', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Import');
                } else {
                    $this->addMessageError('Import zakończył się błędem. ' . $result);
                    $form->populate($_POST);
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function ustawieniaAction() {

        $this->view->lang = $this->_lang;
        $this->view->dystrybutorzy = $this->_entityManager->getRepository('Entities\Dystrybutor')->orderByNazwa(1);
    }

    public function raportyAction() {

        $this->view->lang = $this->_lang;
        $path = realpath(Atacama_Config::getInstance()->importLog->path);


        $file = $this->getRequest()->getParam('plik');
        if (is_string($file) && strlen($file) > 0) {
            if (strpos($file, '..') === false) {
                $raport = file_get_contents($path . '/' . $file);
                $raport = str_replace("\n", "<br>", $raport);
                $this->_helper->layout()->disableLayout();
                $this->view->raport = $raport;
                $this->render('raport');
                return;
            }
        }

        $files = array();
        $dir = glob($path . '/*.txt');
        foreach ($dir as $file) {
            $files[] = basename($file);
        }
        $this->view->raporty = $files;
    }

    public function podsumowanieAction() {
        $this->view->headScript()->appendFile($this->view->baseURL('/js/ImportPodsumowanie.js'));

        $this->view->lang = $this->_lang;
        $this->view->raporty = $this->_entityManager->getRepository('Entities\Import_Raport')->wszystkieOdNajnowszych(500);
    }

    public function zmienAction() {

        $dystrybutorId = (int) $this->_request->getParam('id');

        if ($dystrybutorId < 1) {
            $this->_redirect('/' . $this->_lang . '/Import/ustawienia');
        }

        $dystrybutor = $this->_entityManager->getRepository('Entities\Dystrybutor')->getById($dystrybutorId);

        if (null === $dystrybutor) {
            $this->_redirect('/' . $this->_lang . '/Import/ustawienia');
        }

        $this->view->nazwaDystrybutora = $dystrybutor->getNazwa();
        $this->view->dystrybutorId = $dystrybutorId;

        $form = new Application_Form_ImportParametry($this->_entityManager, $dystrybutorId);


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $polecenie = new Application_Model_Commands_ImportZmien($this->_entityManager, $form->getValues(), $dystrybutorId);
                $polecenie->execute();

                $this->addMessageSuccess('Parametry importu zostały zmienione.', TRUE);
                $this->_redirect('/' . $this->_lang . '/Import/zmien/id/' . $dystrybutorId);
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }

        $this->view->form = $form;
    }

    public function ajaxFormAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $dystrybutor = (int) $this->getRequest()->getParam('dystrybutor');

            if ($dystrybutor > 0) {

                $opcje = null;
                $importParams = $this->_entityManager->getRepository('Entities\Import_Parametr')->filtrujPoDysIdParam($dystrybutor, 'nazwaKlasy');
                if ($importParams instanceof Entities\Import_Parametr) {
                    $className = 'Application_Model_Import_' . $importParams->getWartosc();
                    if (class_exists($className)) {
                        $opcje = $className::pobierzOpcje();
                    }
                }


                if (NULL === $opcje || !is_array($opcje)) {
                    return false;
                }

                // Moyoki form do dekoratorow
                $mf = new Moyoki_Form();

                /**
                 * Zakres select
                 */
                $zakres = new Zend_Form_Element_Select('zakres');
                $zakres->setLabel('Zakres')
                        ->setDecorators($mf->divElementDecorators)
                        ->setAttrib('class', 'form-control sm')
                        ->addMultiOption('null', '-- Proszę wybrać --');

                /**
                 * Zrodlo select
                 */
                $zrodlo = new Zend_Form_Element_Select('zrodlo');
                $zrodlo->setLabel('Źródło')
                        ->setDecorators($mf->divElementDecorators)
                        ->setAttrib('class', 'form-control sm');

                /**
                 * Ta tablica jest uzywana w widoku do prezentajci
                 */
                $opcje_js = array();

                foreach ($opcje AS $zakres_id => $zrodlo_id) {
                    switch ($zakres_id) {
                        case Application_Model_Import_Abstract::ZAKRES_OFERTA:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_OFERTA, 'oferta');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_STANY:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_STANY, 'stany');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_ZDJECIA:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_ZDJECIA, 'zdjecia');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN, 'tłumaczenie en');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU, 'tłumaczenie ru');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_DE:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_DE, 'tłumaczenie de');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_PL:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_PL, 'tłumaczenie pl');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_CENY:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_CENY, 'ceny');
                            break;
                        case Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE:
                            $zakres->addMultiOption(Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE, 'znakowanie ');
                            break;
                    }

                    for ($i = 0; $i < count($zrodlo_id); $i++) {
                        switch ($zrodlo_id[$i]) {
                            case Application_Model_Import_Abstract::ZRODLO_INTERNET:
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_INTERNET]['nazwa'] = 'Internet';
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_INTERNET]['id'] = Application_Model_Import_Abstract::ZRODLO_INTERNET;
                                break;
                            case Application_Model_Import_Abstract::ZRODLO_PLIK:
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK]['nazwa'] = 'Plik';
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK]['id'] = Application_Model_Import_Abstract::ZRODLO_PLIK;
                                break;
                            case Application_Model_Import_Abstract::ZRODLO_PLIK_XML:
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_XML]['nazwa'] = 'Plik XML';
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_XML]['id'] = Application_Model_Import_Abstract::ZRODLO_PLIK;
                                break;
                            case Application_Model_Import_Abstract::ZRODLO_PLIK_CSV:
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_CSV]['nazwa'] = 'Plik CSV';
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_CSV]['id'] = Application_Model_Import_Abstract::ZRODLO_PLIK;
                                break;
                            case Application_Model_Import_Abstract::ZRODLO_PLIK_ZIP:
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_ZIP]['nazwa'] = 'Plik ZIP';
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_ZIP]['id'] = Application_Model_Import_Abstract::ZRODLO_PLIK;
                                break;
                            case Application_Model_Import_Abstract::ZRODLO_PLIK_XLS:
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_XLS]['nazwa'] = 'Plik Excel';
                                $opcje_js["zak".$zakres_id][Application_Model_Import_Abstract::ZRODLO_PLIK_XLS]['id'] = Application_Model_Import_Abstract::ZRODLO_PLIK;
                                break;
                        }
                    }
                }

                $this->view->opcje = json_encode($opcje_js);

                $this->view->zakres_element = $zakres;

                $this->view->zrodlo_element = $zrodlo;

            }
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }
    }

}
