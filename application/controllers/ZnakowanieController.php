<?php

class ZnakowanieController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = true;
    }

    public function indexAction() {
        $this->view->headScript()->appendFile($this->view->baseURL('/js/znakowanieIndex.js'));
        $this->view->lang = $this->_lang;
        $this->view->znakowania = $this->_entityManager->getRepository('Entities\Znakowanie')->getOrderByNazwa();
    }

    public function dodajAction() {
        $form = new Application_Form_Znakowanie();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $znakowanie = new Entities\Znakowanie;
                $znakowanie
                        ->setNazwa($_POST['nazwa'])
                        ->setKolory($_POST['kolory'])
                        ->setRyczalt(Atacama_Produkt_Kwota::zapis($_POST['ryczalt']))
                        ->setPrzygotowalnia(Atacama_Produkt_Kwota::zapis($_POST['przygotowalnia']))
                        ->setPowtorzenie(Atacama_Produkt_Kwota::zapis($_POST['powtorzenie']))
                        ->setKod($_POST['kod']);

                $this->_entityManager->persist($znakowanie);
                $this->_entityManager->flush();

                $znakowanie_i18n = new Entities\Znakowanie_i18n();
                $znakowanie_i18n->setJezyk($this->_lang)
                        ->setNazwa($_POST['nazwa'])
                        ->setZnakowaniaId($znakowanie->getId())
                        ->setZnakowania($znakowanie);

                $this->_entityManager->persist($znakowanie_i18n);
                $this->_entityManager->flush();

                $this->addMessageSuccess('Nowa metoda [' . $_POST['nazwa'] . '] znakowania dodana', TRUE);
                $this->_redirect('/' . $this->_lang . '/Znakowanie');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }

        $this->view->form = $form;
    }

    public function zmienAction() {
        $id = (int) $this->_request->getParam('id');

        if ($id < 1) {
            $this->addMessageError('Brak identyfikatora', TRUE);
            $this->_redirect('/' . $this->_lang . '/Znakowanie/');
        }

        $znakowanie = $this->_entityManager->getRepository('Entities\Znakowanie')->getById($id);

        if (null === $znakowanie) {
            $this->addMessageError('Nie ma takiej metody znakowania', TRUE);
            $this->_redirect('/' . $this->_lang . '/Znakowanie/');
        }

        $form = new Application_Form_Znakowanie();


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $this->_entityManager->getRepository('Entities\Znakowanie')->zmien($_POST);

                $this->addMessageSuccess('Metoda znakowania zmieniona pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Znakowanie/');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {
            //$znakowanie = new Entities\Znakowanie;
            $form->populate(array(
                'id' => $znakowanie->getId(),
                'kod' => $znakowanie->getKod(),
                'kolory' => $znakowanie->getKolory(),
                'nazwa' => $znakowanie->getNazwa(),
                'ryczalt' => $znakowanie->getRyczalt(),
                'przygotowalnia' => $znakowanie->getPrzygotowalnia(),
                'powtorzenie' => $znakowanie->getPowtorzenie(),
            ));
        }

        $this->view->form = $form;
    }

    public function tlumaczeniaAction() {
        $id = (int) $this->_request->getParam('id');

        if ($id < 1) {
            $this->addMessageError('Brak identyfikatora', TRUE);
            $this->_redirect('/' . $this->_lang . '/Znakowanie/');
        }

        $znakowanie = $this->_entityManager->getRepository('Entities\Znakowanie')->getById($id);

        if (null === $znakowanie) {
            $this->addMessageError('Nie ma takiej metody znakowania', TRUE);
            $this->_redirect('/' . $this->_lang . '/Znakowanie/');
        }

        $form = new Application_Form_ZnakowanieNazwa();


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    $cmd = new Application_Model_Commands_ZnakowanieNazwy($this->_entityManager, $_POST);
                    $cmd->execute();
                    $this->_redirect('/' . $this->_lang . '/Znakowanie/');

                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {
            //$znakowanie = new Entities\Znakowanie;

            $nazwyTablica = array('id' => $znakowanie->getId());
            foreach ($znakowanie->getZnakowaniaI18n() as $i18n) {
                $nazwyTablica[$i18n->getJezyk()] = array(
                    'nazwa_' . $i18n->getJezyk() => $i18n->getNazwa()
                );
            }
            $form->populate($nazwyTablica);
        }

        $this->view->form = $form;
    }

    public function rodzajeAction() {
        $id = (int) $this->_request->getParam('id');

        if ($id < 1) {
            $this->addMessageError('Brak identyfikatora', TRUE);
            $this->_redirect('/' . $this->_lang . '/Znakowanie/');
        }

        $znakowanie = $this->_entityManager->getRepository('Entities\Znakowanie')->getById($id);

        if (null === $znakowanie) {
            $this->addMessageError('Nie ma takiej metody znakowania', TRUE);
            $this->_redirect('/' . $this->_lang . '/Znakowanie/');
        }

        $this->view->znakowanieId = $znakowanie->getId();

        $this->view->headScript()->appendFile($this->view->baseURL('/js/znakowanieRodzaje.js'));

        $this->view->znakowanieNazwa = $znakowanie->getZnakowaniaI18n()->first()->getNazwa();

        $this->view->rodzaje = $znakowanie->getZnakowaniaRodzaje();


        if ($this->getRequest()->isPost()) {
            try {
                $cmd = new Application_Model_Commands_ZnakowanieRodzaje($this->_entityManager, $_POST);
                $cmd->execute();

                $this->addMessageSuccess('Rodzaje znakowania: <b>' . $this->view->znakowanieNazwa . '</b> zmienione pomyślnie.', TRUE);
                $this->_redirect('/' . $this->_lang . '/Znakowanie');
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
    }

}
