<?php

class AdministracjaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_OGOLNA)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = true;
    }

    public function indexAction() {
        $this->view->acl = $this->_acl;
        $this->view->lang = $this->_lang;
        $this->view->doUzupelnienia = $this->_entityManager->getRepository('Entities\Produkt')->produktyDoUzupelnieniaIlosc();
        $this->view->bezKategoriiGlownej = $this->_entityManager->getRepository('Entities\Produkt')->produktyBezKategoriiGlownejIlosc();
        $this->view->nieprzeczytychPowiadomien = $this->_entityManager->getRepository('Entities\Powiadomienie')->getIloscNieprzeczytanych();

        $this->view->adminView = FALSE;
    }

    public function koloryAction() {
        $this->view->kolory = $this->_entityManager->getRepository('Entities\Kolor')->getOrderByNazwa();
    }

    public function kolorZmienAction() {
        $kolorId = (int) $this->_request->getParam('id');

        if ($kolorId < 1) {
            $this->_redirect('/' . $this->_lang . '/Administracja/kolory');
        }

        $kolor = $this->_entityManager->getRepository('Entities\Kolor')->getById($kolorId);

        if (null === $kolor) {
            $this->_redirect('/' . $this->_lang . '/Administracja/kolory');
        }

        $form = new Application_Form_Kolor($this->_entityManager);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $kolor = $this->_entityManager->getRepository('Entities\Kolor')->zmien($_POST);

                $this->_redirect('/' . $this->_lang . '/Administracja/kolory');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {
            $form->populate(array(
                'id' => $kolor->getid(),
                'nazwa' => $kolor->getNazwa(),
                'kod' => $kolor->getKod(),
                'grupa_id' => $kolor->getGrupyId()
            ));
        }

        $this->view->form = $form;
    }

    public function kolorDodajAction() {
        $form = new Application_Form_Kolor($this->_entityManager);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                $kolor = new Entities\Kolor();
                $kolor->setKod($_POST['kod'])
                        ->setNazwa($_POST['nazwa']);

                $this->_entityManager->persist($kolor);
                $this->_entityManager->flush();

                $this->addMessageSuccess('Nowy kolor został dodany pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Administracja/kolory');
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

}
