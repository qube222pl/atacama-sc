<?php

class HasloController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    /**
     * Przypomnij haslo
     */
    public function przypomnijAction() {
        $form = new Application_Form_HasloPrzypomnij();

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($_POST)) {

                $cmd = new Application_Model_Commands_UzytkownikPrzypomnijEmail($this->_entityManager, $_POST);
                $cmd->execute();

                if ($cmd->isError()) {
                    if ($cmd->getMessage() === Application_Model_Commands_UzytkownikPrzypomnijEmail::BRAK_UZYTKOWNIKA) {
                        $this->addMessageInfo($this->view->translate('brak uzytkownika w systemie'), TRUE);
                    }
                } else {
                    $this->addMessageInfo($this->view->translate('pomyslnie wyslany e-mail z przypomnieniem hasla'), TRUE);
                }
                $this->_redirect('/' . $this->_lang . '/index');
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }

        $this->view->form = $form;
    }

    /**
     * Zmiana hasla przeslanego z maila
     */
    public function zmienAction() {
        $paramA = (int) $this->getRequest()->getParam('a');
        $paramB = $this->getRequest()->getParam('b');

        if (isset($paramA) && $paramA > 0 && isset($paramB)) {
            $form = new Application_Form_HasloZmien(Application_Form_HasloZmien::FORM_PRZYPOMNIENIE);

            $form->getElement('kod')->setValue($paramB);
            $form->getElement('id')->setValue($paramA);

            $this->view->form = $form;


            if ($this->getRequest()->isPost()) {

                if ($form->isValid($_POST)) {

                    $cmd = new Application_Model_Commands_UzytkownikZmienHaslo($this->_entityManager, $_POST);
                    $cmd->execute();

                    if ($cmd->isError() == FALSE) {
                        $this->addMessageInfo($this->view->translate('haslo zostalo zmienione mozna sie zalogowac'), TRUE);
                    }

                    if ($cmd->isError()) {
                        if ($cmd->getMessage() == Application_Model_Commands_UzytkownikZmienHaslo::HASLA_NIEZGODNE)
                            $this->addMessageError($this->view->translate('parametry hasel sie nie zgadzaja'), TRUE);
                    }
                    $this->_redirect('/' . $this->_lang . '/uzytkownik/logowanie');
                }

                if ($form->isErrors()) {
                    $form->populate($_POST);
                }
            }

            $this->view->form = $form;
        } else {
            $this->addMessageError('brak parametrów', TRUE);
            $this->_redirect('/' . $this->_lang . '/uzytkownik/logowanie');
        }
    }

}
