<?php

class StronaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    /**
     * Jedyny widok ogolny. Pozostale sa dla uprawnionej osoby
     */
    public function podgladAction() {
        $kod = $this->getRequest()->getParam('kod');
        $strona = $this->_entityManager->getRepository('Entities\Strona')->pobierzPoKodzie($kod, $this->_lang);

        if ($strona instanceof Entities\Strona) {
            $this->view->tresc = Moyoki_Cms_Parser::init($this->_entityManager, $strona->getTresc());
            $this->view->headTitle($strona->getTytul());
            $this->view->title = $strona->getTytul();
        } else {
            $this->view->e404 = TRUE;
        }
    }

    public function indexAction() {

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_STRONY)) {
            $this->addMessageError($this->view->translate("odmowa dostepu"), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
        $kod = $this->_request->getParam('kod');
        $lang = $this->_request->getParam('lang');

        if (isset($kod) && isset($lang) && strlen($lang) == 2) {
            $strona = $this->_entityManager->getRepository('Entities\Strona')->pobierzPoKodzie($kod, $lang);

            //$strona = new Entities\Strona();
            if ($strona instanceof Entities\Strona) {
                //$this->view->content = $strona->getTresc();
                $this->view->content = Moyoki_Cms_Parser::init($this->_entityManager, $strona->getTresc());
                $this->view->headTitle($strona->getTytul());
                $this->view->tytul = $strona->getTytul();
            }
        }
    }

    public function wszystkieAction() {

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_STRONY)) {
            $this->addMessageError($this->view->translate("odmowa dostepu"), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
        $strony = $this->_entityManager->getRepository('Entities\Strona')->pobierzWszystkie($this->_lang);

        $this->view->strony = $strony;

        $this->view->lang = $this->_lang;
    }

    public function zmienAction() {

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_STRONY)) {
            $this->addMessageError($this->view->translate("odmowa dostepu"), TRUE);
            $this->_redirect('/' . $this->_lang . '/Strona/wszystkie/');
        }
        $kod = $this->_request->getParam('kod');
        $lang = $this->_request->getParam('lang');

        $form = new Application_Form_Strona();
        $this->view->form = $form;
        if (isset($kod) && isset($lang) && strlen($lang) == 2) {
            $stronaObj = $this->_entityManager->getRepository('Entities\Strona')->pobierzPoKodzie($kod, $lang);

            if (is_object($stronaObj) && $stronaObj instanceof Entities\Strona) {
                $form->populate(array('kod' => $stronaObj->getKod(),
                    'lang' => $lang,
                    'slowa_kluczowe' => '',
                    'opis' => $stronaObj->getOpis(),
                    'tytul' => $stronaObj->getTytul(),
                    'tresc' => $stronaObj->getTresc()
                ));
            } else {
                $form->populate(array('kod' => $kod,
                    'lang' => $lang
                ));
            }


            if ($this->getRequest()->isPost()) {
                if ($form->isValid($_POST)) {
                    $cmd = new Application_Model_Commands_StronaZmien($this->_entityManager, $_POST);
                    $result = $cmd->execute();
                    $this->addMessageSuccess('Strona zmieniona pomyślnie', TRUE);
                    $this->_redirect('/' . $this->_lang . '/strona/index/kod/'.$kod.'/lang/'.$lang);
                }

                if ($form->isErrors()) {
                    $form->populate($_POST);
                }
            }
        } else {
            $this->addMessageError('Niepoprawne parametry strony', TRUE);
            $this->_redirect('/' . $this->_lang . '/Strona/wszystkie/');
        }
    }

}
