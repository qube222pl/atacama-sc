<?php

class AjaxController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    public function produktzdjecieAction() {
        $this->_helper->layout()->disableLayout();
        $this->render('index');

        if (!empty($_FILES['produktzdjecie'])) {
            try {
                $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($_POST['produkt_id']);

                if ($produkt instanceof Entities\Produkt) {

                    foreach ($produkt->getProduktyI18n() as $i18n) {
                        if ($i18n->getJezyk() == 'pl') {
                            $nazwaProduktu = $i18n->getNazwa();
                        }
                    }
                    $temp_file = $_FILES['produktzdjecie']['tmp_name'];

                    $config = Atacama_Config::getInstance();

                    $path = realpath($config->zdjecia->path) . '/' . $produkt->getDystrybutorzyId();
                    $file = $_FILES['produktzdjecie']['name'];
                    $basenameAndExtension = explode('.', $file);
                    $ext = end($basenameAndExtension);

                    if (!file_exists($path) and ! is_dir($path)) {
                        mkdir($path, 0775, true);
                    }
                    $filename = strtolower(Moyoki_Friendly::name('atacama-' . $nazwaProduktu) . '-' . $produkt->getId() . date('dzs') . rand(0, 99) . '.' . $ext);

                    if (move_uploaded_file($temp_file, $path . '/' . $filename)) {
                        $kolor_id = (int) $_POST['kolor'];

                        chmod($path . '/' . $filename, 0777);
                        $produktZdjecie = new Entities\Produkt_Zdjecie();
                        $produktZdjecie->setProduktyId($produkt->getId())
                                ->setProdukty($produkt)
                                ->setPlik($produkt->getDystrybutorzyId() . '/' . $filename)
                                ->setWidoczne(1)
                                ->setNazwaZmieniona(1);

                        if ($kolor_id > 0) {
                            $kolor = $this->_entityManager->getRepository('Entities\Kolor')->getById($kolor_id);
                            $produktZdjecie->setKolory($kolor)
                                    ->setKoloryId($kolor_id);
                        }


                        $this->_entityManager->persist($produktZdjecie);
                        $this->_entityManager->flush();

                        echo $produktZdjecie->getId();
                    }
                } else {
                    throw new Exception('Brak produktu');
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function produktpobierzzdjecieAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
            $this->render('index');

            $id = (int) $this->getRequest()->getParam('id');
            if ($id == 0)
                return;

            $zdjeciaTab = $this->_entityManager->getRepository('Entities\Produkt_Zdjecie')->findById($id);

            if (count($zdjeciaTab) == 0)
                return;

            $zdjecie = $zdjeciaTab[0];
            $config = Atacama_Config::getInstance();
            $sWidth = $config->zdjecia->rozmiary->m->w;
            $sHeight = $config->zdjecia->rozmiary->m->h;
            try {
                if ($zdjecie instanceof Entities\Produkt_Zdjecie && strlen($zdjecie->getPlik()) > 4) {
                    $foto = new Moyoki_File($config->zdjecia->path . '/' . $zdjecie->getPlik());
                    $thumbSrc = $this->view->baseUrl($config->zdjecia->urlbase . $zdjecie->getProdukty()->getDystrybutorzyId() . '/' . $foto->getThumbnailFile($sWidth, $sHeight));
                } else {
                    $thumbSrc = $config->resources->frontController->baseUrl . 'images/nophoto_' . $sWidth . 'x' . $sHeight . '.png';
                }
            } catch (Exception $e) {
                $thumbSrc = $config->resources->frontController->baseUrl . 'images/nophoto_' . $sWidth . 'x' . $sHeight . '.png';
            }

            echo $this->view->miniaturkaAdmin($thumbSrc, $zdjecie);
        }
    }

    public function galeriazdjecieAction() {

        $this->_helper->layout()->disableLayout();
        $this->render('index');

        if (!empty($_FILES['galeriazdjecie'])) {
            try {
                $galeria = $this->_entityManager->getRepository('Entities\Galeria')->getById($_POST['galeria_id']);

                $temp_file = $_FILES['galeriazdjecie']['tmp_name'];

                $config = Atacama_Config::getInstance();

                $path = realpath($config->images->path);
                $file = $_FILES['galeriazdjecie']['name'];
                $basenameAndExtension = explode('.', $file);
                $ext = end($basenameAndExtension);
                $filename = strtolower('gal-' . $galeria->getId() . '-' . date('ydm') . rand() . '.' . $ext);


                if (move_uploaded_file($temp_file, $path . '/' . $filename)) {

                    chmod($path . '/' . $filename, 0777);
                    $galZdjecie = new Entities\Galeria_Zdjecie();
                    $galZdjecie->setGalerieId($galeria->getId())
                            ->setGalerie($galeria)
                            ->setPlik($filename);

                    $this->_entityManager->persist($galZdjecie);
                    $this->_entityManager->flush();

                    echo $galZdjecie->getId();
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * 
     * Robi upload
     */
    public function galeriapobierzzdjecieAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
            $this->render('index');

            $id = (int) $this->getRequest()->getParam('id');
            if ($id == 0)
                return;

            $zdjecie = $this->_entityManager->getRepository('Entities\Galeria_Zdjecie')->getById($id);

            if ($zdjecie instanceof Entities\Galeria_Zdjecie) {

                $config = Atacama_Config::getInstance();
                $sWidth = $config->zdjecia->rozmiary->m->w;
                $sHeight = $config->zdjecia->rozmiary->m->h;
                try {
                    if ($zdjecie instanceof \Entities\Galeria_Zdjecie && strlen($zdjecie->getPlik()) > 4) {
                        $foto = new Moyoki_File($config->images->path . $zdjecie->getPlik());
                        $thumbSrc = $config->images->urlbase . $foto->getThumbnailFile($sWidth, $sHeight);
                    } else {
                        $thumbSrc = $config->resources->frontController->baseUrl . 'images/nophoto_' . $sWidth . 'x' . $sHeight . '.png';
                    }
                } catch (Exception $e) {
                    $thumbSrc = $config->resources->frontController->baseUrl . 'images/nophoto_' . $sWidth . 'x' . $sHeight . '.png';
                }

                echo $this->view->miniaturkaGaleria($thumbSrc, $zdjecie);
            } else
                return;
        }
    }

    public function usunzdjecieAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();

            return false;
        } else {
            $this->_helper->layout->disableLayout();
            $this->render('index');
            $zdjecie_id = (int) $this->getRequest()->getParam('zdjecie_id');
            $zdjeciaTab = $this->_entityManager->getRepository('Entities\Produkt_Zdjecie')->findByid($zdjecie_id);

            if (count($zdjeciaTab) > 0) {
                try {
                    $this->_entityManager->getRepository('Entities\Produkt_Zdjecie')->ustawNiewidoczne($zdjecie_id);
                    echo 'Zdjęcie usunięte';
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo '<div class="alert alert-danger">Wystąpił błąd</div>';
            }
        }
    }

    public function galeriausunzdjecieAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();

            return false;
        } else {
            $this->_helper->layout->disableLayout();
            $this->render('index');

            $zdjecie_id = (int) $this->getRequest()->getParam('zdjecie_id');
            $zdjecie = $this->_entityManager->getRepository('Entities\Galeria_Zdjecie')->getByid($zdjecie_id);

            if ($zdjecie instanceof Entities\Galeria_Zdjecie) {
                try {
                    $zdjecie->kasujPlik();
                    $this->_entityManager->remove($zdjecie);
                    $this->_entityManager->flush();
                    echo '<div class="alert alert-success">Zdjęcie usunięte</div>';
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo '<div class="alert alert-danger">Wystąpił błąd</div>';
            }
        }
    }

    public function produktdodajdetalAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();

            return false;
        } else {
            $this->_helper->layout->disableLayout();
            $this->render('index');

            $ID = date('i') . rand(1, 99);

            $kolor = new Zend_Form_Element_Select('a' . $ID . '_kolor');
            $kolor->setAttrib('class', 'form-control md');
            $kolory = $this->_entityManager->getRepository('Entities\Kolor')->getOrderByNazwa();
            foreach ($kolory as $k) {
                $kolor->addMultiOption($k->getId(), $k->getNazwa());
            }

            $rozmiar = new Zend_Form_Element_Select('a' . $ID . '_rozmiar');
            $rozmiar->setAttrib('class', 'form-control md');
            foreach (Atacama_Produkt_Rozmiar::tablica() as $id => $nazwa) {
                $rozmiar->addMultiOption($id, $nazwa);
            }
            $kod = new Zend_Form_Element_Text('a' . $ID . '_kod');
            $kod->setAttrib('class', 'form-control xs')
                    ->setAttrib('readonly', 'readonly');

            $cena = new Zend_Form_Element_Text('a' . $ID . '_cena');
            $cena->setAttrib('class', 'form-control xs text-right');

            $cena = new Zend_Form_Element_Text('a' . $ID . '_cena');
            $cena->setAttrib('class', 'form-control xs text-right');

            $vat = new Zend_Form_Element_Text('a' . $ID . '_vat');
            $vat->setAttrib('class', 'form-control md text-right');

            $promo = new Zend_Form_Element_Text('a' . $ID . '_promo');
            $promo->setAttrib('class', 'form-control md text-right');

            $wyp = new Zend_Form_Element_Text('a' . $ID . '_wyprz');
            $wyp->setAttrib('class', 'form-control md text-right');
            $magazyn1 = new Zend_Form_Element_Text('a' . $ID . '_magazyn1');
            $magazyn1->setAttrib('class', 'form-control md text-right');

            $magazyn2 = new Zend_Form_Element_Text('a' . $ID . '_magazyn2');
            $magazyn2->setAttrib('class', 'form-control md text-right');

            $widoczny = new Zend_Form_Element_Checkbox('a' . $ID . '_widoczny');
            $widoczny->setAttrib('class', 'form-control md text-right');

            echo '<tr>
            <td>' . $kolor . '</td>
            <td>' . $rozmiar . '</td>
            <td>' . $kod . '</td>
            <td>' . $cena . '</td>
            <td>' . $vat . '</td>
            <td>' . $promo . '</td>
            <td>' . $wyp . '</td>
            <td>' . $magazyn1 . '</td>
            <td>' . $magazyn2 . '</td>
            <td>' . $widoczny . '</td>
            </tr>';
        }
    }

    public function znakowanierodzajdodajAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();

            return false;
        } else {
            $this->_helper->layout->disableLayout();
            $this->render('index');

            $ID = date('i') . rand(1, 99);

            $cena = new Zend_Form_Element_Text('a' . $ID . '_cena');
            $cena->setAttrib('class', 'form-control xs text-right');
            $cena_inny = new Zend_Form_Element_Text('a' . $ID . '_cenainny');
            $cena_inny->setAttrib('class', 'form-control xs text-right');

            $min = new Zend_Form_Element_Text('a' . $ID . '_minimum');
            $min->setAttrib('class', 'form-control md text-right');

            $max = new Zend_Form_Element_Text('a' . $ID . '_maximum');
            $max->setAttrib('class', 'form-control md text-right');

            echo '<tr><td>' . $cena . '</td>
            
            <td>' . $cena_inny . '</td>
            <td>' . $min . '</td>
            <td>' . $max . '</td>
        </tr>';
        }
    }

    public function kategoriecenydodajAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();

            return false;
        } else {
            $this->_helper->layout->disableLayout();
            $this->render('index');

            $ID = date('i') . rand(1, 99);

            $cena = new Zend_Form_Element_Text('a' . $ID . '_regularna');
            $cena->setAttrib('class', 'form-control xs text-right');
            $cena_inny = new Zend_Form_Element_Text('a' . $ID . '_promocyjna');
            $cena_inny->setAttrib('class', 'form-control xs text-right');

            $min = new Zend_Form_Element_Text('a' . $ID . '_minimum');
            $min->setAttrib('class', 'form-control md text-right');

            $max = new Zend_Form_Element_Text('a' . $ID . '_maximum');
            $max->setAttrib('class', 'form-control md text-right');

            echo '<tr><td>' . $cena . '</td>
            
            <td>' . $cena_inny . '</td>
            <td>' . $min . '</td>
            <td>' . $max . '</td>
        </tr>';
        }
    }

    public function szukajAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $fraza = $this->_getParam('szukane');
            $result = $this->_entityManager->getRepository('Entities\Produkt')->szukane($fraza, $this->_lang);
            $prod_array = array();
            foreach ($result as $produkt) {
                foreach ($produkt->getProduktyI18n() as $pi18n) {
                    if ($pi18n->getJezyk() == $this->_lang)
                        $prod_array[$produkt->getId()] = $pi18n->getNazwa();
                }
            }
            $this->_helper->json(array_values($prod_array));
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }

        $this->render('index');
    }

    public function doKoszykaAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();
            $this->lang = $this->_lang;

            $kolor_id = (int) $this->getRequest()->getParam('kolorId');
            $produkt_id = (int) $this->getRequest()->getParam('produktId');
            $kategoria_id = (int) $this->getRequest()->getParam('kategoriaId');

            if ($kolor_id > 0 && $produkt_id > 0) {


                $this->view->em = $this->_entityManager;
                $this->view->produkt_id = $produkt_id;
                $this->view->kolor_id = $kolor_id;
                $this->view->kategoria_id = $kategoria_id;

                /**
                 * Detale produkty
                 */
                $detale = $this->_entityManager->getRepository('Entities\Produkt_Detal')->getByProduktIdKolorId($produkt_id, $kolor_id);

                $detale_tablica = array();
                foreach ($detale as $detal) {
                    if ($detal instanceof Entities\Produkt_Detal) {
                        $detale_tablica[] = array(
                        'id' => $detal->getId(),
                        'cena' => $detal->getCena(),
                        'kolor' => $detal->getKolory()->getNazwa(),
                        'vat' => $detal->getVat(),
                        'promocja' => $detal->getPromo(),
                        'wyprzedaz' => $detal->getWyp(),
                        'rozmiar' => $detal->getRozmiar(),
                        'magazyny' => $detal->getStanMagazynowy1() + $detal->getStanMagazynowy2(),
                        'detalObj' => $detal
                        );
                    }
                }

                $this->view->detale = $detale_tablica;
            }
            if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
                $this->view->zas_koszyk = TRUE;
            }
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }
    }

    public function doKoszykaZnakowaniaAction() {

        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $produkt_id = (int) $this->getRequest()->getParam('produktId');
            $ilosc = (int) $this->getRequest()->getParam('ilosc');

            if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK) && $produkt_id > 0) {

                /**
                 * Metody znakowania dla produktu
                 */
                $znakowania = $this->_entityManager->getRepository('Entities\Produkt_Has_Znakowanie')->getByProduktId($produkt_id);

                $znakowanie_tab = array();
                foreach ($znakowania as $znak) {
                    // Konkretny rodzaj zależny od ilości
                    $znak_rodzaj = $this->_entityManager->getRepository('Entities\Znakowanie_Rodzaj')->getByZnakowanieIdAndIlosc($znak->getZnakowanie()->getId(), $ilosc);

                    if (null == $znak_rodzaj) {
                        $cena_pierwszy = '-';
                        $cena_inny = '-';
                    } else {
                        $cena_pierwszy = $znak_rodzaj->getCena();
                        $cena_inny = $znak_rodzaj->getCenaInny();
                    }
                    $znakowanie_tab[] = array(
                        'id' => $znak->getZnakowanie()->getId(),
                        'kod' => $znak->getZnakowanie()->getKod(),
                        'nazwa' => $znak->getZnakowanie()->getZnakowaniaI18n()->first()->getNazwa(),
                        'kolory' => (int) $znak->getZnakowanie()->getKolory(),
                        'ryczalt' => $znak->getZnakowanie()->getRyczalt(),
                        'przygotowalnia' => $znak->getZnakowanie()->getPrzygotowalnia(),
                        'cena_pierwszy' => $cena_pierwszy,
                        'cena_inny' => $cena_inny
                    );
                }
                $this->view->znakowania = $znakowanie_tab;
            } else {
                $this->view->odmowa = TRUE;
                echo 'Brak uprawnień';
            }
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }
    }

    public function doKoszykaProduktAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $koszyk = $this->_entityManager->getRepository('Entities\Koszyk')->getKoszykUzytkownika($this->_user['id']);

            if (!$koszyk instanceof Entities\Koszyk) {
                //Nie ma koszyka - trzeba go stworzyc

                $uzytkownikObj = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($this->_user['id']);
                if ($uzytkownikObj instanceof Entities\Uzytkownik) {
                    $koszyk = new Entities\Koszyk();
                    $koszyk->setUzytkownicy($uzytkownikObj)
                            ->setUzytkownicyId($uzytkownikObj->getId())
                            ->setFirmyId($uzytkownikObj->getFirmyId())
                            ->setFirmy($uzytkownikObj->getFirmy())
                            ->setStatus('0'); // koszyk

                    $this->_entityManager->persist($koszyk);
                    $this->_entityManager->flush();
                }
            }

            /**
             * Nie ma produktu w koszyku
             */
            $prodDetal = $this->_entityManager->getRepository('Entities\Produkt_Detal')->getById($_POST['produkt_detal_id']);

            if ($_POST['cena1szt'] == 0 || $_POST['cena1szt'] == 'NaN') {
                /*
                 * Darmowych produkt - nie dodajemy
                 */
                $result = array(
                    'Kolor' => $prodDetal->getKolory()->getNazwa(),
                    'nr kat' => $prodDetal->getProdukty()->getNrKatalogowy(),
                    'prod_id' => $prodDetal->getProduktyId()
                );
                Atacama_Powiadomienie::dodaj($this->_entityManager, Atacama_Powiadomienie::PROBLEM_Z_DODANIEM_DO_KOSZYKA, json_encode($result));
                echo 'error';
            } else if ($prodDetal instanceof Entities\Produkt_Detal) {

                $przelicz = new Atacama_Produkt_Cenaklienta($this->_entityManager, $prodDetal, $_POST['kategoria'], $_POST['ilosc']);
                $ceny = $przelicz->wylicz();

                $kategoria = $this->_entityManager->getRepository('Entities\Kategoria')->getById($prodDetal->getProdukty()->getKategoriaGlowna());

                $koszyk_produkt = new Entities\Koszyk_Produkt();

                $koszyk_produkt->setKoszyki($koszyk)
                        ->setKoszykiId($koszyk->getId())
                        ->setIlosc($_POST['ilosc'])
                        ->setCenaZakupu($_POST['cena1szt'])
                        ->setCenaSprzedazy($prodDetal->getCena())
                        ->setProduktyDetale($prodDetal)
                        ->setCenaKonfekcjonowanie($kategoria->getKonfekcjonowanie())
                        ->setProduktyDetaleId($prodDetal->getId());

                $this->_entityManager->persist($koszyk_produkt);
                $this->_entityManager->flush();
            }
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }

        $this->render('index');
    }

    public function przeliczProduktDetalAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $kolor_id = (int) $this->getRequest()->getParam('kolor_id');
            $produkt_id = (int) $this->getRequest()->getParam('produkt_id');
            $kategoria_id = (int) $this->getRequest()->getParam('kategoria');
            $rozmiar_id = (int) $this->getRequest()->getParam('rozmiar');
            $ilosc = (int) $this->getRequest()->getParam('ilosc');

            $produkt_detal = $this->_entityManager->getRepository('Entities\Produkt_Detal')->getByProduktIdKolorIdRozmiarId($produkt_id, $kolor_id, $rozmiar_id);
            $return = array();

            if (!$produkt_detal instanceof Entities\Produkt_Detal) {
                $return = 'error';
            } else {
                $przelicz = new Atacama_Produkt_Cenaklienta($this->_entityManager, $produkt_detal, $kategoria_id, $ilosc);
                $return = $przelicz->wylicz();

                if ($return != 'kategoria lub firma error') {
                    $return['detal_id'] = $produkt_detal->getId();
                } else {
                    
                }
                echo json_encode($return);
            }
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }

        $this->render('index');
    }

    /**
     * Akcja do zmiany koloru dla zdjecia przypisanego do produktu
     * @return string kolor/brak
     */
    public function produktzdjeciekolorAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $produkty_zdjecia_id = (int) $this->getRequest()->getParam('produkty_zdjecia_id');
            $kolor = (int) $this->getRequest()->getParam('nowykolor');
            if ($kolor == 0) {
                $kolor = NULL;
            }
            $zdjecie = $this->_entityManager->getRepository('Entities\Produkt_Zdjecie')->zmienKolor($produkty_zdjecia_id, $kolor);

            if ($zdjecie) {
                if ($kolor) {
                    $kolorObj = $this->_entityManager->getRepository('Entities\Kolor')->getById($kolor);
                    if ($kolorObj instanceof Entities\Kolor) {
                        echo $kolorObj->getNazwa() . ' [' . $kolorObj->getKod() . ']';
                    }
                } else {
                    echo 'Zdjęcie detal';
                }
            } else {
                echo 'error';
            }
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }

        $this->render('index');
    }

    /**
     * Podglad produktu w niewidocznych
     * @return string kolor/brak
     */
    public function niewidoczneProduktPodgladAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $produkt_id = (int) $this->getRequest()->getParam('id');
            $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produkt_id);
            $this->view->produkt = $produkt;
            $this->view->lang = $this->_lang;
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }
    }

    /**
     * Podglad produktu w niewidocznych
     * @return string kolor/brak
     */
    public function niewidoczneProduktZmienAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();

            $cmd = new Application_Model_Commands_ProduktNiewidocznyZmien($this->_entityManager, $this->getRequest()->getParam('id'), $this->getRequest()->getParam('status'));
            $cmd->execute();
            echo $cmd->msg;
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }

        $this->render('index');
    }

    /**
     * Akceptacja RODO
     */
    public function rodoAkceptacjaAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);

            $auth = Zend_Auth::getInstance()->getIdentity();
            $user = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($auth['id']);
            if ($user instanceof Entities\Uzytkownik) {
                $user->setDataRodo(new DateTime());

                $this->_entityManager->persist($user);
                $this->_entityManager->flush();
            }
        } else {
            $this->getResponse()->setHttpResponseCode(404)
                    ->sendHeaders();
            return false;
        }
    }

}
