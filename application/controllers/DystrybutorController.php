<?php

class DystrybutorController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = TRUE;
    }

    public function indexAction() {
        $this->view->lang = $this->_lang;
        $this->view->dystrybutorzy = $this->_entityManager->getRepository('Entities\Dystrybutor')->orderByNazwa();
    }

    public function dodajAction() {
        $form = new Application_Form_Dystrybutor($this->_entityManager, 'dodaj');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                $dystrybutor = new Entities\Dystrybutor();
                $dystrybutor->setNazwa($_POST['nazwa'])
                        ->setOpis($_POST['opis']);

                $this->_entityManager->persist($dystrybutor);
                $this->_entityManager->flush();

                $this->addMessageSuccess('Nowy dystrybutor został dodany pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Dystrybutor');
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function zmienAction() {
        $dystrybutorId = (int) $this->_request->getParam('id');

        if ($dystrybutorId < 1) {
            $this->_redirect('/' . $this->_lang . '/Producent');
        }

        $dystrybutor = $this->_entityManager->getRepository('Entities\Dystrybutor')->getById($dystrybutorId);
        
        if (null === $dystrybutor) {
            $this->_redirect('/' . $this->_lang . '/Dystrybutor');
        }

        $form = new Application_Form_Dystrybutor($this->_entityManager, 'zmien');

        
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $polecenie = new Application_Model_Commands_DystrybutorZmien($this->_entityManager, $_POST);
                $polecenie->execute();

                $this->addMessageSuccess('Informacje o dystrybutorze zostały pomyślnie zmienione', TRUE);
                $this->_redirect('/' . $this->_lang . '/Dystrybutor');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {
            $form->populate(array('id' => $dystrybutor->getId(),
            'nazwa' => $dystrybutor->getNazwa(),
            'opis' => $dystrybutor->getOpis()));
        }

        $this->view->form = $form;
    }

}
