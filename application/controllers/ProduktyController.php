<?php

class ProduktyController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    public function listaAction() {

        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->view->adminView = TRUE;
        }
        $this->view->lang = $this->_lang;
        $katId = (int) $this->getRequest()->getParam('kat');
        $strona = (int) $this->getRequest()->getParam('s');

        if ($katId < 1) {
            $this->_redirect('/' . $this->_lang . '/index');
        }

        $config = Atacama_Config::getInstance();

        $kategoriaObj = $this->_entityManager->getRepository('Entities\Kategoria')->getById($katId);
        $this->view->kat_id = $kategoriaObj->getId();
        $this->view->em = $this->_entityManager;
        foreach ($kategoriaObj->getKategorieI18n() as $i18n) {
            if ($i18n->getJezyk() == $this->_lang) {
                $this->view->kategoria = $i18n->getNazwa();
                $this->view->kategoriaOpis = $i18n->getOpis();
                $this->view->description = $i18n->getOpisSeo();
            }
        }

        $kategorieRodzice = $this->_entityManager->getRepository('Entities\Kategoria')->pobierzRodzicow($katId);

        $okruszki[] = array('id' => '0',
            'nazwa' => 'strona glowna');

        foreach ($kategorieRodzice as $rodzic) {

            /**
             * Rozwijanie lewego menu
             */
            if ($rodzic->getPoziom() == 0) {
                $this->view->rozwin = $rodzic->getId();
            }

            foreach ($rodzic->getKategorieI18n() as $i18n) {
                if ($i18n->getJezyk() == $this->_lang) {
                    $rodzicNazwa = $i18n->getNazwa();
                }
            }
            $okruszki[] = array('id' => $rodzic->getId(),
                'nazwa' => $rodzicNazwa);
        }
        $this->view->okruszki = $okruszki;
        $rodzic = $okruszki[count($okruszki) - 2];

        if ($rodzic['nazwa'] != 'strona glowna') {
            $this->view->kategoria = $rodzic['nazwa'] . ' - ' . $this->view->kategoria;
        }
        $kategorieDzieci = $this->_entityManager->getRepository('Entities\Kategoria')->pobierzDzieci($katId, $this->_lang);

        $kategorieArray[] = $katId;
        foreach ($kategorieDzieci as $kategDziecko) {
            $kategorieArray[] = $kategDziecko->getId();
        }

        $paginator = $this->_entityManager->getRepository('Entities\Produkt')->paginatorByKategoriaIdOrderByNazwa($kategorieArray, $this->_lang);

        $paginatorIter = $paginator->getIterator();

        $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

        $zend_paginator = new \Zend_Paginator($adapter);
        if (!isset($strona))
            $strona = 1;

        $iloscNaStronie = $config->paginator->default->itemsCountPerPage;
        $zend_paginator->setItemCountPerPage($iloscNaStronie)
                ->setCurrentPageNumber($strona);

        $this->view->lang = $this->_lang;
        $this->view->iter = $iloscNaStronie * ($strona - 1);

        $this->view->paginator = $zend_paginator;
    }

    public function grupaAction() {
        $this->view->lang = $this->_lang;
        $grupa = (int) $this->getRequest()->getParam('grupa');
        $grupa_nazwa = (int) $this->getRequest()->getParam('nazwa');
        $strona = (int) $this->getRequest()->getParam('s');

        if ($grupa < 1) {
            $this->_redirect('/' . $this->_lang . '/index');
        }

        $config = Atacama_Config::getInstance();

        $this->view->em = $this->_entityManager;

        switch ($grupa) {
            //Nowosci
            case '1': {
                    $this->view->kategoria = $this->view->translate('nowosc');
                    $this->view->kategoriaOpis = '';
                    $paginator = $this->_entityManager
                            ->getRepository('Entities\Produkt')
                            ->paginatorByGrupaOrderByNazwa('1', $this->_lang);
                }
                break;
            // Bestsellery
            case '2': {
                    $this->view->kategoria = $this->view->translate('bestsellery');
                    $this->view->kategoriaOpis = '';
                    $paginator = $this->_entityManager
                            ->getRepository('Entities\Produkt')
                            ->paginatorByGrupaOrderByNazwa('2', $this->_lang);
                }
                break;
            // Promocje
            case '3': {
                    $this->view->kategoria = $this->view->translate('promocje');
                    $this->view->kategoriaOpis = '';
                    $paginator = $this->_entityManager
                            ->getRepository('Entities\Produkt')
                            ->paginatorByGrupaOrderByNazwa('3', $this->_lang);
                }
                break;
            // Wyprzedaz
            case '4': {
                    $this->view->sell = TRUE;
                    $this->view->kategoria = $this->view->translate('wyprzedaz');
                    $this->view->kategoriaOpis = '';
                    $paginator = $this->_entityManager
                            ->getRepository('Entities\Produkt')
                            ->paginatorWyprzedazOrderByNazwa($this->_lang);
                }
                break;
        }

        $okruszki[] = array('id' => '0',
            'nazwa' => 'strona glowna');

        $this->view->okruszki = $okruszki;

        $paginatorIter = $paginator->getIterator();

        $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

        $zend_paginator = new \Zend_Paginator($adapter);
        if (!isset($strona))
            $strona = 1;

        $iloscNaStronie = $config->paginator->default->itemsCountPerPage;
        $zend_paginator->setItemCountPerPage($iloscNaStronie)
                ->setCurrentPageNumber($strona);

        $this->view->lang = $this->_lang;
        $this->view->iter = $iloscNaStronie * ($strona - 1);

        $this->view->paginator = $zend_paginator;

        $this->render('lista');
    }

    public function wynikSzukaniaAction() {

        $fraza = $this->getRequest()->getParam('szukane');
        $this->getRequest()->setParam('szukane', $fraza);
        $this->view->szukane = htmlentities($fraza, ENT_QUOTES);
        $this->view->em = $this->_entityManager;
        $strona = (int) $this->getRequest()->getParam('s');
        //Moyoki_Debug::debug($this->view->szukane);exit;
        $paginator = $this->_entityManager->getRepository('Entities\Produkt')->szukanePaginator(addslashes($fraza), $this->_lang);
        $paginatorIter = $paginator->getIterator();

        $this->view->iloscZnalezionych = count($paginator);

        if (count($paginator) == 0) {
            $this->addMessageError($this->view->translate('brak produktow'), false);
        } if (count($paginator) == 1) {
            foreach ($paginator as $p) {
                foreach ($p->getProduktyI18n() as $pi18n) {
                    if ($pi18n->getJezyk() == $this->_lang) {
                        $nazwaProduktu = Moyoki_Friendly::name($pi18n->getNazwa());
                    }
                }
                $this->_redirect('/' . $this->_lang . '/art/' . $p->getId() . '/' . $nazwaProduktu);
            }
        } else {

            $okruszki[] = array('id' => '0',
                'nazwa' => 'strona glowna');

            $okruszki[] = array('id' => '',
                'nazwa' => $this->view->translate('wynik szukania'));

            $this->view->okruszki = $okruszki;

            $this->view->kategoria = $this->view->translate('wynik szukani');

            $opis = '<dl class="parametry">
        <dt>' . $this->view->translate('szukane') . ': <strong>' . $this->view->szukane . '</strong></dt>'
                    . '<dd>' . $this->view->translate('znalezionych') . ':<b>' . $this->view->iloscZnalezionych . '</b></dd>
        <dt></dd>
    </dl>';
            $this->view->kategoriaOpis = $opis;


            $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

            $zend_paginator = new \Zend_Paginator($adapter);

            $config = Atacama_Config::getInstance();

            if (!isset($strona))
                $strona = 1;

            $iloscNaStronie = $config->paginator->default->itemsCountPerPage;
            $zend_paginator->setItemCountPerPage($iloscNaStronie)
                    ->setCurrentPageNumber($strona);

            $this->view->lang = $this->_lang;
            $this->view->iter = $iloscNaStronie * ($strona - 1);

            $this->view->paginator = $zend_paginator;

            $this->render('lista');
        }
    }

    public function podgladAction() {

        $this->view->headScript()
                //->appendFile($this->view->baseURL('js/minifyjs/produktPodglad-min.js'));
->appendFile($this->view->baseURL('js/produktPodglad.js'));


        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->view->adminView = TRUE;
        }

        $produktId = (int) $this->_request->getParam('id');
        $sell = $this->getRequest()->getParam('sell');

        if ($sell)
            $this->view->sell = TRUE;

        $this->view->produktId = $produktId;
        if ($produktId < 1) {
            $this->_redirect('/' . $this->_lang . '/Index');
        }

        $this->view->em = $this->_entityManager;
        $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produktId);

        if (!$produkt instanceof Entities\Produkt) {
            $this->addMessageInfo('brak produktu', TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
        //$produkt = new Entities\Produkt;

        /**
         * Usunietych nie widzi nikt
         */
        if ($produkt->getUsuniety() == 1) {
            $this->addMessageInfo('brak produktu', TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
        if ($produkt->getWidoczny() == 0 && !$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->addMessageInfo('brak produktu', TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
        /**
         * Pobieranie kategorii 0 poziomu do cen
         */
        if (is_numeric($produkt->getKategoriaGlowna()) && $produkt->getKategoriaGlowna() > 0) {
            $kategorieRodzice = $this->_entityManager->getRepository('Entities\Kategoria')->pobierzRodzicow($produkt->getKategoriaGlowna());

            $this->view->kategoriaId = $kategorieRodzice[0]->getId();

            /**
             * Okruszki
             */
            $okruszki[] = array('id' => '0',
                'nazwa' => 'strona glowna');

            foreach ($kategorieRodzice as $rodzic) {

                /**
                 * Rozwijanie lewego menu
                  if($rodzic->getPoziom() == 0) {
                  $this->view->rozwin = $rodzic->getId();
                  }
                 */
                foreach ($rodzic->getKategorieI18n() as $i18n) {
                    if ($i18n->getJezyk() == $this->_lang) {
                        $kategoriaRodzicNazwa = $i18n->getNazwa();
                    }
                }
                $okruszki[] = array('id' => $rodzic->getId(),
                    'nazwa' => $kategoriaRodzicNazwa);
            }
            $this->view->okruszki = $okruszki;
        }

        $this->view->znakowania = $this->_entityManager->getRepository('Entities\Produkt_Has_Znakowanie')->getByProduktId($produktId);

        /**
         * Kategorie w widoku dla Admina
         */
        $kategorie = $this->_entityManager->getRepository('Entities\Produkt_Has_Kategoria')->getByProduktId($produktId);
        $kategorieTabala = array();
        foreach ($kategorie as $k) {
            $kategorieTabala[] = $k->getKategorie()->getKategorieI18n()->first()->getNazwa();
        }
        $this->view->kategorieLista = $kategorieTabala;

        if (null === $produkt) {
            $this->_redirect('/' . $this->_lang . '/index');
        }

        $this->view->produkt = $produkt;
        $this->view->lang = $this->_lang;
    }

    public function zaawansowaneAction() {

        $this->view->headScript()
                ->appendFile($this->view->baseURL('/js/produktyZaawansowane.js'));

        $form = new Application_Form_Zaawansowane($this->_entityManager);
        $this->view->form = $form;

        if ($this->getRequest()->isGet()) {
            if ($form->isValid($_GET) && !empty($_GET)) {
                $this->view->formularzUkryj = TRUE;


                $strona = (int) $this->getRequest()->getParam('s');

                $cmd = new Application_Model_Commands_WyszukiwanieZaawansowane($this->_entityManager, $_GET);
                $paginator = $cmd->execute();

                if (isset($_POST['kolory']) && (int) $_POST['kolory'] > 0) {
                    $this->view->kolor_id = $_POST['kolory'];
                }

                $paginatorIter = $paginator->getIterator();

                $this->view->znalezionych = count($paginator);

                $tablica = $_GET;
                $tablica['ilosc'] = $this->view->znalezionych;
                Atacama_Log::dodaj($this->_entityManager, Atacama_log::WYSZUKIWANIE_ZAAWANSOWANE, json_encode($tablica));

                $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

                $zend_paginator = new \Zend_Paginator($adapter);

                $config = Atacama_Config::getInstance();

                if (!isset($strona))
                    $strona = 1;

                $iloscNaStronie = $config->paginator->default->itemsCountPerPage;
                $zend_paginator->setItemCountPerPage($iloscNaStronie)
                        ->setCurrentPageNumber($strona);

                $this->view->em = $this->_entityManager;
                $this->view->lang = $this->_lang;
                $this->view->iter = $iloscNaStronie * ($strona - 1);

                $this->view->paginator = $zend_paginator;
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

}
