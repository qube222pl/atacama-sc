<?php

class ZamowieniaSpecjalneController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }
    }

    public function indexAction() {
        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia');
        }
        $auth = Zend_Auth::getInstance()->getIdentity();
        $this->view->zamowienia = $this->_entityManager->getRepository('Entities\Korporacyjny_Koszyk')->getWszystkieDlaUzytkownika($auth['id']);
    }

    public function koszykAction() {
//$this->view->headScript()->appendFile($this->view->baseURL('/js/minifyjs/ZamowieniaSpecjalneKoszyk-min.js'));
        $this->view->headScript()->appendFile($this->view->baseURL('/js/ZamowieniaSpecjalneKoszyk.js'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        if (NULL != $this->getRequest()->getParam('firma')) {
            /**
             * Rola uzytkwonika do ustawiania odpowiednich dostepow
             */
            $this->view->rola = $auth['rola'];

            $korp_user = $this->_entityManager->getRepository('Entities\Korporacyjny_Uzytkownik')->getDlaUserIdIFirmaIdIAktywny($auth['id'], $this->getRequest()->getParam('firma'));
            if ($korp_user instanceof Entities\Korporacyjny_Uzytkownik) {
                $this->view->produkty = $this->_entityManager->getRepository('Entities\Korporacyjny_Produkt')->getWszystkieDlaFirmy($korp_user->getFirmyId());

                $budzet = new Atacama_Uzytkownik_Budzet($this->_entityManager, $korp_user);
                $this->view->budzetUzytkownika = $budzet->getDostepnyBudzetMiesieczny();
                $this->view->rola = $korp_user->getRola();
                $this->view->korp_user = $korp_user;
                if ($this->getRequest()->isPost()) {
                    $cmd = new Application_Model_Commands_KorporacyjnyKoszykZapisz($this->_entityManager, $this->getRequest()->getPost(), $korp_user);
                    $cmd->execute();

                    if (empty($cmd->errors)) {
                        foreach ($cmd->success as $content) {
                            $this->addMessageSuccess($content, TRUE);
                        }
                        $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/szczegoly/id/' . $cmd->zamowienie_id);
                    } else {
                        foreach ($cmd->errors as $error) {
                            $this->addMessageError($error['content'], FALSE);
                        }
                    }
                }
            } else {
                $this->addMessageInfo('Nie posiadasz cennika w podanej firmie', TRUE);
                $this->_redirect('/' . $this->_lang . '/');
            }
        } else {
            /**
             * W pierwszej kolejnosci wybrac firme z ktorej produkty maja byc widoczne
             */
            $firmy = $this->_entityManager->getRepository('Entities\Korporacyjny_Uzytkownik')->getWszystkichByUserId($auth['id']);

            if (count($firmy) == 0) {
                $this->addMessageInfo('Brak cenników specjalnych', TRUE);
                $this->_redirect('/' . $this->_lang . '/');
            }
            if (count($firmy) == 1) {
                $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/koszyk/firma/' . $firmy[0]->getFirmyId());
            }
            $this->view->firmy = $firmy;
            $this->render('koszyk-wybierz-firme');
        }
    }

    public function szczegolyAction() {
        $auth = Zend_Auth::getInstance()->getIdentity();

        $zamowienie_id = (int) $this->getRequest()->getParam('id');
        if ($zamowienie_id == 0) {
            $this->addMessageError('Niepoprawny identyfikator zamówienia', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }
        $zamowienie = $this->_entityManager->getRepository('Entities\Korporacyjny_Koszyk')->getById($zamowienie_id);
        if (!$zamowienie instanceof Entities\Korporacyjny_Koszyk) {
            $this->addMessageError('Niepoprawny identyfikator zamówienia.', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }
        /**
         * Moze zobaczyc albo osoba z uprawnieniami albo uzytkownik, ktory skladal zamowienie
         */
        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH) || $zamowienie->getUzytkownicyId() == $auth['id']) {
            $this->view->zamowienie = $zamowienie;
        } else {
            $this->addMessageError('Niepoprawny identyfikator zamówienia..', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }
    }

    public function uzytkownicyAction() {
        $auth = Zend_Auth::getInstance()->getIdentity();
        if (NULL != $this->getRequest()->getParam('firma')) {
            $korp_user = $this->_entityManager->getRepository('Entities\Korporacyjny_Uzytkownik')->getDlaUserIdIFirmaIdIAktywny($auth['id'], $this->getRequest()->getParam('firma'));
            if ($korp_user instanceof Entities\Korporacyjny_Uzytkownik && $korp_user->getRola() == Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KOORDYNATOR) {

                $this->view->uzytkownicy = $this->_entityManager->getRepository('Entities\Korporacyjny_Uzytkownik')->getWszystkichAktywnychDlaFirmy($korp_user->getFirmyId());
                $this->view->em = $this->_entityManager;
                $this->view->firma = $korp_user->getFirmy();

                if ($this->getRequest()->isPost()) {
                    $cmd = new Application_Model_Commands_KorporacyjnyBudzetyZapisz($this->_entityManager, $this->getRequest()->getPost());
                    $cmd->execute();
                    if (count($cmd->errors) > 0) {
                        foreach ($cmd->errors as $v) {
                            $this->addMessageError($v, TRUE);
                        }
                    } else {
                        foreach ($cmd->success as $v) {
                            $this->addMessageSuccess($v, TRUE);
                        }
                    }
                }
            } else {
                $this->addMessageError('Niepoprawny identyfikator firmy', TRUE);
                $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/uzytkownicy/');
            }
        } else {
            /**
             * W pierwszej kolejnosci wybrac firme z ktorej produkty maja byc widoczne
             */
            $firmy = $this->_entityManager->getRepository('Entities\Korporacyjny_Uzytkownik')->getWszystkichKoordynatorowByUserId($auth['id']);

            if (count($firmy) == 0) {
                $this->addMessageInfo('Widok tylko dla koordynatorów', TRUE);
                $this->_redirect('/' . $this->_lang . '/');
            }
            if (count($firmy) == 1) {
                $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/uzytkownicy/firma/' . $firmy[0]->getFirmyId());
            }
            $this->view->firmy = $firmy;
            $this->view->action = 'uzytkownicy';
            $this->render('koordynator-wybierz-firme');
        }
    }

    public function opiekunZamowieniaAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $auth = Zend_Auth::getInstance()->getIdentity();
        $this->view->zamowienia = $this->_entityManager->getRepository('Entities\Korporacyjny_Koszyk')->getWszystkieZlozoneDlaPh($auth['id']);
    }

    public function opiekunWypelnijProduktAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }
        $id = (int) $this->getRequest()->getParam('id');
        if ($id == 0) {
            $this->addMessageError('Niepoprawny identyfikator produktu', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Korporacyjny_Produkt')->getById($id);
        if (!$produkt instanceof Entities\Korporacyjny_Produkt) {
            $this->addMessageError('Niepoprawny identyfikator produktu.', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $form = new Application_Form_KorporacyjnyProdukt();
        $this->view->form = $form;


        $nazwa = '';
        foreach ($produkt->getProduktyDetale()->getProdukty()->getProduktyI18n() as $i18n) {
            if ($i18n instanceof Entities\Produkt_i18n && $i18n->getJezyk() == 'pl') {
                $nazwa = $i18n->getNazwa();
            }
        }
        $this->view->nazwaProduktu = $nazwa;
        $this->view->produktObj = $produkt;

        $form->populate([
            'cena' => $produkt->getCena(),
            'stock' => $produkt->getStock(),
            'min_stock' => $produkt->getMinStock(),
            'czas_dodruku' => $produkt->getCzasDodruku(),
            'id' => $produkt->getId(),
            'firma_id' => $produkt->getFirmyId(),
            'detal_id' => $produkt->getProduktyDetaleId(),
        ]);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $cmd = new Application_Model_Commands_KorporacyjnyProduktZmien($this->_entityManager, $this->getRequest()->getPost(), $produkt);
                $cmd->execute();
                if (count($cmd->errors) > 0) {
                    foreach ($cmd->errors as $v) {
                        $this->addMessageError($v, TRUE);
                    }
                } else {
                    foreach ($cmd->success as $v) {
                        $this->addMessageSuccess($v, TRUE);
                    }
                }
            }
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-firmy-produkty/id/' . $produkt->getFirmyId());
        }
        if ($form->isErrors()) {
            $form->populate($this->getRequest()->getPost());
        }
    }

    public function opiekunFirmyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $form = new Application_Form_KorporacyjniFirmyDlaPh();
        $firmaElem = $form->getElement('firma');


        $auth = Zend_Auth::getInstance()->getIdentity();
        $firmy = $this->_entityManager->getRepository('Entities\Korporacyjny_Produkt')->getFirmyDlaPhId($auth['id']);
        foreach ($firmy as $v) {
            $firma = $this->_entityManager->getRepository('Entities\Firma')->getById($v['firmy_id']);
            if ($firma instanceof Entities\Firma) {
                $firmaElem->addMultiOption($firma->getId(), $firma->getNazwa());
            }
        }
        if ($this->getRequest()->isGet() && NULL != $this->getRequest()->getParam('firma')) {
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-firmy-produkty/id/' . $this->getRequest()->getParam('firma'));
        }
        $this->view->form = $form;
    }

    /**
     * Widok dla PH
     */
    public function opiekunFirmyUzytkownicyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $firma_id = (int) $this->getRequest()->getParam('firma_id');

        if ($firma_id == 0) {
            $this->addMessageError('Niepoprawny identyfikator', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        $firma = $this->_entityManager->getRepository('Entities\Firma')->getById($firma_id);
        if (!$firma instanceof Entities\Firma) {
            $this->addMessageError('Firma o podanym identyfiaktorze nie istnieje', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        $auth = Zend_Auth::getInstance()->getIdentity();

        if ($auth['id'] != $firma->getPhId()) {
            $this->addMessageError('Ta fima nie należy do Ciebie', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        $form = new Application_Form_KorporacyjniUzytkownicy();
        $select = $form->getElement('uzytkownik');
        /**
         * Uzypelniamy selecta uzytkownikami
         */
        $this->view->KorporacyjniUzytkownicyLista($select, $firma_id);

        $this->view->uzytkownicy = $this->_entityManager->getRepository('Entities\Korporacyjny_Uzytkownik')->getWszystkichDlaFirmy($firma_id);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $cmd = new Application_Model_Commands_KorporacyjnyUzytkownikDodaj($this->_entityManager, $this->getRequest()->getPost(), $firma);
                $cmd->execute();
                if (count($cmd->errors) > 0) {
                    foreach ($cmd->errors as $v) {
                        $this->addMessageError($v, TRUE);
                    }
                } else {
                    foreach ($cmd->success as $v) {
                        $this->addMessageSuccess($v, TRUE);
                    }
                }
                $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-firmy-uzytkownicy/firma_id/' . $firma->getId());
            }
        }
        if (NULL != $this->getRequest()->getParam('typ')) {
            if($this->getRequest()->getParam('typ') == 'usun') {
            $cmd = new Application_Model_Commands_KorporacyjnyUzytkownikUsun($this->_entityManager, $this->getRequest()->getParams(), $firma);
            $cmd->execute();
            }
            if($this->getRequest()->getParam('typ') == 'zmien') {
            $cmd = new Application_Model_Commands_KorporacyjnyUzytkownikZmien($this->_entityManager, $this->getRequest()->getParams(), $firma);
            $cmd->execute();
            }
            if (count($cmd->errors) > 0) {
                foreach ($cmd->errors as $v) {
                    $this->addMessageError($v, TRUE);
                }
            } else {
                foreach ($cmd->success as $v) {
                    $this->addMessageSuccess($v, TRUE);
                }
            }
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-firmy-uzytkownicy/firma_id/' . $firma->getId());
        }

        $this->view->firma = $firma;
        $this->view->form = $form;
    }

    public function opiekunFirmyProduktyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }
        $id = (int) $this->getRequest()->getParam('id');
        if ($id == 0) {
            $this->addMessageError('Niepoprawny identyfikator produktu', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        $firma = $this->_entityManager->getRepository('Entities\Firma')->getById($id);
        if (!$firma instanceof Entities\Firma) {
            $this->addMessageError('Firma o podanym identyfiaktorze nie istnieje', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        
        $auth = Zend_Auth::getInstance()->getIdentity();
        if ($auth['id'] != $firma->getPhId()) {
            $this->addMessageError('Ta fima nie należy do Ciebie', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }

        $produkty = $this->_entityManager->getRepository('Entities\Korporacyjny_Produkt')->getWszystkieDlaFirmy($id);
        if (count($produkty) > 0) {
            $this->view->produkty = $produkty;
        } else {
            $this->addMessageError('Firma nie ma produktów specjalnych', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
    }

    public function opiekunUsunProduktAction() {
        /**
         * Ukrywamy layout i view
         * bo tu nie ma tresci
         */
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }
        $id = (int) $this->getRequest()->getParam('id');
        if ($id == 0) {
            $this->addMessageError('Niepoprawny identyfikator produktu', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Korporacyjny_Produkt')->getById($id);
        if (!$produkt instanceof Entities\Korporacyjny_Produkt) {
            $this->addMessageError('Niepoprawny identyfikator produktu.', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $produkt->setWidoczny(0);

        try {
            $this->_entityManager->persist($produkt);
            $this->_entityManager->flush();
        } catch (DoctrineExtensions $e) {
            $this->addMessageError($e->getMessage(), TRUE);
        } catch (Exception $e) {
            $this->addMessageError($e->getMessage(), TRUE);
        }
        $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-firmy-produkty/id/' . $produkt->getFirmyId());
    }

    public function zmienStatusAction() {
        /**
         * Ukrywamy layout i view
         * bo tu nie ma tresci
         */
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        $id = (int) $this->getRequest()->getParam('id');
        if ($id == 0) {
            $this->addMessageError('Niepoprawny identyfikator zamówienia', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        $zamowienie = $this->_entityManager->getRepository('Entities\Korporacyjny_Koszyk')->getById($id);
        if (!$zamowienie instanceof Entities\Korporacyjny_Koszyk) {
            $this->addMessageError('Niepoprawny identyfikator zamówienia.', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }

        $auth = Zend_Auth::getInstance()->getIdentity();
        if ($zamowienie->getUzytkownicy()->getFirmy()->getPhId() != $auth['id']) {
            $this->addMessageError('To zamówienie nie należy do Ciebie', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }

        if ($zamowienie->getStatus() != Atacama_Static::KORPORACYJNI_ZAMOWIENIE_NOWE) {
            $this->addMessageError('To zamówienie juz zostało zaktualizowane', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
        }
        /*
         * Aktualizacja koszyka
         */
        $zamowienie->setStatus($this->getRequest()->getParam('status'))
                ->setDataRealizacji(new DateTime());

        $this->_entityManager->persist($zamowienie);
        $this->_entityManager->flush();
        $this->_wyslijPowiadomienieDoKlienta($zamowienie, $this->getRequest()->getParam('status'));


        $this->addMessageSuccess('Status zamówienia zmieniony pomyślnie', TRUE);
        $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/opiekun-zamowienia/');
    }

    public function dodajProduktyZOfertyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KORPORACYJNY_KOSZYK_PH)) {
            $this->addMessageError('Brak uprawnień', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }
        $id = (int) $this->getRequest()->getParam('id');
        if ($id == 0) {
            $this->addMessageError('Niepoprawny identyfikator oferty', TRUE);
            $this->_redirect('/' . $this->_lang . '/zamowienia-specjalne/');
        }

        $cmd = new Application_Model_Commands_KorporacyjnyProduktPobierzZOferty($this->_entityManager, $id);
        $cmd->execute();

        if (count($cmd->errors) > 0) {
            foreach ($cmd->errors as $error) {
                $this->addMessageError($error, FALSE);
            }
        } else {
            $this->addMessageSuccess($cmd->success[0], FALSE);
            $this->view->dodane = $cmd->getProduktyDodane();
        }
    }

    private function _wyslijPowiadomienieDoKlienta(Entities\Korporacyjny_Koszyk $zamowienie, $status) {
        $config = Atacama_Config::getInstance();
        $emailPh = new Atacama_Mail($this->_entityManager, 'pl');
        $crypt = Moyoki_Crypt::getInstance();
        $email = $crypt->decrypt($zamowienie->getUzytkownicy()->getEmail());
        $temat = 'Zamówienie specjalne nr ' . $zamowienie->getId() . ' zmieniło status';

        $url = $config->appUrl . $this->view->url(array(
                    'language' => 'pl',
                    'controller' => 'Zamowienia-specjalne',
                    'action' => 'szczegoly',
                    'id' => $zamowienie->getId()
                        ), 'default', true);
        switch ($status) {
            case Atacama_Static::KORPORACYJNI_ZAMOWIENIE_ZREALIZOWANE:
                $nazwa_statusu = "zrealizowane";
                break;
            case Atacama_Static::KORPORACYJNI_ZAMOWIENIE_ANULOWANE:
                $nazwa_statusu = "anulowane";
                break;
        }
        $tresc = 'Dzień dobry,<br>'
                . 'Zamówienie nr <b>' . $zamowienie->getId() . '</b><br>'
                . 'zmieniło status na <b>' . $nazwa_statusu . '<b><br>'
                . '<a href="' . $url . '" class="btn btn-primary">Link do zamówienia</a>';


        $emailPh->phNoweZamowienie($email, $temat, $tresc);
    }

}
