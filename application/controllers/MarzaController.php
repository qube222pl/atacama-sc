<?php

class MarzaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_WIDOK_DLA_PH)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = true;
    }

    public function indexAction() {
        $this->view->acl = $this->_acl;
        $this->view->lang = $this->_lang;

        $userID = (int) $this->getRequest()->getParam('user_id');
        if ($userID > 0) {
            $ph = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($userID);
        }
        if (isset($ph) && $ph instanceof Entities\Uzytkownik && $this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_MARZA_HANDLOWCOW)) {
            $phID = $ph->getId();
            $this->view->osoba = $ph->getImie() . ' ' . $ph->getNazwisko();
        } else {
            $user = Zend_Auth::getInstance()->getIdentity();
            $phID = $user['id'];
        }
//$phID = 2;
        $this->view->zamowienia = $this->_entityManager->getRepository('Entities\Koszyk')->getMarzaZZamowienDlaPH($phID, date("Y"), date("m"));
    }

    public function poprzedniMiesiacAction() {
        $this->view->lang = $this->_lang;

        $userID = (int) $this->getRequest()->getParam('user_id');
        if ($userID > 0) {
            $ph = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($userID);
        }
        if (isset($ph) && $ph instanceof Entities\Uzytkownik && $this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_MARZA_HANDLOWCOW)) {
            $phID = $ph->getId();
            $this->view->osoba = $ph->getImie() . ' ' . $ph->getNazwisko();
        } else {
            $user = Zend_Auth::getInstance()->getIdentity();
            $phID = $user['id'];
        }

        $rok = date("Y", strtotime("first day of previous month"));
        $miesiac = date("m", strtotime("first day of previous month"));

        $this->view->okres = $rok . '.' . $miesiac;
        $this->view->zamowienia = $this->_entityManager->getRepository('Entities\Koszyk')->getMarzaZZamowienDlaPH($phID, $rok, $miesiac);
    }

    public function handlowcyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_MARZA_HANDLOWCOW)) {
            $this->_redirect('/' . $this->_lang . '/marza/');
        }

        $this->view->lang = $this->_lang;
        $this->view->uzytkownicy = $this->_entityManager->getRepository('Entities\Uzytkownik')->phOrderByNazwisko();
    }

    public function exportAction() {
        $this->view->lang = $this->_lang;

        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_MARZA_HANDLOWCOW)) {
            $phID = null;
        } else {
            $user = Zend_Auth::getInstance()->getIdentity();
            $phID = $user['id'];
        }
//$phID = 2;        
        $this->exportExcel($phID, date("Y"), date("m"));
    }

    public function exportPoprzedniMiesiacAction() {
        $this->view->lang = $this->_lang;

        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_MARZA_HANDLOWCOW)) {
            $phID = null;
        } else {
            $user = Zend_Auth::getInstance()->getIdentity();
            $phID = $user['id'];
        }

        $rok = date("Y", strtotime("first day of previous month"));
        $miesiac = date("m", strtotime("first day of previous month"));
        $this->exportExcel($phID, $rok, $miesiac);
    }

    private function exportExcel($phID, $rok, $miesiac) {

        require_once 'PHPExcel/PHPExcel.php';
        require_once 'PHPExcel/PHPExcel/IOFactory.php';

        $zamowienia = $this->_entityManager->getRepository('Entities\Koszyk')->getMarzaZZamowienDlaPH($phID, $rok, $miesiac);

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $excel = new PHPExcel();
        $styleBold = array(
            'font' => array(
                'bold' => true
            )
        );

        if (count($zamowienia)) {

            $phID = 0;
            foreach ($zamowienia as $zamowienie) {

                // jeśli nowy PH -> tworzymy nowy arkusz
                $ph2 = (int) $zamowienie->getFirmy()->getPhId();
                if ($phID != $ph2) {

                    if ($phID != 0) {
                        // podsumowanie poprzedniego PH
                        $this->excelPodsumowanie($worksheet, $suma, $wiersz, $styleBold);
                    }

                    $worksheet = $excel->createSheet();
                    $ph = $zamowienie->getFirmy()->getPh();
                    $worksheet->setTitle($ph->getImie() . ' ' . $ph->getNazwisko());

                    // linia nagłówka
                    $header = array(0 => 'Lp.', 'Nr zamówienia', 'Firma', 'Zamawiający', '', 'Marża');
                    foreach ($header as $col => $name) {
                        $worksheet->setCellValueByColumnAndRow($col, 1, $name);
                    }

                    $worksheet->getStyle('A1:F1')->applyFromArray($styleBold);

                    $suma = 0.0;
                    $lp = 1;
                    $wiersz = 2;
                    $phID = $ph2;
                }

                $worksheet->setCellValueByColumnAndRow(0, $wiersz, $lp);
                $worksheet->setCellValueByColumnAndRow(1, $wiersz, $zamowienie->getNrZamowienia());
                $worksheet->setCellValueByColumnAndRow(2, $wiersz, $zamowienie->getFirmy()->getNazwa());
                $worksheet->setCellValueByColumnAndRow(3, $wiersz, $zamowienie->getUzytkownicy()->getImie() . ' ' . $zamowienie->getUzytkownicy()->getNazwisko());
                $worksheet->getStyleByColumnAndRow(5, $wiersz)->getNumberFormat()->setFormatCode('#,##0.00');
                $worksheet->setCellValueByColumnAndRow(5, $wiersz, $zamowienie->getMarza());
                $worksheet->getStyle('A' . $wiersz . ':F' . $wiersz)->applyFromArray($styleBold);
                $wiersz++;
                $lp++;
                $suma += $zamowienie->getMarza();
                

                foreach ($zamowienie->getKoszykiProdukty() as $koszykiProdukty) :

                    //$koszykiProdukty = new Entities\Koszyk_Produkt;

                    foreach ($koszykiProdukty->getProduktyDetale()->getProdukty()->getProduktyI18n() as $i18n) {
                        if ($i18n->getJezyk() == $this->_lang) {
                            $nazwaProduktu = $i18n->getNazwa();
                        }
                    }

                    $cenaSztuka = $koszykiProdukty->getCenaZakupu() - $koszykiProdukty->getCenaSprzedazy();

                    $worksheet->setCellValueByColumnAndRow(0, $wiersz, '');
                    $worksheet->setCellValueByColumnAndRow(1, $wiersz, $nazwaProduktu);
                    $worksheet->setCellValueByColumnAndRow(2, $wiersz, $koszykiProdukty->getIlosc());
                    $worksheet->getStyleByColumnAndRow(3, $wiersz)->getNumberFormat()->setFormatCode('#,##0.00');
                    $worksheet->setCellValueByColumnAndRow(3, $wiersz, $koszykiProdukty->getCenaSprzedazy());
                    $worksheet->getStyleByColumnAndRow(4, $wiersz)->getNumberFormat()->setFormatCode('#,##0.00');
                    $worksheet->setCellValueByColumnAndRow(4, $wiersz, $koszykiProdukty->getCenaZakupu());
                    $worksheet->getStyleByColumnAndRow(5, $wiersz)->getNumberFormat()->setFormatCode('#,##0.00');
                    $worksheet->setCellValueByColumnAndRow(5, $wiersz, $cenaSztuka * $koszykiProdukty->getIlosc());
                    $wiersz++;
                endforeach;
            }

// podsumowanie ostatniego PH
            $this->excelPodsumowanie($worksheet, $suma, $wiersz, $styleBold);

            $excel->removeSheetByIndex(0);
            $excel->setActiveSheetIndex(0);
        } else {

            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->SetCellValue('A1', 'Brak danych.');
        }


        ob_end_clean();

        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Marże.xls"');
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');

//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment;filename="Report.xlsx"');
//        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');

        ob_end_clean();

        $objWriter->save('php://output');
        $excel->disconnectWorksheets();
        unset($excel);
    }

    private function excelPodsumowanie(PHPExcel_Worksheet $worksheet, $suma, $wiersz, $style) {

        $worksheet->setCellValueByColumnAndRow(0, $wiersz, 'Razem');
        $worksheet->getStyleByColumnAndRow(4, $wiersz)->getNumberFormat()->setFormatCode('#,##0.00');
        $worksheet->setCellValueByColumnAndRow(5, $wiersz, $suma);
        $worksheet->getStyle('A' . $wiersz . ':F' . $wiersz)->applyFromArray($style);
    }

}
