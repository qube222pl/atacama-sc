<?php

class RaportController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_OGOLNA)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = true;
    }

    public function indexAction() {
        
    }

}
