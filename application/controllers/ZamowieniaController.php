<?php

class ZamowieniaController extends Atacama_Controller_Action {

    public function init() {
        $this->view->headScript()->appendFile($this->view->baseURL('/js/minifyjs/zamowieniaIndex-min.js'));

        $zmienStatusForm = new Application_Form_ZamowienieZmienStatus();
        $zmienStatusForm->setAction($this->view->baseUrl('/pl/Zamowienia/zmien-status/'));

        $this->view->formStatus = $zmienStatusForm;
        parent::init();
    }

    public function mojeAction() {
        $user = Zend_Auth::getInstance()->getIdentity();
        $config = Atacama_Config::getInstance();

        $id = (int) $this->getRequest()->getParam('id');

        /**
         * Wyroznione zamowienie na liscie
         */
        if ($id > 0) {
            $this->view->wyrozniony = $id;
        }

        $paginator = $this->_entityManager->getRepository('Entities\Koszyk')->getZamowieniaUzytkownikaPaginator($user['id']);

        $paginatorIter = $paginator->getIterator();

        $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);
        $zend_paginator = new \Zend_Paginator($adapter);

        $strona = (int) $this->getRequest()->getParam('s');
        if (!isset($strona) || $strona == 0)
            $strona = 1;

        $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
        $zend_paginator->setItemCountPerPage($iloscNaStronie)
                ->setCurrentPageNumber($strona);

        $this->view->lang = $this->_lang;
        $this->view->iter = $iloscNaStronie * ($strona - 1);

        $this->view->paginator = $zend_paginator;

        $this->view->lang = $this->_lang;
        $this->view->acl = $this->_acl;

        $this->view->naglowek = 'moje zamowienia';
        $this->view->nazwaAkcjiDoRedirecta = 'moje';
    }

    public function domnieAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ZAMOWIENIA_PODGLAD)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $f = trim($this->getRequest()->getParam('f'));

        $form = new Application_Form_AdminWyszukiwarka();
        $form->getElement('f')->setAttrib('placeholder', 'Nazwa firmy');
        $this->view->form = $form;


        if (strlen($f) > 2) {
            $fraza = $f;
            $form->getElement('f')->setValue($f);
        } else {
            $fraza = NULL;
        }

        $id = (int) $this->getRequest()->getParam('id');

        /**
         * Wyroznione zamowienie na liscie
         */
        if ($id > 0) {
            $this->view->wyrozniony = $id;
        }

        $auth = Zend_Auth::getInstance()->getIdentity();
        $config = Atacama_Config::getInstance();

        $paginator = $this->_entityManager->getRepository('Entities\Koszyk')->getZamowieniaDoMniePaginator($auth['id'], $fraza);

        $paginatorIter = $paginator->getIterator();

        $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

        $zend_paginator = new \Zend_Paginator($adapter);

        $strona = (int) $this->getRequest()->getParam('s');
        if (!isset($strona) || $strona == 0)
            $strona = 1;

        $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
        $zend_paginator->setItemCountPerPage($iloscNaStronie)
                ->setCurrentPageNumber($strona);

        $this->view->lang = $this->_lang;
        $this->view->iter = $iloscNaStronie * ($strona - 1);

        $this->view->paginator = $zend_paginator;
        $this->view->em = $this->_entityManager;
        $this->view->lang = $this->_lang;
        $this->view->acl = $this->_acl;

        $this->view->naglowek = 'zamowienia moich klientow';
        $this->view->nazwaAkcjiDoRedirecta = 'domnie';
        $this->render('moje');
    }

    public function wszystkieAction() {
        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_ZAMOWIENIA_WSZYSTKIE)) {
            $config = Atacama_Config::getInstance();

            $id = (int) $this->getRequest()->getParam('id');

            /**
             * Wyroznione zamowienie na liscie
             */
            if ($id > 0) {
                $this->view->wyrozniony = $id;
            }
            $this->view->lang = $this->_lang;
            $this->view->acl = $this->_acl;

            $this->view->naglowek = 'wszystkie zamowienia';
            $this->view->nazwaAkcjiDoRedirecta = 'wszystkie';

            $f = trim($this->getRequest()->getParam('f'));

            $form = new Application_Form_AdminWyszukiwarka();
            $this->view->form = $form;


            if (strlen($f) > 2) {
                $fraza = $f;
                $form->getElement('f')->setValue($f);
            } else {
                $fraza = NULL;
            }


            $paginator = $this->_entityManager->getRepository('Entities\Koszyk')->getWszystkieZamowieniaPaginator($fraza);

            $paginatorIter = $paginator->getIterator();

            $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

            $zend_paginator = new \Zend_Paginator($adapter);

            $strona = (int) $this->getRequest()->getParam('s');
            if (!isset($strona) || $strona == 0)
                $strona = 1;

            $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
            $zend_paginator->setItemCountPerPage($iloscNaStronie)
                    ->setCurrentPageNumber($strona);

            $this->view->lang = $this->_lang;
            $this->view->iter = $iloscNaStronie * ($strona - 1);

            $this->view->paginator = $zend_paginator;

            $this->render('moje');
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
    }

    public function ofertyAction() {
        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_GENERUJ_OFERTE)) {
            $auth = Zend_Auth::getInstance()->getIdentity();
            $config = Atacama_Config::getInstance();

            $this->view->lang = $this->_lang;
            $this->view->acl = $this->_acl;
            $this->view->naglowek = 'moje wygenerowane oferty';
            $this->view->nazwaAkcjiDoRedirecta = 'oferty';


            $paginator = $this->_entityManager->getRepository('Entities\Koszyk')->getOfertyDlaPhPaginator($auth['id']);
            $paginatorIter = $paginator->getIterator();

            $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

            $zend_paginator = new \Zend_Paginator($adapter);

            $strona = (int) $this->getRequest()->getParam('s');
            if (!isset($strona) || $strona == 0)
                $strona = 1;

            $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
            $zend_paginator->setItemCountPerPage($iloscNaStronie)
                    ->setCurrentPageNumber($strona);

            $this->view->lang = $this->_lang;
            $this->view->iter = $iloscNaStronie * ($strona - 1);

            $this->view->paginator = $zend_paginator;

            $this->view->lang = $this->_lang;
            $this->view->acl = $this->_acl;

            $this->render('moje');
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
    }

    public function ofertyWszystkieAction() {
        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_ZAMOWIENIA_WSZYSTKIE)) {
            $this->view->lang = $this->_lang;
            $this->view->acl = $this->_acl;
            $this->view->naglowek = 'Wszystkie oferty';
            $this->view->nazwaAkcjiDoRedirecta = 'oferty';

            $f = trim($this->getRequest()->getParam('f'));

            $form = new Application_Form_AdminWyszukiwarka();
            $form->getElement('f')->setAttrib('placeholder', 'Nazwa firmy');
            $this->view->form = $form;

            if (strlen($f) > 2) {
                $fraza = $f;
                $form->getElement('f')->setValue($f);
            } else {
                $fraza = NULL;
            }

            $strona = (int) $this->getRequest()->getParam('s');
            $config = Atacama_Config::getInstance();

            $paginator = $this->_entityManager->getRepository('Entities\Koszyk')->getWszystkieOfertyPaginator($fraza);

            $paginatorIter = $paginator->getIterator();

            $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

            $zend_paginator = new \Zend_Paginator($adapter);

            $strona = (int) $this->getRequest()->getParam('s');
            if (!isset($strona) || $strona == 0)
                $strona = 1;

            $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
            $zend_paginator->setItemCountPerPage($iloscNaStronie)
                    ->setCurrentPageNumber($strona);

            $this->view->lang = $this->_lang;
            $this->view->iter = $iloscNaStronie * ($strona - 1);

            $this->view->paginator = $zend_paginator;
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
    }

    public function ofertyDoZatwierdzeniaAction() {
        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_WIDOK_DLA_PH)) {
            $this->view->lang = $this->_lang;
            $this->view->acl = $this->_acl;
            $this->view->naglowek = 'Zamówienia i oferty wysłane';
            $this->view->nazwaAkcjiDoRedirecta = 'oferty';

            $f = trim($this->getRequest()->getParam('f'));

            $form = new Application_Form_AdminWyszukiwarka();
            $form->getElement('f')->setAttrib('placeholder', 'Nazwa firmy');
            $this->view->form = $form;

            if (strlen($f) > 2) {
                $fraza = $f;
                $form->getElement('f')->setValue($f);
            } else {
                $fraza = NULL;
            }

            $strona = (int) $this->getRequest()->getParam('s');
            $config = Atacama_Config::getInstance();

            if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_ZAMOWIENIA_WSZYSTKIE)) {
                $paginator = $this->_entityManager->getRepository('Entities\Koszyk')->getWyslaneOfertyPaginator($fraza);
            } else {
                $user = Zend_Auth::getInstance()->getIdentity();
                $paginator = $this->_entityManager->getRepository('Entities\Koszyk')->getWyslaneOfertyPhPaginator($user['id'], $fraza);
            }

            $paginatorIter = $paginator->getIterator();

            $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

            $zend_paginator = new \Zend_Paginator($adapter);

            $strona = (int) $this->getRequest()->getParam('s');
            if (!isset($strona) || $strona == 0)
                $strona = 1;

            $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
            $zend_paginator->setItemCountPerPage($iloscNaStronie)
                    ->setCurrentPageNumber($strona);

            $this->view->lang = $this->_lang;
            $this->view->iter = $iloscNaStronie * ($strona - 1);

            $this->view->paginator = $zend_paginator;
            $this->view->naglowek = "Wszystkie oferty wysłane";
            $this->render('oferty-wszystkie');
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Index');
        }
    }

    public function zmienAction() {
        $this->view->headScript()->appendFile($this->view->baseURL('/bootstrap/js/bootstrap-datepicker.js'));

        $this->view->headScript()->appendFile($this->view->baseURL('/js/minifyjs/zamowieniaZmien-min.js'));

        $id = (int) $this->getRequest()->getParam('id');

        $redirect = $this->getRequest()->getParam('redirect');
        if (strlen($redirect) > 2) {
            $this->view->redirect = $redirect;
        }

        if ($id == 0) {
            $this->addMessageError($this->view->translate('zly identyfiaktor zamowienia'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ZAMOWIENIA_EDYCJA)) {
// Nie jestes adminem
            $auth = Zend_Auth::getInstance()->getIdentity();
            $zamowienie = $this->_entityManager->getRepository('Entities\Koszyk')->czyMozeWidziecToZamowienie($id, $auth['id']);

            if (NULL === $zamowienie) {
                $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
                $this->_redirect('/' . $this->_lang . '/index/');
            }
        } else {
// Jestes adminem, wiec widzisz i tak wszystko
            $zamowienie = $this->_entityManager->getRepository('Entities\Koszyk')->getById($id);
        }

        if ($zamowienie instanceof Entities\Koszyk) {

            //Ustawiana data, gdy nie bylo przeliczenia
            $terminDostawy = (null === $zamowienie->getDataDostawy() ? '' : date_format($zamowienie->getDataDostawy(), ' Y-m-d'));
            if ($this->getRequest()->isPost()) {
                //Moyoki_Debug::debug($_POST);return;
                $cmd = new Application_Model_Commands_ZamowienieAktualizuj($this->_entityManager, $this->getRequest()->getPost());
                $zamowienie = $cmd->execute();
                //Data dostawy nadpisywana ta z formularza
                $terminDostawy = $_POST['data_dostawy'];
            }
            $this->view->em = $this->_entityManager;
            $this->view->id = $id;
            $this->view->acl = $this->_acl;
            $this->view->zamowienie = $zamowienie;
            $this->view->lang = $this->_lang;


            $this->view->terminDostawy = $terminDostawy;
            /**
             * Formularz do zmiany stautusu
             */
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }
    }

    public function podgladAction() {
        $id = (int) $this->getRequest()->getParam('id');

        if ($id == 0) {
            $this->addMessageError($this->view->translate('zly identyfiaktor zamowienia'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_ZAMOWIENIA_WSZYSTKIE)) {
// Nie jestes adminem
            $auth = Zend_Auth::getInstance()->getIdentity();
            $zamowienie = $this->_entityManager->getRepository('Entities\Koszyk')->czyMozeWidziecToZamowienie($id, $auth['id']);

            if (NULL === $zamowienie) {
                $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
                $this->_redirect('/' . $this->_lang . '/index/');
            }
        } else {
// Jestes adminem, wiec widzisz i tak wszystko
            $zamowienie = $this->_entityManager->getRepository('Entities\Koszyk')->getById($id);
        }

        if ($zamowienie instanceof Entities\Koszyk) {
            $redirect = $this->getRequest()->getParam('redirect');
            if (strlen($redirect) > 2) {
                $this->view->redirect = $redirect;
            }
            $this->view->acl = $this->_acl;
            $this->view->zamowienie = $zamowienie;
            $this->view->lang = $this->_lang;
            $this->view->uwagi = $this->_entityManager->getRepository('Entities\Koszyk_Uwaga')->getByKoszykId($zamowienie->getId(), 'DESC');
            $this->view->em = $this->_entityManager;
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }
    }

    public function zmienStatusAction() {
        if ($this->getRequest()->isPost()) {
            $cmd = new Application_Model_Commands_ZamowienieZmienStatus($this->_entityManager, $_POST);
            $zamowienie = $cmd->execute();

            if ($zamowienie instanceof Entities\Koszyk) {
                $this->addMessageSuccess($this->view->translate('status zamowienia zmieniony pomylsnie'), TRUE);
                $this->_redirect('/' . $this->_lang . '/Zamowienia/' . $_POST['redirect']);
            } else {
                $this->addMessageError($this->view->translate('nieznany blad aplikacji'), TRUE);
                $this->_redirect('/' . $this->_lang . '/Zamowienia/' . $_POST['redirect']);
            }
        } else {
            $this->addMessageError($this->view->translate('bledne przekazanie danych'), TRUE);
            $this->_redirect('/' . $this->_lang . '/Zamowienia/' . $_POST['redirect']);
        }
    }

    public function podgladPdfAction() {
        $this->getHelper('layout')->setLayout('layout-pdf');

        $id = (int) $this->getRequest()->getParam('id');

        if ($id == 0) {
            $this->addMessageError($this->view->translate('zly identyfiaktor zamowienia'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $zamowienie = $this->_entityManager->getRepository('Entities\Koszyk')->getById($id);

        if ($zamowienie instanceof Entities\Koszyk) {
            $this->view->acl = $this->_acl;
            $this->view->zamowienie = $zamowienie;
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }
    }

    public function ofertaPdfAction() {
        $this->getHelper('layout')->setLayout('layout-pdf');

        $id = (int) $this->getRequest()->getParam('id');

        if ($id == 0) {
            $this->addMessageError($this->view->translate('zly identyfiaktor zamowienia'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $zamowienie = $this->_entityManager->getRepository('Entities\Koszyk')->getById($id);

        if ($zamowienie instanceof Entities\Koszyk) {

            Zend_Layout::getMvcInstance()->assign('phId', $zamowienie->getFirmy()->getPhId());

            $this->view->acl = $this->_acl;
            $this->view->lang = $this->_lang;
            $this->view->zamowienie = $zamowienie;
            $this->view->odbiorca = $zamowienie->getUzytkownicy();
            $this->view->em = $this->_entityManager;
            $zamowienie->setOfertaPlik('atacama-oferta-nr-' . $zamowienie->getId() . '.pdf');

            $this->_entityManager->getRepository('Entities\Koszyk')->dodajPlikOferty($zamowienie);
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }
    }

    public function ofertaWyslijAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $redirectUrl = '/' . $this->_lang . '/Zamowienia/oferty';
        $id = (int) $this->getRequest()->getParam('id');

        if ($id == 0) {
            $this->addMessageError($this->view->translate('zly identyfiaktor zamowienia'), TRUE);
            $this->_redirect($redirectUrl);
        }

        $oferta = $this->_entityManager->getRepository('Entities\Koszyk')->getById($id);

        if ($oferta instanceof Entities\Koszyk) {

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //$oferta = new Entities\Koszyk;
            if ($oferta->getStatus() != Atacama_Acl::STATUS_OFERTA_TWORZENIE) {
                $this->addMessageError($this->view->translate('To nie jest oferta'), TRUE);
                $this->_redirect($redirectUrl);
            }

            if ($oferta->getOfertaPlik() != NULL) {
                $config = Atacama_Config::getInstance();
                $sciezka_pliku = $config->pdf->path;

                if (file_exists($sciezka_pliku . $oferta->getOfertaPlik())) {
                    $plikOferty = $sciezka_pliku . $oferta->getOfertaPlik();
                    $widget = $this->_entityManager->getRepository('Entities\Widget')->getByKod('MAIL_OFERTA_KLIENT');
                    $temat = $this->view->translate('atacama oferta specjalna');

                    $adresEmail = Moyoki_Crypt::getInstance()->decrypt($oferta->getUzytkownicy()->getEmail());

                    try {
                        $email = new Atacama_Mail($this->_entityManager);
                        $wynikWysylki = $email->ofertaDlaKlienta($adresEmail, $this->view->translate('oferta nr') . ' ' . $oferta->getId(), $widget->getTresc(), $plikOferty);
                    } catch (Exception $exc) {
                        Atacama_Log::dodaj($this->_entityManager, Atacama_Log::BLAD_SYSTEMU, $exc->getMessage());
                    }

                    if (!$wynikWysylki) {
                        $this->addMessageError('Oferta nie została wysłana. Błąd wysyłania', TRUE);
                    } else {

                        /**
                         * Zmieniamy status oferty
                         */
                        $tablicaDoZmianyStatusu = array(
                            'uwagi' => 'Oferta została wysłana do Klienta',
                            'status' => Atacama_Acl::STATUS_WYSLANE,
                            'id' => $oferta->getId()
                        );

                        $cmd = new Application_Model_Commands_ZamowienieZmienStatus($this->_entityManager, $tablicaDoZmianyStatusu);
                        $cmd->execute();

                        $this->addMessageSuccess('Oferta została pomyślnie wysłana do Klienta.', TRUE);
                        @unlink($plikOferty);
                    }
                }
            } else {
                $this->addMessageError('Plik z ofertą nie istnieje na serwerze.', TRUE);
            }
        } else {
            $this->addMessageError('Brak pliku z ofertą', TRUE);
        }
        $this->_redirect($redirectUrl);
    }

    public function ofertaKopiujAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_GENERUJ_OFERTE)) {
            $id = (int) $this->getRequest()->getParam('id');

            if ($id == 0) {
                $this->addMessageError($this->view->translate('zly identyfiaktor oferty'), TRUE);
                $this->_redirect('/' . $this->_lang . '/Zamowienia/oferty/');
            }

            $oferta = $this->_entityManager->getRepository('Entities\Koszyk')->getById($id);

            if ($oferta instanceof Entities\Koszyk && $oferta->getOferta() == 1) {
                $cmd = new Application_Model_Commands_OfertaKopiuj($this->_entityManager, $oferta);
                $nowaOferta = $cmd->execute();
            }
            if (is_numeric($nowaOferta->getId())) {
                $this->addMessageSuccess('oferta skopiowana pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Koszyk/');
            } else {
                $this->addMessageError('Problem z kopiowaniem oferty', TRUE);
                $this->_redirect('/' . $this->_lang . '/Zamowienia/oferty/');
            }
        } else {
            $this->addMessageError($this->view->translate('brak uprawnien'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }
    }

    public function usunProduktAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $id = (int) $this->getRequest()->getParam('id');

        $config = Atacama_Config::getInstance();

        $tablica = array('error' => '',
            'razemProduktow' => 0,
            'razemNetto' => 0,
            'razemBrutto' => 0
        );

        $kp = $this->_entityManager->getRepository('Entities\Koszyk_Produkt')->getById($id);
        if ($kp instanceof Entities\Koszyk_Produkt) {
            $kpusuniety = $this->_entityManager->getRepository('Entities\Koszyk_Produkt')->deleteById($kp->getId());

            if ($kpusuniety == 1) {
                /**
                 * Koszyk zostal usuniety - przeliczamy podsumowanie
                 */
                if (count($kp->getKoszyki()->getKoszykiProdukty()) > 0) {
                    foreach ($kp->getKoszyki()->getKoszykiProdukty() as $koszykProdukt) {
                        $razemProduktow = + $koszykProdukt->getIlosc();
                        $netto = $koszykProdukt->getCenaZakupu() * $koszykProdukt->getIlosc();
                        $razemNetto = + $netto;
                    }

                    $tablica['razemProduktow'] = $razemProduktow;
                    $tablica['razemNetto'] = Atacama_Produkt_Kwota::odczyt($razemNetto, 'zł');
                    $tablica['razemBrutto'] = Atacama_Produkt_Kwota::odczyt($razemNetto + ($razemNetto * $config->importVat / 100), 'zł');
                }
            } else {
                $this->addMessageError('Pozycja nie została usunięta', TRUE);
            }
        } else {
            $this->addMessageError('Nie ma takiej pozycji', TRUE);
        }
        $this->_redirect('/' . $this->_lang . '/Zamowienia/zmien/id/' . $this->getRequest()->getParam('zam_id'));
    }

    public function dodajZnakowanieAction() {

        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $cmd = new Application_Model_Commands_ZamowienieDodajZnakowanie($this->_entityManager, $_POST);
        $result = $cmd->execute();
        if ($result instanceof Entities\Koszyk_Produkt_Znakowanie) {
            $this->addMessageSuccess('Znakowanie dodane', TRUE);
        } else {
            $this->addMessageError('Coś nie poszło dobrze.', TRUE);
        }

        $this->_redirect('/' . $this->_lang . '/Zamowienia/zmien/id/' . $this->getRequest()->getParam('id'));
    }

    public function usunZnakowanieAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_KOSZYK)) {
            $this->addMessageError($this->view->translate('odmowa dostepu'), TRUE);
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $id = (int) $this->getRequest()->getParam('id');

        $auth = Zend_Auth::getInstance()->getIdentity();
        $config = Atacama_Config::getInstance();


        $kpz = $this->_entityManager->getRepository('Entities\Koszyk_Produkt_Znakowanie')->getById($id);
        if ($kpz instanceof Entities\Koszyk_Produkt_Znakowanie) {
//$kpz = new Entities\Koszyk_Produkt_Znakowanie;
            $kpusuniety = $this->_entityManager->getRepository('Entities\Koszyk_Produkt_Znakowanie')->deleteById($kpz->getId());
            if ($kpusuniety == 1) {
                /**
                 * Produkt zostal usuniety - przeliczamy podsumowanie
                 */
            } else {
                $this->addMessageError('Pozycja nie została usunięta', TRUE);
            }
        } else {
            $this->addMessageError('Nie ma takiej pozycji', TRUE);
        }

        $this->_redirect('/' . $this->_lang . '/Zamowienia/zmien/id/' . $this->getRequest()->getParam('zam_id'));
    }

}
