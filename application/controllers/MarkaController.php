<?php

class MarkaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    public function indexAction() {
        $this->view->marki = $this->_entityManager->getRepository('Entities\Producent')->orderByNazwaWidoczne();
        $this->view->em = $this->_entityManager;
        $this->view->lang = $this->_lang;
    }

    public function widokAction() {
        $markaId = (int) $this->getRequest()->getParam('id');

        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->view->adminView = TRUE;
        }

        $this->view->lang = $this->_lang;

        if ($markaId > 0) {
            $marka = $this->_entityManager->getRepository('Entities\Producent')->getById($markaId);
            if ($marka->getWidoczny() == 1) {
                $this->view->marka = $marka;
                $this->view->produkty = $this->_entityManager->getRepository('Entities\Producent_Produkt')->getByProducentId($markaId);

                $this->view->em = $this->_entityManager;
                $this->view->lang = $this->_lang;
            } else {
                $this->addMessageSuccess('nie ma takiej marki', TRUE);
                $this->_redirect('/' . $this->_lang . '/Marka');
            }
        } else {
            
        }
    }

}
