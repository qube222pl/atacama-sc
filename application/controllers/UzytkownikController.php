<?php

class UzytkownikController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        $this->view->lang = $this->_lang;
    }

    public function indexAction() {
        $user_id = $this->_user['id'];

        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_OGOLNA)) {
            if (null !== $this->getRequest()->getParam('id')) {
                $user_id = (int) $this->getRequest()->getParam('id');
            }
        }

        $user = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($user_id);
        if ($user instanceof Entities\Uzytkownik) {
            $this->view->user = $user;
            $this->view->ph = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($user->getFirmy()->getPhId());
        } else {
            $this->view->user = NULL;
        }
    }

    public function logowanieAction() {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }
        $this->view->headTitle("Logowanie");

        $form = new Application_Form_Logowanie();


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $logowanieCmd = new Application_Model_Commands_Logowanie($this->_entityManager, $_POST);
                $result = $logowanieCmd->execute();

                if ($result) {
                    if (isset($_GET['redirect'])) {
                        $this->_redirect($_GET['redirect']);
                    }
                    Atacama_Log::dodaj($this->_entityManager, Atacama_Log::LOGOWANIE);
                    $ctrl = $this->getRequest()->getParam('controller');
                    $act = $this->getRequest()->getParam('action');

                    if (strcasecmp($ctrl, 'uzytkownik') == 0 && strcasecmp($act, 'logowanie') == 0) {
                        $this->_redirect('/' . $this->_lang . '/index/');
                    } else {
                        $this->_redirect($_SERVER['REDIRECT_URL']);
                    }
                } else {
                    $this->addMessageError($logowanieCmd->msg[0]);
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }

        $this->view->form = $form;
    }

    public function wylogowanieAction() {
        Zend_Auth::getInstance()->clearIdentity();
        $this->addMessageSuccess('Nastąpiło pomyślne wylogowanie. <br/>Zapraszamy ponownie', TRUE);
        $this->_redirect('/' . $this->_lang . '/index/');
    }

    public function rejestracjaAction() {
        $form = new Application_Form_Rejestracja();
        $this->view->headTitle("Rejestracja");


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                $crypt = Moyoki_Crypt::getInstance();

                $user = $this->_entityManager->getRepository('Entities\Uzytkownik')->getByEmail($crypt->encrypt(trim($_POST['email'])));
                if (null !== $user) {
                    $form->populate($_POST);
                    $this->addMessageError($this->view->translate('uzytkownik o podanym adresie e-mail jest juz w systemie'), TRUE);
                    $this->_redirect('/' . $this->_lang . '/Uzytkownik/logowanie');
                } else {
                    try {
                        $rejestracjaCmd = new Application_Model_Commands_Rejestracja($this->_entityManager, $_POST);
                        $result = $rejestracjaCmd->execute();
                        $this->addMessageSuccess($this->view->translate('uzytkownik zarejestrowany'), TRUE);
                        $this->_redirect('/' . $this->_lang . '/Uzytkownik/logowanie');
                    } catch (Exception $exc) {
                        echo $exc->getTraceAsString();
                    }
                }
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }

        $this->view->form = $form;
    }

    public function wszyscyAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_UZYTKOWNICY)) {
            $this->addMessageError($this->view->translate("odmowa dostepu"), TRUE);
            $this->_redirect('/' . $this->_lang . '/Uzytkownik/');
        }

        $strona = (int) $this->getRequest()->getParam('s');
        $config = Atacama_Config::getInstance();

        $form = new Application_Form_AdminWyszukiwarka();
        $this->view->form = $form;

        $f = trim($this->getRequest()->getParam('f'));
        if (strlen($f) > 1) {
            $fraza = $f;
            $form->getElement('f')->setValue($f);
        } else {
            $fraza = FALSE;
        }

        $paginator = $this->_entityManager->getRepository('Entities\Uzytkownik')->paginatorByNazwisko($fraza);

        $adapter = new \Zend_Paginator_Adapter_Iterator($paginator->getIterator());

        $zend_paginator = new \Zend_Paginator($adapter);
        if (!isset($strona))
            $strona = 1;

        $iloscNaStronie = $config->paginator->admin->itemsCountPerPage;
        $zend_paginator->setItemCountPerPage($iloscNaStronie)
                ->setCurrentPageNumber($strona);

        $this->view->lang = $this->_lang;
        $this->view->iter = $iloscNaStronie * ($strona - 1);

        $this->view->paginator = $zend_paginator;
    }

    public function dodajAction() {
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_UZYTKOWNICY)) {
            $this->addMessageError($this->view->translate("odmowa dostepu"), TRUE);
            $this->_redirect('/' . $this->_lang . '/Uzytkownik/');
        }

        $form = new Application_Form_Uzytkownik($this->_entityManager, 'dodawanie');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                $crypt = Moyoki_Crypt::getInstance();

                $user = $this->_entityManager->getRepository('Entities\Uzytkownik')->getByEmail($crypt->encrypt($_POST['email']));

                if ($user instanceof Entities\Uzytkownik) {
                    $this->addMessageError('Użytkownik o adresie <b>' . $_POST['email'] . '</b> już jest w systemie', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Uzytkownik/wszyscy');
                } else {


                    $firma = $this->_entityManager->getRepository('Entities\Firma')->getById($_POST['firmy_id']);
                    $uzytkownik = new Entities\Uzytkownik();
                    $uzytkownik->setImie($_POST['imie'])
                            ->setNazwisko($_POST['nazwisko'])
                            ->setEmail($crypt->encrypt($_POST['email']))
                            ->setRola($_POST['rola'])
                            ->setActive(($_POST['active'] > 0 ? 1 : 0))
                            ->setHaslo(md5($_POST['haslo1']))
                            ->setTelefon($_POST['telefon'])
                            ->setFirmy($firma)
                            ->setFirmyId($firma->getId());


                    $this->_entityManager->persist($uzytkownik);
                    $this->_entityManager->flush();
                }

                $this->addMessageSuccess('Nowy użytkownik został dodany pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Uzytkownik/wszyscy');
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function zmienAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_UZYTKOWNICY)) {
            $id = (int) $this->getRequest()->getParam('id');
        } else {
            $user = Zend_Auth::getInstance()->getIdentity();
            $id = $user['id'];
        }
        $crypt = Moyoki_Crypt::getInstance();


        if ($id > 0) {

            $widget = $this->_entityManager->getRepository('Entities\Widget')->getByKod('WWW_FORM_UZYTKOWNIK');

            $this->view->widget = $widget->getTresc();
            $form = new Application_Form_Uzytkownik($this->_entityManager);
            $this->view->form = $form;

            $user = $this->_entityManager->getRepository('Entities\Uzytkownik')->getById($id);

            if ($user instanceof Entities\Uzytkownik) {
                $userTab = array(
                    'id' => $user->getId(),
                    'imie' => $user->getImie(),
                    'nazwisko' => $user->getNazwisko(),
                    'email' => $crypt->decrypt($user->getEmail()),
                    'rola' => $user->getRola(),
                    'telefon' => $user->getTelefon(),
                    'firmy_id' => $user->getFirmyId(),
                    'active' => $user->getActive()
                );

                $form->populate($userTab);
            }

            if ($this->getRequest()->isPost()) {
                if ($form->isValid($this->getRequest()->getPost())) {
                    try {
                        $cmd = new Application_Model_Commands_UzytkownikZmien($this->_entityManager, $this->getRequest()->getPost());
                        $cmd->execute();
                    } catch (Exception $exc) {
                        $this->addMessageError($this->view->translate($exc->getMessage()), FALSE);
                        return;
                    }

                    $this->addMessageSuccess('Użytkownik został zmieniony', TRUE);

                    if ($this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_UZYTKOWNICY)) {
                        $this->_redirect('/' . $this->_lang . '/Uzytkownik/wszyscy');
                    } else {
                        $this->_redirect('/' . $this->_lang . '/Uzytkownik');
                    }
                }

                if ($form->isErrors()) {
                    $form->populate($_POST);
                }
            }
        } else {
            $this->addMessageError('Brak użytkownika w systemie', TRUE);
            $this->_redirect('/' . $this->_lang . '/Uzytkownik/wszyscy');
        }
    }

}
