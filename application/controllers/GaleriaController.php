<?php

class GaleriaController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_STRONY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = true;
    }

    public function indexAction() {
        $this->view->lang = $this->_lang;
        $this->view->galerie = $this->_entityManager->getRepository('Entities\Galeria')->galerieAlfabetycznie();
    }

    public function dodajAction() {
        $form = new Application_Form_Galeria();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $galeria = new Entities\Galeria;
                $galeria
                        ->setDataUtworzenia(new \DateTime(date("Y-m-d H:i:s")))
                        ->setNazwa($_POST['nazwa'])
                        ->setOpis($_POST['opis'])
                        ->setWidoczna($_POST['widoczna']);
                $this->_entityManager->persist($galeria);
                $this->_entityManager->flush();
                $this->addMessageSuccess('Galeria dodana', TRUE);
                $this->_redirect('/' . $this->_lang . '/Galeria');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }

        $form->getElement('submit')->setLabel('Dodaj');
        $this->view->form = $form;
    }

    public function zmienAction() {
        $id = (int) $this->_request->getParam('id');

        if ($id < 1) {
            $this->addMessageError('Brak ID galerii', TRUE);
            $this->_redirect('/' . $this->_lang . '/Galerie/');
        }

        $galeria = $this->_entityManager->getRepository('Entities\Galeria')->getById($id);

        if (null === $galeria) {
            $this->addMessageError('Nie ma takiej galerii', TRUE);
            $this->_redirect('/' . $this->_lang . '/Galeria');
        }

        $form = new Application_Form_Galeria();


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $galeria = new Application_Model_Commands_GaleriaZmien($this->_entityManager, $_POST);
                $result = $galeria->execute();

                if ($result) {
                    $this->addMessageSuccess('Galeria zmieniona', TRUE);
                } else {
                    $this->addMessageError('Nie ma takiej galerii', TRUE);
                }
                $this->_redirect('/' . $this->_lang . '/Galeria');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {
            $form->populate($galeria->toArray());
        }

        $this->view->form = $form;
    }

    public function zdjeciaAction() {
        $id = (int) $this->_request->getParam('id');

        if ($id < 1) {
            $this->addMessageError('Brak ID galerii', TRUE);
            $this->_redirect('/' . $this->_lang . '/Galeria/');
        }

        $galeria = $this->_entityManager->getRepository('Entities\Galeria')->getById($id);

        if (null === $galeria) {
            $this->addMessageError('Nie ma takiej galerii', TRUE);
            $this->_redirect('/' . $this->_lang . '/Galeria');
        }

        $this->view->zdjecia = $galeria->getGalerieZdjecia();


        // UPLOAD
        $this->view->uploadfile = true;
        $this->view->headScript()->appendFile($this->view->baseURL('/js/uploadGaleriaZdjecie.js'));

        $form = new Application_Form_GaleriaZdjecie($galeria->getId());
        $form->setAction($this->view->url(array('language' => 'pl', 'controller' => 'Ajax', 'action' => 'galeriazdjecie'), 'default', true));

        $this->view->form = $form;
    }

}
