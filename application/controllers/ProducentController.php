<?php

class ProducentController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = TRUE;
    }

    public function indexAction() {
        $this->view->lang = $this->_lang;
        $this->view->producenci = $this->_entityManager->getRepository('Entities\Producent')->orderByNazwa();
    }

    public function dodajAction() {
        $form = new Application_Form_Producent($this->_entityManager, 'dodaj');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                if (!empty($_FILES['logotyp'])) {
                    try {
                        $temp_file = $_FILES['logotyp']['tmp_name'];

                        $config = Atacama_Config::getInstance();

                        $path = realpath($config->logotypy->path);
                        $file = $_FILES['logotyp']['name'];
                        $basenameAndExtension = explode('.', $file);
                        $ext = end($basenameAndExtension);

                        $filename = strtolower(Moyoki_Friendly::name($_POST['nazwa']) . '-' . rand(1, 99) . '.' . $ext);

                        if (move_uploaded_file($temp_file, $path . '/' . $filename)) {

                            $logotypNazwa = $filename;
                        }
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }
                $producent = new Entities\Producent();
                $producent->setNazwa($_POST['nazwa']);
                if (isset($logotypNazwa) && strlen($logotypNazwa) > 3) {
                    $producent->setLogo($logotypNazwa);
                }

                $this->_entityManager->persist($producent);
                $this->_entityManager->flush();

                $this->addMessageSuccess('Nowy producent został dodany pomyślnie', TRUE);
                $this->_redirect('/' . $this->_lang . '/Producent');
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function zmienAction() {
        $producentId = (int) $this->_request->getParam('id');

        if ($producentId < 1) {
            $this->_redirect('/' . $this->_lang . '/Producent');
        }

        $producent = $this->_entityManager->getRepository('Entities\Producent')->getById($producentId);

        if (null === $producent) {
            $this->_redirect('/' . $this->_lang . '/Producent');
        }

        $this->view->producentId = $producent->getId();
        $form = new Application_Form_Producent($this->_entityManager, 'zmien');


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $polecenie = new Application_Model_Commands_ProducentZmien($this->_entityManager, $_POST);
                $polecenie->execute();

                $this->addMessageSuccess('Informacje o producencie zostały pomyślnie zmienione', TRUE);
                $this->_redirect('/' . $this->_lang . '/Producent');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {
            $form->populate(array('id' => $producent->getId(),
                'nazwa' => $producent->getNazwa(),
                'logo' => $producent->getLogo(),
                'widoczny' => $producent->getWidoczny()
            ));
        }

        $this->view->form = $form;
    }

    public function tlumaczeniaAction() {
        $producentId = (int) $this->_request->getParam('id');

        if ($producentId < 1) {
            $this->_redirect('/' . $this->_lang . '/Producent');
        }

        $producent = $this->_entityManager->getRepository('Entities\Producent')->getById($producentId);

        if (null === $producent) {
            $this->_redirect('/' . $this->_lang . '/Producent');
        }

        $this->view->nazwaProducenta = $producent->getNazwa();
        $this->view->producentId = $producent->getId();

        $form = new Application_Form_ProducentNazwa($this->_entityManager);


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $polecenie = new Application_Model_Commands_ProducentNazwy($this->_entityManager, $_POST);
                $polecenie->execute();

                $this->addMessageSuccess('Informacje o producencie zostały pomyślnie zmienione', TRUE);
                $this->_redirect('/' . $this->_lang . '/Producent');
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {

            $nazwyTablica = array('id' => $producent->getId());
            foreach ($producent->getProducenciI18n() as $i18n) {
                $nazwyTablica[$i18n->getJezyk()] = array(
                    'nazwa_' . $i18n->getJezyk() => $i18n->getNazwa(),
                    'opis_' . $i18n->getJezyk() => $i18n->getOpis(),
                    'seo_opis_' . $i18n->getJezyk() => $i18n->getSeoOpis()
                );
            }

            $form->populate($nazwyTablica);
        }

        $this->view->form = $form;
    }

    public function produktyAction() {
        $id = (int) $this->getRequest()->getParam('id');
        if ($id == 0) {
            $this->addMessageError('Brak identyfikatora', TRUE);
            $this->_redirect('/' . $this->_lang . '/Producent');
        }
        $form = new Application_Form_ProducentProdukt($this->_entityManager, $id, $this->_lang);
        $this->view->form = $form;

        $this->view->producentId = $id;
        $this->view->em = $this->_entityManager;
        $this->view->lang = $this->_lang;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $produkty_id = (int) $_POST['produkty_id'];
                $producenci_id = (int) $_POST['producenci_id'];

                if ($producenci_id > 0 && $produkty_id > 0) {
                    $producent = $this->_entityManager->getRepository('Entities\Producent')->getById($producenci_id);
                    $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produkty_id);

                    $producentProdukt = new Entities\Producent_Produkt();
                    $producentProdukt->setProducenci($producent)
                            ->setProducenciId($producent->getId())
                            ->setProdukty($produkt)
                            ->setProduktyId($produkt->getId());

                    $this->_entityManager->persist($producentProdukt);
                    $this->_entityManager->flush();

                    $this->addMessageSuccess('Produkt został przypisany do producenta', FALSE);
                } else {
                    $this->addMessageError('Przekazany niepoprawny parametr', FALSE);
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
        
        $usunac = (int) $this->getRequest()->getParam('usun');
        if($usunac > 0) {
            $this->_entityManager->getRepository('Entities\Producent_Produkt')->deleteById($usunac);
        }

        $this->view->produkty = $this->_entityManager->getRepository('Entities\Producent_Produkt')->getByProducentId($id);
    }

}
