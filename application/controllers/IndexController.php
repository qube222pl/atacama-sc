<?php

class IndexController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    public function indexAction() {
        
        $this->view->displayContainer = FALSE;
        $this->view->lang = $this->_lang;
        $this->view->em = $this->_entityManager;

        $promocje = $this->_entityManager->getRepository('Entities\Produkt')->stronaGlowna('promocje');
        if (is_array($promocje)) {
            shuffle($promocje);
        }
        $this->view->promocje = $promocje;

        $nowosci = $this->_entityManager->getRepository('Entities\Produkt')->stronaGlowna('nowosci');
        if (is_array($nowosci)) {
            shuffle($nowosci);
        }
        $this->view->nowosci = $nowosci;

        $bestsellery = $this->_entityManager->getRepository('Entities\Produkt')->stronaGlowna('bestseller');
        if (is_array($bestsellery)) {
            shuffle($bestsellery);
        }
        $this->view->bestsellery = $bestsellery;
    }

    public function testAction() {
        
    }

    public function cronAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //echo "CRON OK";
    }

}
