<?php

class ProduktController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }
        $this->view->adminView = TRUE;
    }

    public function indexAction() {
        $this->view->lang = $this->_lang;
        $this->view->produkty = $this->_entityManager->getRepository('Entities\Produkt')->wszystkieProduktyOrderByNazwa();
    }

    public function niewidoczneAction() {
        $this->view->headScript()
                ->appendFile($this->view->baseURL('/js//minifyjs/produktNiewidoczne-min.js'));

        $strona = (int) $this->getRequest()->getParam('s');
        $iloscNaStronie = (NULL != $this->getRequest()->getParam('ilosc') ? $this->getRequest()->getParam('ilosc') : 100);

        $this->view->lang = $this->_lang;

        $form = new Application_Form_ProduktyNiewidoczne($this->_entityManager);
        $params = [
            'szukane' => $this->getRequest()->getParam('szukane'),
            'dystrybutor' => $this->getRequest()->getParam('dystrybutor'),
            'ilosc' => $iloscNaStronie
        ];
        $form->populate($params);


        /**
         * Usuwanie produktu niewidocznego
         * To przychodzi z podgladu produktu
         */
        $usunetyId = (int) $this->getRequest()->getParam('usunac');
        if (isset($usunetyId) && $usunetyId > 0) {
            $cmd = new Application_Model_Commands_ProduktUsunNiewidoczny($this->_entityManager, $usunetyId);
            $result = $cmd->execute();
            if ($result) {
                $this->addMessageSuccess($cmd->msg, TRUE);
            } else {
                $this->addMessageError($cmd->msg, TRUE);
            }
            $this->_redirect('/' . $this->_lang . '/Produkt/niewidoczne/');
        }
        if ($this->getRequest()->isPost()) {
            $usunietych = 0;
            $params = $this->getRequest()->getParams();
            if (isset($params['usuniety']) && count($params['usuniety']) > 0) {
                foreach ($params['usuniety'] as $k => $v) {
                    $this->_entityManager->getRepository('Entities\Produkt')->ustawUsuniety($v);
                    $usunietych++;
                }
                $this->addMessageSuccess('Usuniętych produktów: ' . $usunietych, FALSE);
            }
        }

        $paginator = $this->_entityManager->getRepository('Entities\Produkt')->wszystkieNiewidoczneNieusunieteOrderByNazwa($this->_lang, $params);

        $paginatorIter = $paginator->getIterator();

        $adapter = new \Zend_Paginator_Adapter_Iterator($paginatorIter);

        $zend_paginator = new \Zend_Paginator($adapter);

        if (!isset($strona))
            $strona = 1;

        $zend_paginator->setItemCountPerPage($iloscNaStronie)
                ->setCurrentPageNumber($strona);

        $this->view->iter = $iloscNaStronie * ($strona - 1);

        $this->view->form = $form;

        $this->view->paginator = $zend_paginator;
    }

    public function doUzupelnieniaAction() {
        $this->view->lang = $this->_lang;
        $this->view->doUzupelnienia = $this->_entityManager->getRepository('Entities\Produkt')->produktyDoUzupelnienia();
    }

    public function bezKategoriiGlownejAction() {
        $this->view->lang = $this->_lang;
        $this->view->bezKategoriiGlownej = $this->_entityManager->getRepository('Entities\Produkt')->produktyBezKategoriiGlownej();
    }

    public function dodajAction() {

        $form = new Application_Form_Produkt($this->_entityManager, 'dodaj');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    $cmd = new Application_Model_Commands_ProduktDodaj($this->_entityManager, $_POST);
                    $cmd->execute();
                    $this->addMessageSuccess('produkt dodany pomyślnie', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Produkt/index/');
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function detaleAction() {
        $produktId = (int) $this->_request->getParam('id');
        $strona = (int) $this->getRequest()->getParam('strona');
        $this->view->iloscNaStronie = 80;
        $this->view->nrStrony = ($strona == 0 ? 1 : $strona);

        $this->view->produktId = $produktId;
        if ($produktId < 1) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produktId);

        if (null === $produkt) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $this->view->headScript()->appendFile($this->view->baseURL('/js/produktDetale.js'));

        $produktI18n = $this->_entityManager->getRepository('Entities\Produkt_i18n')->getByProduktIdAndLang($produktId, $this->_lang);

        $this->view->nazwaProduktu = $produktI18n->getNazwa();

        $this->view->detale = $produkt->getProduktyDetale();

        $this->view->kolory = $this->_entityManager->getRepository('Entities\Kolor')->getOrderByNazwa();

        if ($this->getRequest()->isPost()) {
            try {
                $cmd = new Application_Model_Commands_ProduktDetale($this->_entityManager, $this->getRequest()->getPost());
                $cmd->execute();
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
            $this->addMessageSuccess('Detale zostały pomyślnie zmienione', TRUE);
            $this->_redirect('/' . $this->_lang . '/art/' . $produktId);
        }
    }

    public function znakowanieAction() {
        $produktId = (int) $this->_request->getParam('id');

        $this->view->produktId = $produktId;
        if ($produktId < 1) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produktId);

        if (null === $produkt) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produktI18n = $this->_entityManager->getRepository('Entities\Produkt_i18n')->getByProduktIdAndLang($produktId, $this->_lang);

        $this->view->nazwaProduktu = $produktI18n->getNazwa();

        $form = new Application_Form_ProduktZnakowanie($this->_entityManager);
        $this->view->form = $form;

        $znakowania = $this->_entityManager->getRepository('Entities\Produkt_Has_Znakowanie')->getByProduktId($produktId);
        $tablicaPopulate = array('id' => $produktId);

        foreach ($znakowania as $z) {
            $tablicaPopulate['znak_' . $z->getZnakowanieId()] = 1;
        }
        $form->populate($tablicaPopulate);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    $cmd = new Application_Model_Commands_ProduktZnakowanie($this->_entityManager, $_POST);
                    $result = $cmd->execute();
                    Moyoki_Debug::getType($result);

                    if ($result) {
                        $this->addMessageSuccess('metody znakowania zmienione', TRUE);
                    } else {
                        $this->addMessageError('Błąd przy zapisie metod znakowania', TRUE);
                    }
                    $this->_redirect('/' . $this->_lang . '/art/' . $produktId);
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function kategorieAction() {

        $this->view->headScript()->appendFile($this->view->baseURL('/js/produktKategorie.js'));

        $produktId = (int) $this->_request->getParam('id');

        $this->view->produktId = $produktId;
        if ($produktId < 1) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produktId);

        if (null === $produkt) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produktI18n = $this->_entityManager->getRepository('Entities\Produkt_i18n')->getByProduktIdAndLang($produktId, $this->_lang);

        $this->view->nazwaProduktu = $produktI18n->getNazwa();

        $form = new Application_Form_ProduktKategorie($this->_entityManager);
        $this->view->form = $form;

        $kategorie = $this->_entityManager->getRepository('Entities\Produkt_Has_Kategoria')->getByProduktId($produktId);

        $tabelaPopulate = array('id' => $produktId);
        $kategorieTabala = array();
        foreach ($kategorie as $k) {
            $tabelaPopulate['kat_' . $k->getKategorieId()] = 1;
            $kategorieTabala[$k->getKategorieId()] = $k->getKategorie()->getKategorieI18n()->first()->getNazwa();
        }

        $kategoriaGlowna = $this->_entityManager->getRepository('Entities\Kategoria')->getById($produkt->getKategoriaGlowna());
        if ($kategoriaGlowna instanceof Entities\Kategoria) {

            $this->view->kategoriaGlowna = $kategoriaGlowna->getKategorieI18n()->first()->getNazwa();
            if (!key_exists($kategoriaGlowna->getId(), $kategorieTabala)) {
                $this->view->kateriaGlownaNieIstnieje = TRUE;
            }
        }
        $form->populate($tabelaPopulate);

        $this->view->kategorieLista = $kategorieTabala;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    $cmd = new Application_Model_Commands_ProduktKategorie($this->_entityManager, $_POST);
                    $cmd->execute();
                    $this->addMessageSuccess('Kategorie zmienione', TRUE);
                    $this->_redirect('/' . $this->_lang . '/art/' . $produktId);
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        }
    }

    public function zmienAction() {

        $produktId = (int) $this->_request->getParam('id');

        $this->view->produktId = $produktId;
        if ($produktId < 1) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produktId);

        if (null === $produkt) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produktI18n = $this->_entityManager->getRepository('Entities\Produkt_i18n')->getByProduktIdAndLang($produktId, $this->_lang);

        $this->view->nazwaProduktu = $produktI18n->getNazwa();

        $form = new Application_Form_Produkt($this->_entityManager, 'zmien');

        /**
         * Kategorie Produktu
         */
        $kategorie = $this->_entityManager->getRepository('Entities\Produkt_Has_Kategoria')->getByProduktId($produktId);
        $kategorieTablica = array();
        foreach ($kategorie as $k) {
            $kategorieTablica[$k->getKategorie()->getId()] = $k->getKategorie()->getKategorieI18n()->first()->getNazwa();
        }

        $kategoriaGlowna = $this->_entityManager->getRepository('Entities\Kategoria')->getById($produkt->getKategoriaGlowna());
        if ($kategoriaGlowna instanceof Entities\Kategoria) {

            $kategorieTablica[$kategoriaGlowna->getId()] = $kategoriaGlowna->getKategorieI18n()->first()->getNazwa();
        }
        $katGlownaElement = $form->getElement('kategoria_glowna');
        $katGlownaElement->addMultiOptions($kategorieTablica);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $polecenie = new Application_Model_Commands_ProduktZmien($this->_entityManager, $_POST);
                $polecenie->execute();

                $this->addMessageSuccess('Informacje o produkcie zostały pomyślnie zmienione', TRUE);
                $this->_redirect('/' . $this->_lang . '/art/' . $produktId);
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {
            $form->populate(array(
                'id' => $produkt->getId(),
                'dystrybutorzy_id' => $produkt->getDystrybutorzyId(),
                'producenci_id' => $produkt->getProducenciId(),
                'kategoria_glowna' => $produkt->getKategoriaGlowna(),
                'nr_katalogowy' => $produkt->getNrKatalogowy(),
                'nr_producenta' => $produkt->getNrProducenta(),
                'identyfikator_producenta' => $produkt->getIdentyfikatorProducenta(),
                'widoczny' => $produkt->getWidoczny(),
                'nowosc' => $produkt->getNowosc(),
                'bestseller' => $produkt->getBestseller(),
                'promocja' => $produkt->getPromocja(),
                'model' => $produkt->getModel()
            ));
        }

        $this->view->form = $form;
    }

    public function tlumaczeniaAction() {

        $produktId = (int) $this->_request->getParam('id');

        $this->view->produktId = $produktId;

        if ($produktId < 1) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produktId);

        $produktI18n = $this->_entityManager->getRepository('Entities\Produkt_i18n')->getByProduktIdAndLang($produktId, $this->_lang);

        $this->view->nazwaProduktu = $produktI18n->getNazwa();

        if (null === $produkt) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $form = new Application_Form_ProduktNazwa();


        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {

                $polecenie = new Application_Model_Commands_ProduktNazwy($this->_entityManager, $this->getRequest()->getPost());
                $polecenie->execute();

                $this->addMessageSuccess('Informacje o produkcie zostały pomyślnie zmienione', TRUE);
                $this->_redirect('/' . $this->_lang . '/art/' . $produktId);
            }
            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } else {

            $nazwyTablica = array('id' => $produkt->getId());
            foreach ($produkt->getProduktyI18n() as $i18n) {
                $i18n->getJezyk();
                $nazwyTablica[$i18n->getJezyk()] = array(
                    'nazwa_' . $i18n->getJezyk() => $i18n->getNazwa(),
                    'opis_' . $i18n->getJezyk() => $i18n->getOpis(),
                    'seo_opis_' . $i18n->getJezyk() => $i18n->getSeoOpis()
                );
            }

            $form->populate($nazwyTablica);
        }

        $this->view->form = $form;
    }

    public function zdjeciaAction() {
        $produktId = (int) $this->_request->getParam('id');

        if ($produktId < 1) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        $produkt = $this->_entityManager->getRepository('Entities\Produkt')->getById($produktId);

        $produktI18n = $this->_entityManager->getRepository('Entities\Produkt_i18n')->getByProduktIdAndLang($produktId, $this->_lang);

        $this->view->produkt = $produkt;
        $this->view->nazwaProduktu = $produktI18n->getNazwa();
        $this->view->produktId = $produktId;

        if (null === $produkt) {
            $this->_redirect('/' . $this->_lang . '/Produkt');
        }

        //$produkt = new Entities\Produkt;
        $this->view->zdjecia = $produkt->getProduktyZdjecia();


        /**
         * Zmiana koloru zdjecia
         */
        $formKolory = new Application_Form_ProduktZdjecieZmienKolor();
        $formKolory->setAction($this->view->url(array('language' => 'pl', 'controller' => 'Ajax', 'action' => 'produktzdjeciekolor'), 'default', true));

        $nowykolor = $formKolory->getElement('nowykolor');

        // UPLOAD
        $this->view->uploadfile = true;
        $this->view->headScript()->appendFile($this->view->baseURL('/js/uploadProduktZdjecie.js'));

        $form = new Application_Form_ProduktZdjecie($produkt->getId());
        $form->setAction($this->view->url(array('language' => 'pl', 'controller' => 'Ajax', 'action' => 'produktzdjecie'), 'default', true));
        $form->populate(array('produkt_id' => $produktId));

        $kolorSelect = $form->getElement('kolor');

        foreach ($produkt->getProduktyDetale() as $detal) {
            if ($detal->getKolory() instanceof Entities\Kolor) {
                $kolorSelect->addMultiOption($detal->getKoloryId(), $detal->getKolory()->getNazwa() . '[' . $detal->getKolory()->getKod() . ']');
                $nowykolor->addMultiOption($detal->getKoloryId(), $detal->getKolory()->getNazwa() . '[' . $detal->getKolory()->getKod() . ']');
            }
        }

        $this->view->form = $form;
        $this->view->formKolory = $formKolory;



// Ustawiamy inne zdjecie glowne
        $glowne = (int) $this->getRequest()->getParam('glowne');
        $polecenie = new Application_Model_Commands_ZdjecieUstawGlowne($this->_entityManager, $glowne);
        $result = $polecenie->execute();

        if ($result) {
            $this->addMessageSuccess('Zdjęcie główne zostało ustawione', TRUE);
            $this->_redirect('/' . $this->_lang . '/Produkt/zdjecia/id/' . $produkt->getId());
        }
    }

}
