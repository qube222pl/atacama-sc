<?php

class MapowanieController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
        if (!$this->_acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_PRODUKTY)) {
            $this->_redirect('/' . $this->_lang . '/index/');
        }

        $this->view->adminView = TRUE;
    }

    public function indexAction() {

    }
    
    public function koloryAction() {
    
        $this->_mapowanie('Application_Form_MapowanieKolory', 'kolor', 'kolory');
        
    }
    
    public function kategorieAction() {
    
        $this->_mapowanie('Application_Form_MapowanieKategorie', 'kategoria', 'kategorie');
        
    }
    
    public function znakowaniaAction() {
    
        $this->_mapowanie('Application_Form_MapowanieZnakowania', 'znakowanie', 'znakowania');
        
    }
    
    public function producenciAction() {
    
        $this->_mapowanie('Application_Form_MapowanieProducenci', 'producent', 'producenci');
        
    }
    
    private function _mapowanie($formName, $option, $redirect) {
        
        $dystrybutorId = (int) $this->_request->getParam('id');

        if ($dystrybutorId < 1) {
            $this->_redirect('/' . $this->_lang . '/Import/ustawienia');
        }

        $dystrybutor = $this->_entityManager->getRepository('Entities\Dystrybutor')->getById($dystrybutorId);

        if (null === $dystrybutor) {
            $this->_redirect('/' . $this->_lang . '/Import/ustawienia');
        }

        $this->view->nazwaDystrybutora = $dystrybutor->getNazwa();
        $this->view->dystrybutorId = $dystrybutorId;

        $form = new $formName($this->_entityManager, $dystrybutorId);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($_POST)) {

                $cmd = new Application_Model_Commands_ImportMapowanie($this->_entityManager, $form->getValues(), $option, $dystrybutorId);
                $result = $cmd->execute();
                
                if($result === true) {
                    $this->addMessageSuccess('Mapowanie zapisano pomyślnie.', TRUE);
                    $this->_redirect('/' . $this->_lang . '/Mapowanie/' . $redirect . '/id/' . $dystrybutorId);
                } else {
                    $this->addMessageError('Wystąpił błąd podczas zapisywania mapowania.');
                    $form->populate($_POST);
                }
            }

            if ($form->isErrors()) {
                $form->populate($_POST);
            }
        } 

    }    
}
