<?php

class PdfController extends Atacama_Controller_Action {

    public function init() {
        parent::init();
    }

    public function indexAction() {
        // action body
    }

    public function generujAction() {
        $this->_helper->layout->disableLayout();

        $this->getHelper('viewRenderer')->setNoRender();

        $this->getResponse()->clearBody();
        $conf = Atacama_Config::getInstance();

        $complete = $this->getRequest()->getParam('complete');
        $url = Moyoki_Crypt::getInstance()->decrypt($this->getRequest()->getParam('key'));
        $sciezka_pliku = $conf->pdf->path;
        $nazwa_produktu = Moyoki_Crypt::getInstance()->decrypt($this->getRequest()->getParam('name'));

        $nazwa_pliku = Moyoki_Friendly::name(strtolower($nazwa_produktu)) . '.pdf';
        $cmd = "/usr/bin/xvfb-run /usr/bin/wkhtmltopdf  --margin-bottom 15 --footer-html " . $conf->appUrl . "pdf-footer.html " . $conf->appUrl . $url . " " . $sciezka_pliku . $nazwa_pliku . " 2>&1";
        $result = shell_exec($cmd);
        $wynik_generowania = trim(substr(trim($result), -5, 5));

        if ($wynik_generowania == 'Done') {

            $str = file_get_contents($sciezka_pliku . $nazwa_pliku);

            header('Content-Type: application/pdf');
            header('Content-Length: ' . strlen($str));
            header('Content-Disposition: inline; filename="' . $nazwa_pliku . '"');
            header('Cache-Control: private, max-age=0, must-revalidate');
            header('Pragma: public');
            ini_set('zlib.output_compression', '0');
            echo $str;

            if (isset($complete) && $complete == 'delete') {
                @unlink($sciezka_pliku . $nazwa_pliku);
            }
            //die($str);
        } else {
            echo 'Problem<br />generowania !';
        }
    }

}
