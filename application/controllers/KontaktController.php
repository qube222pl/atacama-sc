<?php

class KontaktController extends Atacama_Controller_Action
{

	public function init()
	{
		parent::init();
	}

	public function indexAction()
	{
		$strona = $this->_entityManager->getRepository('Entities\Strona')->pobierzPoKodzie('kontakt', $this->_lang);

		if ($strona instanceof Entities\Strona) {
			$this->view->content = Moyoki_Cms_Parser::init($this->_entityManager, $strona->getTresc());
			$this->view->headTitle($strona->getTytul());
		}
	}

}

