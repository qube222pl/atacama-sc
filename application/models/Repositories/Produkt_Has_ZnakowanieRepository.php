<?php

namespace Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * KategoriaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class Produkt_Has_ZnakowanieRepository extends EntityRepository
{

	public function kasujDlaProduktId($id)
	{

		$qb = $this->getEntityManager()->createQueryBuilder();
		
		$q = $qb->delete('Entities\Produkt_Has_Znakowanie', 'zp')
				->where('zp.produkty_id = :id')
				->setParameter(':id', (int) $id)
				->getQuery();

		return $q->execute();
	}
	
	public function getByProduktId($id)
	{
		$dql = 'select pz FROM Entities\Produkt_Has_Znakowanie pz where pz.produkty_id = :id';
		$query = $this->getEntityManager()->createQuery($dql)->setParameter('id', (int) $id);
		return $query->getResult();
	}
	
	public function getByZnakowanieId($id)
	{

		$dql = 'select pz FROM Entities\Produkt_Has_Znakowanie pz where pz.znakowanie_id = :id';
		$query = $this->getEntityManager()->createQuery($dql)->setParameter('id', (int) $id);
		return $query->getResult();
	}

    public function pobierzIdProduktow($idDystrybutora) {
        
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT DISTINCT phz.produkty_id id FROM produkty_has_znakowania phz INNER JOIN produkty p ON p.id = phz.produkty_id WHERE p.dystrybutorzy_id = " . $idDystrybutora;
        $znakowania = $conn->fetchAll($sql); 
        
        $ids = array();
        foreach ($znakowania as $znakowanie) {
            $ids[($znakowanie['id'])] = true;
        }
        
        return $ids;
    }
    
}