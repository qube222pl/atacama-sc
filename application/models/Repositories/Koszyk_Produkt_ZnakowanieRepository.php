<?php

namespace Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * KategoriaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class Koszyk_Produkt_ZnakowanieRepository extends EntityRepository {

    public function getById($id) {
        $dql = "SELECT k FROM Entities\Koszyk_Produkt_Znakowanie k WHERE k.id = :id";
        $query = $this->getEntityManager()->createQuery($dql)->setParameter('id', (int) $id);
        return $query->getOneOrNullResult();
    }

    public function zmien(\Entities\Koszyk_Produkt_Znakowanie $koszyk_produkt_znakowanie) {

        if ($koszyk_produkt_znakowanie instanceof \Entities\Koszyk_Produkt_Znakowanie) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $q = $qb->update('Entities\Koszyk_Produkt_Znakowanie', 'k')
                    ->set('k.ilosc_kolorow', ':ilosc_kolorow')
                    ->set('k.ilosc_sztuk', ':ilosc_sztuk')
                    ->set('k.cena', ':cena')
                    ->set('k.powtorzenie', ':powtorzenie')
                    ->set('k.przygotowalnia', ':przygotowalnia')
                    ->set('k.przygotowalnia_mnozenie', ':przygotowalnia_mnozenie')
                    ->where('k.id = :id')
                    ->setParameter('ilosc_kolorow', $koszyk_produkt_znakowanie->getIloscKolorow())
                    ->setParameter('cena', $koszyk_produkt_znakowanie->getCena())
                    ->setParameter('ilosc_sztuk', $koszyk_produkt_znakowanie->getIloscSztuk())
                    ->setParameter('powtorzenie', $koszyk_produkt_znakowanie->getPowtorzenie())
                    ->setParameter('przygotowalnia', $koszyk_produkt_znakowanie->getPrzygotowalnia())
                    ->setParameter('przygotowalnia_mnozenie', $koszyk_produkt_znakowanie->getPrzygotowalniaMnozenie())
                    ->setParameter('id', $koszyk_produkt_znakowanie->getId())
                    ->getQuery();

            return $q->execute();
        }
    }

    public function deleteById($id) {
        $qb = $this->_em->createQueryBuilder();

        $qb->delete('Entities\Koszyk_Produkt_Znakowanie', 'kp')
                ->where('kp.id = :id')
                ->setParameter('id', (int) $id);

        return $qb->getQuery()->execute();
    }

}
