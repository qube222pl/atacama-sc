<?php

namespace Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * BannerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BannerRepository extends EntityRepository {

    public function getById($id) {
        $dql = 'SELECT b from Entities\Banner b where b.id = :id';
        $query = $this->getEntityManager()->createQuery($dql)->setParameter('id', (int) $id);
        return $query->getOneOrNullResult();
    }

    public function pobierzWidoczne($lang = 'pl') {
        $dql = 'SELECT b from Entities\Banner b WHERE b.jezyk = :lang AND b.widoczny = 1 ORDER BY b.data_dodania DESC';
        $query = $this->getEntityManager()
                ->createQuery($dql)
                ->setParameter('lang', $lang);
        return $query->getResult();
    }

    public function orderByDataUtworzenia($kierunek = 'DESC') {
        $qb = $this->_em->createQueryBuilder();

        $qb->select(array('b'))
                ->from('Entities\Banner', 'b')
                ->orderBy('b.data_dodania', $kierunek);

        return $qb->getQuery()->getResult();
    }

    public function zmien(\Entities\Banner $banner) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $q = $qb->update('Entities\Banner', 'b')
                ->set('b.nazwa', ':nazwa')
                ->set('b.opis', ':opis')
                ->set('b.jezyk', ':jezyk')
                ->set('b.plik', ':plik')
                ->set('b.link', ':link')
                ->set('b.okno', ':okno')
                ->set('b.typ', ':typ')
                ->set('b.widoczny', ':widoczny')
                ->where('b.id = :id')
                ->setParameter('nazwa', $banner->getNazwa())
                ->setParameter('opis', $banner->getOpis())
                ->setParameter('jezyk', $banner->getJezyk())
                ->setParameter('plik', $banner->getPlik())
                ->setParameter('link', $banner->getLink())
                ->setParameter('okno', $banner->getOkno())
                ->setParameter('typ', $banner->getTyp())
                ->setParameter('widoczny', $banner->getWidoczny())
                ->setParameter('id', $banner->getId())
                ->getQuery();
        return $q->execute();
    }

    public function deleteById($id) {
        $qb = $this->_em->createQueryBuilder();

        $qb->delete('Entities\Banner', 'b')
                ->where('b.id = :id')
                ->setParameter('id', (int) $id);

        return $qb->getQuery()->execute();
    }

}
