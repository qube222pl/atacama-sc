<?php

namespace Entities;

/**
 * Producent
 */
class Producent
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var integer
     */
    private $widoczny;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $producenci_i18n;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $producenci_produkty;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->producenci_i18n = new \Doctrine\Common\Collections\ArrayCollection();
        $this->producenci_produkty = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Producent
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Producent
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set widoczny
     *
     * @param integer $widoczny
     *
     * @return Producent
     */
    public function setWidoczny($widoczny)
    {
        $this->widoczny = $widoczny;
    
        return $this;
    }

    /**
     * Get widoczny
     *
     * @return integer
     */
    public function getWidoczny()
    {
        return $this->widoczny;
    }

    /**
     * Add producenciI18n
     *
     * @param \Entities\Producent_i18n $producenciI18n
     *
     * @return Producent
     */
    public function addProducenciI18n(\Entities\Producent_i18n $producenciI18n)
    {
        $this->producenci_i18n[] = $producenciI18n;
    
        return $this;
    }

    /**
     * Remove producenciI18n
     *
     * @param \Entities\Producent_i18n $producenciI18n
     */
    public function removeProducenciI18n(\Entities\Producent_i18n $producenciI18n)
    {
        $this->producenci_i18n->removeElement($producenciI18n);
    }

    /**
     * Get producenciI18n
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducenciI18n()
    {
        return $this->producenci_i18n;
    }

    /**
     * Add producenciProdukty
     *
     * @param \Entities\Producent_Produkt $producenciProdukty
     *
     * @return Producent
     */
    public function addProducenciProdukty(\Entities\Producent_Produkt $producenciProdukty)
    {
        $this->producenci_produkty[] = $producenciProdukty;
    
        return $this;
    }

    /**
     * Remove producenciProdukty
     *
     * @param \Entities\Producent_Produkt $producenciProdukty
     */
    public function removeProducenciProdukty(\Entities\Producent_Produkt $producenciProdukty)
    {
        $this->producenci_produkty->removeElement($producenciProdukty);
    }

    /**
     * Get producenciProdukty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducenciProdukty()
    {
        return $this->producenci_produkty;
    }
}

