<?php

namespace Entities;

/**
 * Galeria_Zdjecie
 */
class Galeria_Zdjecie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $galerie_id;

    /**
     * @var string
     */
    private $plik;

    /**
     * @var \Entities\Galeria
     */
    private $galerie;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set galerieId
     *
     * @param integer $galerieId
     *
     * @return Galeria_Zdjecie
     */
    public function setGalerieId($galerieId)
    {
        $this->galerie_id = $galerieId;
    
        return $this;
    }

    /**
     * Get galerieId
     *
     * @return integer
     */
    public function getGalerieId()
    {
        return $this->galerie_id;
    }

    /**
     * Set plik
     *
     * @param string $plik
     *
     * @return Galeria_Zdjecie
     */
    public function setPlik($plik)
    {
        $this->plik = $plik;
    
        return $this;
    }

    /**
     * Get plik
     *
     * @return string
     */
    public function getPlik()
    {
        return $this->plik;
    }

    /**
     * Set galerie
     *
     * @param \Entities\Galeria $galerie
     *
     * @return Galeria_Zdjecie
     */
    public function setGalerie(\Entities\Galeria $galerie = null)
    {
        $this->galerie = $galerie;
    
        return $this;
    }

    /**
     * Get galerie
     *
     * @return \Entities\Galeria
     */
    public function getGalerie()
    {
        return $this->galerie;
    }
}

