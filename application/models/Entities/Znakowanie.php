<?php

namespace Entities;

/**
 * Znakowanie
 */
class Znakowanie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $kod;

    /**
     * @var integer
     */
    private $kolory;

    /**
     * @var string
     */
    private $ryczalt;

    /**
     * @var string
     */
    private $przygotowalnia;

    /**
     * @var string
     */
    private $powtorzenie;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $produkty_has_znakowanie;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $znakowania_rodzaje;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $znakowania_i18n;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produkty_has_znakowanie = new \Doctrine\Common\Collections\ArrayCollection();
        $this->znakowania_rodzaje = new \Doctrine\Common\Collections\ArrayCollection();
        $this->znakowania_i18n = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Znakowanie
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set kod
     *
     * @param string $kod
     *
     * @return Znakowanie
     */
    public function setKod($kod)
    {
        $this->kod = $kod;
    
        return $this;
    }

    /**
     * Get kod
     *
     * @return string
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set kolory
     *
     * @param integer $kolory
     *
     * @return Znakowanie
     */
    public function setKolory($kolory)
    {
        $this->kolory = $kolory;
    
        return $this;
    }

    /**
     * Get kolory
     *
     * @return integer
     */
    public function getKolory()
    {
        return $this->kolory;
    }

    /**
     * Set ryczalt
     *
     * @param string $ryczalt
     *
     * @return Znakowanie
     */
    public function setRyczalt($ryczalt)
    {
        $this->ryczalt = $ryczalt;
    
        return $this;
    }

    /**
     * Get ryczalt
     *
     * @return string
     */
    public function getRyczalt()
    {
        return $this->ryczalt;
    }

    /**
     * Set przygotowalnia
     *
     * @param string $przygotowalnia
     *
     * @return Znakowanie
     */
    public function setPrzygotowalnia($przygotowalnia)
    {
        $this->przygotowalnia = $przygotowalnia;
    
        return $this;
    }

    /**
     * Get przygotowalnia
     *
     * @return string
     */
    public function getPrzygotowalnia()
    {
        return $this->przygotowalnia;
    }

    /**
     * Set powtorzenie
     *
     * @param string $powtorzenie
     *
     * @return Znakowanie
     */
    public function setPowtorzenie($powtorzenie)
    {
        $this->powtorzenie = $powtorzenie;
    
        return $this;
    }

    /**
     * Get powtorzenie
     *
     * @return string
     */
    public function getPowtorzenie()
    {
        return $this->powtorzenie;
    }

    /**
     * Add produktyHasZnakowanie
     *
     * @param \Entities\Produkt_Has_Znakowanie $produktyHasZnakowanie
     *
     * @return Znakowanie
     */
    public function addProduktyHasZnakowanie(\Entities\Produkt_Has_Znakowanie $produktyHasZnakowanie)
    {
        $this->produkty_has_znakowanie[] = $produktyHasZnakowanie;
    
        return $this;
    }

    /**
     * Remove produktyHasZnakowanie
     *
     * @param \Entities\Produkt_Has_Znakowanie $produktyHasZnakowanie
     */
    public function removeProduktyHasZnakowanie(\Entities\Produkt_Has_Znakowanie $produktyHasZnakowanie)
    {
        $this->produkty_has_znakowanie->removeElement($produktyHasZnakowanie);
    }

    /**
     * Get produktyHasZnakowanie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduktyHasZnakowanie()
    {
        return $this->produkty_has_znakowanie;
    }

    /**
     * Add znakowaniaRodzaje
     *
     * @param \Entities\Znakowanie_Rodzaj $znakowaniaRodzaje
     *
     * @return Znakowanie
     */
    public function addZnakowaniaRodzaje(\Entities\Znakowanie_Rodzaj $znakowaniaRodzaje)
    {
        $this->znakowania_rodzaje[] = $znakowaniaRodzaje;
    
        return $this;
    }

    /**
     * Remove znakowaniaRodzaje
     *
     * @param \Entities\Znakowanie_Rodzaj $znakowaniaRodzaje
     */
    public function removeZnakowaniaRodzaje(\Entities\Znakowanie_Rodzaj $znakowaniaRodzaje)
    {
        $this->znakowania_rodzaje->removeElement($znakowaniaRodzaje);
    }

    /**
     * Get znakowaniaRodzaje
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZnakowaniaRodzaje()
    {
        return $this->znakowania_rodzaje;
    }

    /**
     * Add znakowaniaI18n
     *
     * @param \Entities\Znakowanie_i18n $znakowaniaI18n
     *
     * @return Znakowanie
     */
    public function addZnakowaniaI18n(\Entities\Znakowanie_i18n $znakowaniaI18n)
    {
        $this->znakowania_i18n[] = $znakowaniaI18n;
    
        return $this;
    }

    /**
     * Remove znakowaniaI18n
     *
     * @param \Entities\Znakowanie_i18n $znakowaniaI18n
     */
    public function removeZnakowaniaI18n(\Entities\Znakowanie_i18n $znakowaniaI18n)
    {
        $this->znakowania_i18n->removeElement($znakowaniaI18n);
    }

    /**
     * Get znakowaniaI18n
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZnakowaniaI18n()
    {
        return $this->znakowania_i18n;
    }
}

