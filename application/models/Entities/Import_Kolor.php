<?php

namespace Entities;

/**
 * Import_Kolor
 */
class Import_Kolor
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dystrybutorzy_id;

    /**
     * @var integer
     */
    private $kolory_id;

    /**
     * @var string
     */
    private $kolor_dystr_id;

    /**
     * @var string
     */
    private $kolor_dystr_nazwa;

    /**
     * @var \Entities\Dystrybutor
     */
    private $dystrybutorzy;

    /**
     * @var \Entities\Kolor
     */
    private $kolory;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dystrybutorzyId
     *
     * @param integer $dystrybutorzyId
     *
     * @return Import_Kolor
     */
    public function setDystrybutorzyId($dystrybutorzyId)
    {
        $this->dystrybutorzy_id = $dystrybutorzyId;
    
        return $this;
    }

    /**
     * Get dystrybutorzyId
     *
     * @return integer
     */
    public function getDystrybutorzyId()
    {
        return $this->dystrybutorzy_id;
    }

    /**
     * Set koloryId
     *
     * @param integer $koloryId
     *
     * @return Import_Kolor
     */
    public function setKoloryId($koloryId)
    {
        $this->kolory_id = $koloryId;
    
        return $this;
    }

    /**
     * Get koloryId
     *
     * @return integer
     */
    public function getKoloryId()
    {
        return $this->kolory_id;
    }

    /**
     * Set kolorDystrId
     *
     * @param string $kolorDystrId
     *
     * @return Import_Kolor
     */
    public function setKolorDystrId($kolorDystrId)
    {
        $this->kolor_dystr_id = $kolorDystrId;
    
        return $this;
    }

    /**
     * Get kolorDystrId
     *
     * @return string
     */
    public function getKolorDystrId()
    {
        return $this->kolor_dystr_id;
    }

    /**
     * Set kolorDystrNazwa
     *
     * @param string $kolorDystrNazwa
     *
     * @return Import_Kolor
     */
    public function setKolorDystrNazwa($kolorDystrNazwa)
    {
        $this->kolor_dystr_nazwa = $kolorDystrNazwa;
    
        return $this;
    }

    /**
     * Get kolorDystrNazwa
     *
     * @return string
     */
    public function getKolorDystrNazwa()
    {
        return $this->kolor_dystr_nazwa;
    }

    /**
     * Set dystrybutorzy
     *
     * @param \Entities\Dystrybutor $dystrybutorzy
     *
     * @return Import_Kolor
     */
    public function setDystrybutorzy(\Entities\Dystrybutor $dystrybutorzy = null)
    {
        $this->dystrybutorzy = $dystrybutorzy;
    
        return $this;
    }

    /**
     * Get dystrybutorzy
     *
     * @return \Entities\Dystrybutor
     */
    public function getDystrybutorzy()
    {
        return $this->dystrybutorzy;
    }

    /**
     * Set kolory
     *
     * @param \Entities\Kolor $kolory
     *
     * @return Import_Kolor
     */
    public function setKolory(\Entities\Kolor $kolory = null)
    {
        $this->kolory = $kolory;
    
        return $this;
    }

    /**
     * Get kolory
     *
     * @return \Entities\Kolor
     */
    public function getKolory()
    {
        return $this->kolory;
    }
}

