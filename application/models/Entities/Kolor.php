<?php

namespace Entities;

/**
 * Kolor
 */
class Kolor
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $kod;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var integer
     */
    private $grupy_id;

    /**
     * @var \Entities\Kolor_Grupa
     */
    private $kolory_grupy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kod
     *
     * @param string $kod
     *
     * @return Kolor
     */
    public function setKod($kod)
    {
        $this->kod = $kod;
    
        return $this;
    }

    /**
     * Get kod
     *
     * @return string
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Kolor
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set grupyId
     *
     * @param integer $grupyId
     *
     * @return Kolor
     */
    public function setGrupyId($grupyId)
    {
        $this->grupy_id = $grupyId;
    
        return $this;
    }

    /**
     * Get grupyId
     *
     * @return integer
     */
    public function getGrupyId()
    {
        return $this->grupy_id;
    }

    /**
     * Set koloryGrupy
     *
     * @param \Entities\Kolor_Grupa $koloryGrupy
     *
     * @return Kolor
     */
    public function setKoloryGrupy(\Entities\Kolor_Grupa $koloryGrupy = null)
    {
        $this->kolory_grupy = $koloryGrupy;
    
        return $this;
    }

    /**
     * Get koloryGrupy
     *
     * @return \Entities\Kolor_Grupa
     */
    public function getKoloryGrupy()
    {
        return $this->kolory_grupy;
    }
}

