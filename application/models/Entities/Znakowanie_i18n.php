<?php

namespace Entities;

/**
 * Znakowanie_i18n
 */
class Znakowanie_i18n
{
    /**
     * @var integer
     */
    private $znakowania_id;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var \Entities\Znakowanie
     */
    private $znakowania;


    /**
     * Set znakowaniaId
     *
     * @param integer $znakowaniaId
     *
     * @return Znakowanie_i18n
     */
    public function setZnakowaniaId($znakowaniaId)
    {
        $this->znakowania_id = $znakowaniaId;
    
        return $this;
    }

    /**
     * Get znakowaniaId
     *
     * @return integer
     */
    public function getZnakowaniaId()
    {
        return $this->znakowania_id;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return Znakowanie_i18n
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Znakowanie_i18n
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set znakowania
     *
     * @param \Entities\Znakowanie $znakowania
     *
     * @return Znakowanie_i18n
     */
    public function setZnakowania(\Entities\Znakowanie $znakowania = null)
    {
        $this->znakowania = $znakowania;
    
        return $this;
    }

    /**
     * Get znakowania
     *
     * @return \Entities\Znakowanie
     */
    public function getZnakowania()
    {
        return $this->znakowania;
    }
}

