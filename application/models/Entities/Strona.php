<?php

namespace Entities;

/**
 * Strona
 */
class Strona
{
    /**
     * @var string
     */
    private $kod;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var string
     */
    private $tytul;

    /**
     * @var string
     */
    private $tresc;

    /**
     * @var string
     */
    private $slowa_kluczowe;

    /**
     * @var string
     */
    private $opis;


    /**
     * Set kod
     *
     * @param string $kod
     *
     * @return Strona
     */
    public function setKod($kod)
    {
        $this->kod = $kod;
    
        return $this;
    }

    /**
     * Get kod
     *
     * @return string
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return Strona
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set tytul
     *
     * @param string $tytul
     *
     * @return Strona
     */
    public function setTytul($tytul)
    {
        $this->tytul = $tytul;
    
        return $this;
    }

    /**
     * Get tytul
     *
     * @return string
     */
    public function getTytul()
    {
        return $this->tytul;
    }

    /**
     * Set tresc
     *
     * @param string $tresc
     *
     * @return Strona
     */
    public function setTresc($tresc)
    {
        $this->tresc = $tresc;
    
        return $this;
    }

    /**
     * Get tresc
     *
     * @return string
     */
    public function getTresc()
    {
        return $this->tresc;
    }

    /**
     * Set slowaKluczowe
     *
     * @param string $slowaKluczowe
     *
     * @return Strona
     */
    public function setSlowaKluczowe($slowaKluczowe)
    {
        $this->slowa_kluczowe = $slowaKluczowe;
    
        return $this;
    }

    /**
     * Get slowaKluczowe
     *
     * @return string
     */
    public function getSlowaKluczowe()
    {
        return $this->slowa_kluczowe;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Strona
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }
}

