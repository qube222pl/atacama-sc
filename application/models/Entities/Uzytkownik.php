<?php

namespace Entities;

/**
 * Uzytkownik
 */
class Uzytkownik
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $firmy_id;

    /**
     * @var string
     */
    private $imie;

    /**
     * @var string
     */
    private $nazwisko;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $haslo;

    /**
     * @var string
     */
    private $telefon;

    /**
     * @var integer
     */
    private $rola;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $data_rejestracji;

    /**
     * @var \DateTime
     */
    private $data_rodo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $koszyki;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $korporacyjni_uzytkownicy;

    /**
     * @var \Entities\Firma
     */
    private $firmy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->koszyki = new \Doctrine\Common\Collections\ArrayCollection();
        $this->korporacyjni_uzytkownicy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firmyId
     *
     * @param integer $firmyId
     *
     * @return Uzytkownik
     */
    public function setFirmyId($firmyId)
    {
        $this->firmy_id = $firmyId;
    
        return $this;
    }

    /**
     * Get firmyId
     *
     * @return integer
     */
    public function getFirmyId()
    {
        return $this->firmy_id;
    }

    /**
     * Set imie
     *
     * @param string $imie
     *
     * @return Uzytkownik
     */
    public function setImie($imie)
    {
        $this->imie = $imie;
    
        return $this;
    }

    /**
     * Get imie
     *
     * @return string
     */
    public function getImie()
    {
        return $this->imie;
    }

    /**
     * Set nazwisko
     *
     * @param string $nazwisko
     *
     * @return Uzytkownik
     */
    public function setNazwisko($nazwisko)
    {
        $this->nazwisko = $nazwisko;
    
        return $this;
    }

    /**
     * Get nazwisko
     *
     * @return string
     */
    public function getNazwisko()
    {
        return $this->nazwisko;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Uzytkownik
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set haslo
     *
     * @param string $haslo
     *
     * @return Uzytkownik
     */
    public function setHaslo($haslo)
    {
        $this->haslo = $haslo;
    
        return $this;
    }

    /**
     * Get haslo
     *
     * @return string
     */
    public function getHaslo()
    {
        return $this->haslo;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     *
     * @return Uzytkownik
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    
        return $this;
    }

    /**
     * Get telefon
     *
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set rola
     *
     * @param integer $rola
     *
     * @return Uzytkownik
     */
    public function setRola($rola)
    {
        $this->rola = $rola;
    
        return $this;
    }

    /**
     * Get rola
     *
     * @return integer
     */
    public function getRola()
    {
        return $this->rola;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return Uzytkownik
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dataRejestracji
     *
     * @param \DateTime $dataRejestracji
     *
     * @return Uzytkownik
     */
    public function setDataRejestracji($dataRejestracji)
    {
        $this->data_rejestracji = $dataRejestracji;
    
        return $this;
    }

    /**
     * Get dataRejestracji
     *
     * @return \DateTime
     */
    public function getDataRejestracji()
    {
        return $this->data_rejestracji;
    }

    /**
     * Set dataRodo
     *
     * @param \DateTime $dataRodo
     *
     * @return Uzytkownik
     */
    public function setDataRodo($dataRodo)
    {
        $this->data_rodo = $dataRodo;
    
        return $this;
    }

    /**
     * Get dataRodo
     *
     * @return \DateTime
     */
    public function getDataRodo()
    {
        return $this->data_rodo;
    }

    /**
     * Add koszyki
     *
     * @param \Entities\Koszyk $koszyki
     *
     * @return Uzytkownik
     */
    public function addKoszyki(\Entities\Koszyk $koszyki)
    {
        $this->koszyki[] = $koszyki;
    
        return $this;
    }

    /**
     * Remove koszyki
     *
     * @param \Entities\Koszyk $koszyki
     */
    public function removeKoszyki(\Entities\Koszyk $koszyki)
    {
        $this->koszyki->removeElement($koszyki);
    }

    /**
     * Get koszyki
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKoszyki()
    {
        return $this->koszyki;
    }

    /**
     * Add korporacyjniUzytkownicy
     *
     * @param \Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy
     *
     * @return Uzytkownik
     */
    public function addKorporacyjniUzytkownicy(\Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy)
    {
        $this->korporacyjni_uzytkownicy[] = $korporacyjniUzytkownicy;
    
        return $this;
    }

    /**
     * Remove korporacyjniUzytkownicy
     *
     * @param \Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy
     */
    public function removeKorporacyjniUzytkownicy(\Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy)
    {
        $this->korporacyjni_uzytkownicy->removeElement($korporacyjniUzytkownicy);
    }

    /**
     * Get korporacyjniUzytkownicy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKorporacyjniUzytkownicy()
    {
        return $this->korporacyjni_uzytkownicy;
    }

    /**
     * Set firmy
     *
     * @param \Entities\Firma $firmy
     *
     * @return Uzytkownik
     */
    public function setFirmy(\Entities\Firma $firmy = null)
    {
        $this->firmy = $firmy;
    
        return $this;
    }

    /**
     * Get firmy
     *
     * @return \Entities\Firma
     */
    public function getFirmy()
    {
        return $this->firmy;
    }
}

