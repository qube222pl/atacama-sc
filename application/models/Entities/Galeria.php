<?php

namespace Entities;

/**
 * Galeria
 */
class Galeria
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dataUtworzenia;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var boolean
     */
    private $widoczna;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $galerie_zdjecia;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->galerie_zdjecia = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataUtworzenia
     *
     * @param \DateTime $dataUtworzenia
     *
     * @return Galeria
     */
    public function setDataUtworzenia($dataUtworzenia)
    {
        $this->dataUtworzenia = $dataUtworzenia;
    
        return $this;
    }

    /**
     * Get dataUtworzenia
     *
     * @return \DateTime
     */
    public function getDataUtworzenia()
    {
        return $this->dataUtworzenia;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Galeria
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Galeria
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set widoczna
     *
     * @param boolean $widoczna
     *
     * @return Galeria
     */
    public function setWidoczna($widoczna)
    {
        $this->widoczna = $widoczna;
    
        return $this;
    }

    /**
     * Get widoczna
     *
     * @return boolean
     */
    public function getWidoczna()
    {
        return $this->widoczna;
    }

    /**
     * Add galerieZdjecium
     *
     * @param \Entities\Galeria_Zdjecie $galerieZdjecium
     *
     * @return Galeria
     */
    public function addGalerieZdjecium(\Entities\Galeria_Zdjecie $galerieZdjecium)
    {
        $this->galerie_zdjecia[] = $galerieZdjecium;
    
        return $this;
    }

    /**
     * Remove galerieZdjecium
     *
     * @param \Entities\Galeria_Zdjecie $galerieZdjecium
     */
    public function removeGalerieZdjecium(\Entities\Galeria_Zdjecie $galerieZdjecium)
    {
        $this->galerie_zdjecia->removeElement($galerieZdjecium);
    }

    /**
     * Get galerieZdjecia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGalerieZdjecia()
    {
        return $this->galerie_zdjecia;
    }
}

