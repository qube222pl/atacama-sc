<?php

namespace Entities;

/**
 * Producent_Produkt
 */
class Producent_Produkt
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $producenci_id;

    /**
     * @var integer
     */
    private $produkty_id;

    /**
     * @var \Entities\Producent
     */
    private $producenci;

    /**
     * @var \Entities\Produkt
     */
    private $produkty;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producenciId
     *
     * @param integer $producenciId
     *
     * @return Producent_Produkt
     */
    public function setProducenciId($producenciId)
    {
        $this->producenci_id = $producenciId;
    
        return $this;
    }

    /**
     * Get producenciId
     *
     * @return integer
     */
    public function getProducenciId()
    {
        return $this->producenci_id;
    }

    /**
     * Set produktyId
     *
     * @param integer $produktyId
     *
     * @return Producent_Produkt
     */
    public function setProduktyId($produktyId)
    {
        $this->produkty_id = $produktyId;
    
        return $this;
    }

    /**
     * Get produktyId
     *
     * @return integer
     */
    public function getProduktyId()
    {
        return $this->produkty_id;
    }

    /**
     * Set producenci
     *
     * @param \Entities\Producent $producenci
     *
     * @return Producent_Produkt
     */
    public function setProducenci(\Entities\Producent $producenci = null)
    {
        $this->producenci = $producenci;
    
        return $this;
    }

    /**
     * Get producenci
     *
     * @return \Entities\Producent
     */
    public function getProducenci()
    {
        return $this->producenci;
    }

    /**
     * Set produkty
     *
     * @param \Entities\Produkt $produkty
     *
     * @return Producent_Produkt
     */
    public function setProdukty(\Entities\Produkt $produkty = null)
    {
        $this->produkty = $produkty;
    
        return $this;
    }

    /**
     * Get produkty
     *
     * @return \Entities\Produkt
     */
    public function getProdukty()
    {
        return $this->produkty;
    }
}

