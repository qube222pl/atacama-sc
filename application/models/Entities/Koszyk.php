<?php

namespace Entities;

/**
 * Koszyk
 */
class Koszyk
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $uzytkownicy_id;

    /**
     * @var integer
     */
    private $firmy_id;

    /**
     * @var \DateTime
     */
    private $data_utworzenia;

    /**
     * @var \DateTime
     */
    private $data_zamowienia;

    /**
     * @var integer
     */
    private $adres_fv_id;

    /**
     * @var integer
     */
    private $adresy_dostawy_id;

    /**
     * @var string
     */
    private $nr_zamowienia;

    /**
     * @var integer
     */
    private $id_zamowienia;

    /**
     * @var string
     */
    private $marza;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var integer
     */
    private $ilosc_kartonow;

    /**
     * @var integer
     */
    private $vies;

    /**
     * @var integer
     */
    private $oferta;

    /**
     * @var string
     */
    private $oferta_plik;

    /**
     * @var \DateTime
     */
    private $data_dostawy;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $koszyki_produkty;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $koszyki_uwagi;

    /**
     * @var \Entities\Uzytkownik
     */
    private $uzytkownicy;

    /**
     * @var \Entities\Firma
     */
    private $firmy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->koszyki_produkty = new \Doctrine\Common\Collections\ArrayCollection();
        $this->koszyki_uwagi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uzytkownicyId
     *
     * @param integer $uzytkownicyId
     *
     * @return Koszyk
     */
    public function setUzytkownicyId($uzytkownicyId)
    {
        $this->uzytkownicy_id = $uzytkownicyId;
    
        return $this;
    }

    /**
     * Get uzytkownicyId
     *
     * @return integer
     */
    public function getUzytkownicyId()
    {
        return $this->uzytkownicy_id;
    }

    /**
     * Set firmyId
     *
     * @param integer $firmyId
     *
     * @return Koszyk
     */
    public function setFirmyId($firmyId)
    {
        $this->firmy_id = $firmyId;
    
        return $this;
    }

    /**
     * Get firmyId
     *
     * @return integer
     */
    public function getFirmyId()
    {
        return $this->firmy_id;
    }

    /**
     * Set dataUtworzenia
     *
     * @param \DateTime $dataUtworzenia
     *
     * @return Koszyk
     */
    public function setDataUtworzenia($dataUtworzenia)
    {
        $this->data_utworzenia = $dataUtworzenia;
    
        return $this;
    }

    /**
     * Get dataUtworzenia
     *
     * @return \DateTime
     */
    public function getDataUtworzenia()
    {
        return $this->data_utworzenia;
    }

    /**
     * Set dataZamowienia
     *
     * @param \DateTime $dataZamowienia
     *
     * @return Koszyk
     */
    public function setDataZamowienia($dataZamowienia)
    {
        $this->data_zamowienia = $dataZamowienia;
    
        return $this;
    }

    /**
     * Get dataZamowienia
     *
     * @return \DateTime
     */
    public function getDataZamowienia()
    {
        return $this->data_zamowienia;
    }

    /**
     * Set adresFvId
     *
     * @param integer $adresFvId
     *
     * @return Koszyk
     */
    public function setAdresFvId($adresFvId)
    {
        $this->adres_fv_id = $adresFvId;
    
        return $this;
    }

    /**
     * Get adresFvId
     *
     * @return integer
     */
    public function getAdresFvId()
    {
        return $this->adres_fv_id;
    }

    /**
     * Set adresyDostawyId
     *
     * @param integer $adresyDostawyId
     *
     * @return Koszyk
     */
    public function setAdresyDostawyId($adresyDostawyId)
    {
        $this->adresy_dostawy_id = $adresyDostawyId;
    
        return $this;
    }

    /**
     * Get adresyDostawyId
     *
     * @return integer
     */
    public function getAdresyDostawyId()
    {
        return $this->adresy_dostawy_id;
    }

    /**
     * Set nrZamowienia
     *
     * @param string $nrZamowienia
     *
     * @return Koszyk
     */
    public function setNrZamowienia($nrZamowienia)
    {
        $this->nr_zamowienia = $nrZamowienia;
    
        return $this;
    }

    /**
     * Get nrZamowienia
     *
     * @return string
     */
    public function getNrZamowienia()
    {
        return $this->nr_zamowienia;
    }

    /**
     * Set idZamowienia
     *
     * @param integer $idZamowienia
     *
     * @return Koszyk
     */
    public function setIdZamowienia($idZamowienia)
    {
        $this->id_zamowienia = $idZamowienia;
    
        return $this;
    }

    /**
     * Get idZamowienia
     *
     * @return integer
     */
    public function getIdZamowienia()
    {
        return $this->id_zamowienia;
    }

    /**
     * Set marza
     *
     * @param string $marza
     *
     * @return Koszyk
     */
    public function setMarza($marza)
    {
        $this->marza = $marza;
    
        return $this;
    }

    /**
     * Get marza
     *
     * @return string
     */
    public function getMarza()
    {
        return $this->marza;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Koszyk
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set iloscKartonow
     *
     * @param integer $iloscKartonow
     *
     * @return Koszyk
     */
    public function setIloscKartonow($iloscKartonow)
    {
        $this->ilosc_kartonow = $iloscKartonow;
    
        return $this;
    }

    /**
     * Get iloscKartonow
     *
     * @return integer
     */
    public function getIloscKartonow()
    {
        return $this->ilosc_kartonow;
    }

    /**
     * Set vies
     *
     * @param integer $vies
     *
     * @return Koszyk
     */
    public function setVies($vies)
    {
        $this->vies = $vies;
    
        return $this;
    }

    /**
     * Get vies
     *
     * @return integer
     */
    public function getVies()
    {
        return $this->vies;
    }

    /**
     * Set oferta
     *
     * @param integer $oferta
     *
     * @return Koszyk
     */
    public function setOferta($oferta)
    {
        $this->oferta = $oferta;
    
        return $this;
    }

    /**
     * Get oferta
     *
     * @return integer
     */
    public function getOferta()
    {
        return $this->oferta;
    }

    /**
     * Set ofertaPlik
     *
     * @param string $ofertaPlik
     *
     * @return Koszyk
     */
    public function setOfertaPlik($ofertaPlik)
    {
        $this->oferta_plik = $ofertaPlik;
    
        return $this;
    }

    /**
     * Get ofertaPlik
     *
     * @return string
     */
    public function getOfertaPlik()
    {
        return $this->oferta_plik;
    }

    /**
     * Set dataDostawy
     *
     * @param \DateTime $dataDostawy
     *
     * @return Koszyk
     */
    public function setDataDostawy($dataDostawy)
    {
        $this->data_dostawy = $dataDostawy;
    
        return $this;
    }

    /**
     * Get dataDostawy
     *
     * @return \DateTime
     */
    public function getDataDostawy()
    {
        return $this->data_dostawy;
    }

    /**
     * Add koszykiProdukty
     *
     * @param \Entities\Koszyk_Produkt $koszykiProdukty
     *
     * @return Koszyk
     */
    public function addKoszykiProdukty(\Entities\Koszyk_Produkt $koszykiProdukty)
    {
        $this->koszyki_produkty[] = $koszykiProdukty;
    
        return $this;
    }

    /**
     * Remove koszykiProdukty
     *
     * @param \Entities\Koszyk_Produkt $koszykiProdukty
     */
    public function removeKoszykiProdukty(\Entities\Koszyk_Produkt $koszykiProdukty)
    {
        $this->koszyki_produkty->removeElement($koszykiProdukty);
    }

    /**
     * Get koszykiProdukty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKoszykiProdukty()
    {
        return $this->koszyki_produkty;
    }

    /**
     * Add koszykiUwagi
     *
     * @param \Entities\Koszyk_Uwaga $koszykiUwagi
     *
     * @return Koszyk
     */
    public function addKoszykiUwagi(\Entities\Koszyk_Uwaga $koszykiUwagi)
    {
        $this->koszyki_uwagi[] = $koszykiUwagi;
    
        return $this;
    }

    /**
     * Remove koszykiUwagi
     *
     * @param \Entities\Koszyk_Uwaga $koszykiUwagi
     */
    public function removeKoszykiUwagi(\Entities\Koszyk_Uwaga $koszykiUwagi)
    {
        $this->koszyki_uwagi->removeElement($koszykiUwagi);
    }

    /**
     * Get koszykiUwagi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKoszykiUwagi()
    {
        return $this->koszyki_uwagi;
    }

    /**
     * Set uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     *
     * @return Koszyk
     */
    public function setUzytkownicy(\Entities\Uzytkownik $uzytkownicy = null)
    {
        $this->uzytkownicy = $uzytkownicy;
    
        return $this;
    }

    /**
     * Get uzytkownicy
     *
     * @return \Entities\Uzytkownik
     */
    public function getUzytkownicy()
    {
        return $this->uzytkownicy;
    }

    /**
     * Set firmy
     *
     * @param \Entities\Firma $firmy
     *
     * @return Koszyk
     */
    public function setFirmy(\Entities\Firma $firmy = null)
    {
        $this->firmy = $firmy;
    
        return $this;
    }

    /**
     * Get firmy
     *
     * @return \Entities\Firma
     */
    public function getFirmy()
    {
        return $this->firmy;
    }
}

