<?php

namespace Entities;

/**
 * Kolor_Grupa
 */
class Kolor_Grupa
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $hex;

    /**
     * @var string
     */
    private $plik;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Kolor_Grupa
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set hex
     *
     * @param string $hex
     *
     * @return Kolor_Grupa
     */
    public function setHex($hex)
    {
        $this->hex = $hex;
    
        return $this;
    }

    /**
     * Get hex
     *
     * @return string
     */
    public function getHex()
    {
        return $this->hex;
    }

    /**
     * Set plik
     *
     * @param string $plik
     *
     * @return Kolor_Grupa
     */
    public function setPlik($plik)
    {
        $this->plik = $plik;
    
        return $this;
    }

    /**
     * Get plik
     *
     * @return string
     */
    public function getPlik()
    {
        return $this->plik;
    }
}

