<?php

namespace Entities;

/**
 * Korporacyjny_Uzytkownik
 */
class Korporacyjny_Uzytkownik
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $uzytkownicy_id;

    /**
     * @var integer
     */
    private $firmy_id;

    /**
     * @var \DateTime
     */
    private $data_dodania;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var integer
     */
    private $rola;

    /**
     * @var string
     */
    private $budzet;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $korporacyjni_koszyki;

    /**
     * @var \Entities\Uzytkownik
     */
    private $uzytkownicy;

    /**
     * @var \Entities\Firma
     */
    private $firmy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->korporacyjni_koszyki = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uzytkownicyId
     *
     * @param integer $uzytkownicyId
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setUzytkownicyId($uzytkownicyId)
    {
        $this->uzytkownicy_id = $uzytkownicyId;
    
        return $this;
    }

    /**
     * Get uzytkownicyId
     *
     * @return integer
     */
    public function getUzytkownicyId()
    {
        return $this->uzytkownicy_id;
    }

    /**
     * Set firmyId
     *
     * @param integer $firmyId
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setFirmyId($firmyId)
    {
        $this->firmy_id = $firmyId;
    
        return $this;
    }

    /**
     * Get firmyId
     *
     * @return integer
     */
    public function getFirmyId()
    {
        return $this->firmy_id;
    }

    /**
     * Set dataDodania
     *
     * @param \DateTime $dataDodania
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setDataDodania($dataDodania)
    {
        $this->data_dodania = $dataDodania;
    
        return $this;
    }

    /**
     * Get dataDodania
     *
     * @return \DateTime
     */
    public function getDataDodania()
    {
        return $this->data_dodania;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set rola
     *
     * @param integer $rola
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setRola($rola)
    {
        $this->rola = $rola;
    
        return $this;
    }

    /**
     * Get rola
     *
     * @return integer
     */
    public function getRola()
    {
        return $this->rola;
    }

    /**
     * Set budzet
     *
     * @param string $budzet
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setBudzet($budzet)
    {
        $this->budzet = $budzet;
    
        return $this;
    }

    /**
     * Get budzet
     *
     * @return string
     */
    public function getBudzet()
    {
        return $this->budzet;
    }

    /**
     * Add korporacyjniKoszyki
     *
     * @param \Entities\Korporacyjny_Koszyk $korporacyjniKoszyki
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function addKorporacyjniKoszyki(\Entities\Korporacyjny_Koszyk $korporacyjniKoszyki)
    {
        $this->korporacyjni_koszyki[] = $korporacyjniKoszyki;
    
        return $this;
    }

    /**
     * Remove korporacyjniKoszyki
     *
     * @param \Entities\Korporacyjny_Koszyk $korporacyjniKoszyki
     */
    public function removeKorporacyjniKoszyki(\Entities\Korporacyjny_Koszyk $korporacyjniKoszyki)
    {
        $this->korporacyjni_koszyki->removeElement($korporacyjniKoszyki);
    }

    /**
     * Get korporacyjniKoszyki
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKorporacyjniKoszyki()
    {
        return $this->korporacyjni_koszyki;
    }

    /**
     * Set uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setUzytkownicy(\Entities\Uzytkownik $uzytkownicy = null)
    {
        $this->uzytkownicy = $uzytkownicy;
    
        return $this;
    }

    /**
     * Get uzytkownicy
     *
     * @return \Entities\Uzytkownik
     */
    public function getUzytkownicy()
    {
        return $this->uzytkownicy;
    }

    /**
     * Set firmy
     *
     * @param \Entities\Firma $firmy
     *
     * @return Korporacyjny_Uzytkownik
     */
    public function setFirmy(\Entities\Firma $firmy = null)
    {
        $this->firmy = $firmy;
    
        return $this;
    }

    /**
     * Get firmy
     *
     * @return \Entities\Firma
     */
    public function getFirmy()
    {
        return $this->firmy;
    }
}

