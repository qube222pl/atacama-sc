<?php

namespace Entities;

/**
 * Produkt_i18n
 */
class Produkt_i18n
{
    /**
     * @var integer
     */
    private $produkty_id;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var string
     */
    private $seo_opis;

    /**
     * @var \DateTime
     */
    private $data_modyfikacji;

    /**
     * @var \Entities\Produkt
     */
    private $produkty;


    /**
     * Set produktyId
     *
     * @param integer $produktyId
     *
     * @return Produkt_i18n
     */
    public function setProduktyId($produktyId)
    {
        $this->produkty_id = $produktyId;
    
        return $this;
    }

    /**
     * Get produktyId
     *
     * @return integer
     */
    public function getProduktyId()
    {
        return $this->produkty_id;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return Produkt_i18n
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Produkt_i18n
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Produkt_i18n
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set seoOpis
     *
     * @param string $seoOpis
     *
     * @return Produkt_i18n
     */
    public function setSeoOpis($seoOpis)
    {
        $this->seo_opis = $seoOpis;
    
        return $this;
    }

    /**
     * Get seoOpis
     *
     * @return string
     */
    public function getSeoOpis()
    {
        return $this->seo_opis;
    }

    /**
     * Set dataModyfikacji
     *
     * @param \DateTime $dataModyfikacji
     *
     * @return Produkt_i18n
     */
    public function setDataModyfikacji($dataModyfikacji)
    {
        $this->data_modyfikacji = $dataModyfikacji;
    
        return $this;
    }

    /**
     * Get dataModyfikacji
     *
     * @return \DateTime
     */
    public function getDataModyfikacji()
    {
        return $this->data_modyfikacji;
    }

    /**
     * Set produkty
     *
     * @param \Entities\Produkt $produkty
     *
     * @return Produkt_i18n
     */
    public function setProdukty(\Entities\Produkt $produkty = null)
    {
        $this->produkty = $produkty;
    
        return $this;
    }

    /**
     * Get produkty
     *
     * @return \Entities\Produkt
     */
    public function getProdukty()
    {
        return $this->produkty;
    }
}

