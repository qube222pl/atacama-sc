<?php

namespace Entities;

/**
 * Adres
 */
class Adres
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $firmy_id;

    /**
     * @var integer
     */
    private $typ_adresu;

    /**
     * @var boolean
     */
    private $domyslny;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $ulica;

    /**
     * @var string
     */
    private $miasto;

    /**
     * @var string
     */
    private $kod_pocztowy;

    /**
     * @var string
     */
    private $kraj;

    /**
     * @var string
     */
    private $nr_telefonu;

    /**
     * @var integer
     */
    private $widoczny;

    /**
     * @var \Entities\Firma
     */
    private $firmy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firmyId
     *
     * @param integer $firmyId
     *
     * @return Adres
     */
    public function setFirmyId($firmyId)
    {
        $this->firmy_id = $firmyId;
    
        return $this;
    }

    /**
     * Get firmyId
     *
     * @return integer
     */
    public function getFirmyId()
    {
        return $this->firmy_id;
    }

    /**
     * Set typAdresu
     *
     * @param integer $typAdresu
     *
     * @return Adres
     */
    public function setTypAdresu($typAdresu)
    {
        $this->typ_adresu = $typAdresu;
    
        return $this;
    }

    /**
     * Get typAdresu
     *
     * @return integer
     */
    public function getTypAdresu()
    {
        return $this->typ_adresu;
    }

    /**
     * Set domyslny
     *
     * @param boolean $domyslny
     *
     * @return Adres
     */
    public function setDomyslny($domyslny)
    {
        $this->domyslny = $domyslny;
    
        return $this;
    }

    /**
     * Get domyslny
     *
     * @return boolean
     */
    public function getDomyslny()
    {
        return $this->domyslny;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Adres
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set ulica
     *
     * @param string $ulica
     *
     * @return Adres
     */
    public function setUlica($ulica)
    {
        $this->ulica = $ulica;
    
        return $this;
    }

    /**
     * Get ulica
     *
     * @return string
     */
    public function getUlica()
    {
        return $this->ulica;
    }

    /**
     * Set miasto
     *
     * @param string $miasto
     *
     * @return Adres
     */
    public function setMiasto($miasto)
    {
        $this->miasto = $miasto;
    
        return $this;
    }

    /**
     * Get miasto
     *
     * @return string
     */
    public function getMiasto()
    {
        return $this->miasto;
    }

    /**
     * Set kodPocztowy
     *
     * @param string $kodPocztowy
     *
     * @return Adres
     */
    public function setKodPocztowy($kodPocztowy)
    {
        $this->kod_pocztowy = $kodPocztowy;
    
        return $this;
    }

    /**
     * Get kodPocztowy
     *
     * @return string
     */
    public function getKodPocztowy()
    {
        return $this->kod_pocztowy;
    }

    /**
     * Set kraj
     *
     * @param string $kraj
     *
     * @return Adres
     */
    public function setKraj($kraj)
    {
        $this->kraj = $kraj;
    
        return $this;
    }

    /**
     * Get kraj
     *
     * @return string
     */
    public function getKraj()
    {
        return $this->kraj;
    }

    /**
     * Set nrTelefonu
     *
     * @param string $nrTelefonu
     *
     * @return Adres
     */
    public function setNrTelefonu($nrTelefonu)
    {
        $this->nr_telefonu = $nrTelefonu;
    
        return $this;
    }

    /**
     * Get nrTelefonu
     *
     * @return string
     */
    public function getNrTelefonu()
    {
        return $this->nr_telefonu;
    }

    /**
     * Set widoczny
     *
     * @param integer $widoczny
     *
     * @return Adres
     */
    public function setWidoczny($widoczny)
    {
        $this->widoczny = $widoczny;
    
        return $this;
    }

    /**
     * Get widoczny
     *
     * @return integer
     */
    public function getWidoczny()
    {
        return $this->widoczny;
    }

    /**
     * Set firmy
     *
     * @param \Entities\Firma $firmy
     *
     * @return Adres
     */
    public function setFirmy(\Entities\Firma $firmy = null)
    {
        $this->firmy = $firmy;
    
        return $this;
    }

    /**
     * Get firmy
     *
     * @return \Entities\Firma
     */
    public function getFirmy()
    {
        return $this->firmy;
    }
}

