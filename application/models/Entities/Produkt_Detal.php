<?php

namespace Entities;

/**
 * Produkt_Detal
 */
class Produkt_Detal
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $produkty_id;

    /**
     * @var integer
     */
    private $kolory_id;

    /**
     * @var integer
     */
    private $rozmiar;

    /**
     * @var string
     */
    private $kod_produktu_dystr;

    /**
     * @var string
     */
    private $id_produktu_dystr;

    /**
     * @var string
     */
    private $cena;

    /**
     * @var integer
     */
    private $vat;

    /**
     * @var string
     */
    private $promo;

    /**
     * @var string
     */
    private $wyp;

    /**
     * @var string
     */
    private $srp;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var integer
     */
    private $stan_magazynowy_1;

    /**
     * @var integer
     */
    private $stan_magazynowy_2;

    /**
     * @var integer
     */
    private $widoczny;

    /**
     * @var integer
     */
    private $promo_min;

    /**
     * @var \Entities\Kolor
     */
    private $kolory;

    /**
     * @var \Entities\Produkt
     */
    private $produkty;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set produktyId
     *
     * @param integer $produktyId
     *
     * @return Produkt_Detal
     */
    public function setProduktyId($produktyId)
    {
        $this->produkty_id = $produktyId;
    
        return $this;
    }

    /**
     * Get produktyId
     *
     * @return integer
     */
    public function getProduktyId()
    {
        return $this->produkty_id;
    }

    /**
     * Set koloryId
     *
     * @param integer $koloryId
     *
     * @return Produkt_Detal
     */
    public function setKoloryId($koloryId)
    {
        $this->kolory_id = $koloryId;
    
        return $this;
    }

    /**
     * Get koloryId
     *
     * @return integer
     */
    public function getKoloryId()
    {
        return $this->kolory_id;
    }

    /**
     * Set rozmiar
     *
     * @param integer $rozmiar
     *
     * @return Produkt_Detal
     */
    public function setRozmiar($rozmiar)
    {
        $this->rozmiar = $rozmiar;
    
        return $this;
    }

    /**
     * Get rozmiar
     *
     * @return integer
     */
    public function getRozmiar()
    {
        return $this->rozmiar;
    }

    /**
     * Set kodProduktuDystr
     *
     * @param string $kodProduktuDystr
     *
     * @return Produkt_Detal
     */
    public function setKodProduktuDystr($kodProduktuDystr)
    {
        $this->kod_produktu_dystr = $kodProduktuDystr;
    
        return $this;
    }

    /**
     * Get kodProduktuDystr
     *
     * @return string
     */
    public function getKodProduktuDystr()
    {
        return $this->kod_produktu_dystr;
    }

    /**
     * Set idProduktuDystr
     *
     * @param string $idProduktuDystr
     *
     * @return Produkt_Detal
     */
    public function setIdProduktuDystr($idProduktuDystr)
    {
        $this->id_produktu_dystr = $idProduktuDystr;
    
        return $this;
    }

    /**
     * Get idProduktuDystr
     *
     * @return string
     */
    public function getIdProduktuDystr()
    {
        return $this->id_produktu_dystr;
    }

    /**
     * Set cena
     *
     * @param string $cena
     *
     * @return Produkt_Detal
     */
    public function setCena($cena)
    {
        $this->cena = $cena;
    
        return $this;
    }

    /**
     * Get cena
     *
     * @return string
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * Set vat
     *
     * @param integer $vat
     *
     * @return Produkt_Detal
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    
        return $this;
    }

    /**
     * Get vat
     *
     * @return integer
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set promo
     *
     * @param string $promo
     *
     * @return Produkt_Detal
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
    
        return $this;
    }

    /**
     * Get promo
     *
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Set wyp
     *
     * @param string $wyp
     *
     * @return Produkt_Detal
     */
    public function setWyp($wyp)
    {
        $this->wyp = $wyp;
    
        return $this;
    }

    /**
     * Get wyp
     *
     * @return string
     */
    public function getWyp()
    {
        return $this->wyp;
    }

    /**
     * Set srp
     *
     * @param string $srp
     *
     * @return Produkt_Detal
     */
    public function setSrp($srp)
    {
        $this->srp = $srp;
    
        return $this;
    }

    /**
     * Get srp
     *
     * @return string
     */
    public function getSrp()
    {
        return $this->srp;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Produkt_Detal
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set stanMagazynowy1
     *
     * @param integer $stanMagazynowy1
     *
     * @return Produkt_Detal
     */
    public function setStanMagazynowy1($stanMagazynowy1)
    {
        $this->stan_magazynowy_1 = $stanMagazynowy1;
    
        return $this;
    }

    /**
     * Get stanMagazynowy1
     *
     * @return integer
     */
    public function getStanMagazynowy1()
    {
        return $this->stan_magazynowy_1;
    }

    /**
     * Set stanMagazynowy2
     *
     * @param integer $stanMagazynowy2
     *
     * @return Produkt_Detal
     */
    public function setStanMagazynowy2($stanMagazynowy2)
    {
        $this->stan_magazynowy_2 = $stanMagazynowy2;
    
        return $this;
    }

    /**
     * Get stanMagazynowy2
     *
     * @return integer
     */
    public function getStanMagazynowy2()
    {
        return $this->stan_magazynowy_2;
    }

    /**
     * Set widoczny
     *
     * @param integer $widoczny
     *
     * @return Produkt_Detal
     */
    public function setWidoczny($widoczny)
    {
        $this->widoczny = $widoczny;
    
        return $this;
    }

    /**
     * Get widoczny
     *
     * @return integer
     */
    public function getWidoczny()
    {
        return $this->widoczny;
    }

    /**
     * Set promoMin
     *
     * @param integer $promoMin
     *
     * @return Produkt_Detal
     */
    public function setPromoMin($promoMin)
    {
        $this->promo_min = $promoMin;
    
        return $this;
    }

    /**
     * Get promoMin
     *
     * @return integer
     */
    public function getPromoMin()
    {
        return $this->promo_min;
    }

    /**
     * Set kolory
     *
     * @param \Entities\Kolor $kolory
     *
     * @return Produkt_Detal
     */
    public function setKolory(\Entities\Kolor $kolory = null)
    {
        $this->kolory = $kolory;
    
        return $this;
    }

    /**
     * Get kolory
     *
     * @return \Entities\Kolor
     */
    public function getKolory()
    {
        return $this->kolory;
    }

    /**
     * Set produkty
     *
     * @param \Entities\Produkt $produkty
     *
     * @return Produkt_Detal
     */
    public function setProdukty(\Entities\Produkt $produkty = null)
    {
        $this->produkty = $produkty;
    
        return $this;
    }

    /**
     * Get produkty
     *
     * @return \Entities\Produkt
     */
    public function getProdukty()
    {
        return $this->produkty;
    }
}

