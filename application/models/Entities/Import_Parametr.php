<?php

namespace Entities;

/**
 * Import_Parametr
 */
class Import_Parametr
{
    /**
     * @var integer
     */
    private $dystrybutorzy_id;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $wartosc;

    /**
     * @var integer
     */
    private $edytowalny;

    /**
     * @var \Entities\Dystrybutor
     */
    private $dystrybutorzy;


    /**
     * Set dystrybutorzyId
     *
     * @param integer $dystrybutorzyId
     *
     * @return Import_Parametr
     */
    public function setDystrybutorzyId($dystrybutorzyId)
    {
        $this->dystrybutorzy_id = $dystrybutorzyId;
    
        return $this;
    }

    /**
     * Get dystrybutorzyId
     *
     * @return integer
     */
    public function getDystrybutorzyId()
    {
        return $this->dystrybutorzy_id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Import_Parametr
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set wartosc
     *
     * @param string $wartosc
     *
     * @return Import_Parametr
     */
    public function setWartosc($wartosc)
    {
        $this->wartosc = $wartosc;
    
        return $this;
    }

    /**
     * Get wartosc
     *
     * @return string
     */
    public function getWartosc()
    {
        return $this->wartosc;
    }

    /**
     * Set edytowalny
     *
     * @param integer $edytowalny
     *
     * @return Import_Parametr
     */
    public function setEdytowalny($edytowalny)
    {
        $this->edytowalny = $edytowalny;
    
        return $this;
    }

    /**
     * Get edytowalny
     *
     * @return integer
     */
    public function getEdytowalny()
    {
        return $this->edytowalny;
    }

    /**
     * Set dystrybutorzy
     *
     * @param \Entities\Dystrybutor $dystrybutorzy
     *
     * @return Import_Parametr
     */
    public function setDystrybutorzy(\Entities\Dystrybutor $dystrybutorzy = null)
    {
        $this->dystrybutorzy = $dystrybutorzy;
    
        return $this;
    }

    /**
     * Get dystrybutorzy
     *
     * @return \Entities\Dystrybutor
     */
    public function getDystrybutorzy()
    {
        return $this->dystrybutorzy;
    }
}

