<?php

namespace Entities;

/**
 * Kategoria_Cena
 */
class Kategoria_Cena
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $kategoria_id;

    /**
     * @var string
     */
    private $procent_regularna;

    /**
     * @var string
     */
    private $procent_promocja;

    /**
     * @var integer
     */
    private $minimum;

    /**
     * @var integer
     */
    private $maximum;

    /**
     * @var \Entities\Kategoria
     */
    private $kategorie;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kategoriaId
     *
     * @param integer $kategoriaId
     *
     * @return Kategoria_Cena
     */
    public function setKategoriaId($kategoriaId)
    {
        $this->kategoria_id = $kategoriaId;
    
        return $this;
    }

    /**
     * Get kategoriaId
     *
     * @return integer
     */
    public function getKategoriaId()
    {
        return $this->kategoria_id;
    }

    /**
     * Set procentRegularna
     *
     * @param string $procentRegularna
     *
     * @return Kategoria_Cena
     */
    public function setProcentRegularna($procentRegularna)
    {
        $this->procent_regularna = $procentRegularna;
    
        return $this;
    }

    /**
     * Get procentRegularna
     *
     * @return string
     */
    public function getProcentRegularna()
    {
        return $this->procent_regularna;
    }

    /**
     * Set procentPromocja
     *
     * @param string $procentPromocja
     *
     * @return Kategoria_Cena
     */
    public function setProcentPromocja($procentPromocja)
    {
        $this->procent_promocja = $procentPromocja;
    
        return $this;
    }

    /**
     * Get procentPromocja
     *
     * @return string
     */
    public function getProcentPromocja()
    {
        return $this->procent_promocja;
    }

    /**
     * Set minimum
     *
     * @param integer $minimum
     *
     * @return Kategoria_Cena
     */
    public function setMinimum($minimum)
    {
        $this->minimum = $minimum;
    
        return $this;
    }

    /**
     * Get minimum
     *
     * @return integer
     */
    public function getMinimum()
    {
        return $this->minimum;
    }

    /**
     * Set maximum
     *
     * @param integer $maximum
     *
     * @return Kategoria_Cena
     */
    public function setMaximum($maximum)
    {
        $this->maximum = $maximum;
    
        return $this;
    }

    /**
     * Get maximum
     *
     * @return integer
     */
    public function getMaximum()
    {
        return $this->maximum;
    }

    /**
     * Set kategorie
     *
     * @param \Entities\Kategoria $kategorie
     *
     * @return Kategoria_Cena
     */
    public function setKategorie(\Entities\Kategoria $kategorie = null)
    {
        $this->kategorie = $kategorie;
    
        return $this;
    }

    /**
     * Get kategorie
     *
     * @return \Entities\Kategoria
     */
    public function getKategorie()
    {
        return $this->kategorie;
    }
}

