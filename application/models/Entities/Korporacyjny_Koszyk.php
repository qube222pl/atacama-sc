<?php

namespace Entities;

/**
 * Korporacyjny_Koszyk
 */
class Korporacyjny_Koszyk
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $uzytkownicy_id;

    /**
     * @var integer
     */
    private $korporacyjni_uzytkownicy_id;

    /**
     * @var \DateTime
     */
    private $data_utworzenia;

    /**
     * @var \DateTime
     */
    private $data_realizacji;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $netto;

    /**
     * @var string
     */
    private $uwagi;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $korporacyjni_koszyki_produkty;

    /**
     * @var \Entities\Uzytkownik
     */
    private $uzytkownicy;

    /**
     * @var \Entities\Korporacyjny_Uzytkownik
     */
    private $korporacyjni_uzytkownicy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->korporacyjni_koszyki_produkty = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uzytkownicyId
     *
     * @param integer $uzytkownicyId
     *
     * @return Korporacyjny_Koszyk
     */
    public function setUzytkownicyId($uzytkownicyId)
    {
        $this->uzytkownicy_id = $uzytkownicyId;
    
        return $this;
    }

    /**
     * Get uzytkownicyId
     *
     * @return integer
     */
    public function getUzytkownicyId()
    {
        return $this->uzytkownicy_id;
    }

    /**
     * Set korporacyjniUzytkownicyId
     *
     * @param integer $korporacyjniUzytkownicyId
     *
     * @return Korporacyjny_Koszyk
     */
    public function setKorporacyjniUzytkownicyId($korporacyjniUzytkownicyId)
    {
        $this->korporacyjni_uzytkownicy_id = $korporacyjniUzytkownicyId;
    
        return $this;
    }

    /**
     * Get korporacyjniUzytkownicyId
     *
     * @return integer
     */
    public function getKorporacyjniUzytkownicyId()
    {
        return $this->korporacyjni_uzytkownicy_id;
    }

    /**
     * Set dataUtworzenia
     *
     * @param \DateTime $dataUtworzenia
     *
     * @return Korporacyjny_Koszyk
     */
    public function setDataUtworzenia($dataUtworzenia)
    {
        $this->data_utworzenia = $dataUtworzenia;
    
        return $this;
    }

    /**
     * Get dataUtworzenia
     *
     * @return \DateTime
     */
    public function getDataUtworzenia()
    {
        return $this->data_utworzenia;
    }

    /**
     * Set dataRealizacji
     *
     * @param \DateTime $dataRealizacji
     *
     * @return Korporacyjny_Koszyk
     */
    public function setDataRealizacji($dataRealizacji)
    {
        $this->data_realizacji = $dataRealizacji;
    
        return $this;
    }

    /**
     * Get dataRealizacji
     *
     * @return \DateTime
     */
    public function getDataRealizacji()
    {
        return $this->data_realizacji;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Korporacyjny_Koszyk
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set netto
     *
     * @param string $netto
     *
     * @return Korporacyjny_Koszyk
     */
    public function setNetto($netto)
    {
        $this->netto = $netto;
    
        return $this;
    }

    /**
     * Get netto
     *
     * @return string
     */
    public function getNetto()
    {
        return $this->netto;
    }

    /**
     * Set uwagi
     *
     * @param string $uwagi
     *
     * @return Korporacyjny_Koszyk
     */
    public function setUwagi($uwagi)
    {
        $this->uwagi = $uwagi;
    
        return $this;
    }

    /**
     * Get uwagi
     *
     * @return string
     */
    public function getUwagi()
    {
        return $this->uwagi;
    }

    /**
     * Add korporacyjniKoszykiProdukty
     *
     * @param \Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty
     *
     * @return Korporacyjny_Koszyk
     */
    public function addKorporacyjniKoszykiProdukty(\Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty)
    {
        $this->korporacyjni_koszyki_produkty[] = $korporacyjniKoszykiProdukty;
    
        return $this;
    }

    /**
     * Remove korporacyjniKoszykiProdukty
     *
     * @param \Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty
     */
    public function removeKorporacyjniKoszykiProdukty(\Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty)
    {
        $this->korporacyjni_koszyki_produkty->removeElement($korporacyjniKoszykiProdukty);
    }

    /**
     * Get korporacyjniKoszykiProdukty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKorporacyjniKoszykiProdukty()
    {
        return $this->korporacyjni_koszyki_produkty;
    }

    /**
     * Set uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     *
     * @return Korporacyjny_Koszyk
     */
    public function setUzytkownicy(\Entities\Uzytkownik $uzytkownicy = null)
    {
        $this->uzytkownicy = $uzytkownicy;
    
        return $this;
    }

    /**
     * Get uzytkownicy
     *
     * @return \Entities\Uzytkownik
     */
    public function getUzytkownicy()
    {
        return $this->uzytkownicy;
    }

    /**
     * Set korporacyjniUzytkownicy
     *
     * @param \Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy
     *
     * @return Korporacyjny_Koszyk
     */
    public function setKorporacyjniUzytkownicy(\Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy = null)
    {
        $this->korporacyjni_uzytkownicy = $korporacyjniUzytkownicy;
    
        return $this;
    }

    /**
     * Get korporacyjniUzytkownicy
     *
     * @return \Entities\Korporacyjny_Uzytkownik
     */
    public function getKorporacyjniUzytkownicy()
    {
        return $this->korporacyjni_uzytkownicy;
    }
}

