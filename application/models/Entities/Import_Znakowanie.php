<?php

namespace Entities;

/**
 * Import_Znakowanie
 */
class Import_Znakowanie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dystrybutorzy_id;

    /**
     * @var string
     */
    private $znakowanie_dystr_id;

    /**
     * @var integer
     */
    private $znakowanie_id;

    /**
     * @var string
     */
    private $znakowanie_dystr_nazwa;

    /**
     * @var \Entities\Dystrybutor
     */
    private $dystrybutorzy;

    /**
     * @var \Entities\Znakowanie
     */
    private $znakowanie;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dystrybutorzyId
     *
     * @param integer $dystrybutorzyId
     *
     * @return Import_Znakowanie
     */
    public function setDystrybutorzyId($dystrybutorzyId)
    {
        $this->dystrybutorzy_id = $dystrybutorzyId;
    
        return $this;
    }

    /**
     * Get dystrybutorzyId
     *
     * @return integer
     */
    public function getDystrybutorzyId()
    {
        return $this->dystrybutorzy_id;
    }

    /**
     * Set znakowanieDystrId
     *
     * @param string $znakowanieDystrId
     *
     * @return Import_Znakowanie
     */
    public function setZnakowanieDystrId($znakowanieDystrId)
    {
        $this->znakowanie_dystr_id = $znakowanieDystrId;
    
        return $this;
    }

    /**
     * Get znakowanieDystrId
     *
     * @return string
     */
    public function getZnakowanieDystrId()
    {
        return $this->znakowanie_dystr_id;
    }

    /**
     * Set znakowanieId
     *
     * @param integer $znakowanieId
     *
     * @return Import_Znakowanie
     */
    public function setZnakowanieId($znakowanieId)
    {
        $this->znakowanie_id = $znakowanieId;
    
        return $this;
    }

    /**
     * Get znakowanieId
     *
     * @return integer
     */
    public function getZnakowanieId()
    {
        return $this->znakowanie_id;
    }

    /**
     * Set znakowanieDystrNazwa
     *
     * @param string $znakowanieDystrNazwa
     *
     * @return Import_Znakowanie
     */
    public function setZnakowanieDystrNazwa($znakowanieDystrNazwa)
    {
        $this->znakowanie_dystr_nazwa = $znakowanieDystrNazwa;
    
        return $this;
    }

    /**
     * Get znakowanieDystrNazwa
     *
     * @return string
     */
    public function getZnakowanieDystrNazwa()
    {
        return $this->znakowanie_dystr_nazwa;
    }

    /**
     * Set dystrybutorzy
     *
     * @param \Entities\Dystrybutor $dystrybutorzy
     *
     * @return Import_Znakowanie
     */
    public function setDystrybutorzy(\Entities\Dystrybutor $dystrybutorzy = null)
    {
        $this->dystrybutorzy = $dystrybutorzy;
    
        return $this;
    }

    /**
     * Get dystrybutorzy
     *
     * @return \Entities\Dystrybutor
     */
    public function getDystrybutorzy()
    {
        return $this->dystrybutorzy;
    }

    /**
     * Set znakowanie
     *
     * @param \Entities\Znakowanie $znakowanie
     *
     * @return Import_Znakowanie
     */
    public function setZnakowanie(\Entities\Znakowanie $znakowanie = null)
    {
        $this->znakowanie = $znakowanie;
    
        return $this;
    }

    /**
     * Get znakowanie
     *
     * @return \Entities\Znakowanie
     */
    public function getZnakowanie()
    {
        return $this->znakowanie;
    }
}

