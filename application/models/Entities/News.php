<?php

namespace Entities;

/**
 * News
 */
class News
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var string
     */
    private $tytul;

    /**
     * @var string
     */
    private $zdjecie;

    /**
     * @var string
     */
    private $www;

    /**
     * @var string
     */
    private $zajawka;

    /**
     * @var string
     */
    private $tresc;

    /**
     * @var \DateTime
     */
    private $data_publikacji;

    /**
     * @var string
     */
    private $slowa_kluczowe;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var integer
     */
    private $widoczny;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return News
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set tytul
     *
     * @param string $tytul
     *
     * @return News
     */
    public function setTytul($tytul)
    {
        $this->tytul = $tytul;
    
        return $this;
    }

    /**
     * Get tytul
     *
     * @return string
     */
    public function getTytul()
    {
        return $this->tytul;
    }

    /**
     * Set zdjecie
     *
     * @param string $zdjecie
     *
     * @return News
     */
    public function setZdjecie($zdjecie)
    {
        $this->zdjecie = $zdjecie;
    
        return $this;
    }

    /**
     * Get zdjecie
     *
     * @return string
     */
    public function getZdjecie()
    {
        return $this->zdjecie;
    }

    /**
     * Set www
     *
     * @param string $www
     *
     * @return News
     */
    public function setWww($www)
    {
        $this->www = $www;
    
        return $this;
    }

    /**
     * Get www
     *
     * @return string
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Set zajawka
     *
     * @param string $zajawka
     *
     * @return News
     */
    public function setZajawka($zajawka)
    {
        $this->zajawka = $zajawka;
    
        return $this;
    }

    /**
     * Get zajawka
     *
     * @return string
     */
    public function getZajawka()
    {
        return $this->zajawka;
    }

    /**
     * Set tresc
     *
     * @param string $tresc
     *
     * @return News
     */
    public function setTresc($tresc)
    {
        $this->tresc = $tresc;
    
        return $this;
    }

    /**
     * Get tresc
     *
     * @return string
     */
    public function getTresc()
    {
        return $this->tresc;
    }

    /**
     * Set dataPublikacji
     *
     * @param \DateTime $dataPublikacji
     *
     * @return News
     */
    public function setDataPublikacji($dataPublikacji)
    {
        $this->data_publikacji = $dataPublikacji;
    
        return $this;
    }

    /**
     * Get dataPublikacji
     *
     * @return \DateTime
     */
    public function getDataPublikacji()
    {
        return $this->data_publikacji;
    }

    /**
     * Set slowaKluczowe
     *
     * @param string $slowaKluczowe
     *
     * @return News
     */
    public function setSlowaKluczowe($slowaKluczowe)
    {
        $this->slowa_kluczowe = $slowaKluczowe;
    
        return $this;
    }

    /**
     * Get slowaKluczowe
     *
     * @return string
     */
    public function getSlowaKluczowe()
    {
        return $this->slowa_kluczowe;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return News
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set widoczny
     *
     * @param integer $widoczny
     *
     * @return News
     */
    public function setWidoczny($widoczny)
    {
        $this->widoczny = $widoczny;
    
        return $this;
    }

    /**
     * Get widoczny
     *
     * @return integer
     */
    public function getWidoczny()
    {
        return $this->widoczny;
    }
}

