<?php

namespace Entities;

/**
 * Produkt_Has_Znakowanie
 */
class Produkt_Has_Znakowanie
{
    /**
     * @var integer
     */
    private $produkty_id;

    /**
     * @var integer
     */
    private $znakowanie_id;

    /**
     * @var \Entities\Produkt
     */
    private $produkty;

    /**
     * @var \Entities\Znakowanie
     */
    private $znakowanie;


    /**
     * Set produktyId
     *
     * @param integer $produktyId
     *
     * @return Produkt_Has_Znakowanie
     */
    public function setProduktyId($produktyId)
    {
        $this->produkty_id = $produktyId;
    
        return $this;
    }

    /**
     * Get produktyId
     *
     * @return integer
     */
    public function getProduktyId()
    {
        return $this->produkty_id;
    }

    /**
     * Set znakowanieId
     *
     * @param integer $znakowanieId
     *
     * @return Produkt_Has_Znakowanie
     */
    public function setZnakowanieId($znakowanieId)
    {
        $this->znakowanie_id = $znakowanieId;
    
        return $this;
    }

    /**
     * Get znakowanieId
     *
     * @return integer
     */
    public function getZnakowanieId()
    {
        return $this->znakowanie_id;
    }

    /**
     * Set produkty
     *
     * @param \Entities\Produkt $produkty
     *
     * @return Produkt_Has_Znakowanie
     */
    public function setProdukty(\Entities\Produkt $produkty = null)
    {
        $this->produkty = $produkty;
    
        return $this;
    }

    /**
     * Get produkty
     *
     * @return \Entities\Produkt
     */
    public function getProdukty()
    {
        return $this->produkty;
    }

    /**
     * Set znakowanie
     *
     * @param \Entities\Znakowanie $znakowanie
     *
     * @return Produkt_Has_Znakowanie
     */
    public function setZnakowanie(\Entities\Znakowanie $znakowanie = null)
    {
        $this->znakowanie = $znakowanie;
    
        return $this;
    }

    /**
     * Get znakowanie
     *
     * @return \Entities\Znakowanie
     */
    public function getZnakowanie()
    {
        return $this->znakowanie;
    }
}

