<?php

namespace Entities;

/**
 * Cennik
 */
class Cennik
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $firmy_id;

    /**
     * @var \DateTime
     */
    private $data_utworzenia;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $koszyki_produkty;

    /**
     * @var \Entities\Firma
     */
    private $firmy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->koszyki_produkty = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firmyId
     *
     * @param integer $firmyId
     *
     * @return Cennik
     */
    public function setFirmyId($firmyId)
    {
        $this->firmy_id = $firmyId;
    
        return $this;
    }

    /**
     * Get firmyId
     *
     * @return integer
     */
    public function getFirmyId()
    {
        return $this->firmy_id;
    }

    /**
     * Set dataUtworzenia
     *
     * @param \DateTime $dataUtworzenia
     *
     * @return Cennik
     */
    public function setDataUtworzenia($dataUtworzenia)
    {
        $this->data_utworzenia = $dataUtworzenia;
    
        return $this;
    }

    /**
     * Get dataUtworzenia
     *
     * @return \DateTime
     */
    public function getDataUtworzenia()
    {
        return $this->data_utworzenia;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Cennik
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Add koszykiProdukty
     *
     * @param \Entities\Cennik_Produkt $koszykiProdukty
     *
     * @return Cennik
     */
    public function addKoszykiProdukty(\Entities\Cennik_Produkt $koszykiProdukty)
    {
        $this->koszyki_produkty[] = $koszykiProdukty;
    
        return $this;
    }

    /**
     * Remove koszykiProdukty
     *
     * @param \Entities\Cennik_Produkt $koszykiProdukty
     */
    public function removeKoszykiProdukty(\Entities\Cennik_Produkt $koszykiProdukty)
    {
        $this->koszyki_produkty->removeElement($koszykiProdukty);
    }

    /**
     * Get koszykiProdukty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKoszykiProdukty()
    {
        return $this->koszyki_produkty;
    }

    /**
     * Set firmy
     *
     * @param \Entities\Firma $firmy
     *
     * @return Cennik
     */
    public function setFirmy(\Entities\Firma $firmy = null)
    {
        $this->firmy = $firmy;
    
        return $this;
    }

    /**
     * Get firmy
     *
     * @return \Entities\Firma
     */
    public function getFirmy()
    {
        return $this->firmy;
    }
}

