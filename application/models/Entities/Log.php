<?php

namespace Entities;

/**
 * Log
 */
class Log
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $rodzic_id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var \DateTime
     */
    private $data;

    /**
     * @var integer
     */
    private $typ;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dziecko;

    /**
     * @var \Entities\Log
     */
    private $rodzic;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dziecko = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rodzicId
     *
     * @param integer $rodzicId
     *
     * @return Log
     */
    public function setRodzicId($rodzicId)
    {
        $this->rodzic_id = $rodzicId;
    
        return $this;
    }

    /**
     * Get rodzicId
     *
     * @return integer
     */
    public function getRodzicId()
    {
        return $this->rodzic_id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Log
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Log
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set typ
     *
     * @param integer $typ
     *
     * @return Log
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;
    
        return $this;
    }

    /**
     * Get typ
     *
     * @return integer
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Log
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Add dziecko
     *
     * @param \Entities\Log $dziecko
     *
     * @return Log
     */
    public function addDziecko(\Entities\Log $dziecko)
    {
        $this->dziecko[] = $dziecko;
    
        return $this;
    }

    /**
     * Remove dziecko
     *
     * @param \Entities\Log $dziecko
     */
    public function removeDziecko(\Entities\Log $dziecko)
    {
        $this->dziecko->removeElement($dziecko);
    }

    /**
     * Get dziecko
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDziecko()
    {
        return $this->dziecko;
    }

    /**
     * Set rodzic
     *
     * @param \Entities\Log $rodzic
     *
     * @return Log
     */
    public function setRodzic(\Entities\Log $rodzic = null)
    {
        $this->rodzic = $rodzic;
    
        return $this;
    }

    /**
     * Get rodzic
     *
     * @return \Entities\Log
     */
    public function getRodzic()
    {
        return $this->rodzic;
    }
}

