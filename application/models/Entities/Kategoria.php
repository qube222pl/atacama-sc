<?php

namespace Entities;

/**
 * Kategoria
 */
class Kategoria
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $rodzic;

    /**
     * @var string
     */
    private $sciezka;

    /**
     * @var integer
     */
    private $poziom = 0;

    /**
     * @var integer
     */
    private $pozycja = 0;

    /**
     * @var string
     */
    private $macierz;

    /**
     * @var string
     */
    private $konfekcjonowanie;

    /**
     * @var integer
     */
    private $opcje;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $kategorie_i18n;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $kategorie_ceny;

    /**
     * @var \Entities\Kategoria
     */
    private $kategorie;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kategorie_i18n = new \Doctrine\Common\Collections\ArrayCollection();
        $this->kategorie_ceny = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rodzic
     *
     * @param integer $rodzic
     *
     * @return Kategoria
     */
    public function setRodzic($rodzic)
    {
        $this->rodzic = $rodzic;
    
        return $this;
    }

    /**
     * Get rodzic
     *
     * @return integer
     */
    public function getRodzic()
    {
        return $this->rodzic;
    }

    /**
     * Set sciezka
     *
     * @param string $sciezka
     *
     * @return Kategoria
     */
    public function setSciezka($sciezka)
    {
        $this->sciezka = $sciezka;
    
        return $this;
    }

    /**
     * Get sciezka
     *
     * @return string
     */
    public function getSciezka()
    {
        return $this->sciezka;
    }

    /**
     * Set poziom
     *
     * @param integer $poziom
     *
     * @return Kategoria
     */
    public function setPoziom($poziom)
    {
        $this->poziom = $poziom;
    
        return $this;
    }

    /**
     * Get poziom
     *
     * @return integer
     */
    public function getPoziom()
    {
        return $this->poziom;
    }

    /**
     * Set pozycja
     *
     * @param integer $pozycja
     *
     * @return Kategoria
     */
    public function setPozycja($pozycja)
    {
        $this->pozycja = $pozycja;
    
        return $this;
    }

    /**
     * Get pozycja
     *
     * @return integer
     */
    public function getPozycja()
    {
        return $this->pozycja;
    }

    /**
     * Set macierz
     *
     * @param string $macierz
     *
     * @return Kategoria
     */
    public function setMacierz($macierz)
    {
        $this->macierz = $macierz;
    
        return $this;
    }

    /**
     * Get macierz
     *
     * @return string
     */
    public function getMacierz()
    {
        return $this->macierz;
    }

    /**
     * Set konfekcjonowanie
     *
     * @param string $konfekcjonowanie
     *
     * @return Kategoria
     */
    public function setKonfekcjonowanie($konfekcjonowanie)
    {
        $this->konfekcjonowanie = $konfekcjonowanie;
    
        return $this;
    }

    /**
     * Get konfekcjonowanie
     *
     * @return string
     */
    public function getKonfekcjonowanie()
    {
        return $this->konfekcjonowanie;
    }

    /**
     * Set opcje
     *
     * @param integer $opcje
     *
     * @return Kategoria
     */
    public function setOpcje($opcje)
    {
        $this->opcje = $opcje;
    
        return $this;
    }

    /**
     * Get opcje
     *
     * @return integer
     */
    public function getOpcje()
    {
        return $this->opcje;
    }

    /**
     * Add kategorieI18n
     *
     * @param \Entities\Kategoria_i18n $kategorieI18n
     *
     * @return Kategoria
     */
    public function addKategorieI18n(\Entities\Kategoria_i18n $kategorieI18n)
    {
        $this->kategorie_i18n[] = $kategorieI18n;
    
        return $this;
    }

    /**
     * Remove kategorieI18n
     *
     * @param \Entities\Kategoria_i18n $kategorieI18n
     */
    public function removeKategorieI18n(\Entities\Kategoria_i18n $kategorieI18n)
    {
        $this->kategorie_i18n->removeElement($kategorieI18n);
    }

    /**
     * Get kategorieI18n
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKategorieI18n()
    {
        return $this->kategorie_i18n;
    }

    /**
     * Add kategorieCeny
     *
     * @param \Entities\Kategoria_Cena $kategorieCeny
     *
     * @return Kategoria
     */
    public function addKategorieCeny(\Entities\Kategoria_Cena $kategorieCeny)
    {
        $this->kategorie_ceny[] = $kategorieCeny;
    
        return $this;
    }

    /**
     * Remove kategorieCeny
     *
     * @param \Entities\Kategoria_Cena $kategorieCeny
     */
    public function removeKategorieCeny(\Entities\Kategoria_Cena $kategorieCeny)
    {
        $this->kategorie_ceny->removeElement($kategorieCeny);
    }

    /**
     * Get kategorieCeny
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKategorieCeny()
    {
        return $this->kategorie_ceny;
    }

    /**
     * Set kategorie
     *
     * @param \Entities\Kategoria $kategorie
     *
     * @return Kategoria
     */
    public function setKategorie(\Entities\Kategoria $kategorie = null)
    {
        $this->kategorie = $kategorie;
    
        return $this;
    }

    /**
     * Get kategorie
     *
     * @return \Entities\Kategoria
     */
    public function getKategorie()
    {
        return $this->kategorie;
    }
}

