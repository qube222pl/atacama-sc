<?php

namespace Entities;

/**
 * Banner
 */
class Banner
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $typ;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var \DateTime
     */
    private $data_dodania;

    /**
     * @var string
     */
    private $plik;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var integer
     */
    private $kolejnosc;

    /**
     * @var integer
     */
    private $okno;

    /**
     * @var integer
     */
    private $widoczny;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typ
     *
     * @param string $typ
     *
     * @return Banner
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;
    
        return $this;
    }

    /**
     * Get typ
     *
     * @return string
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return Banner
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set dataDodania
     *
     * @param \DateTime $dataDodania
     *
     * @return Banner
     */
    public function setDataDodania($dataDodania)
    {
        $this->data_dodania = $dataDodania;
    
        return $this;
    }

    /**
     * Get dataDodania
     *
     * @return \DateTime
     */
    public function getDataDodania()
    {
        return $this->data_dodania;
    }

    /**
     * Set plik
     *
     * @param string $plik
     *
     * @return Banner
     */
    public function setPlik($plik)
    {
        $this->plik = $plik;
    
        return $this;
    }

    /**
     * Get plik
     *
     * @return string
     */
    public function getPlik()
    {
        return $this->plik;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Banner
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Banner
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Banner
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     *
     * @return Banner
     */
    public function setKolejnosc($kolejnosc)
    {
        $this->kolejnosc = $kolejnosc;
    
        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc()
    {
        return $this->kolejnosc;
    }

    /**
     * Set okno
     *
     * @param integer $okno
     *
     * @return Banner
     */
    public function setOkno($okno)
    {
        $this->okno = $okno;
    
        return $this;
    }

    /**
     * Get okno
     *
     * @return integer
     */
    public function getOkno()
    {
        return $this->okno;
    }

    /**
     * Set widoczny
     *
     * @param integer $widoczny
     *
     * @return Banner
     */
    public function setWidoczny($widoczny)
    {
        $this->widoczny = $widoczny;
    
        return $this;
    }

    /**
     * Get widoczny
     *
     * @return integer
     */
    public function getWidoczny()
    {
        return $this->widoczny;
    }
}

