<?php

namespace Entities;

/**
 * Import_Kategoria
 */
class Import_Kategoria
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dystrybutorzy_id;

    /**
     * @var integer
     */
    private $kategorie_id;

    /**
     * @var string
     */
    private $kategoria_dystr_id;

    /**
     * @var string
     */
    private $kategoria_dystr_nazwa;

    /**
     * @var \Entities\Dystrybutor
     */
    private $dystrybutorzy;

    /**
     * @var \Entities\Kategoria
     */
    private $kategorie;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dystrybutorzyId
     *
     * @param integer $dystrybutorzyId
     *
     * @return Import_Kategoria
     */
    public function setDystrybutorzyId($dystrybutorzyId)
    {
        $this->dystrybutorzy_id = $dystrybutorzyId;
    
        return $this;
    }

    /**
     * Get dystrybutorzyId
     *
     * @return integer
     */
    public function getDystrybutorzyId()
    {
        return $this->dystrybutorzy_id;
    }

    /**
     * Set kategorieId
     *
     * @param integer $kategorieId
     *
     * @return Import_Kategoria
     */
    public function setKategorieId($kategorieId)
    {
        $this->kategorie_id = $kategorieId;
    
        return $this;
    }

    /**
     * Get kategorieId
     *
     * @return integer
     */
    public function getKategorieId()
    {
        return $this->kategorie_id;
    }

    /**
     * Set kategoriaDystrId
     *
     * @param string $kategoriaDystrId
     *
     * @return Import_Kategoria
     */
    public function setKategoriaDystrId($kategoriaDystrId)
    {
        $this->kategoria_dystr_id = $kategoriaDystrId;
    
        return $this;
    }

    /**
     * Get kategoriaDystrId
     *
     * @return string
     */
    public function getKategoriaDystrId()
    {
        return $this->kategoria_dystr_id;
    }

    /**
     * Set kategoriaDystrNazwa
     *
     * @param string $kategoriaDystrNazwa
     *
     * @return Import_Kategoria
     */
    public function setKategoriaDystrNazwa($kategoriaDystrNazwa)
    {
        $this->kategoria_dystr_nazwa = $kategoriaDystrNazwa;
    
        return $this;
    }

    /**
     * Get kategoriaDystrNazwa
     *
     * @return string
     */
    public function getKategoriaDystrNazwa()
    {
        return $this->kategoria_dystr_nazwa;
    }

    /**
     * Set dystrybutorzy
     *
     * @param \Entities\Dystrybutor $dystrybutorzy
     *
     * @return Import_Kategoria
     */
    public function setDystrybutorzy(\Entities\Dystrybutor $dystrybutorzy = null)
    {
        $this->dystrybutorzy = $dystrybutorzy;
    
        return $this;
    }

    /**
     * Get dystrybutorzy
     *
     * @return \Entities\Dystrybutor
     */
    public function getDystrybutorzy()
    {
        return $this->dystrybutorzy;
    }

    /**
     * Set kategorie
     *
     * @param \Entities\Kategoria $kategorie
     *
     * @return Import_Kategoria
     */
    public function setKategorie(\Entities\Kategoria $kategorie = null)
    {
        $this->kategorie = $kategorie;
    
        return $this;
    }

    /**
     * Get kategorie
     *
     * @return \Entities\Kategoria
     */
    public function getKategorie()
    {
        return $this->kategorie;
    }
}

