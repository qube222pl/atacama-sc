<?php

namespace Entities;

/**
 * Cennik_Produkt
 */
class Cennik_Produkt
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $cenniki_id;

    /**
     * @var integer
     */
    private $produkty_id;

    /**
     * @var string
     */
    private $cena;

    /**
     * @var integer
     */
    private $ilosc;

    /**
     * @var \Entities\Cennik
     */
    private $cenniki;

    /**
     * @var \Entities\Produkt
     */
    private $produkty;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cennikiId
     *
     * @param integer $cennikiId
     *
     * @return Cennik_Produkt
     */
    public function setCennikiId($cennikiId)
    {
        $this->cenniki_id = $cennikiId;
    
        return $this;
    }

    /**
     * Get cennikiId
     *
     * @return integer
     */
    public function getCennikiId()
    {
        return $this->cenniki_id;
    }

    /**
     * Set produktyId
     *
     * @param integer $produktyId
     *
     * @return Cennik_Produkt
     */
    public function setProduktyId($produktyId)
    {
        $this->produkty_id = $produktyId;
    
        return $this;
    }

    /**
     * Get produktyId
     *
     * @return integer
     */
    public function getProduktyId()
    {
        return $this->produkty_id;
    }

    /**
     * Set cena
     *
     * @param string $cena
     *
     * @return Cennik_Produkt
     */
    public function setCena($cena)
    {
        $this->cena = $cena;
    
        return $this;
    }

    /**
     * Get cena
     *
     * @return string
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * Set ilosc
     *
     * @param integer $ilosc
     *
     * @return Cennik_Produkt
     */
    public function setIlosc($ilosc)
    {
        $this->ilosc = $ilosc;
    
        return $this;
    }

    /**
     * Get ilosc
     *
     * @return integer
     */
    public function getIlosc()
    {
        return $this->ilosc;
    }

    /**
     * Set cenniki
     *
     * @param \Entities\Cennik $cenniki
     *
     * @return Cennik_Produkt
     */
    public function setCenniki(\Entities\Cennik $cenniki = null)
    {
        $this->cenniki = $cenniki;
    
        return $this;
    }

    /**
     * Get cenniki
     *
     * @return \Entities\Cennik
     */
    public function getCenniki()
    {
        return $this->cenniki;
    }

    /**
     * Set produkty
     *
     * @param \Entities\Produkt $produkty
     *
     * @return Cennik_Produkt
     */
    public function setProdukty(\Entities\Produkt $produkty = null)
    {
        $this->produkty = $produkty;
    
        return $this;
    }

    /**
     * Get produkty
     *
     * @return \Entities\Produkt
     */
    public function getProdukty()
    {
        return $this->produkty;
    }
}

