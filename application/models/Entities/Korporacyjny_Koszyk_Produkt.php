<?php

namespace Entities;

/**
 * Korporacyjny_Koszyk_Produkt
 */
class Korporacyjny_Koszyk_Produkt
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $korporacyjni_koszyki_id;

    /**
     * @var integer
     */
    private $korporacyjni_produkty_id;

    /**
     * @var integer
     */
    private $ilosc;

    /**
     * @var \DateTime
     */
    private $data_dodruku;

    /**
     * @var string
     */
    private $cena_zakupu;

    /**
     * @var \Entities\Korporacyjny_Koszyk
     */
    private $korporacyjni_koszyki;

    /**
     * @var \Entities\Korporacyjny_Produkt
     */
    private $korporacyjni_produkty;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set korporacyjniKoszykiId
     *
     * @param integer $korporacyjniKoszykiId
     *
     * @return Korporacyjny_Koszyk_Produkt
     */
    public function setKorporacyjniKoszykiId($korporacyjniKoszykiId)
    {
        $this->korporacyjni_koszyki_id = $korporacyjniKoszykiId;
    
        return $this;
    }

    /**
     * Get korporacyjniKoszykiId
     *
     * @return integer
     */
    public function getKorporacyjniKoszykiId()
    {
        return $this->korporacyjni_koszyki_id;
    }

    /**
     * Set korporacyjniProduktyId
     *
     * @param integer $korporacyjniProduktyId
     *
     * @return Korporacyjny_Koszyk_Produkt
     */
    public function setKorporacyjniProduktyId($korporacyjniProduktyId)
    {
        $this->korporacyjni_produkty_id = $korporacyjniProduktyId;
    
        return $this;
    }

    /**
     * Get korporacyjniProduktyId
     *
     * @return integer
     */
    public function getKorporacyjniProduktyId()
    {
        return $this->korporacyjni_produkty_id;
    }

    /**
     * Set ilosc
     *
     * @param integer $ilosc
     *
     * @return Korporacyjny_Koszyk_Produkt
     */
    public function setIlosc($ilosc)
    {
        $this->ilosc = $ilosc;
    
        return $this;
    }

    /**
     * Get ilosc
     *
     * @return integer
     */
    public function getIlosc()
    {
        return $this->ilosc;
    }

    /**
     * Set dataDodruku
     *
     * @param \DateTime $dataDodruku
     *
     * @return Korporacyjny_Koszyk_Produkt
     */
    public function setDataDodruku($dataDodruku)
    {
        $this->data_dodruku = $dataDodruku;
    
        return $this;
    }

    /**
     * Get dataDodruku
     *
     * @return \DateTime
     */
    public function getDataDodruku()
    {
        return $this->data_dodruku;
    }

    /**
     * Set cenaZakupu
     *
     * @param string $cenaZakupu
     *
     * @return Korporacyjny_Koszyk_Produkt
     */
    public function setCenaZakupu($cenaZakupu)
    {
        $this->cena_zakupu = $cenaZakupu;
    
        return $this;
    }

    /**
     * Get cenaZakupu
     *
     * @return string
     */
    public function getCenaZakupu()
    {
        return $this->cena_zakupu;
    }

    /**
     * Set korporacyjniKoszyki
     *
     * @param \Entities\Korporacyjny_Koszyk $korporacyjniKoszyki
     *
     * @return Korporacyjny_Koszyk_Produkt
     */
    public function setKorporacyjniKoszyki(\Entities\Korporacyjny_Koszyk $korporacyjniKoszyki = null)
    {
        $this->korporacyjni_koszyki = $korporacyjniKoszyki;
    
        return $this;
    }

    /**
     * Get korporacyjniKoszyki
     *
     * @return \Entities\Korporacyjny_Koszyk
     */
    public function getKorporacyjniKoszyki()
    {
        return $this->korporacyjni_koszyki;
    }

    /**
     * Set korporacyjniProdukty
     *
     * @param \Entities\Korporacyjny_Produkt $korporacyjniProdukty
     *
     * @return Korporacyjny_Koszyk_Produkt
     */
    public function setKorporacyjniProdukty(\Entities\Korporacyjny_Produkt $korporacyjniProdukty = null)
    {
        $this->korporacyjni_produkty = $korporacyjniProdukty;
    
        return $this;
    }

    /**
     * Get korporacyjniProdukty
     *
     * @return \Entities\Korporacyjny_Produkt
     */
    public function getKorporacyjniProdukty()
    {
        return $this->korporacyjni_produkty;
    }
}

