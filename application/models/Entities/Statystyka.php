<?php

namespace Entities;

/**
 * Statystyka
 */
class Statystyka
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $ns_id;

    /**
     * @var integer
     */
    private $uzytkownicy_id;

    /**
     * @var \DateTime
     */
    private $data;

    /**
     * @var string
     */
    private $czynnosc;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var \Entities\Uzytkownik
     */
    private $uzytkownicy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nsId
     *
     * @param integer $nsId
     *
     * @return Statystyka
     */
    public function setNsId($nsId)
    {
        $this->ns_id = $nsId;
    
        return $this;
    }

    /**
     * Get nsId
     *
     * @return integer
     */
    public function getNsId()
    {
        return $this->ns_id;
    }

    /**
     * Set uzytkownicyId
     *
     * @param integer $uzytkownicyId
     *
     * @return Statystyka
     */
    public function setUzytkownicyId($uzytkownicyId)
    {
        $this->uzytkownicy_id = $uzytkownicyId;
    
        return $this;
    }

    /**
     * Get uzytkownicyId
     *
     * @return integer
     */
    public function getUzytkownicyId()
    {
        return $this->uzytkownicy_id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Statystyka
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set czynnosc
     *
     * @param string $czynnosc
     *
     * @return Statystyka
     */
    public function setCzynnosc($czynnosc)
    {
        $this->czynnosc = $czynnosc;
    
        return $this;
    }

    /**
     * Get czynnosc
     *
     * @return string
     */
    public function getCzynnosc()
    {
        return $this->czynnosc;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Statystyka
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     *
     * @return Statystyka
     */
    public function setUzytkownicy(\Entities\Uzytkownik $uzytkownicy = null)
    {
        $this->uzytkownicy = $uzytkownicy;
    
        return $this;
    }

    /**
     * Get uzytkownicy
     *
     * @return \Entities\Uzytkownik
     */
    public function getUzytkownicy()
    {
        return $this->uzytkownicy;
    }
}

