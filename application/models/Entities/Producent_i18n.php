<?php

namespace Entities;

/**
 * Producent_i18n
 */
class Producent_i18n
{
    /**
     * @var integer
     */
    private $producenci_id;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var string
     */
    private $seo_opis;

    /**
     * @var \Entities\Producent
     */
    private $producenci;


    /**
     * Set producenciId
     *
     * @param integer $producenciId
     *
     * @return Producent_i18n
     */
    public function setProducenciId($producenciId)
    {
        $this->producenci_id = $producenciId;
    
        return $this;
    }

    /**
     * Get producenciId
     *
     * @return integer
     */
    public function getProducenciId()
    {
        return $this->producenci_id;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return Producent_i18n
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Producent_i18n
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set seoOpis
     *
     * @param string $seoOpis
     *
     * @return Producent_i18n
     */
    public function setSeoOpis($seoOpis)
    {
        $this->seo_opis = $seoOpis;
    
        return $this;
    }

    /**
     * Get seoOpis
     *
     * @return string
     */
    public function getSeoOpis()
    {
        return $this->seo_opis;
    }

    /**
     * Set producenci
     *
     * @param \Entities\Producent $producenci
     *
     * @return Producent_i18n
     */
    public function setProducenci(\Entities\Producent $producenci = null)
    {
        $this->producenci = $producenci;
    
        return $this;
    }

    /**
     * Get producenci
     *
     * @return \Entities\Producent
     */
    public function getProducenci()
    {
        return $this->producenci;
    }
}

