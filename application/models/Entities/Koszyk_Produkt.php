<?php

namespace Entities;

/**
 * Koszyk_Produkt
 */
class Koszyk_Produkt
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $koszyki_id;

    /**
     * @var integer
     */
    private $produkty_detale_id;

    /**
     * @var string
     */
    private $cena_sprzedazy;

    /**
     * @var string
     */
    private $cena_zakupu;

    /**
     * @var string
     */
    private $cena_konfekcjonowanie;

    /**
     * @var integer
     */
    private $ilosc;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $koszyki_produkty_znakowania;

    /**
     * @var \Entities\Koszyk
     */
    private $koszyki;

    /**
     * @var \Entities\Produkt_Detal
     */
    private $produkty_detale;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->koszyki_produkty_znakowania = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set koszykiId
     *
     * @param integer $koszykiId
     *
     * @return Koszyk_Produkt
     */
    public function setKoszykiId($koszykiId)
    {
        $this->koszyki_id = $koszykiId;
    
        return $this;
    }

    /**
     * Get koszykiId
     *
     * @return integer
     */
    public function getKoszykiId()
    {
        return $this->koszyki_id;
    }

    /**
     * Set produktyDetaleId
     *
     * @param integer $produktyDetaleId
     *
     * @return Koszyk_Produkt
     */
    public function setProduktyDetaleId($produktyDetaleId)
    {
        $this->produkty_detale_id = $produktyDetaleId;
    
        return $this;
    }

    /**
     * Get produktyDetaleId
     *
     * @return integer
     */
    public function getProduktyDetaleId()
    {
        return $this->produkty_detale_id;
    }

    /**
     * Set cenaSprzedazy
     *
     * @param string $cenaSprzedazy
     *
     * @return Koszyk_Produkt
     */
    public function setCenaSprzedazy($cenaSprzedazy)
    {
        $this->cena_sprzedazy = $cenaSprzedazy;
    
        return $this;
    }

    /**
     * Get cenaSprzedazy
     *
     * @return string
     */
    public function getCenaSprzedazy()
    {
        return $this->cena_sprzedazy;
    }

    /**
     * Set cenaZakupu
     *
     * @param string $cenaZakupu
     *
     * @return Koszyk_Produkt
     */
    public function setCenaZakupu($cenaZakupu)
    {
        $this->cena_zakupu = $cenaZakupu;
    
        return $this;
    }

    /**
     * Get cenaZakupu
     *
     * @return string
     */
    public function getCenaZakupu()
    {
        return $this->cena_zakupu;
    }

    /**
     * Set cenaKonfekcjonowanie
     *
     * @param string $cenaKonfekcjonowanie
     *
     * @return Koszyk_Produkt
     */
    public function setCenaKonfekcjonowanie($cenaKonfekcjonowanie)
    {
        $this->cena_konfekcjonowanie = $cenaKonfekcjonowanie;
    
        return $this;
    }

    /**
     * Get cenaKonfekcjonowanie
     *
     * @return string
     */
    public function getCenaKonfekcjonowanie()
    {
        return $this->cena_konfekcjonowanie;
    }

    /**
     * Set ilosc
     *
     * @param integer $ilosc
     *
     * @return Koszyk_Produkt
     */
    public function setIlosc($ilosc)
    {
        $this->ilosc = $ilosc;
    
        return $this;
    }

    /**
     * Get ilosc
     *
     * @return integer
     */
    public function getIlosc()
    {
        return $this->ilosc;
    }

    /**
     * Add koszykiProduktyZnakowanium
     *
     * @param \Entities\Koszyk_Produkt_Znakowanie $koszykiProduktyZnakowanium
     *
     * @return Koszyk_Produkt
     */
    public function addKoszykiProduktyZnakowanium(\Entities\Koszyk_Produkt_Znakowanie $koszykiProduktyZnakowanium)
    {
        $this->koszyki_produkty_znakowania[] = $koszykiProduktyZnakowanium;
    
        return $this;
    }

    /**
     * Remove koszykiProduktyZnakowanium
     *
     * @param \Entities\Koszyk_Produkt_Znakowanie $koszykiProduktyZnakowanium
     */
    public function removeKoszykiProduktyZnakowanium(\Entities\Koszyk_Produkt_Znakowanie $koszykiProduktyZnakowanium)
    {
        $this->koszyki_produkty_znakowania->removeElement($koszykiProduktyZnakowanium);
    }

    /**
     * Get koszykiProduktyZnakowania
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKoszykiProduktyZnakowania()
    {
        return $this->koszyki_produkty_znakowania;
    }

    /**
     * Set koszyki
     *
     * @param \Entities\Koszyk $koszyki
     *
     * @return Koszyk_Produkt
     */
    public function setKoszyki(\Entities\Koszyk $koszyki = null)
    {
        $this->koszyki = $koszyki;
    
        return $this;
    }

    /**
     * Get koszyki
     *
     * @return \Entities\Koszyk
     */
    public function getKoszyki()
    {
        return $this->koszyki;
    }

    /**
     * Set produktyDetale
     *
     * @param \Entities\Produkt_Detal $produktyDetale
     *
     * @return Koszyk_Produkt
     */
    public function setProduktyDetale(\Entities\Produkt_Detal $produktyDetale = null)
    {
        $this->produkty_detale = $produktyDetale;
    
        return $this;
    }

    /**
     * Get produktyDetale
     *
     * @return \Entities\Produkt_Detal
     */
    public function getProduktyDetale()
    {
        return $this->produkty_detale;
    }
}

