<?php

namespace Entities;

/**
 * Produkt
 */
class Produkt
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdat;

    /**
     * @var \DateTime
     */
    private $updatedat;

    /**
     * @var \DateTime
     */
    private $updatedatuser;

    /**
     * @var integer
     */
    private $kategoria_glowna;

    /**
     * @var integer
     */
    private $producenci_id;

    /**
     * @var integer
     */
    private $dystrybutorzy_id;

    /**
     * @var string
     */
    private $nr_katalogowy;

    /**
     * @var string
     */
    private $nr_producenta;

    /**
     * @var string
     */
    private $identyfikator_producenta;

    /**
     * @var integer
     */
    private $model;

    /**
     * @var integer
     */
    private $odpowiednik;

    /**
     * @var integer
     */
    private $widoczny;

    /**
     * @var boolean
     */
    private $bestseller;

    /**
     * @var boolean
     */
    private $nowosc;

    /**
     * @var boolean
     */
    private $promocja;

    /**
     * @var string
     */
    private $cena_min;

    /**
     * @var integer
     */
    private $usuniety;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $produkty_detale;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $produkty_i18n;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $produkty_zdjecia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $produkty_has_kategorie;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $produkty_has_znakowania;

    /**
     * @var \Entities\Producent
     */
    private $producenci;

    /**
     * @var \Entities\Dystrybutor
     */
    private $dystrybutorzy;

    /**
     * @var \Entities\Kategoria
     */
    private $kategorie;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produkty_detale = new \Doctrine\Common\Collections\ArrayCollection();
        $this->produkty_i18n = new \Doctrine\Common\Collections\ArrayCollection();
        $this->produkty_zdjecia = new \Doctrine\Common\Collections\ArrayCollection();
        $this->produkty_has_kategorie = new \Doctrine\Common\Collections\ArrayCollection();
        $this->produkty_has_znakowania = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Produkt
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;
    
        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set updatedat
     *
     * @param \DateTime $updatedat
     *
     * @return Produkt
     */
    public function setUpdatedat($updatedat)
    {
        $this->updatedat = $updatedat;
    
        return $this;
    }

    /**
     * Get updatedat
     *
     * @return \DateTime
     */
    public function getUpdatedat()
    {
        return $this->updatedat;
    }

    /**
     * Set updatedatuser
     *
     * @param \DateTime $updatedatuser
     *
     * @return Produkt
     */
    public function setUpdatedatuser($updatedatuser)
    {
        $this->updatedatuser = $updatedatuser;
    
        return $this;
    }

    /**
     * Get updatedatuser
     *
     * @return \DateTime
     */
    public function getUpdatedatuser()
    {
        return $this->updatedatuser;
    }

    /**
     * Set kategoriaGlowna
     *
     * @param integer $kategoriaGlowna
     *
     * @return Produkt
     */
    public function setKategoriaGlowna($kategoriaGlowna)
    {
        $this->kategoria_glowna = $kategoriaGlowna;
    
        return $this;
    }

    /**
     * Get kategoriaGlowna
     *
     * @return integer
     */
    public function getKategoriaGlowna()
    {
        return $this->kategoria_glowna;
    }

    /**
     * Set producenciId
     *
     * @param integer $producenciId
     *
     * @return Produkt
     */
    public function setProducenciId($producenciId)
    {
        $this->producenci_id = $producenciId;
    
        return $this;
    }

    /**
     * Get producenciId
     *
     * @return integer
     */
    public function getProducenciId()
    {
        return $this->producenci_id;
    }

    /**
     * Set dystrybutorzyId
     *
     * @param integer $dystrybutorzyId
     *
     * @return Produkt
     */
    public function setDystrybutorzyId($dystrybutorzyId)
    {
        $this->dystrybutorzy_id = $dystrybutorzyId;
    
        return $this;
    }

    /**
     * Get dystrybutorzyId
     *
     * @return integer
     */
    public function getDystrybutorzyId()
    {
        return $this->dystrybutorzy_id;
    }

    /**
     * Set nrKatalogowy
     *
     * @param string $nrKatalogowy
     *
     * @return Produkt
     */
    public function setNrKatalogowy($nrKatalogowy)
    {
        $this->nr_katalogowy = $nrKatalogowy;
    
        return $this;
    }

    /**
     * Get nrKatalogowy
     *
     * @return string
     */
    public function getNrKatalogowy()
    {
        return $this->nr_katalogowy;
    }

    /**
     * Set nrProducenta
     *
     * @param string $nrProducenta
     *
     * @return Produkt
     */
    public function setNrProducenta($nrProducenta)
    {
        $this->nr_producenta = $nrProducenta;
    
        return $this;
    }

    /**
     * Get nrProducenta
     *
     * @return string
     */
    public function getNrProducenta()
    {
        return $this->nr_producenta;
    }

    /**
     * Set identyfikatorProducenta
     *
     * @param string $identyfikatorProducenta
     *
     * @return Produkt
     */
    public function setIdentyfikatorProducenta($identyfikatorProducenta)
    {
        $this->identyfikator_producenta = $identyfikatorProducenta;
    
        return $this;
    }

    /**
     * Get identyfikatorProducenta
     *
     * @return string
     */
    public function getIdentyfikatorProducenta()
    {
        return $this->identyfikator_producenta;
    }

    /**
     * Set model
     *
     * @param integer $model
     *
     * @return Produkt
     */
    public function setModel($model)
    {
        $this->model = $model;
    
        return $this;
    }

    /**
     * Get model
     *
     * @return integer
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set odpowiednik
     *
     * @param integer $odpowiednik
     *
     * @return Produkt
     */
    public function setOdpowiednik($odpowiednik)
    {
        $this->odpowiednik = $odpowiednik;
    
        return $this;
    }

    /**
     * Get odpowiednik
     *
     * @return integer
     */
    public function getOdpowiednik()
    {
        return $this->odpowiednik;
    }

    /**
     * Set widoczny
     *
     * @param integer $widoczny
     *
     * @return Produkt
     */
    public function setWidoczny($widoczny)
    {
        $this->widoczny = $widoczny;
    
        return $this;
    }

    /**
     * Get widoczny
     *
     * @return integer
     */
    public function getWidoczny()
    {
        return $this->widoczny;
    }

    /**
     * Set bestseller
     *
     * @param boolean $bestseller
     *
     * @return Produkt
     */
    public function setBestseller($bestseller)
    {
        $this->bestseller = $bestseller;
    
        return $this;
    }

    /**
     * Get bestseller
     *
     * @return boolean
     */
    public function getBestseller()
    {
        return $this->bestseller;
    }

    /**
     * Set nowosc
     *
     * @param boolean $nowosc
     *
     * @return Produkt
     */
    public function setNowosc($nowosc)
    {
        $this->nowosc = $nowosc;
    
        return $this;
    }

    /**
     * Get nowosc
     *
     * @return boolean
     */
    public function getNowosc()
    {
        return $this->nowosc;
    }

    /**
     * Set promocja
     *
     * @param boolean $promocja
     *
     * @return Produkt
     */
    public function setPromocja($promocja)
    {
        $this->promocja = $promocja;
    
        return $this;
    }

    /**
     * Get promocja
     *
     * @return boolean
     */
    public function getPromocja()
    {
        return $this->promocja;
    }

    /**
     * Set cenaMin
     *
     * @param string $cenaMin
     *
     * @return Produkt
     */
    public function setCenaMin($cenaMin)
    {
        $this->cena_min = $cenaMin;
    
        return $this;
    }

    /**
     * Get cenaMin
     *
     * @return string
     */
    public function getCenaMin()
    {
        return $this->cena_min;
    }

    /**
     * Set usuniety
     *
     * @param integer $usuniety
     *
     * @return Produkt
     */
    public function setUsuniety($usuniety)
    {
        $this->usuniety = $usuniety;
    
        return $this;
    }

    /**
     * Get usuniety
     *
     * @return integer
     */
    public function getUsuniety()
    {
        return $this->usuniety;
    }

    /**
     * Add produktyDetale
     *
     * @param \Entities\Produkt_Detal $produktyDetale
     *
     * @return Produkt
     */
    public function addProduktyDetale(\Entities\Produkt_Detal $produktyDetale)
    {
        $this->produkty_detale[] = $produktyDetale;
    
        return $this;
    }

    /**
     * Remove produktyDetale
     *
     * @param \Entities\Produkt_Detal $produktyDetale
     */
    public function removeProduktyDetale(\Entities\Produkt_Detal $produktyDetale)
    {
        $this->produkty_detale->removeElement($produktyDetale);
    }

    /**
     * Get produktyDetale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduktyDetale()
    {
        return $this->produkty_detale;
    }

    /**
     * Add produktyI18n
     *
     * @param \Entities\Produkt_i18n $produktyI18n
     *
     * @return Produkt
     */
    public function addProduktyI18n(\Entities\Produkt_i18n $produktyI18n)
    {
        $this->produkty_i18n[] = $produktyI18n;
    
        return $this;
    }

    /**
     * Remove produktyI18n
     *
     * @param \Entities\Produkt_i18n $produktyI18n
     */
    public function removeProduktyI18n(\Entities\Produkt_i18n $produktyI18n)
    {
        $this->produkty_i18n->removeElement($produktyI18n);
    }

    /**
     * Get produktyI18n
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduktyI18n()
    {
        return $this->produkty_i18n;
    }

    /**
     * Add produktyZdjecium
     *
     * @param \Entities\Produkt_Zdjecie $produktyZdjecium
     *
     * @return Produkt
     */
    public function addProduktyZdjecium(\Entities\Produkt_Zdjecie $produktyZdjecium)
    {
        $this->produkty_zdjecia[] = $produktyZdjecium;
    
        return $this;
    }

    /**
     * Remove produktyZdjecium
     *
     * @param \Entities\Produkt_Zdjecie $produktyZdjecium
     */
    public function removeProduktyZdjecium(\Entities\Produkt_Zdjecie $produktyZdjecium)
    {
        $this->produkty_zdjecia->removeElement($produktyZdjecium);
    }

    /**
     * Get produktyZdjecia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduktyZdjecia()
    {
        return $this->produkty_zdjecia;
    }

    /**
     * Add produktyHasKategorie
     *
     * @param \Entities\Produkt_Has_Kategoria $produktyHasKategorie
     *
     * @return Produkt
     */
    public function addProduktyHasKategorie(\Entities\Produkt_Has_Kategoria $produktyHasKategorie)
    {
        $this->produkty_has_kategorie[] = $produktyHasKategorie;
    
        return $this;
    }

    /**
     * Remove produktyHasKategorie
     *
     * @param \Entities\Produkt_Has_Kategoria $produktyHasKategorie
     */
    public function removeProduktyHasKategorie(\Entities\Produkt_Has_Kategoria $produktyHasKategorie)
    {
        $this->produkty_has_kategorie->removeElement($produktyHasKategorie);
    }

    /**
     * Get produktyHasKategorie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduktyHasKategorie()
    {
        return $this->produkty_has_kategorie;
    }

    /**
     * Add produktyHasZnakowanium
     *
     * @param \Entities\Produkt_Has_Znakowanie $produktyHasZnakowanium
     *
     * @return Produkt
     */
    public function addProduktyHasZnakowanium(\Entities\Produkt_Has_Znakowanie $produktyHasZnakowanium)
    {
        $this->produkty_has_znakowania[] = $produktyHasZnakowanium;
    
        return $this;
    }

    /**
     * Remove produktyHasZnakowanium
     *
     * @param \Entities\Produkt_Has_Znakowanie $produktyHasZnakowanium
     */
    public function removeProduktyHasZnakowanium(\Entities\Produkt_Has_Znakowanie $produktyHasZnakowanium)
    {
        $this->produkty_has_znakowania->removeElement($produktyHasZnakowanium);
    }

    /**
     * Get produktyHasZnakowania
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduktyHasZnakowania()
    {
        return $this->produkty_has_znakowania;
    }

    /**
     * Set producenci
     *
     * @param \Entities\Producent $producenci
     *
     * @return Produkt
     */
    public function setProducenci(\Entities\Producent $producenci = null)
    {
        $this->producenci = $producenci;
    
        return $this;
    }

    /**
     * Get producenci
     *
     * @return \Entities\Producent
     */
    public function getProducenci()
    {
        return $this->producenci;
    }

    /**
     * Set dystrybutorzy
     *
     * @param \Entities\Dystrybutor $dystrybutorzy
     *
     * @return Produkt
     */
    public function setDystrybutorzy(\Entities\Dystrybutor $dystrybutorzy = null)
    {
        $this->dystrybutorzy = $dystrybutorzy;
    
        return $this;
    }

    /**
     * Get dystrybutorzy
     *
     * @return \Entities\Dystrybutor
     */
    public function getDystrybutorzy()
    {
        return $this->dystrybutorzy;
    }

    /**
     * Set kategorie
     *
     * @param \Entities\Kategoria $kategorie
     *
     * @return Produkt
     */
    public function setKategorie(\Entities\Kategoria $kategorie = null)
    {
        $this->kategorie = $kategorie;
    
        return $this;
    }

    /**
     * Get kategorie
     *
     * @return \Entities\Kategoria
     */
    public function getKategorie()
    {
        return $this->kategorie;
    }
}

