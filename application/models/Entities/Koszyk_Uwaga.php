<?php

namespace Entities;

/**
 * Koszyk_Uwaga
 */
class Koszyk_Uwaga
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $koszyki_id;

    /**
     * @var integer
     */
    private $uzytkownicy_id;

    /**
     * @var string
     */
    private $tresc;

    /**
     * @var \DateTime
     */
    private $data_wpisu;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \Entities\Koszyk
     */
    private $koszyki;

    /**
     * @var \Entities\Uzytkownik
     */
    private $uzytkownicy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set koszykiId
     *
     * @param integer $koszykiId
     *
     * @return Koszyk_Uwaga
     */
    public function setKoszykiId($koszykiId)
    {
        $this->koszyki_id = $koszykiId;
    
        return $this;
    }

    /**
     * Get koszykiId
     *
     * @return integer
     */
    public function getKoszykiId()
    {
        return $this->koszyki_id;
    }

    /**
     * Set uzytkownicyId
     *
     * @param integer $uzytkownicyId
     *
     * @return Koszyk_Uwaga
     */
    public function setUzytkownicyId($uzytkownicyId)
    {
        $this->uzytkownicy_id = $uzytkownicyId;
    
        return $this;
    }

    /**
     * Get uzytkownicyId
     *
     * @return integer
     */
    public function getUzytkownicyId()
    {
        return $this->uzytkownicy_id;
    }

    /**
     * Set tresc
     *
     * @param string $tresc
     *
     * @return Koszyk_Uwaga
     */
    public function setTresc($tresc)
    {
        $this->tresc = $tresc;
    
        return $this;
    }

    /**
     * Get tresc
     *
     * @return string
     */
    public function getTresc()
    {
        return $this->tresc;
    }

    /**
     * Set dataWpisu
     *
     * @param \DateTime $dataWpisu
     *
     * @return Koszyk_Uwaga
     */
    public function setDataWpisu($dataWpisu)
    {
        $this->data_wpisu = $dataWpisu;
    
        return $this;
    }

    /**
     * Get dataWpisu
     *
     * @return \DateTime
     */
    public function getDataWpisu()
    {
        return $this->data_wpisu;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Koszyk_Uwaga
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set koszyki
     *
     * @param \Entities\Koszyk $koszyki
     *
     * @return Koszyk_Uwaga
     */
    public function setKoszyki(\Entities\Koszyk $koszyki = null)
    {
        $this->koszyki = $koszyki;
    
        return $this;
    }

    /**
     * Get koszyki
     *
     * @return \Entities\Koszyk
     */
    public function getKoszyki()
    {
        return $this->koszyki;
    }

    /**
     * Set uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     *
     * @return Koszyk_Uwaga
     */
    public function setUzytkownicy(\Entities\Uzytkownik $uzytkownicy = null)
    {
        $this->uzytkownicy = $uzytkownicy;
    
        return $this;
    }

    /**
     * Get uzytkownicy
     *
     * @return \Entities\Uzytkownik
     */
    public function getUzytkownicy()
    {
        return $this->uzytkownicy;
    }
}

