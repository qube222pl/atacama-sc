<?php

namespace Entities;

/**
 * Znakowanie_Rodzaj
 */
class Znakowanie_Rodzaj
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $znakowania_id;

    /**
     * @var string
     */
    private $cena;

    /**
     * @var string
     */
    private $cena_inny;

    /**
     * @var integer
     */
    private $minimum;

    /**
     * @var integer
     */
    private $maximum;

    /**
     * @var \Entities\Znakowanie
     */
    private $znakowania;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set znakowaniaId
     *
     * @param integer $znakowaniaId
     *
     * @return Znakowanie_Rodzaj
     */
    public function setZnakowaniaId($znakowaniaId)
    {
        $this->znakowania_id = $znakowaniaId;
    
        return $this;
    }

    /**
     * Get znakowaniaId
     *
     * @return integer
     */
    public function getZnakowaniaId()
    {
        return $this->znakowania_id;
    }

    /**
     * Set cena
     *
     * @param string $cena
     *
     * @return Znakowanie_Rodzaj
     */
    public function setCena($cena)
    {
        $this->cena = $cena;
    
        return $this;
    }

    /**
     * Get cena
     *
     * @return string
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * Set cenaInny
     *
     * @param string $cenaInny
     *
     * @return Znakowanie_Rodzaj
     */
    public function setCenaInny($cenaInny)
    {
        $this->cena_inny = $cenaInny;
    
        return $this;
    }

    /**
     * Get cenaInny
     *
     * @return string
     */
    public function getCenaInny()
    {
        return $this->cena_inny;
    }

    /**
     * Set minimum
     *
     * @param integer $minimum
     *
     * @return Znakowanie_Rodzaj
     */
    public function setMinimum($minimum)
    {
        $this->minimum = $minimum;
    
        return $this;
    }

    /**
     * Get minimum
     *
     * @return integer
     */
    public function getMinimum()
    {
        return $this->minimum;
    }

    /**
     * Set maximum
     *
     * @param integer $maximum
     *
     * @return Znakowanie_Rodzaj
     */
    public function setMaximum($maximum)
    {
        $this->maximum = $maximum;
    
        return $this;
    }

    /**
     * Get maximum
     *
     * @return integer
     */
    public function getMaximum()
    {
        return $this->maximum;
    }

    /**
     * Set znakowania
     *
     * @param \Entities\Znakowanie $znakowania
     *
     * @return Znakowanie_Rodzaj
     */
    public function setZnakowania(\Entities\Znakowanie $znakowania = null)
    {
        $this->znakowania = $znakowania;
    
        return $this;
    }

    /**
     * Get znakowania
     *
     * @return \Entities\Znakowanie
     */
    public function getZnakowania()
    {
        return $this->znakowania;
    }
}

