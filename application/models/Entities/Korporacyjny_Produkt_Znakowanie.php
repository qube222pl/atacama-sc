<?php

namespace Entities;

/**
 * Korporacyjny_Produkt_Znakowanie
 */
class Korporacyjny_Produkt_Znakowanie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $korporacyjni_produkty_id;

    /**
     * @var integer
     */
    private $znakowanie_id;

    /**
     * @var integer
     */
    private $ilosc_kolorow;

    /**
     * @var \Entities\Korporacyjny_Produkt
     */
    private $korporacyjni_produkty;

    /**
     * @var \Entities\Znakowanie
     */
    private $znakowanie;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set korporacyjniProduktyId
     *
     * @param integer $korporacyjniProduktyId
     *
     * @return Korporacyjny_Produkt_Znakowanie
     */
    public function setKorporacyjniProduktyId($korporacyjniProduktyId)
    {
        $this->korporacyjni_produkty_id = $korporacyjniProduktyId;
    
        return $this;
    }

    /**
     * Get korporacyjniProduktyId
     *
     * @return integer
     */
    public function getKorporacyjniProduktyId()
    {
        return $this->korporacyjni_produkty_id;
    }

    /**
     * Set znakowanieId
     *
     * @param integer $znakowanieId
     *
     * @return Korporacyjny_Produkt_Znakowanie
     */
    public function setZnakowanieId($znakowanieId)
    {
        $this->znakowanie_id = $znakowanieId;
    
        return $this;
    }

    /**
     * Get znakowanieId
     *
     * @return integer
     */
    public function getZnakowanieId()
    {
        return $this->znakowanie_id;
    }

    /**
     * Set iloscKolorow
     *
     * @param integer $iloscKolorow
     *
     * @return Korporacyjny_Produkt_Znakowanie
     */
    public function setIloscKolorow($iloscKolorow)
    {
        $this->ilosc_kolorow = $iloscKolorow;
    
        return $this;
    }

    /**
     * Get iloscKolorow
     *
     * @return integer
     */
    public function getIloscKolorow()
    {
        return $this->ilosc_kolorow;
    }

    /**
     * Set korporacyjniProdukty
     *
     * @param \Entities\Korporacyjny_Produkt $korporacyjniProdukty
     *
     * @return Korporacyjny_Produkt_Znakowanie
     */
    public function setKorporacyjniProdukty(\Entities\Korporacyjny_Produkt $korporacyjniProdukty = null)
    {
        $this->korporacyjni_produkty = $korporacyjniProdukty;
    
        return $this;
    }

    /**
     * Get korporacyjniProdukty
     *
     * @return \Entities\Korporacyjny_Produkt
     */
    public function getKorporacyjniProdukty()
    {
        return $this->korporacyjni_produkty;
    }

    /**
     * Set znakowanie
     *
     * @param \Entities\Znakowanie $znakowanie
     *
     * @return Korporacyjny_Produkt_Znakowanie
     */
    public function setZnakowanie(\Entities\Znakowanie $znakowanie = null)
    {
        $this->znakowanie = $znakowanie;
    
        return $this;
    }

    /**
     * Get znakowanie
     *
     * @return \Entities\Znakowanie
     */
    public function getZnakowanie()
    {
        return $this->znakowanie;
    }
}

