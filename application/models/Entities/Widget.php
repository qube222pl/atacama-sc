<?php

namespace Entities;

/**
 * Widget
 */
class Widget
{
    /**
     * @var string
     */
    private $kod;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var string
     */
    private $tresc;


    /**
     * Set kod
     *
     * @param string $kod
     *
     * @return Widget
     */
    public function setKod($kod)
    {
        $this->kod = $kod;
    
        return $this;
    }

    /**
     * Get kod
     *
     * @return string
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return Widget
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Widget
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set tresc
     *
     * @param string $tresc
     *
     * @return Widget
     */
    public function setTresc($tresc)
    {
        $this->tresc = $tresc;
    
        return $this;
    }

    /**
     * Get tresc
     *
     * @return string
     */
    public function getTresc()
    {
        return $this->tresc;
    }
}

