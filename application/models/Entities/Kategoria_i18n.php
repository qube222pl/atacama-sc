<?php

namespace Entities;

/**
 * Kategoria_i18n
 */
class Kategoria_i18n
{
    /**
     * @var integer
     */
    private $kategorie_id;

    /**
     * @var string
     */
    private $jezyk;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var string
     */
    private $opis_seo;

    /**
     * @var \DateTime
     */
    private $data_modyfikacji;

    /**
     * @var \Entities\Kategoria
     */
    private $kategorie;


    /**
     * Set kategorieId
     *
     * @param integer $kategorieId
     *
     * @return Kategoria_i18n
     */
    public function setKategorieId($kategorieId)
    {
        $this->kategorie_id = $kategorieId;
    
        return $this;
    }

    /**
     * Get kategorieId
     *
     * @return integer
     */
    public function getKategorieId()
    {
        return $this->kategorie_id;
    }

    /**
     * Set jezyk
     *
     * @param string $jezyk
     *
     * @return Kategoria_i18n
     */
    public function setJezyk($jezyk)
    {
        $this->jezyk = $jezyk;
    
        return $this;
    }

    /**
     * Get jezyk
     *
     * @return string
     */
    public function getJezyk()
    {
        return $this->jezyk;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Kategoria_i18n
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Kategoria_i18n
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set opisSeo
     *
     * @param string $opisSeo
     *
     * @return Kategoria_i18n
     */
    public function setOpisSeo($opisSeo)
    {
        $this->opis_seo = $opisSeo;
    
        return $this;
    }

    /**
     * Get opisSeo
     *
     * @return string
     */
    public function getOpisSeo()
    {
        return $this->opis_seo;
    }

    /**
     * Set dataModyfikacji
     *
     * @param \DateTime $dataModyfikacji
     *
     * @return Kategoria_i18n
     */
    public function setDataModyfikacji($dataModyfikacji)
    {
        $this->data_modyfikacji = $dataModyfikacji;
    
        return $this;
    }

    /**
     * Get dataModyfikacji
     *
     * @return \DateTime
     */
    public function getDataModyfikacji()
    {
        return $this->data_modyfikacji;
    }

    /**
     * Set kategorie
     *
     * @param \Entities\Kategoria $kategorie
     *
     * @return Kategoria_i18n
     */
    public function setKategorie(\Entities\Kategoria $kategorie = null)
    {
        $this->kategorie = $kategorie;
    
        return $this;
    }

    /**
     * Get kategorie
     *
     * @return \Entities\Kategoria
     */
    public function getKategorie()
    {
        return $this->kategorie;
    }
}

