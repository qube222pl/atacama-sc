<?php

namespace Entities;

/**
 * Powiadomienie
 */
class Powiadomienie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $uzytkownicy_id;

    /**
     * @var \DateTime
     */
    private $data;

    /**
     * @var integer
     */
    private $typ;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var integer
     */
    private $przeczytane;

    /**
     * @var integer
     */
    private $widoczne;

    /**
     * @var \Entities\Uzytkownik
     */
    private $uzytkownicy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uzytkownicyId
     *
     * @param integer $uzytkownicyId
     *
     * @return Powiadomienie
     */
    public function setUzytkownicyId($uzytkownicyId)
    {
        $this->uzytkownicy_id = $uzytkownicyId;
    
        return $this;
    }

    /**
     * Get uzytkownicyId
     *
     * @return integer
     */
    public function getUzytkownicyId()
    {
        return $this->uzytkownicy_id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Powiadomienie
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set typ
     *
     * @param integer $typ
     *
     * @return Powiadomienie
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;
    
        return $this;
    }

    /**
     * Get typ
     *
     * @return integer
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Powiadomienie
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set przeczytane
     *
     * @param integer $przeczytane
     *
     * @return Powiadomienie
     */
    public function setPrzeczytane($przeczytane)
    {
        $this->przeczytane = $przeczytane;
    
        return $this;
    }

    /**
     * Get przeczytane
     *
     * @return integer
     */
    public function getPrzeczytane()
    {
        return $this->przeczytane;
    }

    /**
     * Set widoczne
     *
     * @param integer $widoczne
     *
     * @return Powiadomienie
     */
    public function setWidoczne($widoczne)
    {
        $this->widoczne = $widoczne;
    
        return $this;
    }

    /**
     * Get widoczne
     *
     * @return integer
     */
    public function getWidoczne()
    {
        return $this->widoczne;
    }

    /**
     * Set uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     *
     * @return Powiadomienie
     */
    public function setUzytkownicy(\Entities\Uzytkownik $uzytkownicy = null)
    {
        $this->uzytkownicy = $uzytkownicy;
    
        return $this;
    }

    /**
     * Get uzytkownicy
     *
     * @return \Entities\Uzytkownik
     */
    public function getUzytkownicy()
    {
        return $this->uzytkownicy;
    }
}

