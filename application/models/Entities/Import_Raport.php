<?php

namespace Entities;

/**
 * Import_Raport
 */
class Import_Raport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dystrybutorzy_id;

    /**
     * @var string
     */
    private $pliklog;

    /**
     * @var string
     */
    private $opis;

    /**
     * @var \DateTime
     */
    private $data;

    /**
     * @var \Entities\Dystrybutor
     */
    private $dystrybutorzy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dystrybutorzyId
     *
     * @param integer $dystrybutorzyId
     *
     * @return Import_Raport
     */
    public function setDystrybutorzyId($dystrybutorzyId)
    {
        $this->dystrybutorzy_id = $dystrybutorzyId;
    
        return $this;
    }

    /**
     * Get dystrybutorzyId
     *
     * @return integer
     */
    public function getDystrybutorzyId()
    {
        return $this->dystrybutorzy_id;
    }

    /**
     * Set pliklog
     *
     * @param string $pliklog
     *
     * @return Import_Raport
     */
    public function setPliklog($pliklog)
    {
        $this->pliklog = $pliklog;
    
        return $this;
    }

    /**
     * Get pliklog
     *
     * @return string
     */
    public function getPliklog()
    {
        return $this->pliklog;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return Import_Raport
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Import_Raport
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set dystrybutorzy
     *
     * @param \Entities\Dystrybutor $dystrybutorzy
     *
     * @return Import_Raport
     */
    public function setDystrybutorzy(\Entities\Dystrybutor $dystrybutorzy = null)
    {
        $this->dystrybutorzy = $dystrybutorzy;
    
        return $this;
    }

    /**
     * Get dystrybutorzy
     *
     * @return \Entities\Dystrybutor
     */
    public function getDystrybutorzy()
    {
        return $this->dystrybutorzy;
    }
}

