<?php

namespace Entities;

/**
 * Produkt_Zdjecie
 */
class Produkt_Zdjecie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $produkty_id;

    /**
     * @var integer
     */
    private $kolory_id;

    /**
     * @var string
     */
    private $plik;

    /**
     * @var string
     */
    private $plik_dystr;

    /**
     * @var integer
     */
    private $glowne;

    /**
     * @var integer
     */
    private $nazwa_zmieniona;

    /**
     * @var integer
     */
    private $pozycja;

    /**
     * @var string
     */
    private $widoczne;

    /**
     * @var \Entities\Produkt
     */
    private $produkty;

    /**
     * @var \Entities\Kolor
     */
    private $kolory;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set produktyId
     *
     * @param integer $produktyId
     *
     * @return Produkt_Zdjecie
     */
    public function setProduktyId($produktyId)
    {
        $this->produkty_id = $produktyId;
    
        return $this;
    }

    /**
     * Get produktyId
     *
     * @return integer
     */
    public function getProduktyId()
    {
        return $this->produkty_id;
    }

    /**
     * Set koloryId
     *
     * @param integer $koloryId
     *
     * @return Produkt_Zdjecie
     */
    public function setKoloryId($koloryId)
    {
        $this->kolory_id = $koloryId;
    
        return $this;
    }

    /**
     * Get koloryId
     *
     * @return integer
     */
    public function getKoloryId()
    {
        return $this->kolory_id;
    }

    /**
     * Set plik
     *
     * @param string $plik
     *
     * @return Produkt_Zdjecie
     */
    public function setPlik($plik)
    {
        $this->plik = $plik;
    
        return $this;
    }

    /**
     * Get plik
     *
     * @return string
     */
    public function getPlik()
    {
        return $this->plik;
    }

    /**
     * Set plikDystr
     *
     * @param string $plikDystr
     *
     * @return Produkt_Zdjecie
     */
    public function setPlikDystr($plikDystr)
    {
        $this->plik_dystr = $plikDystr;
    
        return $this;
    }

    /**
     * Get plikDystr
     *
     * @return string
     */
    public function getPlikDystr()
    {
        return $this->plik_dystr;
    }

    /**
     * Set glowne
     *
     * @param integer $glowne
     *
     * @return Produkt_Zdjecie
     */
    public function setGlowne($glowne)
    {
        $this->glowne = $glowne;
    
        return $this;
    }

    /**
     * Get glowne
     *
     * @return integer
     */
    public function getGlowne()
    {
        return $this->glowne;
    }

    /**
     * Set nazwaZmieniona
     *
     * @param integer $nazwaZmieniona
     *
     * @return Produkt_Zdjecie
     */
    public function setNazwaZmieniona($nazwaZmieniona)
    {
        $this->nazwa_zmieniona = $nazwaZmieniona;
    
        return $this;
    }

    /**
     * Get nazwaZmieniona
     *
     * @return integer
     */
    public function getNazwaZmieniona()
    {
        return $this->nazwa_zmieniona;
    }

    /**
     * Set pozycja
     *
     * @param integer $pozycja
     *
     * @return Produkt_Zdjecie
     */
    public function setPozycja($pozycja)
    {
        $this->pozycja = $pozycja;
    
        return $this;
    }

    /**
     * Get pozycja
     *
     * @return integer
     */
    public function getPozycja()
    {
        return $this->pozycja;
    }

    /**
     * Set widoczne
     *
     * @param string $widoczne
     *
     * @return Produkt_Zdjecie
     */
    public function setWidoczne($widoczne)
    {
        $this->widoczne = $widoczne;
    
        return $this;
    }

    /**
     * Get widoczne
     *
     * @return string
     */
    public function getWidoczne()
    {
        return $this->widoczne;
    }

    /**
     * Set produkty
     *
     * @param \Entities\Produkt $produkty
     *
     * @return Produkt_Zdjecie
     */
    public function setProdukty(\Entities\Produkt $produkty = null)
    {
        $this->produkty = $produkty;
    
        return $this;
    }

    /**
     * Get produkty
     *
     * @return \Entities\Produkt
     */
    public function getProdukty()
    {
        return $this->produkty;
    }

    /**
     * Set kolory
     *
     * @param \Entities\Kolor $kolory
     *
     * @return Produkt_Zdjecie
     */
    public function setKolory(\Entities\Kolor $kolory = null)
    {
        $this->kolory = $kolory;
    
        return $this;
    }

    /**
     * Get kolory
     *
     * @return \Entities\Kolor
     */
    public function getKolory()
    {
        return $this->kolory;
    }
}

