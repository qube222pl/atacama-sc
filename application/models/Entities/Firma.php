<?php

namespace Entities;

/**
 * Firma
 */
class Firma
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var string
     */
    private $nip;

    /**
     * @var string
     */
    private $regon;

    /**
     * @var integer
     */
    private $upust;

    /**
     * @var integer
     */
    private $ph_id;

    /**
     * @var integer
     */
    private $budzet_typ_rozliczenia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $uzytkownicy;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $koszyki;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $korporacyjni_produkty;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $korporacyjni_uzytkownicy;

    /**
     * @var \Entities\Uzytkownik
     */
    private $ph;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->uzytkownicy = new \Doctrine\Common\Collections\ArrayCollection();
        $this->koszyki = new \Doctrine\Common\Collections\ArrayCollection();
        $this->korporacyjni_produkty = new \Doctrine\Common\Collections\ArrayCollection();
        $this->korporacyjni_uzytkownicy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Firma
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set nip
     *
     * @param string $nip
     *
     * @return Firma
     */
    public function setNip($nip)
    {
        $this->nip = $nip;
    
        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set regon
     *
     * @param string $regon
     *
     * @return Firma
     */
    public function setRegon($regon)
    {
        $this->regon = $regon;
    
        return $this;
    }

    /**
     * Get regon
     *
     * @return string
     */
    public function getRegon()
    {
        return $this->regon;
    }

    /**
     * Set upust
     *
     * @param integer $upust
     *
     * @return Firma
     */
    public function setUpust($upust)
    {
        $this->upust = $upust;
    
        return $this;
    }

    /**
     * Get upust
     *
     * @return integer
     */
    public function getUpust()
    {
        return $this->upust;
    }

    /**
     * Set phId
     *
     * @param integer $phId
     *
     * @return Firma
     */
    public function setPhId($phId)
    {
        $this->ph_id = $phId;
    
        return $this;
    }

    /**
     * Get phId
     *
     * @return integer
     */
    public function getPhId()
    {
        return $this->ph_id;
    }

    /**
     * Set budzetTypRozliczenia
     *
     * @param integer $budzetTypRozliczenia
     *
     * @return Firma
     */
    public function setBudzetTypRozliczenia($budzetTypRozliczenia)
    {
        $this->budzet_typ_rozliczenia = $budzetTypRozliczenia;
    
        return $this;
    }

    /**
     * Get budzetTypRozliczenia
     *
     * @return integer
     */
    public function getBudzetTypRozliczenia()
    {
        return $this->budzet_typ_rozliczenia;
    }

    /**
     * Add uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     *
     * @return Firma
     */
    public function addUzytkownicy(\Entities\Uzytkownik $uzytkownicy)
    {
        $this->uzytkownicy[] = $uzytkownicy;
    
        return $this;
    }

    /**
     * Remove uzytkownicy
     *
     * @param \Entities\Uzytkownik $uzytkownicy
     */
    public function removeUzytkownicy(\Entities\Uzytkownik $uzytkownicy)
    {
        $this->uzytkownicy->removeElement($uzytkownicy);
    }

    /**
     * Get uzytkownicy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUzytkownicy()
    {
        return $this->uzytkownicy;
    }

    /**
     * Add koszyki
     *
     * @param \Entities\Koszyk $koszyki
     *
     * @return Firma
     */
    public function addKoszyki(\Entities\Koszyk $koszyki)
    {
        $this->koszyki[] = $koszyki;
    
        return $this;
    }

    /**
     * Remove koszyki
     *
     * @param \Entities\Koszyk $koszyki
     */
    public function removeKoszyki(\Entities\Koszyk $koszyki)
    {
        $this->koszyki->removeElement($koszyki);
    }

    /**
     * Get koszyki
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKoszyki()
    {
        return $this->koszyki;
    }

    /**
     * Add korporacyjniProdukty
     *
     * @param \Entities\Korporacyjny_Produkt $korporacyjniProdukty
     *
     * @return Firma
     */
    public function addKorporacyjniProdukty(\Entities\Korporacyjny_Produkt $korporacyjniProdukty)
    {
        $this->korporacyjni_produkty[] = $korporacyjniProdukty;
    
        return $this;
    }

    /**
     * Remove korporacyjniProdukty
     *
     * @param \Entities\Korporacyjny_Produkt $korporacyjniProdukty
     */
    public function removeKorporacyjniProdukty(\Entities\Korporacyjny_Produkt $korporacyjniProdukty)
    {
        $this->korporacyjni_produkty->removeElement($korporacyjniProdukty);
    }

    /**
     * Get korporacyjniProdukty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKorporacyjniProdukty()
    {
        return $this->korporacyjni_produkty;
    }

    /**
     * Add korporacyjniUzytkownicy
     *
     * @param \Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy
     *
     * @return Firma
     */
    public function addKorporacyjniUzytkownicy(\Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy)
    {
        $this->korporacyjni_uzytkownicy[] = $korporacyjniUzytkownicy;
    
        return $this;
    }

    /**
     * Remove korporacyjniUzytkownicy
     *
     * @param \Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy
     */
    public function removeKorporacyjniUzytkownicy(\Entities\Korporacyjny_Uzytkownik $korporacyjniUzytkownicy)
    {
        $this->korporacyjni_uzytkownicy->removeElement($korporacyjniUzytkownicy);
    }

    /**
     * Get korporacyjniUzytkownicy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKorporacyjniUzytkownicy()
    {
        return $this->korporacyjni_uzytkownicy;
    }

    /**
     * Set ph
     *
     * @param \Entities\Uzytkownik $ph
     *
     * @return Firma
     */
    public function setPh(\Entities\Uzytkownik $ph = null)
    {
        $this->ph = $ph;
    
        return $this;
    }

    /**
     * Get ph
     *
     * @return \Entities\Uzytkownik
     */
    public function getPh()
    {
        return $this->ph;
    }
}

