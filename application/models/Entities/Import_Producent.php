<?php

namespace Entities;

/**
 * Import_Producent
 */
class Import_Producent
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dystrybutorzy_id;

    /**
     * @var integer
     */
    private $producenci_id;

    /**
     * @var string
     */
    private $producent_dystr_id;

    /**
     * @var string
     */
    private $producent_dystr_nazwa;

    /**
     * @var string
     */
    private $rabat;

    /**
     * @var \Entities\Dystrybutor
     */
    private $dystrybutorzy;

    /**
     * @var \Entities\Producent
     */
    private $producenci;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dystrybutorzyId
     *
     * @param integer $dystrybutorzyId
     *
     * @return Import_Producent
     */
    public function setDystrybutorzyId($dystrybutorzyId)
    {
        $this->dystrybutorzy_id = $dystrybutorzyId;
    
        return $this;
    }

    /**
     * Get dystrybutorzyId
     *
     * @return integer
     */
    public function getDystrybutorzyId()
    {
        return $this->dystrybutorzy_id;
    }

    /**
     * Set producenciId
     *
     * @param integer $producenciId
     *
     * @return Import_Producent
     */
    public function setProducenciId($producenciId)
    {
        $this->producenci_id = $producenciId;
    
        return $this;
    }

    /**
     * Get producenciId
     *
     * @return integer
     */
    public function getProducenciId()
    {
        return $this->producenci_id;
    }

    /**
     * Set producentDystrId
     *
     * @param string $producentDystrId
     *
     * @return Import_Producent
     */
    public function setProducentDystrId($producentDystrId)
    {
        $this->producent_dystr_id = $producentDystrId;
    
        return $this;
    }

    /**
     * Get producentDystrId
     *
     * @return string
     */
    public function getProducentDystrId()
    {
        return $this->producent_dystr_id;
    }

    /**
     * Set producentDystrNazwa
     *
     * @param string $producentDystrNazwa
     *
     * @return Import_Producent
     */
    public function setProducentDystrNazwa($producentDystrNazwa)
    {
        $this->producent_dystr_nazwa = $producentDystrNazwa;
    
        return $this;
    }

    /**
     * Get producentDystrNazwa
     *
     * @return string
     */
    public function getProducentDystrNazwa()
    {
        return $this->producent_dystr_nazwa;
    }

    /**
     * Set rabat
     *
     * @param string $rabat
     *
     * @return Import_Producent
     */
    public function setRabat($rabat)
    {
        $this->rabat = $rabat;
    
        return $this;
    }

    /**
     * Get rabat
     *
     * @return string
     */
    public function getRabat()
    {
        return $this->rabat;
    }

    /**
     * Set dystrybutorzy
     *
     * @param \Entities\Dystrybutor $dystrybutorzy
     *
     * @return Import_Producent
     */
    public function setDystrybutorzy(\Entities\Dystrybutor $dystrybutorzy = null)
    {
        $this->dystrybutorzy = $dystrybutorzy;
    
        return $this;
    }

    /**
     * Get dystrybutorzy
     *
     * @return \Entities\Dystrybutor
     */
    public function getDystrybutorzy()
    {
        return $this->dystrybutorzy;
    }

    /**
     * Set producenci
     *
     * @param \Entities\Producent $producenci
     *
     * @return Import_Producent
     */
    public function setProducenci(\Entities\Producent $producenci = null)
    {
        $this->producenci = $producenci;
    
        return $this;
    }

    /**
     * Get producenci
     *
     * @return \Entities\Producent
     */
    public function getProducenci()
    {
        return $this->producenci;
    }
}

