<?php

namespace Entities;

/**
 * Korporacyjny_Produkt
 */
class Korporacyjny_Produkt
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $firmy_id;

    /**
     * @var integer
     */
    private $produkty_detale_id;

    /**
     * @var string
     */
    private $zdjecie;

    /**
     * @var string
     */
    private $cena;

    /**
     * @var integer
     */
    private $stock;

    /**
     * @var integer
     */
    private $min_ilosc;

    /**
     * @var integer
     */
    private $min_stock;

    /**
     * @var \DateTime
     */
    private $data_dodania;

    /**
     * @var integer
     */
    private $czas_dodruku;

    /**
     * @var integer
     */
    private $powiadomienie;

    /**
     * @var integer
     */
    private $widoczny;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $korporacyjni_produkty_znakowania;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $korporacyjni_koszyki_produkty;

    /**
     * @var \Entities\Firma
     */
    private $firmy;

    /**
     * @var \Entities\Produkt_Detal
     */
    private $produkty_detale;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->korporacyjni_produkty_znakowania = new \Doctrine\Common\Collections\ArrayCollection();
        $this->korporacyjni_koszyki_produkty = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firmyId
     *
     * @param integer $firmyId
     *
     * @return Korporacyjny_Produkt
     */
    public function setFirmyId($firmyId)
    {
        $this->firmy_id = $firmyId;
    
        return $this;
    }

    /**
     * Get firmyId
     *
     * @return integer
     */
    public function getFirmyId()
    {
        return $this->firmy_id;
    }

    /**
     * Set produktyDetaleId
     *
     * @param integer $produktyDetaleId
     *
     * @return Korporacyjny_Produkt
     */
    public function setProduktyDetaleId($produktyDetaleId)
    {
        $this->produkty_detale_id = $produktyDetaleId;
    
        return $this;
    }

    /**
     * Get produktyDetaleId
     *
     * @return integer
     */
    public function getProduktyDetaleId()
    {
        return $this->produkty_detale_id;
    }

    /**
     * Set zdjecie
     *
     * @param string $zdjecie
     *
     * @return Korporacyjny_Produkt
     */
    public function setZdjecie($zdjecie)
    {
        $this->zdjecie = $zdjecie;
    
        return $this;
    }

    /**
     * Get zdjecie
     *
     * @return string
     */
    public function getZdjecie()
    {
        return $this->zdjecie;
    }

    /**
     * Set cena
     *
     * @param string $cena
     *
     * @return Korporacyjny_Produkt
     */
    public function setCena($cena)
    {
        $this->cena = $cena;
    
        return $this;
    }

    /**
     * Get cena
     *
     * @return string
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Korporacyjny_Produkt
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    
        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set minIlosc
     *
     * @param integer $minIlosc
     *
     * @return Korporacyjny_Produkt
     */
    public function setMinIlosc($minIlosc)
    {
        $this->min_ilosc = $minIlosc;
    
        return $this;
    }

    /**
     * Get minIlosc
     *
     * @return integer
     */
    public function getMinIlosc()
    {
        return $this->min_ilosc;
    }

    /**
     * Set minStock
     *
     * @param integer $minStock
     *
     * @return Korporacyjny_Produkt
     */
    public function setMinStock($minStock)
    {
        $this->min_stock = $minStock;
    
        return $this;
    }

    /**
     * Get minStock
     *
     * @return integer
     */
    public function getMinStock()
    {
        return $this->min_stock;
    }

    /**
     * Set dataDodania
     *
     * @param \DateTime $dataDodania
     *
     * @return Korporacyjny_Produkt
     */
    public function setDataDodania($dataDodania)
    {
        $this->data_dodania = $dataDodania;
    
        return $this;
    }

    /**
     * Get dataDodania
     *
     * @return \DateTime
     */
    public function getDataDodania()
    {
        return $this->data_dodania;
    }

    /**
     * Set czasDodruku
     *
     * @param integer $czasDodruku
     *
     * @return Korporacyjny_Produkt
     */
    public function setCzasDodruku($czasDodruku)
    {
        $this->czas_dodruku = $czasDodruku;
    
        return $this;
    }

    /**
     * Get czasDodruku
     *
     * @return integer
     */
    public function getCzasDodruku()
    {
        return $this->czas_dodruku;
    }

    /**
     * Set powiadomienie
     *
     * @param integer $powiadomienie
     *
     * @return Korporacyjny_Produkt
     */
    public function setPowiadomienie($powiadomienie)
    {
        $this->powiadomienie = $powiadomienie;
    
        return $this;
    }

    /**
     * Get powiadomienie
     *
     * @return integer
     */
    public function getPowiadomienie()
    {
        return $this->powiadomienie;
    }

    /**
     * Set widoczny
     *
     * @param integer $widoczny
     *
     * @return Korporacyjny_Produkt
     */
    public function setWidoczny($widoczny)
    {
        $this->widoczny = $widoczny;
    
        return $this;
    }

    /**
     * Get widoczny
     *
     * @return integer
     */
    public function getWidoczny()
    {
        return $this->widoczny;
    }

    /**
     * Add korporacyjniProduktyZnakowanium
     *
     * @param \Entities\Korporacyjny_Produkt_Znakowanie $korporacyjniProduktyZnakowanium
     *
     * @return Korporacyjny_Produkt
     */
    public function addKorporacyjniProduktyZnakowanium(\Entities\Korporacyjny_Produkt_Znakowanie $korporacyjniProduktyZnakowanium)
    {
        $this->korporacyjni_produkty_znakowania[] = $korporacyjniProduktyZnakowanium;
    
        return $this;
    }

    /**
     * Remove korporacyjniProduktyZnakowanium
     *
     * @param \Entities\Korporacyjny_Produkt_Znakowanie $korporacyjniProduktyZnakowanium
     */
    public function removeKorporacyjniProduktyZnakowanium(\Entities\Korporacyjny_Produkt_Znakowanie $korporacyjniProduktyZnakowanium)
    {
        $this->korporacyjni_produkty_znakowania->removeElement($korporacyjniProduktyZnakowanium);
    }

    /**
     * Get korporacyjniProduktyZnakowania
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKorporacyjniProduktyZnakowania()
    {
        return $this->korporacyjni_produkty_znakowania;
    }

    /**
     * Add korporacyjniKoszykiProdukty
     *
     * @param \Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty
     *
     * @return Korporacyjny_Produkt
     */
    public function addKorporacyjniKoszykiProdukty(\Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty)
    {
        $this->korporacyjni_koszyki_produkty[] = $korporacyjniKoszykiProdukty;
    
        return $this;
    }

    /**
     * Remove korporacyjniKoszykiProdukty
     *
     * @param \Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty
     */
    public function removeKorporacyjniKoszykiProdukty(\Entities\Korporacyjny_Koszyk_Produkt $korporacyjniKoszykiProdukty)
    {
        $this->korporacyjni_koszyki_produkty->removeElement($korporacyjniKoszykiProdukty);
    }

    /**
     * Get korporacyjniKoszykiProdukty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKorporacyjniKoszykiProdukty()
    {
        return $this->korporacyjni_koszyki_produkty;
    }

    /**
     * Set firmy
     *
     * @param \Entities\Firma $firmy
     *
     * @return Korporacyjny_Produkt
     */
    public function setFirmy(\Entities\Firma $firmy = null)
    {
        $this->firmy = $firmy;
    
        return $this;
    }

    /**
     * Get firmy
     *
     * @return \Entities\Firma
     */
    public function getFirmy()
    {
        return $this->firmy;
    }

    /**
     * Set produktyDetale
     *
     * @param \Entities\Produkt_Detal $produktyDetale
     *
     * @return Korporacyjny_Produkt
     */
    public function setProduktyDetale(\Entities\Produkt_Detal $produktyDetale = null)
    {
        $this->produkty_detale = $produktyDetale;
    
        return $this;
    }

    /**
     * Get produktyDetale
     *
     * @return \Entities\Produkt_Detal
     */
    public function getProduktyDetale()
    {
        return $this->produkty_detale;
    }
}

