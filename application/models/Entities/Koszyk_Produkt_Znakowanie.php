<?php

namespace Entities;

/**
 * Koszyk_Produkt_Znakowanie
 */
class Koszyk_Produkt_Znakowanie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $ilosc_kolorow;

    /**
     * @var integer
     */
    private $ilosc_sztuk;

    /**
     * @var string
     */
    private $cena;

    /**
     * @var boolean
     */
    private $powtorzenie;

    /**
     * @var string
     */
    private $przygotowalnia;

    /**
     * @var integer
     */
    private $przygotowalnia_mnozenie;

    /**
     * @var integer
     */
    private $koszyki_detale_id;

    /**
     * @var integer
     */
    private $znakowanie_id;

    /**
     * @var \Entities\Koszyk_Produkt
     */
    private $koszyki_produkty;

    /**
     * @var \Entities\Znakowanie
     */
    private $znakowanie;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iloscKolorow
     *
     * @param integer $iloscKolorow
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setIloscKolorow($iloscKolorow)
    {
        $this->ilosc_kolorow = $iloscKolorow;
    
        return $this;
    }

    /**
     * Get iloscKolorow
     *
     * @return integer
     */
    public function getIloscKolorow()
    {
        return $this->ilosc_kolorow;
    }

    /**
     * Set iloscSztuk
     *
     * @param integer $iloscSztuk
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setIloscSztuk($iloscSztuk)
    {
        $this->ilosc_sztuk = $iloscSztuk;
    
        return $this;
    }

    /**
     * Get iloscSztuk
     *
     * @return integer
     */
    public function getIloscSztuk()
    {
        return $this->ilosc_sztuk;
    }

    /**
     * Set cena
     *
     * @param string $cena
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setCena($cena)
    {
        $this->cena = $cena;
    
        return $this;
    }

    /**
     * Get cena
     *
     * @return string
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * Set powtorzenie
     *
     * @param boolean $powtorzenie
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setPowtorzenie($powtorzenie)
    {
        $this->powtorzenie = $powtorzenie;
    
        return $this;
    }

    /**
     * Get powtorzenie
     *
     * @return boolean
     */
    public function getPowtorzenie()
    {
        return $this->powtorzenie;
    }

    /**
     * Set przygotowalnia
     *
     * @param string $przygotowalnia
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setPrzygotowalnia($przygotowalnia)
    {
        $this->przygotowalnia = $przygotowalnia;
    
        return $this;
    }

    /**
     * Get przygotowalnia
     *
     * @return string
     */
    public function getPrzygotowalnia()
    {
        return $this->przygotowalnia;
    }

    /**
     * Set przygotowalniaMnozenie
     *
     * @param integer $przygotowalniaMnozenie
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setPrzygotowalniaMnozenie($przygotowalniaMnozenie)
    {
        $this->przygotowalnia_mnozenie = $przygotowalniaMnozenie;
    
        return $this;
    }

    /**
     * Get przygotowalniaMnozenie
     *
     * @return integer
     */
    public function getPrzygotowalniaMnozenie()
    {
        return $this->przygotowalnia_mnozenie;
    }

    /**
     * Set koszykiDetaleId
     *
     * @param integer $koszykiDetaleId
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setKoszykiDetaleId($koszykiDetaleId)
    {
        $this->koszyki_detale_id = $koszykiDetaleId;
    
        return $this;
    }

    /**
     * Get koszykiDetaleId
     *
     * @return integer
     */
    public function getKoszykiDetaleId()
    {
        return $this->koszyki_detale_id;
    }

    /**
     * Set znakowanieId
     *
     * @param integer $znakowanieId
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setZnakowanieId($znakowanieId)
    {
        $this->znakowanie_id = $znakowanieId;
    
        return $this;
    }

    /**
     * Get znakowanieId
     *
     * @return integer
     */
    public function getZnakowanieId()
    {
        return $this->znakowanie_id;
    }

    /**
     * Set koszykiProdukty
     *
     * @param \Entities\Koszyk_Produkt $koszykiProdukty
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setKoszykiProdukty(\Entities\Koszyk_Produkt $koszykiProdukty = null)
    {
        $this->koszyki_produkty = $koszykiProdukty;
    
        return $this;
    }

    /**
     * Get koszykiProdukty
     *
     * @return \Entities\Koszyk_Produkt
     */
    public function getKoszykiProdukty()
    {
        return $this->koszyki_produkty;
    }

    /**
     * Set znakowanie
     *
     * @param \Entities\Znakowanie $znakowanie
     *
     * @return Koszyk_Produkt_Znakowanie
     */
    public function setZnakowanie(\Entities\Znakowanie $znakowanie = null)
    {
        $this->znakowanie = $znakowanie;
    
        return $this;
    }

    /**
     * Get znakowanie
     *
     * @return \Entities\Znakowanie
     */
    public function getZnakowanie()
    {
        return $this->znakowanie;
    }
}

