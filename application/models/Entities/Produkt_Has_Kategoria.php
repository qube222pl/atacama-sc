<?php

namespace Entities;

/**
 * Produkt_Has_Kategoria
 */
class Produkt_Has_Kategoria
{
    /**
     * @var integer
     */
    private $produkty_id;

    /**
     * @var integer
     */
    private $kategorie_id;

    /**
     * @var \Entities\Produkt
     */
    private $produkty;

    /**
     * @var \Entities\Kategoria
     */
    private $kategorie;


    /**
     * Set produktyId
     *
     * @param integer $produktyId
     *
     * @return Produkt_Has_Kategoria
     */
    public function setProduktyId($produktyId)
    {
        $this->produkty_id = $produktyId;
    
        return $this;
    }

    /**
     * Get produktyId
     *
     * @return integer
     */
    public function getProduktyId()
    {
        return $this->produkty_id;
    }

    /**
     * Set kategorieId
     *
     * @param integer $kategorieId
     *
     * @return Produkt_Has_Kategoria
     */
    public function setKategorieId($kategorieId)
    {
        $this->kategorie_id = $kategorieId;
    
        return $this;
    }

    /**
     * Get kategorieId
     *
     * @return integer
     */
    public function getKategorieId()
    {
        return $this->kategorie_id;
    }

    /**
     * Set produkty
     *
     * @param \Entities\Produkt $produkty
     *
     * @return Produkt_Has_Kategoria
     */
    public function setProdukty(\Entities\Produkt $produkty = null)
    {
        $this->produkty = $produkty;
    
        return $this;
    }

    /**
     * Get produkty
     *
     * @return \Entities\Produkt
     */
    public function getProdukty()
    {
        return $this->produkty;
    }

    /**
     * Set kategorie
     *
     * @param \Entities\Kategoria $kategorie
     *
     * @return Produkt_Has_Kategoria
     */
    public function setKategorie(\Entities\Kategoria $kategorie = null)
    {
        $this->kategorie = $kategorie;
    
        return $this;
    }

    /**
     * Get kategorie
     *
     * @return \Entities\Kategoria
     */
    public function getKategorie()
    {
        return $this->kategorie;
    }
}

