<?php

namespace Moyoki_Proxies\__CG__\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Cennik_Produkt extends \Entities\Cennik_Produkt implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'id', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'cenniki_id', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'produkty_id', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'cena', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'ilosc', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'cenniki', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'produkty');
        }

        return array('__isInitialized__', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'id', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'cenniki_id', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'produkty_id', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'cena', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'ilosc', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'cenniki', '' . "\0" . 'Entities\\Cennik_Produkt' . "\0" . 'produkty');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Cennik_Produkt $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCennikiId($cennikiId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCennikiId', array($cennikiId));

        return parent::setCennikiId($cennikiId);
    }

    /**
     * {@inheritDoc}
     */
    public function getCennikiId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCennikiId', array());

        return parent::getCennikiId();
    }

    /**
     * {@inheritDoc}
     */
    public function setProduktyId($produktyId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProduktyId', array($produktyId));

        return parent::setProduktyId($produktyId);
    }

    /**
     * {@inheritDoc}
     */
    public function getProduktyId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProduktyId', array());

        return parent::getProduktyId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCena($cena)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCena', array($cena));

        return parent::setCena($cena);
    }

    /**
     * {@inheritDoc}
     */
    public function getCena()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCena', array());

        return parent::getCena();
    }

    /**
     * {@inheritDoc}
     */
    public function setIlosc($ilosc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIlosc', array($ilosc));

        return parent::setIlosc($ilosc);
    }

    /**
     * {@inheritDoc}
     */
    public function getIlosc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIlosc', array());

        return parent::getIlosc();
    }

    /**
     * {@inheritDoc}
     */
    public function setCenniki(\Entities\Cennik $cenniki = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCenniki', array($cenniki));

        return parent::setCenniki($cenniki);
    }

    /**
     * {@inheritDoc}
     */
    public function getCenniki()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCenniki', array());

        return parent::getCenniki();
    }

    /**
     * {@inheritDoc}
     */
    public function setProdukty(\Entities\Produkt $produkty = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProdukty', array($produkty));

        return parent::setProdukty($produkty);
    }

    /**
     * {@inheritDoc}
     */
    public function getProdukty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProdukty', array());

        return parent::getProdukty();
    }

}
