<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_StronaZmien implements Moyoki_Command_Interface {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $strona = $this->_em->getRepository('Entities\Strona')->pobierzPoKodzie($this->_post['kod'], $this->_post['lang']);

        if ($strona instanceof Entities\Strona) {
            Moyoki_Debug::debug($_POST, 'post');


            //$strona = new Entities\Strona;
            $strona->setOpis($this->_post['opis'])
                    ->setSlowaKluczowe($this->_post['slowa_kluczowe'])
                    ->setTresc($this->_post['tresc'])
                    ->setTytul($this->_post['tytul']);

            $this->_em->getRepository('Entities\Strona')->update($strona);
        } else {
            $strona = new Entities\Strona();
            $strona->setJezyk($this->_post['lang'])
                    ->setKod($this->_post['kod'])
                    ->setOpis($this->_post['opis'])
                    ->setSlowaKluczowe($this->_post['slowa_kluczowe'])
                    ->setTytul($this->_post['tytul'])
                    ->setTresc($this->_post['tresc']);

            $this->_em->persist($strona);
            $this->_em->flush();
        }
    }

}
