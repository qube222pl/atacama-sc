<?php

/**
 * Polecenie - import
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZdjeciaKopiuj {

    private $_conn;
    private $_path;
    private $_srcDir;
    private $_dstDir;
    private $_distrId;
    //private $_dirs;
    

    /**
     *
     * @param array form values
     */
    public function __construct($cmdParams) {
        

        $zc = Zend_Controller_Front::getInstance();
        $this->_conn = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Entitymanagerfactory')->getConnection();
        $this->_path = realpath(Atacama_Config::getInstance()->zdjecia->path) . '/';
        
        $this->_distrId = $cmdParams[1];
        $this->_dstDir = $this->_path . $this->_distrId;
        
        if($this->_distrId != '18') {
            // nieobsługiwany dystrybutor
            throw new Exception('Ten dystrybutor nie jest obsługiwany ' . $this->_dstDir);
        }

        if(!file_exists($this->_dstDir) || !is_dir($this->_dstDir)) {
            // katalog na zdjęcia nie istnieje
            throw new Exception('Katalog docelowy na zdjecia dystrybutora nie istnieje! ' . $this->_dstDir);
        }

        $this->_srcDir = $cmdParams[2];
        
        if(!file_exists($this->_srcDir) || !is_dir($this->_srcDir)) {
            // katalog na zdjęcia nie istnieje
            throw new Exception('Katalog zrodlowy ze zdjeciami nie istnieje! ' . $this->_srcDir);
        }

    }

   
    public function execute() {

        $newPhotos = 0;
        
        // pobieramy wszystkie zdjęcia z BAZY dla wybranego dystrybutora
        $sql = "SELECT pz.plik, pz.produkty_id FROM produkty_zdjecia pz INNER JOIN produkty p ON pz.produkty_id = p.id WHERE p.dystrybutorzy_id = " . $this->_distrId;
        $photos = $this->_conn->fetchAll($sql);

        // pobieramy wszystkie produkty z BAZY dla wybranego dystrybutora
        $sql = "SELECT p.id, p.nr_katalogowy FROM produkty p WHERE p.dystrybutorzy_id = " . $this->_distrId;
        $products = $this->_conn->fetchAll($sql);
        
        if(count($photos) == 0) {
            echo "Brak zdjec w bazie dla wybranego dystrybutora.\n";
            return;
        } else {
            echo "Znaleziono " . count($photos) . " zdjec dystrybutora w bazie SQL.\n";
        }
        
        if(count($products) == 0) {
            echo "Brak produktow w bazie dla wybranego dystrybutora.\n";
            return;
        } else {
            echo "Znaleziono " . count($products) . " produktow dystrybutora w bazie SQL.\n";
        }
        
        // tworzymy tablicę: nazwa_pliku_zdjęcia => id w tabeli produkty_zdjecia
        foreach ($photos as $photo) {
            $photosByName[$photo['plik']] = $photo['produkty_id'];
        }
        
        // tworzymy tablicę: nr_katalogowy => id produktu w tabeli produkty
        foreach ($products as $product) {
            $productsByNumber[$product['nr_katalogowy']] = $product['id'];
        }
        
        set_time_limit(360);

        //print_r($photosByName);
        //print_r($productsByNumber);
        //print_r($photos);

        // dla każdego pliku w katalogu źródłowym sprawdzamy czy jest on w bazie.
        // Jeśli nie to dodajemy do bazy i kopiujemy do katalogu docelowego.
        foreach (scandir($this->_srcDir) as $srcPhoto) {
            if($srcPhoto != '.' && $srcPhoto != '..') {
                
                $photo = $this->_distrId . '/' . $srcPhoto;
                echo  $photo ;
                
                if(isset($photosByName[$photo])) {
                    echo "   juz jest w bazie.\n";
                } else {
                    echo "   brak w bazie:  ";
                    
                    
                    $productId = $this->_getProductId($srcPhoto);
//echo $productId . '  ';
                    if(isset($productsByNumber[$productId])) {
//echo $productsByNumber[$productId] . '  ';

                        // dodanie do bazy
                        $res = 0;
                        $res = $this->_conn->insert('produkty_zdjecia', array('produkty_id' => $productsByNumber[$productId], 'plik' => $photo, 'widoczne' => 1));
                        if($res == 1) {
                            echo "dodany do bazy   ";
                            // kopiujemy plik
                            if(copy($this->_srcDir . '/' . $srcPhoto, $this->_dstDir . '/' . $srcPhoto) === true) {
                                echo "skopiowany\n";
                                $newPhotos++;
                            } else {
                                echo "NIE skopiowany\n";
                            }

                        } else {
                            echo "NIE dodany do bazy\n";
                        }                    
                        
                    }
                }
                
                
                
            }
        }        
        
        echo "Dodano " . $newPhotos . " zdjec/zdjecia do systemu.\n";
        return;
        
    }
    
    private function _getProductId($fileName) {

        // UWAGA: poniższe tylko dla ASGARD == 18
        return $this->_getAsgard($fileName);
        
    }
    
    private function _getAsgard($index) {
        
        //$color = '';
        $kreska = strpos($index, '-');

        if($kreska !== false) {
            $code = substr($index, 0, $kreska);
            //$color = substr($index, $kreska + 1);
        } else {
            // trzeba sprawdzić czy wszyskie znaki są cyframi a gdy nie to usunąć końcowe litery
            while(ctype_digit($index) === false) {
                $index = substr($index, 0, -1);
            }
            $code = $index;
        }

        return($code);
        
    }
    
}
