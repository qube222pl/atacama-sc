<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktUstawCenyMin {

    public function __construct() {
        
    }

    public function execute() {

        $entityManager = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Entitymanagerfactory');

        $produkty = $entityManager->getRepository('Entities\Produkt')->findAll();

        $entityManager->beginTransaction();
        try {

            $i = 0;
            foreach ($produkty as $p) {
                $i++;
                $detal = $p->getProduktyDetale()->first();
                if ($detal instanceof Entities\Produkt_Detal) {
                    $najnizsza = new Atacama_Produkt_Cenaklienta($entityManager, $detal, NULL, NULL);
                    $cena = $najnizsza->najnizszaCena(0);
                    if ($cena) {
                        $p->setCenaMin($cena);
                        $entityManager->persist($p);
                    }
                }
            }

            $entityManager->flush();
        } catch (Exception $exc) {
            $entityManager->rollback();
        }
        $entityManager->commit();
    }

}
