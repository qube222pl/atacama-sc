<?php

/**
 * Polecenie - logowanie czynnosci
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_UzytkownikZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $uzytkownik = $this->_em->getRepository('Entities\Uzytkownik')->getById($this->_post['id']);

        $crypt = Moyoki_Crypt::getInstance();

        if ($uzytkownik instanceof Entities\Uzytkownik) {
            $uzytkownik->setImie($this->_post['imie'])
                    ->setNazwisko($this->_post['nazwisko'])
                    ->setTelefon($this->_post['telefon']);


            if ($uzytkownik->getEmail() != $this->_post['email']) {
                $zdublowanyEmail = $this->_em->getRepository('Entities\Uzytkownik')->getByEmailBezTegoUserId($crypt->encrypt(trim($this->_post['email'])), $uzytkownik->getId());
                if ($zdublowanyEmail instanceof Entities\Uzytkownik) {
                    throw new Exception('inny uzytkownik juz ma taki adres e-mail', 112);
                    return;
                } else {
                    $uzytkownik->setEmail($crypt->encrypt($this->_post['email']));
                }
            }
            if (isset($this->_post['active'])) {
                $uzytkownik->setActive(($this->_post['active'] > 0 ? 1 : 0));
            }
            if (isset($this->_post['rola'])) {
                $uzytkownik->setRola($this->_post['rola']);
            }
            if (isset($this->_post['firmy_id'])) {
                $uzytkownik->setFirmyId($this->_post['firmy_id']);
            }
            if (isset($this->_post['haslo1']) && isset($this->_post['haslo2']) && strlen($this->_post['haslo1']) > 7 && $this->_post['haslo1'] === $this->_post['haslo2']) {
                $uzytkownik->setHaslo($this->_post['haslo1']);
            }

            $this->_em->persist($uzytkownik);
            $this->_em->flush();
        } else {
            throw new Exception('Brak uzytkownika o podanym identyfikatorze', 997);
        }
    }

}
