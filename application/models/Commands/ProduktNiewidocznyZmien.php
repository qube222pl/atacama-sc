<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktNiewidocznyZmien {

    private $_id;
    public $msg = null;
    private $_em;
    private $_status;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, $id, $status) {
        $this->_id = $id;
        $this->_em = $entityManager;
        $this->_status = $status;
    }

    public function execute() {

        /**
         * Pobieranie potrzebnych obiektow
         */
        $produkt = $this->_em->getRepository('Entities\Produkt')->getById($this->_id);

        if (!$produkt instanceof Entities\Produkt) {
            $this->msg = 'Produkt nie istnieje';
            return FALSE;
        }

        if ($produkt->getWidoczny() == 1) {
            $this->msg = 'Nie można usuwać produktów widocznych';
            return FALSE;
        }

        if ($this->_status == 'widoczny') {
            $produkt->setWidoczny(1);
            $this->msg = 'Produkt widoczny';
        }

        if ($this->_status == 'usun') {
            $produkt->setUsuniety(1);
            $this->msg = 'Produkt usunięty';
        }

        $this->_em->persist($produkt);
        $this->_em->flush();

        return TRUE;
    }

}
