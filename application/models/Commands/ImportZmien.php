<?php

/**
 * Polecenie - parametry importu
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ImportZmien {

    private $_formValues;
    private $_em;
    private $_dystrybutorId;
    

    /**
     *
     * @param array form values
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $formValues, $dystrybutorId) {
        $this->_formValues = $formValues;
        $this->_em = $entityManager;
        $this->_dystrybutorId = $dystrybutorId;
    }

    public function execute() {
        
        $conn = $this->_em->getConnection();
        
        foreach ($this->_formValues as $key => $val) {
            $conn->update('import_parametry', array(
                'wartosc' => $val
            ), array(
                'dystrybutorzy_id' => $this->_dystrybutorId,
                'nazwa' => $key
            ));
        }
        
        return true;
    }
    
    

}
