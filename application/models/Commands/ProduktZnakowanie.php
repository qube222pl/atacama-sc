<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktZnakowanie {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Generowanie produktu
         */
        $produkt = $this->_em->getRepository('Entities\Produkt')->getById($this->_post['id']);

        if ($produkt instanceof Entities\Produkt) {


            $znakowaniaTab = array();
            foreach ($this->_post as $k => $v) {
                if (stristr($k, '_')) {
                    list($tag, $metodaID) = explode('_', $k);
                    if (is_numeric($metodaID) && $tag == 'znak' && $v == 1) {
                        $znakowaniaTab[] = (int) $metodaID;
                    }
                }
            }

            $this->_em->beginTransaction();
            try {
                try {
                    try {
                        $this->_em->getRepository('Entities\Produkt_Has_Znakowanie')->kasujDlaProduktId($this->_post['id']);

                        $iter = 0;
                        foreach ($znakowaniaTab as $k => $id) {
                            $iter++;
                            $metodaObj = NULL;
                            $metodaObj = $this->_em->getRepository('Entities\Znakowanie')->getById($id);
                            if ($metodaObj instanceof Entities\Znakowanie) {
                                $produktZnakowanie = new Entities\Produkt_Has_Znakowanie();
                                $produktZnakowanie->setProdukty($produkt)
                                        ->setProduktyId($produkt->getId())
                                        ->setZnakowanie($metodaObj)
                                        ->setZnakowanieId($metodaObj->getId());

                                $this->_em->persist($produktZnakowanie);
                            } else {
                                Atacama_Log::dodaj($this->_em, Atacama_Log::BLAD_SYSTEMU, 'Nie ma takiego znakowania: ' . $id);
                            }
                        }
                        $this->_em->flush();
                    } catch (\Doctrine\ORM\NoResultException $e) {
                        $this->_em->rollback();
                        return FALSE;
                    }
                } catch (Doctrine\DBAL\DBALException $e) {
                    $this->_em->rollback();
                    return FALSE;
                }
            } catch (Exception $exc) {
                $this->_em->rollback();
                return FALSE;
            }
            if (count($znakowaniaTab) == $iter) {
                $this->_em->commit();
                return TRUE;
            } else {
                $this->_em->rollback();
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
