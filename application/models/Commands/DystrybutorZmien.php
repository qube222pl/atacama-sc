<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_DystrybutorZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $dystrybutor = $this->_em->getRepository('Entities\Dystrybutor')->getById($this->_post['id']);
        $dystrybutor->setNazwa($this->_post['nazwa'])
                ->setOpis($this->_post['opis']);

        $this->_em->getRepository('Entities\Dystrybutor')->zmien($dystrybutor);
    }

}
