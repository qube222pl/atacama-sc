<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyProduktPobierzZOferty {

    public $errors = [];
    public $success = [];
    private $_obecne_produkty_firmy = [];
    private $_produkty_dodane = [];
    private $_koszyk;
    private $_firma;
    private $_em;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param int $koszyk_id
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, $koszyk_id) {
        $this->_em = $entityManager;
        $this->_pobierzKoszyk((int) $koszyk_id);
    }

    public function execute() {
        try {
            $this->_pobierzProduktyFirmy();
            if (count($this->errors) == 0 && $this->_koszyk instanceof Entities\Koszyk) {
                foreach ($this->_koszyk->getKoszykiProdukty() as $produkt) {
                    if ($produkt instanceof Entities\Koszyk_Produkt) {
                        /**
                         * Dodawanie produtkow, ktore jeszcze nie sa przypisane do tej firmy
                         * Aby nie dublowac tych samych produtkow
                         */
                        if (!key_exists($produkt->getProduktyDetaleId(), $this->_obecne_produkty_firmy)) {
                            $this->_dodajProdukt($produkt);
                        }
                    }
                }
            }

            $this->_em->flush();
            $this->success[] = "Produkt został zmieniony pomyślnie";
        } catch (DoctrineExtensions $e) {
            $this->errors[] = $e;
            return FALSE;
        } catch (Exception $e) {
            $this->errors[] = $e;
            return FALSE;
        }
    }

    public function getProduktyDodane() {
        return $this->_produkty_dodane;
    }

    private function _pobierzKoszyk($koszyk_id) {
        if ($koszyk_id == 0) {
            $this->errors[] = 'Niepoprawny identyfikator koszyka';
            return FALSE;
        }
        $koszyk = $this->_em->getRepository('Entities\Koszyk')->getById($koszyk_id);
        if (!$koszyk instanceof Entities\Koszyk) {
            $this->errors[] = 'Niepoprawny identyfikator koszyka';
            return FALSE;
        }
        /**
         * 

          $auth = Zend_Auth::getInstance()->getIdentity();
          if ($koszyk->getFirmy()->getPh()->getId() != $auth['id']) {
          $this->errors[] = 'Nie masz uprawnień do obsługi tej oferty';
          return FALSE;
          }
         * 
         */
        $this->_firma = $koszyk->getFirmy();
        $this->_koszyk = $koszyk;
    }

    private function _pobierzProduktyFirmy() {

        $produktyFirmy = $this->_em->getRepository('Entities\Korporacyjny_Produkt')->getWszystkieDlaFirmy($this->_koszyk->getFirmyId());
        foreach ($produktyFirmy as $produkt) {
            if ($produkt instanceof Entities\Korporacyjny_Produkt) {
                $this->_obecne_produkty_firmy[$produkt->getProduktyDetaleId()] = $produkt->getProduktyDetaleId();
            }
        }
    }

    private function _dodajProdukt(Entities\Koszyk_Produkt $produkt) {
        /**
         * Aby nie dublowac tych samych produktow (a takie sie trafiaja w ofertach)
         * sprawdzam czy teraz detal ID nie zostal dodany
         */
        if (key_exists($produkt->getProduktyDetaleId(), $this->_produkty_dodane)) {
            return FALSE;
        }

        $k_produkt = new Entities\Korporacyjny_Produkt();
        $k_produkt->setFirmy($this->_firma)
                ->setFirmyId($this->_firma->getId())
                ->setProduktyDetale($produkt->getProduktyDetale())
                ->setProduktyDetaleId($produkt->getProduktyDetaleId())
                ->setCena(0)
                ->setWidoczny(1);


        /**
         * Pobieramy znakowanie i dodajemy dla produktu
         */
        $this->_dodajZnakowanie($k_produkt, $produkt);
        /**
         * Dodajemy informacje o nowo dodanym produkcie
         */
        $this->_dodajProduktDoLoga($produkt->getProduktyDetale());

        /**
         * Zapisujemy obiekt do parsera
         */
        $this->_em->persist($k_produkt);
    }

    private function _dodajZnakowanie(Entities\Korporacyjny_Produkt $korporacyjny_produkt, Entities\Koszyk_Produkt $koszyk_produkt) {
        foreach ($koszyk_produkt->getKoszykiProduktyZnakowania() as $znakowanie) {
            if ($znakowanie instanceof Entities\Koszyk_Produkt_Znakowanie) {

                $korp_znakowanie = new \Entities\Korporacyjny_Produkt_Znakowanie();
                $korp_znakowanie
                        ->setIloscKolorow($znakowanie->getIloscKolorow())
                        ->setKorporacyjniProdukty($korporacyjny_produkt)
                        ->setKorporacyjniProduktyId($korporacyjny_produkt->getId())
                        ->setZnakowanieId($znakowanie->getZnakowanieId())
                        ->setZnakowanie($znakowanie->getZnakowanie());

                $this->_em->persist($korp_znakowanie);
            }
        }
    }

    /**
     * Dodaje informacje o produkcie aby mozna bylo
     * wyswietlic informacje jakie produkty zostaly dodane dla firmy
     * @param Entities\Produkt_Detal $detal
     */
    private function _dodajProduktDoLoga(Entities\Produkt_Detal $detal) {
        $nazwa = '';
        foreach ($detal->getProdukty()->getProduktyI18n() as $i18n) {
            if ($i18n instanceof Entities\Produkt_i18n && $i18n->getJezyk() == 'pl') {
                $nazwa = $i18n->getNazwa();
            }
        }
        $this->_produkty_dodane[$detal->getId()] = [
            'id' => $detal->getId(),
            'nr_kat' => $detal->getProdukty()->getNrKatalogowy(),
            'nazwa' => $nazwa
        ];
    }

}
