<?php

/**
 *
 * Aktulizacja ilosci i cen zamowienia
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZamowienieAktualizuj {

    private $_post;
    public $msg = null;
    private $_em;
    public $acl = null;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
        $this->acl = Atacama_Acl::getInstance();
    }

    public function execute() {
        $user = Zend_Auth::getInstance()->getIdentity();
        $zamowienie_id = $this->_post['id'];

        foreach ($this->_post['koszyk'] as $id => $params) {

            /**
             * Informacja do loga, ze ktos zmienial zawartosc zamowienia
             */
            $logOpis = 'Użytkownik ID: ' . $user['id'] . ' zmienił zawartość zamówienia ID: ' . $zamowienie_id;
            Atacama_Log::dodaj($this->_em, Atacama_Log::ZAMOWIENIE_EDYCJA, $logOpis);


            $koszyk_produkt = $this->_em->getRepository('Entities\Koszyk_Produkt')->getById($id);
            if ($koszyk_produkt instanceof Entities\Koszyk_Produkt) {

                /**
                 * Ktos chce kasowac w tym miejscu, wiec usuwamy
                 */
                if ($params['ilosc'] == 0) {
                    $this->_em->getRepository('Entities\Koszyk_Produkt')->deleteById($koszyk_produkt->getId());
                } else {
//$koszyk_produkt = new Entities\Koszyk_Produkt;

                    $cenaProduktu = Atacama_Produkt_Kwota::zapis($params['cena']);
                    if (count($koszyk_produkt->getKoszykiProduktyZnakowania()) > 0) {
                        $cenaProduktu -= $koszyk_produkt->getCenaKonfekcjonowanie();
                    }

                    $koszyk_produkt->setIlosc((int) $params['ilosc'])
                            ->setCenaZakupu(Atacama_Produkt_Kwota::zapis($cenaProduktu));


                    $this->_em->getRepository('Entities\Koszyk_Produkt')->zmien($koszyk_produkt);
                    /**
                     * Aktualizujemy ilosci kolorow w znakowaniu
                     */
                    if (isset($this->_post['znak'])) {
                        $this->_znakowania($koszyk_produkt);
                    }
                }
            }
        }

        /**
         * Zwracamy to zamowienie
         */
        $zamowienie = $this->_em->getRepository('Entities\Koszyk')->getById($this->_post['id']);
        $zamowienie->setIloscKartonow($this->_post['ilosc_kartonow'])
                ->setVies((isset($this->_post['vies']) ? '1' : '0'));

        if (strlen($this->_post['data_dostawy']) > 3) {
            $zamowienie->setDataDostawy($this->_post['data_dostawy']);
        }

        $this->_em->getRepository('Entities\Koszyk')->zmien($zamowienie);

        return $zamowienie;
    }

    private function _znakowania($koszyk_produkt) {
        foreach ($this->_post['znak'] as $znak_id => $params) {

            $kpz = $this->_em->getRepository('Entities\Koszyk_Produkt_Znakowanie')->getById($znak_id);
            if ($kpz instanceof Entities\Koszyk_Produkt_Znakowanie) {

                /**
                 * Sprawdzamy czy koszyk_produkt_znakowanie nalezy do koszyk_produkt ktory iterujemy
                 * Moze powodowac duplikacje, bo robi sie petla w petli, ale powinno sie wyrobic
                 */
                if ($koszyk_produkt->getId() == $kpz->getKoszykiDetaleId()) {

                    //$kpz = new Entities\Koszyk_Produkt_Znakowanie;

                    if ($kpz->getIloscKolorow() != $params['ilosc_kolorow'] || $kpz->getIloscSztuk() != $params['ilosc']) {
                        /**
                         * Zmiana ilosci kolorow. Trzeba przeliczyc inne paramatery
                         */
                        $znak_rodzaj = $this->_em->getRepository('Entities\Znakowanie_Rodzaj')->getByZnakowanieIdAndIlosc($kpz->getZnakowanieId(), $kpz->getIloscSztuk());

                        if ($znak_rodzaj instanceof Entities\Znakowanie_Rodzaj) {
                            $cena = 0;
                            for ($i = 1; $i <= $kpz->getIloscKolorow(); $i++) {
                                if ($i == 1) {
                                    $cena += $znak_rodzaj->getCena();
                                } else {
                                    $cena += $znak_rodzaj->getCenaInny();
                                }
                            }
                            $kpz->setCena(Atacama_Produkt_Kwota::zapis($cena));

                            /**
                             * Jest znakowanie, to przypisujemy przygotowalnie ze znakowania
                             */
                            if ($kpz->getPrzygotowalniaMnozenie() == 0) {
                                $przygotowalnia = $kpz->getPowtorzenie() == 1 ? $znak_rodzaj->getZnakowania()->getPowtorzenie() : $znak_rodzaj->getZnakowania()->getPrzygotowalnia();
                            } else {
                                $przygotowalnia = $kpz->getPowtorzenie() == 1 ? $znak_rodzaj->getZnakowania()->getPowtorzenie() : $znak_rodzaj->getZnakowania()->getPrzygotowalnia() * $params['ilosc_kolorow'];
                            }
                            $kpz->setPrzygotowalnia(Atacama_Produkt_Kwota::zapis($przygotowalnia));
                        } else {
                            /**
                             * Ryczalt
                             */
                            $kwotaRyczaltu = $kpz->getZnakowanie()->getRyczalt() / $kpz->getIloscSztuk();
                            $przykladowyRodzaj = $kpz->getZnakowanie()->getZnakowaniaRodzaje()->first();
                            if ($przykladowyRodzaj instanceof Entities\Znakowanie_Rodzaj) {
                                if ($przykladowyRodzaj->getCenaInny() > 0) {
                                    $kpz->setCena(Atacama_Produkt_Kwota::zapis($kwotaRyczaltu * $kpz->getIloscKolorow()));
                                } else {
                                    $kpz->setCena(Atacama_Produkt_Kwota::zapis($kwotaRyczaltu));
                                }
                                /**
                                 * Idzie z ryczaltu. Ale trzeba przypisac przygotowalnie
                                 */
                                if ($kpz->getPrzygotowalniaMnozenie() == 0) {
                                    $przygotowalnia = $kpz->getPowtorzenie() == 1 ? $przykladowyRodzaj->getZnakowania()->getPowtorzenie() : $przykladowyRodzaj->getZnakowania()->getPrzygotowalnia();
                                } else {
                                    $przygotowalnia = $kpz->getPowtorzenie() == 1 ? $przykladowyRodzaj->getZnakowania()->getPowtorzenie() : $przykladowyRodzaj->getZnakowania()->getPrzygotowalnia() * $params['ilosc_kolorow'];
                                }

                                $kpz->setPrzygotowalnia(Atacama_Produkt_Kwota::zapis($przygotowalnia));
                            }
                        }
                    } else {
                        if ($this->acl->sprawdzDostep(Atacama_Acl::ZAS_ADMINISTRACJA_ZAMOWIENIA_WSZYSTKIE)) {
                            $kpz->setCena(Atacama_Produkt_Kwota::zapis($params['szt']));
                        }
                        $kpz->setPrzygotowalnia(Atacama_Produkt_Kwota::zapis($params['przygotowalnia']));
                    }

                    $kpz->setIloscKolorow($params['ilosc_kolorow'])
                            ->setIloscSztuk($params['ilosc']);
                }
                try {
                    $updateResult = $this->_em->getRepository('Entities\Koszyk_Produkt_Znakowanie')->zmien($kpz);
                } catch (Doctrine\DBAL\DBALException $exc) {
                    Moyoki_Debug::debug($exc->getMessage(), __METHOD__);
                    exit;
                } catch (Doctrine\ORM\ORMException $exc) {
                    Moyoki_Debug::debug($exc->getMessage(), __METHOD__);
                    exit;
                } catch (Exception $exc) {
                    Moyoki_Debug::debug($exc->getMessage(), __METHOD__);
                    exit;
                }
            }
        }
    }

}
