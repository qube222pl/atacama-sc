<?php

/**
 *
 * Przeliczanie kosztow znakowania dla produktu w koszyku
 * @author Studio Moyoki
 */
class Application_Model_Commands_KoszykPrzeliczZnakowaniaProduktu {

    private $_kp;
    public $msg = null;
    private $_em;
    private $_lang;
    private $_tablica = array();

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, $koszyk_produkt) {
        $this->_lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $this->_kp = $koszyk_produkt;
        $this->_em = $entityManager;
    }

    public function execute() {



        if ($this->_kp instanceof Entities\Koszyk_Produkt) {

            $produkt_ilosc = $this->_kp->getIlosc();

            //$_kp = new Entities\Koszyk_Produkt;


            if (count($this->_kp->getKoszykiProduktyZnakowania()) > 0) {
                /**
                 * Sa znakowania
                 */
                foreach ($this->_kp->getKoszykiProduktyZnakowania() as $koszyk_znakowanie) {
                    /**
                     * Duza petla po calosci
                     */
                    //$koszyk_znakowanie = new Entities\Koszyk_Produkt_Znakowanie;
                    $metoda_znakowania = $koszyk_znakowanie->getZnakowanie();
                    $nieMnozycPrzygotowalni = FALSE;
                    $dodajKonfekcjonowanie = TRUE;

                    foreach ($koszyk_znakowanie->getZnakowanie()->getZnakowaniaI18n() as $i18n) {
                        if ($i18n->getJezyk() == $this->_lang)
                            $znakowanie['nazwa'] = $i18n->getNazwa();
                    }

                    $znakowanie_rodzaj = $this->_em
                            ->getRepository('Entities\Znakowanie_Rodzaj')
                            ->getByZnakowanieIdAndIlosc($koszyk_znakowanie->getZnakowanieId(), $produkt_ilosc);

                    if ($znakowanie_rodzaj instanceof Entities\Znakowanie_Rodzaj) {
                        $cenaPierwszegoKoloru = $koszyk_znakowanie->getCenaPierwszy();
                        $cenaPozostalychKolorow = $koszyk_znakowanie->getCenaInny();
                        if ($koszyk_znakowanie->getCenaInny() == '0.00') {
                            $nieMnozycPrzygotowalni = TRUE;
                        }
                        $znakowanie['Ryczałt'] = 'NIE';
                    } else {
                        /**
                         * Trzeba brac z ryczaltu cene
                         */
                        $kwotaRyczaltu = $metoda_znakowania->getRyczalt() / $produkt_ilosc;
                        $cenaPierwszegoKoloru = $kwotaRyczaltu;
                        $cenaPozostalychKolorow = $kwotaRyczaltu;
                        $znakowanie['Ryczałt'] = $kwotaRyczaltu;
                    }
                    /**
                     * Wymnazanie ceny przez ilosc kolorow
                     */
                    $ilosc_kolorow = $koszyk_znakowanie->getIloscKolorow();

                    $znakowanie['ilosc kolorow'] = $koszyk_znakowanie->getIloscKolorow();

                    $suma = 0;
                    for ($i = 1; $i <= $ilosc_kolorow; $i++) {
                        if ($i == 1) {
                            $suma = $suma + $produkt_ilosc * $cenaPierwszegoKoloru;
                        } else {
                            $suma = $suma + $produkt_ilosc * $cenaPozostalychKolorow;
                        }
                    }
                    if ($dodajKonfekcjonowanie) {
                        $znakowanie['suma'] = $suma + $this->_kp->getCenaKonfekcjonowanie() * $produkt_ilosc;
                        $dodajKonfekcjonowanie = FALSE;
                    } else {
                        $znakowanie['suma'] = $suma;
                    }

                    if ($koszyk_znakowanie->getPowtorzenie() == 1) {
                        $znakowanie['powtorzenie'] = 'TAK';
                    } else {
                        $znakowanie['powtorzenie'] = 'NIE';
                    }
                    $znakowanie['przygotowalnia'] = $koszyk_znakowanie->getPrzygotowalnia();


                    $this->_tablica['znakowania'][] = $znakowanie;
                    unset($znakowanie);
                }

                $this->_tablica['konfekcjonowanie'] = $this->_kp->getCenaKonfekcjonowanie() * $produkt_ilosc;
            } else {
                /**
                 * Produkt nie posiada znakowan
                 */
                $this->_tablica['brak'] = 'brak';
            }
        }

        return $this->_tablica;
    }

}
