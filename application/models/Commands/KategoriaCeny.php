<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KategoriaCeny {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Generowanie kategorii
         */
        $kategoria = $this->_em->getRepository('Entities\Kategoria')->getById($this->_post['id']);

        if ($kategoria instanceof Entities\Kategoria) {

            /**
             * Tworzna jest tablica, ktora pozwoli na obsluge pol formularza
             */
            $rodzajeTab = array();
            foreach ($this->_post as $k => $v) {
                if (stristr($k, '_')) {
                    list($detalID, $klucz) = explode('_', $k);
                    $rodzajeTab[$detalID][$klucz] = $v;
                }
            }
            /**
             * Teraz obslugujemy juz tablice i w zaleznosci czy dodac czy zmienic reagujemy
             */
            foreach ($rodzajeTab as $k => $v) {
                if (is_numeric($k)) {
                    /**
                     * Zmiana
                     */
                    $rodzaj = $this->_em->getRepository('Entities\Kategoria_Cena')->getById($k);
                    $rodzaj->setProcentRegularna(\Atacama_Produkt_Kwota::zapis($v['regularna']))
                            ->setProcentPromocja(\Atacama_Produkt_Kwota::zapis($v['promocyjna']))
                            ->setMinimum($v['minimum'])
                            ->setMaximum($v['maximum']);

                    $this->_em->getRepository('Entities\Kategoria_Cena')->zmien($rodzaj);
                } else {
                    /**
                     * Dodanie nowej pozycji
                     */
                    $rodzaj = new Entities\Kategoria_Cena();
                    $rodzaj->setProcentRegularna(\Atacama_Produkt_Kwota::zapis($v['regularna']))
                            ->setProcentPromocja(\Atacama_Produkt_Kwota::zapis($v['promocyjna']))
                            ->setMinimum($v['minimum'])
                            ->setMaximum($v['maximum'])
                            ->setKategorie($kategoria)
                            ->setKategoriaId($kategoria->getId());

                    $this->_em->persist($rodzaj);
                    $this->_em->flush();
                }
            }
        } else {
            return false;
        }
    }

}
