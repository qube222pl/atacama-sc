<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_WyszukiwanieZaawansowane {

    public $msg = null;
    public $lang = null;
    public $em;
    private $_post;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->em = $entityManager;
        $this->lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
    }

    public function execute() {
        $fraza = (isset($this->_post['fraza']) ? $this->_post['fraza'] : NULL);
        $ceny = (isset($this->_post['ceny']) && is_numeric($this->_post['ceny']) ? (int) $this->_post['ceny'] : NULL);
        $kolory = (isset($this->_post['kolory']) && is_numeric($this->_post['kolory']) ? (int) $this->_post['kolory'] : NULL);
        $stany = (isset($this->_post['stany']) && is_numeric($this->_post['stany']) ? (int) $this->_post['stany'] : NULL);

        $query = $this->createQuery($fraza, $ceny, $kolory, $stany);

        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);
        return $paginator;
    }

    private function createQuery($fraza, $cena = null, $kolor = null, $stany = null) {

        $qb = $this->em->createQueryBuilder();

        $qb->select('p')
                ->from('Entities\Produkt', 'p')
                ->leftJoin('p.produkty_i18n', 'pi')
                ->leftJoin('p.produkty_detale', 'pd');

        if (!is_null($kolor) && $kolor > 0) {
            $qb->leftJoin('pd.kolory', 'k');
        }


        /**
         * Poczatek warunkow
         */
        $qb->where('p.widoczny = 1');

        if (!is_null($kolor) && $kolor > 0) {
            $qb->andWhere('k.grupy_id = :grupy_id')
                    ->setParameter('grupy_id', $kolor);
        }

        if (!is_null($fraza) && !empty($fraza) && strlen($fraza) > 1) {
            $qb->andWhere($qb->expr()->like('pi.nazwa', ':fraza'))
                    ->setParameter('fraza', "%" . $fraza . "%");
        }

        if (!is_null($stany) && $stany > 0) {
            $val = 'pd.stan_magazynowy_1';
            switch ($stany) {
                case 1:
                    $qb->andWhere($qb->expr()->between($val, 1, 10));
                    break;
                case 2:
                    $qb->andWhere($qb->expr()->between($val, 11, 100));
                    break;
                case 3:
                    $qb->andWhere($qb->expr()->between($val, 101, 500));
                    break;
                case 4:
                    $qb->andWhere($qb->expr()->between($val, 501, 1000));
                    break;
            }
        }

        if (NULL != $cena && $cena > 0) {

            switch ($cena) {
                case 1:
                    $qb->andWhere($qb->expr()->between('p.cena_min', 0, 10));
                    break;
                case 2:
                    $qb->andWhere($qb->expr()->between('p.cena_min', 11, 30));
                    break;
                case 3:
                    $qb->andWhere($qb->expr()->between('p.cena_min', 31, 50));
                    break;
                case 4:
                    $qb->andWhere($qb->expr()->between('p.cena_min', 51, 100));
                    break;
                case 5:
                    $qb->andWhere($qb->expr()->between('p.cena_min', 101, 99999));
                    break;
            }
        }

        $qb->orderBy('pi.nazwa');
        
        return $qb->getQuery();
    }

}
