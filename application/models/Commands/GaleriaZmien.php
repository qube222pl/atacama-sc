<?php

/**
 * Zmiana galerii
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_GaleriaZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $galeria = $this->_em->getRepository('Entities\Galeria')->getById($this->_post['id']);

        if ($galeria instanceof Entities\Galeria) {

            $galeria->setNazwa($this->_post['nazwa'])
                    ->setOpis($this->_post['opis'])
                    ->setWidoczna($this->_post['widoczna']);

            $this->_em->getRepository('Entities\Galeria')->update($galeria);
            return TRUE;
        } else {
            return false;
        }
    }

}
