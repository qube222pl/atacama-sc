<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyProduktZmien {

    public $errors = [];
    public $success = [];
    private $_post;
    private $_produkt;
    private $_em;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param array $post
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post, Entities\Korporacyjny_Produkt $produkt) {
        $this->_post = $post;
        $this->_produkt = $produkt;
        $this->_em = $entityManager;
    }

    public function execute() {
        try {
            if (count($_FILES) > 0) {
                $nazwaPliku = $this->_wgrajZdjecie();
            } else {
                $nazwaPliku = NULL;
            }

            if (count($this->errors) == 0) {
                $this->_produkt->setCena(str_replace(',', '.', $this->_post['cena']))
                        ->setCzasDodruku($this->_post['czas_dodruku'])
                        ->setMinStock($this->_post['min_stock'])
                        ->setStock($this->_post['stock'])
                        ->setZdjecie($nazwaPliku);

                $this->_em->persist($this->_produkt);
                $this->_em->flush();
                $this->success[] = "Produkt został zmieniony pomyślnie";
            }
        } catch (DoctrineExtensions $e) {
            $this->errors[] = $e;
            return FALSE;
        } catch (Exception $e) {
            $this->errors[] = $e;
            return FALSE;
        }
    }

    private function _wgrajZdjecie() {
        if ($_FILES['zdjecie']['error'] != 0) {
            switch ($_FILES['zdjecie']['error']) {
                case 1:
                    $this->errors[] = 'UPLOAD_ERR_INI_SIZE';
                    break;
                case 2:
                    $this->errors[] = 'UPLOAD_ERR_FORM_SIZE';
                    break;
                case 3:
                    $this->errors[] = 'UPLOAD_ERR_PARTIAL';
                    break;
                case 4:
                    //UPLOAD_ERR_NO_FILE
                    $this->errors[] = 'Brak zdjęcia';
                    break;
                case 6:
                    $this->errors[] = 'UPLOAD_ERR_NO_TMP_DIR';
                    break;
                case 7:
                    $this->errors[] = 'UPLOAD_ERR_CANT_WRITE';
                    break;
                case 8:
                    $this->errors[] = 'UPLOAD_ERR_EXTENSION';
                    break;
                default :
                    $this->errors[] = 'ERROR';
                    break;
            }
            return FALSE;
        }


        $allowedTypes = [
            'jpg' => 'jpg',
            'jpeg' => 'jpg',
            'png' => 'png'
        ];

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $files = $adapter->getFileInfo();
        
        $extension = pathinfo($files['zdjecie']['name'], PATHINFO_EXTENSION);

        if (!key_exists($extension, $allowedTypes)) {
            $this->errors[] = 'extension not allowed';
            return FALSE;
        }

        $file_name = $this->_post['firma_id'] . '-' . $this->_post['detal_id'] . '-' . rand(40, 400) . '.' . $extension;

        $config = Atacama_Config::getInstance();
        $path_file = $config->zdjeciaspecjalne->path . $file_name;
        $adapter->addFilter('Rename', $path_file);

        try {
            if ($adapter->receive()) {
                $this->success[] = 'Zdjęcie dodane';
            }
            return $file_name;
        } catch (Zend_File_Transfer_Exception $e) {
            $this->errors[] = $e->getMessage();
        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
        }
    }

}
