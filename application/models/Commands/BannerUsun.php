<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_BannerUsun implements Moyoki_Command_Interface {

    private $_banner;
    private $_em;

    /**
     *
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, $banner) {
        $this->_em = $entityManager;
        $this->_banner = $banner;
    }

    public function execute() {

        $result = '';
        if ($this->_banner instanceof Entities\Banner) {
            $config = Atacama_Config::getInstance();
            $path = $config->bannery->path;

            if (file_exists($path . $this->_banner->getPlik())) {
                if (!unlink($path . $this->_banner->getPlik())) {
                    Atacama_Log::dodaj($this->_em, Atacama_Log::BLAD_SYSTEMU, 'Problem z usunieciem bannera: ' . $this->_banner->getPlik());
                }
            }

            return $this->_em->getRepository('Entities\Banner')->deleteById($this->_banner->getId());
        }
    }

}
