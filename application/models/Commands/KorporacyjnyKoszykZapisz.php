<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyKoszykZapisz {

    public $errors = [];
    public $success = [];
    public $zamowienie_id = null;
    private $_post;
    private $_em;
    private $_produktyArray = [];
    private $_produktyDoDodruku = [];
    private $_uzytkownik;
    private $_kosztZamowienia = 0;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post, Entities\Korporacyjny_Uzytkownik $uzytkownik) {
        $this->_post = $post;
        $this->_em = $entityManager;
        $this->_uzytkownik = $uzytkownik;
    }

    public function execute() {
        $this->_getProduktyZPost();
        $this->_sprawdzBudzet();
        if (empty($this->errors)) {
            $this->_tworzZamowienie();
            $this->_wyslijPowiadomienieDoHandlowca();
        }
    }

    private function _getProduktyZPost() {
        foreach ($this->_post as $k => $v) {
            list($id, $reszta) = array_pad(explode('_', $k), 2, null);
            $value = intval($v);
            $id = intval($id);
            if ($reszta == 'ilosc' && $id > 0 && $value > 0) {
                //pobiera produkty i zapisuje w tablicy
                $produkt = $this->_em->getRepository('Entities\Korporacyjny_Produkt')->getById($id);
                if ($produkt instanceof Entities\Korporacyjny_Produkt) {
                    $this->_produktyArray[$produkt->getId()] = ['produkt' => $produkt,
                        'cena' => $produkt->getCena(),
                        'ilosc' => $v];
                }
            }
        }
    }

    private function _sprawdzBudzet() {
        $budzetUzytkownika = $this->_getBudzetUzytkownika();
        if ($this->_kosztZamowienia > $budzetUzytkownika) {
            $this->errors[] = ['produkt_id' => null,
                'content' => 'Brak budżetu na realizację zamówienia'];
        }
    }

    private function _getBudzetUzytkownika() {
        $budzet = new Atacama_Uzytkownik_Budzet($this->_em, $this->_uzytkownik);
        return $budzet->getDostepnyBudzetMiesieczny();
    }

    public function _tworzZamowienie() {
        $zamowienie = new Entities\Korporacyjny_Koszyk();
        $zamowienie->setStatus(Atacama_Static::KORPORACYJNI_ZAMOWIENIE_NOWE)
                ->setUzytkownicy($this->_uzytkownik->getUzytkownicy())
                ->setUzytkownicyId($this->_uzytkownik->getUzytkownicyId())
                ->setKorporacyjniUzytkownicy($this->_uzytkownik)
                ->setKorporacyjniUzytkownicyId($this->_uzytkownik->getId());

        $this->success[] = 'Zamówienie zostało pomyślnie utworzone';

        $this->_dodajProduktyDoZamowienia($zamowienie);

        $zamowienie->setNetto($this->_kosztZamowienia);

        $this->_em->persist($zamowienie);
        $this->_aktualizujStocki();

        $this->_usunPusteZamowienie($zamowienie);

        if (!empty($this->_produktyDoDodruku)) {

            $cmd = new Application_Model_Commands_KorporacyjnyKoszykPonadStock($this->_em, $this->_uzytkownik, $this->_produktyDoDodruku);
            $id = $cmd->execute();
            if (null != id) {
                $this->success[] = 'Z powodu braków na magazynie zostało utworzone zamówienie uzupełniające';
            }
        }
        // Zapisanie do bazy
        $this->_em->flush();
        $this->zamowienie_id = $zamowienie->getId();
    }

    /**
     * Dodaje produkty do zamowienia
     * Jezeli zamawiana ilosc jest ponizej stocku
     * dodaje dodatkowe zamowienie z terminami realizacji
     * @param Entities\Korporacyjny_Koszyk $zamowienie
     */
    public function _dodajProduktyDoZamowienia(Entities\Korporacyjny_Koszyk $zamowienie) {
        $koszt = 0;
        foreach ($this->_produktyArray as $v) {
            $produkt = new Entities\Korporacyjny_Koszyk_Produkt();
            $produkt
                    ->setKorporacyjniKoszyki($zamowienie)
                    ->setKorporacyjniKoszykiId($zamowienie->getId())
                    ->setKorporacyjniProdukty($v['produkt'])
                    ->setCenaZakupu($v['cena']);
            /**
             * Ilosc mniejsza/rowna niz stock
             * Calosc mozna zamowic
             */
            if ($v['ilosc'] <= $produkt->getKorporacyjniProdukty()->getStock()) {
                $produkt->setIlosc($v['ilosc']);
                $koszt += $v['cena'] * $v['ilosc'];
            } else {
                $stanStock = ($produkt->getKorporacyjniProdukty()->getStock() > 0 ? $produkt->getKorporacyjniProdukty()->getStock() : 0);
                $produkt->setIlosc($stanStock);
                $this->_dodajProduktyDoDodruku($produkt->getKorporacyjniProdukty(), $v['ilosc'] - $stanStock);
            }
            /*
             * Sytuacja:
             * W nieznanych okolicznosciach produktu na stocku jest 0
             * Nie dodajemy go do zamowienia, bo nie ma go na stanie, jest tylko na dodatkowym zamowieniu
             */
            if ($produkt->getKorporacyjniProdukty()->getStock() > 0) {
                $this->_em->persist($produkt);
            }
        }
        $this->_kosztZamowienia = $koszt;
    }

    /**
     * Aktualizuje stock
     * Jezeli stan jest ponizej minimum dla produktu wysyla wiadomosc do PH
     */
    private function _aktualizujStocki() {
        foreach ($this->_produktyArray as $v) {
            $prd = $v['produkt'];
            if ($prd instanceof Entities\Korporacyjny_Produkt) {
                $prd->setStock($prd->getStock() - $v['ilosc']);

                if ($prd->getStock() <= $prd->getMinStock()) {
                    $temat = "Kończy się stock produktu ";
                    $tresc = "produkt dla firmy się kończy";
                    $this->_wyslijPowiadomienieDoHandlowca($prd->getFirmy()->getPh()->getEmail(), $temat, $tresc);
                }
                $this->_em->persist($prd);
            }
        }
    }

    private function _dodajProduktyDoDodruku(Entities\Korporacyjny_Produkt $produkt, $brakujacaIlosc) {
        $this->_produktyDoDodruku[] = [
            'produkt' => $produkt,
            'ilosc' => (int) $brakujacaIlosc
        ];
    }

    private function _wyslijPowiadomienieDoHandlowca() {
        $config = Atacama_Config::getInstance();
        $view = Zend_Layout::getMvcInstance()->getView();
        $emailPh = new Atacama_Mail($this->_em, 'pl');
        $crypt = Moyoki_Crypt::getInstance();
        $email = $crypt->decrypt($this->_uzytkownik->getFirmy()->getPh()->getEmail());
        $temat = 'Nowe zamówienie specjalne dla ' . $this->_uzytkownik->getFirmy()->getNazwa();

        $url = $config->appUrl . $view->url(array(
                    'language' => 'pl',
                    'controller' => 'Zamowienia-specjalne',
                    'action' => 'szczegoly',
                    'id' => $this->zamowienie_id
                        ), 'default', true);
        ;
        $tresc = 'Użytkownik ' . $this->_uzytkownik->getUzytkownicy()->getImie() . ' ' . $this->_uzytkownik->getUzytkownicy()->getNazwisko() . '<br>złożył nowe zamówienie'
                . 'dla firmy <b>' . $this->_uzytkownik->getFirmy()->getNazwa() . '</b><br>'
                . '<a href="' . $url . '" class="btn btn-primary">Link do zamówienia</a>';


        $emailPh->phNoweZamowienie($email, $temat, $tresc);
    }

    /**
     * Usuwa puste zamowienie
     * Taka sytuacja moze sie pojawic, gdy klient zamowi produkty, ktore sa tylko na dodruku
     * Nie powinno sie pojawic, ale kto wie
     * @param Entities\Korporacyjny_Koszyk $zamowienie
     */
    private function _usunPusteZamowienie(Entities\Korporacyjny_Koszyk $zamowienie) {
        if (count($zamowienie->getKorporacyjniKoszykiProdukty()) == 0 && $zamowienie->getNetto() < 1) {
            $this->_em->remove($zamowienie);
        }
    }

}
