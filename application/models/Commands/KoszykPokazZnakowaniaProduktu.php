<?php

/**
 *
 * Przeliczanie kosztow znakowania dla produktu w koszyku
 * @author Studio Moyoki
 */
class Application_Model_Commands_KoszykPokazZnakowaniaProduktu {

    private $_kp;
    public $msg = null;
    private $_em;
    private $_lang;
    private $_tablica = array();

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, $koszyk_produkt) {
        $this->_lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');
        $this->_kp = $koszyk_produkt;
        $this->_em = $entityManager;
    }

    public function execute() {

        if ($this->_kp instanceof Entities\Koszyk_Produkt) {

            //$_kp = new Entities\Koszyk_Produkt;

            if (count($this->_kp->getKoszykiProduktyZnakowania()) > 0) {
                /**
                 * Sa znakowania
                 */
                foreach ($this->_kp->getKoszykiProduktyZnakowania() as $koszyk_znakowanie) {
                    /**
                     * Duza petla po calosci
                     */
                    //$koszyk_znakowanie = new Entities\Koszyk_Produkt_Znakowanie;

                    $znakowanie['id'] = $koszyk_znakowanie->getId();
                    $znakowanie['max_kolorow'] = $koszyk_znakowanie->getZnakowanie()->getKolory();
                    $znakowanie['naklad'] = $koszyk_znakowanie->getIloscSztuk();
                    $znakowanie['cena'] = $koszyk_znakowanie->getCena();
                    $znakowanie['przygotowalnia_mnozenie'] = (int) $koszyk_znakowanie->getPrzygotowalniaMnozenie();

                    foreach ($koszyk_znakowanie->getZnakowanie()->getZnakowaniaI18n() as $i18n) {
                        if ($i18n->getJezyk() == $this->_lang)
                            $znakowanie['nazwa'] = $i18n->getNazwa();
                    }

                    /**
                     * Wymnazanie ceny przez ilosc kolorow
                     */
                    $ilosc_kolorow = $koszyk_znakowanie->getIloscKolorow();

                    $znakowanie['ilosc_kolorow'] = $koszyk_znakowanie->getIloscKolorow();

                    /**
                     * Sprawdzamy czy czy do znakowania nalezy dodac konfekcjonowanie
                     */
                    $suma = $koszyk_znakowanie->getIloscSztuk() * $koszyk_znakowanie->getCena();

                    $znakowanie['suma'] = $suma;
                    $znakowanie['przygotowalnia'] = $koszyk_znakowanie->getPrzygotowalnia();
                    $znakowanie['przygotowalnia_mnozenie'] = $koszyk_znakowanie->getPrzygotowalniaMnozenie();
                    $znakowanie['powtorzenie'] = $koszyk_znakowanie->getPowtorzenie();


                    $this->_tablica['znakowania'][] = $znakowanie;
                    unset($znakowanie);
                }
            } else {
                /**
                 * Produkt nie posiada znakowan
                 */
                $this->_tablica['brak'] = 'brak';
            }
        }
        return $this->_tablica;
    }

}
