<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KategoriaDodaj {

    private $_post;
    public $msg = null;
    private $_em;
    private $_lang;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, $lang = 'pl', array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
        $this->_lang = $lang;
    }

    public function execute() {

        /**
         * Generowanie kategorii
         */
        $kategoria = new Entities\Kategoria();
        $rodzic = (int)$this->_post['kategoria'];
        if($rodzic == 0) {
            $rodzic = null;
        }
        $kategoria->setRodzic($rodzic)
                ->setSciezka(strtolower(System_Slug::string2slug($this->_post['nazwa'])))
                ->setKonfekcjonowanie(str_replace(',', '.', $this->_post['konfekcjonowanie']))
                ->setOpcje($this->_post['opcje'])
                ->setPoziom(0) // zostanie automatycznie przez DB uzupełniony
                ->setPozycja(0) // zostanie automatycznie przez DB uzupełniona
                ->setMacierz('') // zostanie automatycznie przez DB uzupełniona
                ;

        $this->_em->persist($kategoria);
        $this->_em->flush();

        /**
         * Generowanie i18n
         */
        $kategoriaI18n = new Entities\Kategoria_i18n();
        $kategoriaI18n->setJezyk($this->_lang)
                ->setKategorieId($kategoria->getId())
                ->setNazwa($this->_post['nazwa'])
                ->setKategorie($kategoria)
                ;

        $this->_em->persist($kategoriaI18n);
        $this->_em->flush();
    }

}
