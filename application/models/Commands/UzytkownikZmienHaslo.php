<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_UzytkownikZmienHaslo implements Moyoki_Command_Interface
{

	private $_error = false;
	private $_message;

	const BRAK_UZYTKOWNIKA = 1;
	const HASLA_NIEZGODNE = 2;
	const STARE_HASLO_JAK_NOWE = 3;

	private $_post = array();
        public $entityManager;

	/**
	 *
	 * @param array $_POST
	 */
	public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post)
	{
		$this->_post = $post;
                $this->entityManager = $entityManager;
	}

	public function execute()
	{
		/**
		 * Zmiana hasla przez przypomnienie hasla
		 */
		if (isset($_POST['id']) && $_POST['id'] > 0) {
			$uzytkownik = $this->entityManager->getRepository('Entities\Uzytkownik')->getById($_POST['id']);

			if ($uzytkownik instanceof Entities\Uzytkownik) {
				try {
					$haslo = new Atacama_Uzytkownik_Haslo($uzytkownik);
					if ($haslo->sprawdzLink($_POST['tymczasowe'], $_POST['kod']) == TRUE) {

						$uzytkownik->setHaslo($_POST['haslo1']);
						$this->entityManager->getRepository('Entities\Uzytkownik')->zmienHaslo($uzytkownik);
					} else {
						$this->_error = true;
						$this->_message = self::HASLA_NIEZGODNE;
					}
				} catch (Exception $e) {
					Moyoki_Debug::debug($e->getMessage());
				}
			} else {
				$this->_error = true;
				$this->_message = self::BRAK_UZYTKOWNIKA;
			}
		} else {
			/**
			 * Zmiana hasla w panelu klienta
			 */
			$auth = Zend_Auth::getInstance()->getIdentity();

			$uzytkownik = $this->entityManager->getRepository('Entities\Uzytkownik')->getById($auth['id']);

			if ($uzytkownik instanceof Entities\Uzytkownik) {
				try {
					if ($uzytkownik->getHaslo() == md5($_POST['stareHaslo'])) {
						if ($_POST['stareHaslo'] != $_POST['haslo1']) {
							$uzytkownik->setHaslo($_POST['haslo1']);
							$this->entityManager->getRepository('Entities\Uzytkownik')->zmienHaslo($uzytkownik);
						} else {
							$this->_error = true;
						$this->_message = self::STARE_HASLO_JAK_NOWE;
						}
					} else {
						$this->_error = true;
						$this->_message = self::HASLA_NIEZGODNE;
					}
				} catch (Exception $e) {
					Moyoki_Debug::debug($e->getMessage());
				}
			} else {
				$this->_error = true;
				$this->_message = self::BRAK_UZYTKOWNIKA;
			}
		}
	}

	public function isError()
	{
		return $this->_error;
	}

	public function getMessage()
	{
		return $this->_message;
	}

}