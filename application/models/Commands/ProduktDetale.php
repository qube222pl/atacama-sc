<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktDetale {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {
        /**
         * Generowanie produktu
         */
        $produkt = $this->_em->getRepository('Entities\Produkt')->getById($this->_post['id']);

        if ($produkt instanceof Entities\Produkt) {

            /**
             * Tworzna jest tablica, ktora pozwoli na obsluge pol formularza
             */
            $detaleTab = array();
            foreach ($this->_post as $k => $v) {
                if (stristr($k, '_')) {
                    list($detalID, $klucz) = explode('_', $k);
                    $detaleTab[$detalID][$klucz] = $v;
                }
            }

            /**
             * Teraz obslugujemy juz tablice i w zaleznosci czy dodac czy zmienic reagujemy
             */
            foreach ($detaleTab as $k => $v) {
                if (is_numeric($k)) {
                    /**
                     * Zmiana
                     */
                    $detal = $this->_em->getRepository('Entities\Produkt_Detal')->getById($k);
                    $detal->setKoloryId($v['kolor'])
                            ->setRozmiar($v['rozmiar'])
                            ->setCena(Atacama_Produkt_Kwota::zapis($v['cena']))
                            ->setVat($v['vat'])
                            ->setPromo(Atacama_Produkt_Kwota::zapis($v['promo']))
                            ->setWyp(Atacama_Produkt_Kwota::zapis($v['wyprz']))
                            ->setWidoczny($v['widoczny'])
                            ->setStanMagazynowy1($v['magazyn1'])
                            ->setStanMagazynowy2($v['magazyn2']);
                } else {

                    if (strlen($v['cena']) > 1) {
                        /**
                         * Dodanie nowej pozycji
                         */
                        $kolorObj = $this->_em->getRepository('Entities\Kolor')->getById($v['kolor']);

                        $detal = new Entities\Produkt_Detal();
                        $detal->setKoloryId($v['kolor'])
                                ->setKolory($kolorObj)
                                ->setProdukty($produkt)
                                ->setProduktyId($produkt->getId())
                                ->setRozmiar($v['rozmiar'])
                                ->setCena(Atacama_Produkt_Kwota::zapis($v['cena']))
                                ->setVat($v['vat'])
                                ->setPromo(Atacama_Produkt_Kwota::zapis($v['promo']))
                                ->setWyp(Atacama_Produkt_Kwota::zapis($v['wyprz']))
                                ->setWidoczny($v['widoczny'])
                                ->setStanMagazynowy1($v['magazyn1'])
                                ->setStanMagazynowy2($v['magazyn2']);
                    }
                }
                $this->_em->persist($detal);
            }
        }

        try {
            $this->_em->flush();

            return TRUE;
        } catch (DoctrineExtensions $e) {
            Moyoki_Debug::debug($e->getMessage(), 'Blad systemowy - prosze o kontakt i treść poniżej');
            return FALSE;
        } catch (Exception $e) {
            Moyoki_Debug::debug($e->getMessage(), 'Blad systemowy - prosze o kontakt i treść poniżej');
            return FALSE;
        }
    }

}
