<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Generowanie produktu
         */
        $produkt = $this->_em->getRepository('Entities\Produkt')->getById($this->_post['id']);

        if ($produkt instanceof Entities\Produkt) {

            $produkt->setDystrybutorzyId($this->_post['dystrybutorzy_id'])
                    ->setProducenciId($this->_post['producenci_id'])
                    ->setNrKatalogowy($this->_post['nr_katalogowy'])
                    ->setNrProducenta($this->_post['nr_producenta'])
                    ->setIdentyfikatorProducenta($this->_post['identyfikator_producenta'])
                    ->setModel(($this->_post['model'] > 0 ? $this->_post['model'] : NULL))
                    ->setWidoczny($this->_post['widoczny'])
                    ->setBestseller($this->_post['bestseller'])
                    ->setNowosc($this->_post['nowosc'])
                    ->setKategoriaGlowna($this->_post['kategoria_glowna'])
                    ->setPromocja($this->_post['promocja'])
                    ->setUpdatedatuser(date('Y-m-d H:i:s'));

            try {


                $this->_em->getRepository('Entities\Produkt')->zmien($produkt);
            } catch (Exception $ex) {
                Moyoki_Debug::debug($ex, __METHOD__);
                exit;
            }
        } else {
            return false;
        }
    }

}
