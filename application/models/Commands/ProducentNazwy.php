<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProducentNazwy {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $producent = $this->_em->getRepository('Entities\Producent')->getById($this->_post['id']);

        if ($producent instanceof Entities\Producent) {
            foreach ($this->_post as $k => $v) {
                if (is_array($v)) {

                    $i18n = $this->_em->getRepository('Entities\Producent_i18n')->getByProduktIdAndLang($producent->getId(), $k);
                    if ($i18n instanceof Entities\Producent_i18n) {
// Jest juz taki, to zmieniamy                        

                        $i18n->setOpis($v['opis_' . $k])
                                ->setSeoOpis($v['seo_opis_' . $k]);

                        $i18n = $this->_em->getRepository('Entities\Producent_i18n')->zmien($i18n);
                    } else {

// Nie ma jeszcze opisow dla tego produktu w tym jezyku                        
                        $i18n = new Entities\Producent_i18n();
                        $i18n->setJezyk($k)
                                ->setProducenci($producent)
                                ->setProducenciId($producent->getId())
                                ->setOpis($v['opis_' . $k])
                                ->setSeoOpis($v['seo_opis_' . $k]);

                        $this->_em->persist($i18n);
                        $this->_em->flush();
                    }
                }
            }
        } else {
            return false;
        }
    }

}
