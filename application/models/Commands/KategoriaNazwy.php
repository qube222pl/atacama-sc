<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KategoriaNazwy {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $kategoria = $this->_em->getRepository('Entities\Kategoria')->getById($this->_post['id']);

        if ($kategoria instanceof Entities\Kategoria) {
            foreach ($this->_post as $k => $v) {
                if (is_array($v)) {

                    $i18n = $this->_em->getRepository('Entities\Kategoria_i18n')->getByKategoriaIdAndLang($kategoria->getId(), $k);
                    if ($i18n instanceof Entities\Kategoria_i18n) {
// Jest juz taki, to zmieniamy                        
                        $i18n->setNazwa($v['nazwa_' . $k])
                                ->setOpis($v['opis_' . $k])
                                ->setDataModyfikacji(new DateTime());
                    } else {

// Nie ma jeszcze opisow dla tego produktu w tym jezyku                        
                        $i18n = new Entities\Kategoria_i18n();
                        $i18n->setJezyk($k)
                                ->setKategorie($kategoria)
                                ->setKategorieId($kategoria->getId())
                                ->setOpis($v['opis_' . $k])
                                ->setNazwa($v['nazwa_' . $k])
                                ->setDataModyfikacji(new DateTime());
                    }
                    $this->_em->persist($i18n);
                }
            }
            $this->_em->flush();
        } else {
            return false;
        }
    }

}
