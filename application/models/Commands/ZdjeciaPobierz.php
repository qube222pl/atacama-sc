<?php

/**
 * Polecenie - import
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZdjeciaPobierz {

    private $_conn;
    private $_path;
    private $_dirs;
    private $_minFileLen; // minimalna wielkość pliku w bajtach, jaka zostanie uznana za prawidłowe zdjęcie (z application.ini)
    

    /**
     *
     * @param array form values
     */
    public function __construct() {
        

        $zc = Zend_Controller_Front::getInstance();
        $this->_conn = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Entitymanagerfactory')->getConnection();
        $this->_path = realpath(Atacama_Config::getInstance()->zdjecia->path) . '/';
        
        $config = Atacama_Config::getInstance();
        $this->_minFileLen = $config->zdjecia->minimalnyplik;
        
        if($this->_path === false) {
            // katalog na zdjęcia nie istnieje
            return 0;
        }
        
        foreach (scandir($this->_path) as $dir) {
            if($dir != '.' && $dir != '..') {
                $this->_dirs[$dir] = true;
            }
        }
        
    }

   
    public function execute() {

        
        $sql = "SELECT pz.id, pz.plik, pz.plik_dystr, p.dystrybutorzy_id FROM produkty_zdjecia pz INNER JOIN produkty p ON pz.produkty_id = p.id WHERE pz.plik_dystr IS NOT NULL";
        $photos = $this->_conn->fetchAll($sql);
        $i = 0;

        set_time_limit(360);

        foreach ($photos as $photo) {
            
            $idDystr = $photo['dystrybutorzy_id'];
            if(!isset($this->_dirs[$idDystr])) {
                mkdir($this->_path . $idDystr);
                $this->_dirs[$idDystr] = true;
            }
            
            $plik = file_get_contents($photo['plik_dystr']);
            if($plik === false) {
                continue;
            }
            
            if(strlen($plik) < $this->_minFileLen) {
                continue;
            }
            
            $res = file_put_contents($this->_path . $photo['plik'], $plik);
            if($res === false) {
                continue;
            }
            
            $res = $this->_conn->update('produkty_zdjecia', array('plik_dystr' => null), array('id' => $photo['id']));
            if($res == 1) {
                $i++;
            }
        }
        
        return $i;
    }
    
}
