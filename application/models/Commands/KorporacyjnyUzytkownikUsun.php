<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyUzytkownikUsun {

    public $firma;
    public $success = [];
    public $errors = [];
    private $_post;
    private $_em;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param array $post
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $params, Entities\Firma $firma) {
        $this->_post = $params;
        $this->_em = $entityManager;
        $this->firma = $firma;
    }

    public function execute() {
        try {
            $user = $this->_em->getRepository('Entities\Korporacyjny_Uzytkownik')->getById($this->_post['id']);
            if ($user instanceof Entities\Korporacyjny_Uzytkownik) {
                $user->setStatus(Atacama_Static::KORPORACYJNI_UZYTKOWNICY_STATUS_USUNIETY);
               
                $this->_em->persist($user);
                $this->_em->flush();
                $this->success[] = "Użytkownik został zaznaczony jako usunięty. Dostęp został zabrany";
            } else {
                $this->errors[] = "Użytkownik nie istnieje";
            }
        } catch (DoctrineExtensions $e) {
            $this->errors[] = $e;
            return FALSE;
        } catch (Exception $e) {
            $this->errors[] = $e;
            return FALSE;
        }
    }
}
