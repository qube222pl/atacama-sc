<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktKategorie {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Generowanie produktu
         */
        $produkt = $this->_em->getRepository('Entities\Produkt')->getById($this->_post['id']);

        if ($produkt instanceof Entities\Produkt) {


            $kategorieTab = array();
            foreach ($this->_post as $k => $v) {
                if (stristr($k, '_')) {
                    list($tag, $katID) = explode('_', $k);
                    if (is_numeric($katID) && $tag == 'kat') {
                        $kategorieTab[$katID] = $v;
                    }
                }
            }

            $this->_em->beginTransaction();

            try {
                $result = $this->_em->getRepository('Entities\Produkt_Has_Kategoria')->kasujDlaProduktId($this->_post['id']);

                foreach ($kategorieTab as $id => $v) {
                    if ($v == 1) {
                        $katObj = $this->_em->getRepository('Entities\Kategoria')->getById($id);

                        $produktKategoria = new Entities\Produkt_Has_Kategoria();
                        $produktKategoria->setProdukty($produkt)
                                ->setProduktyId($produkt->getId())
                                ->setKategorie($katObj)
                                ->setKategorieId($katObj->getId());

                        $this->_em->persist($produktKategoria);
                        $this->_em->flush();
                    }
                }
            } catch (Exception $exc) {
                $this->_em->rollback();
            }
            $this->_em->commit();
        }
    }

}
