<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_BannerDodaj implements Moyoki_Command_Interface {

    private $_post;
    private $_edycja;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post, $edycja = FALSE) {
        $this->_edycja = $edycja;
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {
        $config = Atacama_Config::getInstance();
        if ($this->_edycja) {
            //Edycja
            
            $banner = $this->_em->getRepository('Entities\Banner')->getById($this->_post['id']);
            if ($banner instanceof Entities\Banner) {

                $banner->setNazwa($this->_post['nazwa'])
                        ->setJezyk('pl')
                        ->setTyp($config->bannery->image)
                        ->setLink($this->_post['link'])
                        ->setOpis($this->_post['opis'])
                        ->setWidoczny($this->_post['widoczny'])
                        ->setKolejnosc(1);

                return $this->_em->getRepository('Entities\Banner')->zmien($banner);
            }
        } else {
            //Dodawanie

            $banner = new Entities\Banner();
            $banner->setNazwa($this->_post['nazwa'])
                    ->setJezyk('pl')
                    ->setTyp($config->bannery->image)
                    ->setLink($this->_post['link'])
                    ->setPlik($this->_post['plik'])
                    ->setOpis($this->_post['opis'])
                    ->setWidoczny($this->_post['widoczny'])
                    ->setKolejnosc(1);

            try {
                $this->_em->persist($banner);
                $this->_em->flush();
            } catch (Exception $exc) {
                Atacama_Log::dodaj($this->_em, Atacama_Log::BLAD_SYSTEMU, $exc->getMessage());
                return FALSE;
            }
        }
        return TRUE;
    }

}
