<?php

/**
 * Polecenie - mapowanie danych importu
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ImportMapowanie {

    private $_formValues;
    private $_em;
    private $_option;
    private $_dystrybutorId;
    

    /**
     *
     * @param array form values
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $formValues, $option, $dystrybutorId) {
        $this->_formValues = $formValues;
        $this->_em = $entityManager;
        $this->_option = $option;
        $this->_dystrybutorId = $dystrybutorId;
    }

    public function execute() {
        
        switch($this->_option) {
            case 'producent':
                $table = 'import_producent'; // nazwa tabeli DB
                $prefix = 'producent_'; // przedrostek w formularzu
                $fields = array(
                    'id' => 'producenci_id',
                    'dystr_id' => 'producent_dystr_id',
                );
                break;
            case 'znakowanie':
                $table = 'import_znakowanie'; // nazwa tabeli DB
                $prefix = 'znakowanie_'; // przedrostek w formularzu
                $fields = array(
                    'id' => 'znakowanie_id',
                    'dystr_id' => 'znakowanie_dystr_id',
                );
                break;
            case 'kolor':
                $table = 'import_kolory'; // nazwa tabeli DB
                $prefix = 'kolor_'; // przedrostek w formularzu
                $fields = array(
                    'id' => 'kolory_id',
                    'dystr_id' => 'kolor_dystr_id',
                );
                break;
            case 'kategoria':
                $table = 'import_kategoria'; // nazwa tabeli DB
                $prefix = 'kategoria_'; // przedrostek w formularzu
                $fields = array(
                    'id' => 'kategorie_id',
                    'dystr_id' => 'kategoria_dystr_id',
                );
                break;
        }
        
        $conn = $this->_em->getConnection();
        $updateData = array();
        
        foreach ($this->_formValues as $key => $val) {
            if(preg_match('/^' . $prefix . '(\d+)$/', $key, $matches)) {
                $rabat = 'rabat_' . $matches[1];
                if(isset($this->_formValues[$rabat])) {
                    $updateData['rabat'] = $this->_formValues[$rabat];
                }
                if($val == '-1') {
                    $conn->delete($table, array(
                        'id' => $matches[1]
                    ));
                } else {
                    $updateData[($fields['id'])] = $val;
                    $conn->update($table, $updateData, array(
                        'id' => $matches[1]
                    ));
                }
            }
            unset($updateData);
        }
        
        return true;
    }
    
    

}
