<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZnakowanieNazwy {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $znakowanie = $this->_em->getRepository('Entities\Znakowanie')->getById($this->_post['id']);

        if ($znakowanie instanceof Entities\Znakowanie) {
            foreach ($this->_post as $k => $v) {
                if (is_array($v)) {

                    $i18n = $this->_em->getRepository('Entities\Znakowanie_i18n')->getByZnakowanieIdAndLang($znakowanie->getId(), $k);
                    if ($i18n instanceof Entities\Znakowanie_i18n) {
// Jest juz taki, to zmieniamy                        

                        $i18n->setNazwa($v['nazwa_' . $k]);

                        $i18n = $this->_em->getRepository('Entities\Znakowanie_i18n')->zmien($i18n);
                    } else {

// Nie ma jeszcze opisow dla tego produktu w tym jezyku                        
                        $i18n = new Entities\Znakowanie_i18n();
                        $i18n->setJezyk($k)
                                ->setZnakowania($znakowanie)
                                ->setZnakowaniaId($znakowanie->getId())
                                ->setNazwa($v['nazwa_' . $k]);

                        $this->_em->persist($i18n);
                        $this->_em->flush();
                    }
                }
            }
        } else {
            return false;
        }
    }

}
