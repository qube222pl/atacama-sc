<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktUsunNiewidoczny {

    private $_id;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, $usunietyId) {
        $this->_id = $usunietyId;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Pobieranie potrzebnych obiektow
         */
        $produkt = $this->_em->getRepository('Entities\Produkt')->getById($this->_id);

        if (!$produkt instanceof Entities\Produkt) {
            $this->msg = 'Produkt nie istnieje';
            return FALSE;
        }

        if ($produkt->getWidoczny() == 1) {
            $this->msg = 'Nie można usuwać produktów widocznych';
            return FALSE;
        }

        $produkt->setUsuniety(1);

        $this->_em->persist($produkt);
        $this->_em->flush();

        $this->msg = 'Produkt usunięty';
        return TRUE;
    }

}
