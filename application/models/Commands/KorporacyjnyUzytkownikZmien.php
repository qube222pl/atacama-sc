<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyUzytkownikZmien {

    public $firma;
    public $success = [];
    public $errors = [];
    private $_post;
    private $_em;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param array $post
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $params, Entities\Firma $firma) {
        $this->_post = $params;
        $this->_em = $entityManager;
        $this->firma = $firma;
    }

    public function execute() {
        try {
            $user = $this->_em->getRepository('Entities\Korporacyjny_Uzytkownik')->getById($this->_post['id']);
            if ($user instanceof Entities\Korporacyjny_Uzytkownik) {
                if ($user->getRola() == Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KLIENT) {
                    $user->setRola(Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KOORDYNATOR);
                } else {
                    $user->setRola(Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KLIENT);
                }

                $this->_em->persist($user);
                $this->_em->flush();
                $this->success[] = "Użytkownik ma przyznają nową rolę";
            } else {
                $this->errors[] = "Użytkownik nie istnieje";
            }
        } catch (DoctrineExtensions $e) {
            $this->errors[] = $e;
            return FALSE;
        } catch (Exception $e) {
            $this->errors[] = $e;
            return FALSE;
        }
    }

}
