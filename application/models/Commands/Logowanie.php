<?php

/**
 * Polecenie - logowanie czynnosci
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_Logowanie {

    private $_post;
    public $msg = null;
    public $entityManager;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->entityManager = $entityManager;
    }

    public function execute() {
        $adapter = new Atacama_Auth_Adapter($this->entityManager, trim($this->_post['email']), trim($this->_post['haslo']));

        $authResult = Zend_Auth::getInstance()->authenticate($adapter);

        if (!$authResult->isValid()) {
            $this->msg = $authResult->getMessages();
            return false;
        } else {
            Zend_Auth::getInstance()->clearIdentity();

            /**
             * Przypisanie Typu klienta
             */
            if (NULL != $adapter->uzytkownik->getFirmyId()) {
                
                $firma = $this->entityManager->getRepository('Entities\Firma')
                        ->getById($adapter->uzytkownik->getFirmyId());
            }
            Zend_Auth::getInstance()->getStorage()->write(array(
                'imie' => $adapter->uzytkownik->getImie(),
                'nazwisko' => $adapter->uzytkownik->getNazwisko(),
                'mail' => $this->_post['email'],
                'rola' => $adapter->uzytkownik->getRola(),
                'id' => $adapter->uzytkownik->getId(),
                'firma_id' => $adapter->uzytkownik->getFirmyId(),
                'ph' => $firma->getPhId(),
            ));

            /**
             * Atacama log - sprawdzam IP userow
             */
            Atacama_Log::dodaj($this->entityManager, atacama_log::LOGOWANIE, $_SERVER['REMOTE_ADDR']);
            return true;
        }
    }

}
