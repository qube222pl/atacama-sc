<?php

/**
 * Polecenie - logowanie czynnosci
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_AdresZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        
        $adres = $this->_em->getRepository('Entities\Adres')->getById($this->_post['id']);

        if ($adres instanceof Entities\Adres) {
            $adres->setNazwa($this->_post['nazwa'])
                    ->setMiasto($this->_post['miasto'])
                    ->setTypAdresu($this->_post['typ_adresu'])
                    ->setUlica($this->_post['ulica'])
                    ->setKraj($this->_post['kraj'])
                    ->setNrTelefonu($this->_post['telefon'])
                    ->setKodPocztowy($this->_post['kod_pocztowy']);
            
           
            $this->_em->getRepository('Entities\Adres')->zmien($adres);
        } else {
            throw new Exception('Brak firmy o podanym identyfikatorze', 997);
        }
    }

}
