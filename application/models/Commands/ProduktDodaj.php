<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktDodaj {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Pobieranie potrzebnych obiektow
         */
        $dystrybutor = $this->_em->getRepository('Entities\Dystrybutor')->getById($this->_post['dystrybutorzy_id']);
        $producent = $this->_em->getRepository('Entities\Producent')->getById($this->_post['producenci_id']);

        /**
         * Generowanie produktu
         */
        $produkt = new Entities\Produkt();
        $produkt->setDystrybutorzy($dystrybutor)
                ->setDystrybutorzyId($dystrybutor->getId())
                ->setProducenci($producent)
                ->setProducenciId($producent->getId())
                ->setNrKatalogowy($this->_post['nr_katalogowy'])
                ->setNrProducenta($this->_post['nr_producenta'])
                ->setIdentyfikatorProducenta($this->_post['identyfikator_producenta'])
                ->setModel($this->_post['model'])
                ->setWidoczny($this->_post['widoczny'])
                ->setBestseller($this->_post['bestseller'])
                ->setNowosc($this->_post['nowosc'])
                ->setPromocja($this->_post['promocja']);

        $this->_em->persist($produkt);
        $this->_em->flush();

        /**
         * Generowanie i18n
         */
        $produktI18n = new Entities\Produkt_i18n();
        $produktI18n->setJezyk('pl')
                ->setProdukty($produkt)
                ->setProduktyId($produkt->getId())
                ->setNazwa($this->_post['nazwa']);

        $this->_em->persist($produktI18n);
        $this->_em->flush();
        
        return($produkt->getId());
    }

    public function setPost(array $post) {
        $this->_post = $post;
    }

}
