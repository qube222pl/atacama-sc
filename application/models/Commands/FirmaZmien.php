<?php

/**
 * Polecenie - logowanie czynnosci
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_FirmaZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $firma = $this->_em->getRepository('Entities\Firma')->getByNip($this->_post['nip']);

        if ($firma instanceof Entities\Firma) {
            $firma->setNazwa($this->_post['nazwa'])
                    ->setRegon($this->_post['regon'])
                    ->setPhId($this->_post['ph_id'])
                    ->setUpust(($this->_post['upust'] > 0 ? (int) $this->_post['upust'] : 0));

            $this->_em->getRepository('Entities\Firma')->zmien($firma);
        } else {
            throw new Exception('Brak firmy o numerze NIP: <b>' . $this->_post['nip'] . '</b>', 997);
        }
    }

}
