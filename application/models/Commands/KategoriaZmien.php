<?php

/**
 * Polecenie - logowanie czynnosci
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KategoriaZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $kategoria = $this->_em->getRepository('Entities\Kategoria')->getById($this->_post['id']);

        if ($kategoria instanceof Entities\Kategoria) {
            
            $rodzic = (int)$this->_post['kategoria'];
            if($rodzic == 0) {
                $rodzic = null;
            }

            if($kategoria->getRodzic() != $rodzic) {
                // nastąpiła zmiana nadrzędnej kategorii
                // trzeba przenieść całe drzewo
                if($kategoria->getId() == $rodzic) {
                    // nie można przenieść kategorii pod samą siebie
                    throw new Exception('Nie można przenieść tam tej kategorii!', 997);
                }
                $this->_em->getRepository('Entities\Kategoria')->przenies($kategoria->getId(), $rodzic);
                
                // kategoria zmieniła się na główną -> trzeba zmienić mapowanie importu na 'do uzupełnienia'
                if($rodzic == null) {
                    $this->_em->getRepository('Entities\Import_Kategoria')->zmienKategorie($kategoria->getId(), 1);
                }
                
            }
            
            $kategoria->setSciezka(strtolower(System_Slug::string2slug($this->_post['nazwa'])))
                    ->setKonfekcjonowanie(str_replace(',', '.', $this->_post['konfekcjonowanie']))
                    ->setOpcje($this->_post['opcje']);

            $kategoria_i18n = $kategoria->getKategorieI18n()->first();
            $kategoria_i18n->setNazwa($this->_post['nazwa']);

            $this->_em->flush();
            
        } else {
            throw new Exception('Brak kategorii o podanym identyfikatorze: <b>' . $this->_post['id'] . '</b>', 997);
        }
    }

}
