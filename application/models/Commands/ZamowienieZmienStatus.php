<?php

/**
 *
 * Aktulizacja ilosci i cen zamowienia
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZamowienieZmienStatus {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $zamowienie = $this->_em->getRepository('Entities\Koszyk')->getById($this->_post['id']);

        /*
         * Zmiana statusu zamowienia (koszyka)
         */
        $zamowienie->setStatus($this->_post['status']);

        /**
         * Jezeli jest zrealizowane, obliczamy marze i wysylamy wiadomosc do Klienta
         */
        if ($this->_post['status'] == Atacama_Acl::STATUS_ZREALIZOWANE) {
            $this->_obliczMarze($zamowienie);
            $this->_wiadomoscDoKlienta($zamowienie);
        }

        if (NULL === $zamowienie->getNrZamowienia() && $this->_post['status'] == Atacama_Acl::STATUS_PRZYJETE) {
            // zmiana statusu koszyka i nadanie nr zamowienia
            $najwyzszyNrZamObj = $this->_em->getRepository('Entities\Koszyk')->getNajwyzszyNrZamowienia();

            $najwyzszyNrZam = $najwyzszyNrZamObj->getIdZamowienia() + 1;

            $zamowienie->setIdZamowienia($najwyzszyNrZam)
                    ->setNrZamowienia($najwyzszyNrZam . '/' . date('Y'))
                    ->setDataZamowienia(new DateTime());
        }
        
        $this->_em->getRepository('Entities\Koszyk')->zmien($zamowienie);

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $uzytkownik = $this->_em->getRepository('Entities\Uzytkownik')->getById($user['id']);

            /*
             * Dodawanie uwag do zamowienia (informacja o zmianie statusu)
             */

            if ($uzytkownik instanceof Entities\Uzytkownik) {
                $uwaga = new Entities\Koszyk_Uwaga();

                $uwaga->setUzytkownicyId($uzytkownik->getId())
                        ->setUzytkownicy($uzytkownik)
                        ->setTresc(nl2br($this->_post['uwagi']))
                        ->setStatus($this->_post['status'])
                        ->setKoszykiId($zamowienie->getId())
                        ->setKoszyki($zamowienie);

                $this->_em->persist($uwaga);
                $this->_em->flush();
            }
        }
        /**
         * Zwracamy to zamowienie
         */
        return $zamowienie;
    }

    private function _obliczMarze(Entities\Koszyk $zamowienie) {
        $marza = 0;
        foreach ($zamowienie->getKoszykiProdukty() as $koszykProdukt) {
            $marza = $marza + ($koszykProdukt->getCenaZakupu() - $koszykProdukt->getCenaSprzedazy()) * $koszykProdukt->getIlosc();
        }

        $zamowienie->setMarza($marza);
        return $zamowienie;
    }

    private function _wiadomoscDoKlienta(Entities\Koszyk $zamowienie) {
        $widget = $this->_em->getRepository('Entities\Widget')->getByKod('MAIL_ZAMOWIENIE_ZREALIZOWANE');

        $view = Zend_Layout::getMvcInstance()->getView();
        $temat = $view->translate('atacama zamowienie zrealizowane - nr') . ' ' . $zamowienie->getNrZamowienia();

        $adresEmail = Moyoki_Crypt::getInstance()->decrypt($zamowienie->getUzytkownicy()->getEmail());

        try {
            $email = new Atacama_Mail($this->_em);
            $wynikWysylki = $email->ofertaDlaKlienta($adresEmail, $temat, $widget->getTresc(), NULL);
        } catch (Exception $exc) {
            Atacama_Log::dodaj($this->_em, Atacama_Log::BLAD_SYSTEMU, $exc->getMessage());
        }

        return $wynikWysylki;
    }

}
