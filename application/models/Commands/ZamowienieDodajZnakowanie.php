<?php

/**
 *
 * Dodawanie znakowania dla produktu w koszyku
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZamowienieDodajZnakowanie {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {
        $koszyk_detal_id = (int) $this->_post['detal_id'];
        $ilosc_kolorow = (int) $this->_post['kolory'];
        $metoda_id = (int) $this->_post['metoda'];
        $produkt_id = (int) $this->_post['prod_id'];
        $powtorzenie = (int) $this->_post['powtorzenie'];
        $zam_id = (int) (isset($this->_post['zam_id']) ? $this->_post['zam_id'] : 0);
        $auth = Zend_Auth::getInstance()->getIdentity();

        /**
         * Sprawdzamy czy koszyk_produkt jest uzytkownika
         */
        if ($zam_id > 0) {
            $koszyk_produkt = $this->_em->getRepository('Entities\Koszyk_Produkt')->getByIdAndZamowienieId($koszyk_detal_id, $zam_id);
        } else {
            $koszyk_produkt = $this->_em->getRepository('Entities\Koszyk_Produkt')->getById($koszyk_detal_id);
            if ($koszyk_produkt instanceof Entities\Koszyk_Produkt) {
//$koszyk_produkt = new Entities\Koszyk_Produkt;
                switch ($koszyk_produkt->getKoszyki()->getStatus()) {
// Koszyk
                    case Atacama_Acl::STATUS_KOSZYK: {
                            if ($koszyk_produkt->getKoszyki()->getUzytkownicyId() != $auth['id']) {
                                $koszyk_produkt = NULL;
                            }
                        }
                        break;
// Tworzona oferta
                    case Atacama_Acl::STATUS_OFERTA_TWORZENIE: {
                            if ($auth['rola'] != Atacama_Acl::ROLA_PH || $koszyk_produkt->getKoszyki()->getFirmy()->getPhId() != $auth['id']) {
                                $koszyk_produkt = NULL;
                            }
                        }
                        break;
// Oferta
                    case Atacama_Acl::STATUS_OFERTA: {
                            if ($koszyk_produkt->getKoszyki()->getUzytkownicyId() != $auth['id']) {
                                $koszyk_produkt = NULL;
                            }
                        }
                        break;
                }
            }
        }
        if ($koszyk_produkt instanceof Entities\Koszyk_Produkt) {

//$koszyk_produkt = new Entities\Koszyk_Produkt;
            /**
             * Pobieramy metode znakowania
             */
            $metoda_znakowania = $this->_em->getRepository('Entities\Znakowanie')->getById($metoda_id);
            /**
             * Pobieramy obiekt znakowania po identyfiaktorze.
             * Jezeli nie ma takiego obiektu, bo nie mozna dodac znakowania dla produktu
             */
            if ($metoda_znakowania instanceof Entities\Znakowanie) {

                $znakowanie = new Entities\Koszyk_Produkt_Znakowanie();
                $znakowanie->setKoszykiDetaleId($koszyk_produkt->getId())
                        ->setKoszykiProdukty($koszyk_produkt)
                        ->setIloscKolorow($ilosc_kolorow)
                        ->setZnakowanieId($metoda_znakowania->getId())
                        ->setZnakowanie($metoda_znakowania)
                        ->setIloscSztuk($koszyk_produkt->getIlosc());

                /*
                 * Sprawdzamy czy dodadac konfekcjonowanie (z Koszyk_Produkt >> Dublowanie - co zrobic <<)
                 * dla tego znakowania
                 */
            }
            $znakowanie_rodzaj = $this->_em
                    ->getRepository('Entities\Znakowanie_Rodzaj')
                    ->getByZnakowanieIdAndIlosc($metoda_znakowania->getId(), $koszyk_produkt->getIlosc());

            $nieMnozycPrzygotowalni = FALSE;

            /**
             * Sprawdzanie czy ryczalt czy nie
             */
            if ($znakowanie_rodzaj instanceof Entities\Znakowanie_Rodzaj) {
                /**
                 * Miesci sie w przedziale
                 */
                $cena = 0;
                for ($i = 1; $i <= $ilosc_kolorow; $i++) {
                    if ($i == 1) {
                        $cena +=$znakowanie_rodzaj->getCena();
                    } else {
                        $cena += $znakowanie_rodzaj->getCenaInny();
                    }
                }
                $znakowanie->setCena($cena);

                /**
                 * To jest jakis wyjatek. Nie mnozymy przygotowalni dla znakowan, ktore inna cene maja = 0
                 */
                if ($znakowanie_rodzaj->getCenaInny() == '0.00') {
                    $nieMnozycPrzygotowalni = TRUE;
                    $znakowanie->setPrzygotowalniaMnozenie(0);
                }
            } else {
                /**
                 * Trzeba brac z ryczaltu cene
                 */
                $kwotaRyczaltu = $metoda_znakowania->getRyczalt() / $koszyk_produkt->getIlosc();

                $przykladowyRodzaj = $metoda_znakowania->getZnakowaniaRodzaje()->first();
                if ($przykladowyRodzaj instanceof Entities\Znakowanie_Rodzaj) {
                    if ($przykladowyRodzaj->getCenaInny() > 0) {
                        $znakowanie->setCena($kwotaRyczaltu * $ilosc_kolorow);
                    } else {
                        $znakowanie->setCena($kwotaRyczaltu);
                    }
                }
            }
            /**
             * Przypisywanie powtorzenia
             */
            $znakowanie->setPowtorzenie(($powtorzenie == 1 ? 1 : 0));

            //$metoda_znakowania = new Entities\Znakowanie;
            if ($nieMnozycPrzygotowalni) {
                $znakowanie->setPrzygotowalnia(($powtorzenie == 1 ? $metoda_znakowania->getPowtorzenie() : $metoda_znakowania->getPrzygotowalnia()));
            } else {
                $znakowanie->setPrzygotowalnia(($powtorzenie == 1 ? $metoda_znakowania->getPowtorzenie() : $metoda_znakowania->getPrzygotowalnia()) * $ilosc_kolorow);
                $znakowanie->setPrzygotowalniaMnozenie(1);
            }
            $this->_em->persist($znakowanie);
            $this->_em->flush();
            return $znakowanie;
        }
    }

}
