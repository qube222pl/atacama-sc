<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProduktNazwy {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $produkt = $this->_em->getRepository('Entities\Produkt')->getById($this->_post['id']);

        if ($produkt instanceof Entities\Produkt) {
            foreach ($this->_post as $k => $v) {
                if (is_array($v)) {
                    $i18n = $this->_em->getRepository('Entities\Produkt_i18n')->getByProduktIdAndLang($produkt->getId(), $k);
                    if ($i18n instanceof Entities\Produkt_i18n) {
// Jest juz taki, to zmieniamy                        

                        $i18n->setNazwa($v['nazwa_' . $k])
                                ->setOpis($v['opis_' . $k])
                                ->setSeoOpis($v['seo_opis_' . $k])
                                ->setDataModyfikacji(new DateTime());
                        
                    } else {
// Nie ma jeszcze opisow dla tego produktu w tym jezyku                        
                        $i18n = new Entities\Produkt_i18n();
                        $i18n->setJezyk($k)
                                ->setProdukty($produkt)
                                ->setProduktyId($produkt->getId())
                                ->setNazwa($v['nazwa_' . $k])
                                ->setOpis($v['opis_' . $k])
                                ->setSeoOpis($v['seo_opis_' . $k])
                                ->setDataModyfikacji(new DateTime());
                        
                    }
                    $this->_em->persist($i18n);
                }
            }
            $this->_em->flush();
        } else {
            return false;
        }
    }

}
