<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_DemoReset {

    public $msg = null;
    public $_em;
    private $_firmaDemoID = 637;
    private $_produktyFirmy = NULL;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager) {
        $this->_em = $entityManager;

        /**
         * Pobieranie produktow firmy demo
         */
        $produkty = $this->_em->getRepository('Entities\Korporacyjny_Produkt')->getWszystkieDlaFirmy($this->_firmaDemoID);
        foreach ($produkty as $produkt) {
            if ($produkt instanceof Entities\Korporacyjny_Produkt) {
                $this->_produktyFirmy[$produkt->getId()] = $produkt;
            }
        }
    }

    public function execute() {

        /**
         * Uzytkownicy firmy DEMO
         */
        $uzytkownicy = $this->_em->getRepository('Entities\Uzytkownik')->getByFirmaId($this->_firmaDemoID);

        foreach ($uzytkownicy as $uzytkownik) {
            if ($uzytkownik instanceof Entities\Uzytkownik) {
                $this->_kasujZamowieniaUzytkownika($uzytkownik);
            }
        }

        $this->_em->flush();
    }
/**
 * Kasowanie zamowien uzytkownika
 * @param Entities\Uzytkownik $uzytkownik
 */
    private function _kasujZamowieniaUzytkownika(Entities\Uzytkownik $uzytkownik) {
        $zamowienienia = $this->_em->getRepository('Entities\Korporacyjny_Koszyk')->getWszystkieDlaUzytkownika($uzytkownik->getId());
        foreach ($zamowienienia as $zamowienie) {
            $this->_obslugaProduktow($zamowienie);
            $this->_em->remove($zamowienie);
        }
    }
/**
 * Kasowanie produktow z zamowien uzytkownika
 * @param Entities\Korporacyjny_Koszyk $zamowienie
 */
    private function _obslugaProduktow(Entities\Korporacyjny_Koszyk $zamowienie) {
        foreach ($zamowienie->getKorporacyjniKoszykiProdukty() as $produkt) {
            if ($produkt instanceof Entities\Korporacyjny_Koszyk_Produkt) {
                
                $this->_zwrocStanyDlaProduktu($produkt);
                $this->_em->remove($produkt);
            }
        }
    }
/**
 * Przywracane stocku do stanu 0
 * @param Entities\Korporacyjny_Koszyk_Produkt $produkt
 */
    private function _zwrocStanyDlaProduktu(Entities\Korporacyjny_Koszyk_Produkt $produkt) {
        if (key_exists($produkt->getKorporacyjniProduktyId(), $this->_produktyFirmy)) {
            $korporacyjny_produkt = $this->_produktyFirmy[$produkt->getKorporacyjniProduktyId()];
            if ($korporacyjny_produkt instanceof Entities\Korporacyjny_Produkt) {
                
                $korporacyjny_produkt->setStock($korporacyjny_produkt->getStock() + $produkt->getIlosc());
                $this->_em->persist($korporacyjny_produkt);
            }
        }
    }

}
