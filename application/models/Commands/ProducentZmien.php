<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ProducentZmien {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $config = \Atacama_Config::getInstance();

        $producent = $this->_em->getRepository('Entities\Producent')->getById($this->_post['id']);

        if ($producent instanceof Entities\Producent) {
            
            $producent->setWidoczny($this->_post['widoczny']);
            
            if (!empty($_FILES['logotyp']) && $_FILES['logotyp']['error'] == 0) {
                try {
                    try {
                        $foto = new \Moyoki_File($config->logotypy->path . $producent->getLogo());
                        $foto->delete();
                    } catch (\Moyoki_Exception $exc) {
                        echo $exc->getMessage();
                    }
                    $temp_file = $_FILES['logotyp']['tmp_name'];

                    $config = Atacama_Config::getInstance();

                    $path = realpath($config->logotypy->path);
                    $file = $_FILES['logotyp']['name'];
                    $basenameAndExtension = explode('.', $file);
                    $ext = end($basenameAndExtension);

                    $filename = strtolower(Moyoki_Friendly::name($this->_post['nazwa']) . '-' . rand(1, 99) . '.' . $ext);

                    if (move_uploaded_file($temp_file, $path . '/' . $filename)) {

                        $logotypNazwa = $filename;
                    }
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }



            $producent->setNazwa($this->_post['nazwa']);

            if (isset($logotypNazwa) && strlen($logotypNazwa) > 3) {
                $producent->setLogo($logotypNazwa);
            }

            $this->_em->getRepository('Entities\Producent')->zmien($producent);
        }
    }

}
