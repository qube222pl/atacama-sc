<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyBudzetyZapisz {

    public $errors = [];
    public $success = [];
    private $_post;
    private $_em;
    private $_firma_id = 0;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param array $post
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
        $this->_firma_id = $post['firma_id'];
    }

    public function execute() {
        try {
            $this->_aktualizujBudzety();
            $this->_em->flush();
            $this->success[] = "Dane zmienione pomyślnie";
        } catch (DoctrineExtensions $e) {
            $this->errors[] = $e;
            return FALSE;
        } catch (Exception $e) {
            $this->errors[] = $e;
            return FALSE;
        }
    }

    private function _aktualizujBudzety() {
        foreach ($this->_post as $k => $v) {
            list($id, $reszta) = array_pad(explode('_', $k), 2, null);
            $budzet = floatval(str_replace(',', '.', $v));
            $id = intval($id);
            if ($reszta == 'budzet' && $id > 0) {
                /**
                 * Jezeli jest budzet i jest user_id to:
                 * szukamy wsrod aktywnych uzytkownikow dla tej firmy wybranej pozycji
                 */
                $uzytkownik = $this->_em->getRepository('Entities\Korporacyjny_Uzytkownik')->getDlaUserIdIFirmaIdIAktywny($id, $this->_firma_id);
                if ($uzytkownik instanceof Entities\Korporacyjny_Uzytkownik) {
                    /**
                     * Jest korporacyjny uzytkownik
                     * Ustawiamy mu nowy budzet
                     */
                    $uzytkownik->setBudzet($budzet);
                    $this->_em->persist($uzytkownik);
                }
            }
        }
    }

}
