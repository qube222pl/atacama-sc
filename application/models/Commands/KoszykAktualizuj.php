<?php

/**
 *
 * Aktulizacja ilosci i cen w koszyku klienta
 * @author Studio Moyoki
 */
class Application_Model_Commands_KoszykAktualizuj {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $user = Zend_Auth::getInstance()->getIdentity();
        $koszykDostep = Atacama_Koszyk_Dostep::getInstance();
        $koszyk_finalny = NULL;

        /**
         * Jezeli jest klient, jest > 0 i jest rozny od osoby zalogowanej
         * to tworzymy oferte
         * Bo jezeli 'klient' == user['id'] to nie ma zmiany wlascicela
         */
        if (isset($this->_post['klient']) && $this->_post['klient'] > 0 && $this->_post['klient'] != $user['id']) {
            $koszyk = $this->_em->getRepository('Entities\Koszyk')->getById($this->_post['kid']);
            if ($koszyk instanceof Entities\Koszyk) {
                //$koszyk = new \Entities\Koszyk;
                $klient = $this->_em->getRepository('Entities\Uzytkownik')->getById($this->_post['klient']);
                //$klient = new Entities\Uzytkownik;
                $koszyk->setUzytkownicyId($klient->getId())
                        ->setUzytkownicy($klient)
                        ->setFirmy($klient->getFirmy())
                        ->setFirmyId($klient->getFirmyId())
                        ->setStatus(Atacama_Acl::STATUS_OFERTA_TWORZENIE)
                        ->setOferta(1);

                $this->_em->getRepository('Entities\Koszyk')->zmienWlasciciela($koszyk);
            }
        }

        foreach ($this->_post['koszyk'] as $id => $ilosc) {

            /**
             * Sprawdzam czy produkt w koszyku nalezy do koszyka uzytkownika
             */
            $koszyk_produkt = $this->_em->getRepository('Entities\Koszyk_Produkt')->getById($id);
            if ($koszyk_produkt instanceof Entities\Koszyk_Produkt) {

                /**
                 * Sprawdzamy czy user ma dostep do tego koszyka
                 */
                if (isset($koszyk) && $koszyk instanceof Entities\Koszyk) {
                    /*
                     * Koszyk byl zmieniany, wiec tylko przypisujemy obiekt
                     */
                    $params[Atacama_Koszyk_Dostep::PARAM_KOSZYK_OBJ] = $koszyk;
                } else {
                    /**
                     * Nie ma obiektu koszyka, wiec pobieramy
                     */
                    $params[Atacama_Koszyk_Dostep::PARAM_KOSZYK_ID] = $koszyk_produkt->getKoszyki()->getId();
                }
                if ($koszykDostep->sprawdzDostep($params, $this->_em)) {
                    //$koszyk_produkt = new Entities\Koszyk_Produkt;
                    $koszyk_finalny = $koszyk_produkt->getKoszyki();
                    /**
                     * Ktos chce kasowac w tym miejscu, wiec usuwamy
                     */
                    if ($ilosc == 0) {
                        $this->_em->getRepository('Entities\Koszyk_Produkt')->deleteById($koszyk_produkt->getId());
                    } else {
                        /**
                         * Kategoria pierwsza aby wyliczyc cene
                         */
                        $katGlownaProduktu = $koszyk_produkt->getProduktyDetale()->getProdukty()->getKategoriaGlowna();
                        /**
                         * Jezeli jej nie ma, to nie uda sie wyliczenie ceny
                         */
                        if (is_numeric($katGlownaProduktu) && $katGlownaProduktu > 0) {
                            $kategorieRodzice = $this->_em->getRepository('Entities\Kategoria')->pobierzRodzicow($katGlownaProduktu);

                            $kategoria_pierwszaID = $kategorieRodzice[0]->getId();

                            $cenaZakupu = new Atacama_Produkt_Cenaklienta($this->_em, $koszyk_produkt->getProduktyDetale(), $kategoria_pierwszaID, $ilosc);
                            $cena = $cenaZakupu->wylicz();

                            if ($cena['cenaWyprzedaz'] != '0.00') {
                                $nowa_cena = $cena['cenaWyprzedaz'];
                            } else if ($cena['cenaPromocyjna'] != '0.00') {
                                $nowa_cena = $cena['cenaPromocyjna'];
                            } else {
                                $nowa_cena = $cena['cenaRegularna'];
                            }
                            //Moyoki_Debug::debug($cena, __METHOD__);
                            $koszyk_produkt->setIlosc($ilosc)
                                    ->setCenaZakupu($nowa_cena);

                            $this->_em->persist($koszyk_produkt);
                            $this->_em->flush();
                        }
                    }
                } else {
                    Atacama_Log::dodaj($this->_em, Atacama_Log::BLAD_SYSTEMU, print_r($this->_post, true));
                    Moyoki_Debug::debug('odmowa', __METHOD__);
                    exit;
                }
            }
            $this->__znakowania($koszyk_produkt, $ilosc);
        }


        if (NULL != $koszyk_finalny)
            return $koszyk_finalny;
    }

    private function __znakowania($koszyk_produkt, $ilosc) {
        /**
         * Aktualizujemy ilosci kolorow w znakowaniu
         */
        if (isset($this->_post['znak'])) {

            $znak = 0;
            foreach ($this->_post['znak'] as $znak_id => $val) {
                $znak++;
                //$val['kolory']
                //$val['ilosc']
                $kpz = $this->_em->getRepository('Entities\Koszyk_Produkt_Znakowanie')->getById($znak_id);
                if ($kpz instanceof Entities\Koszyk_Produkt_Znakowanie) {

                    /**
                     * Sprawdzamy czy koszyk_produkt_znakowanie nalezy do koszyk_produkt ktory iterujemy
                     * Moze powodowac duplikacje, bo robi sie petla w petli, ale powinno sie wyrobic
                     */
                    if ($koszyk_produkt->getId() == $kpz->getKoszykiDetaleId()) {

                        //$kpz = new Entities\Koszyk_Produkt_Znakowanie;

                        $kpz->setIloscKolorow($val['kolory'])
                                ->setIloscSztuk($val['ilosc']);

                        $znak_rodzaj = $this->_em->getRepository('Entities\Znakowanie_Rodzaj')->getByZnakowanieIdAndIlosc($kpz->getZnakowanieId(), $kpz->getIloscSztuk());

                        /**
                         * Pobieramy nowe ceny znakowan jednostkowych w zaleznosci od ilosci sztuk produktu
                         */
                        if ($znak_rodzaj instanceof Entities\Znakowanie_Rodzaj) {

                            $cena = 0;
                            for ($i = 1; $i <= $kpz->getIloscKolorow(); $i++) {
                                if ($i == 1) {
                                    $cena += $znak_rodzaj->getCena();
                                } else {
                                    $cena += $znak_rodzaj->getCenaInny();
                                }
                            }
                            $kpz->setCena($cena);
                        } else {
                            /**
                             * Ryczalt
                             */
                            $kwotaRyczaltu = $kpz->getZnakowanie()->getRyczalt() / $kpz->getIloscSztuk();
                            $przykladowyRodzaj = $kpz->getZnakowanie()->getZnakowaniaRodzaje()->first();
                            if ($przykladowyRodzaj instanceof Entities\Znakowanie_Rodzaj) {
                                if ($przykladowyRodzaj->getCenaInny() > 0) {
                                    $kpz->setCena($kwotaRyczaltu * $kpz->getIloscKolorow());
                                } else {
                                    $kpz->setCena($kwotaRyczaltu);
                                }
                            }
                            $kpz->setCena($kwotaRyczaltu);
                        }

                        //$kpz = new Entities\Koszyk_Produkt_Znakowanie;
                        
                        if ($kpz->getPrzygotowalniaMnozenie() == 0) {
                            $kpz->setPrzygotowalnia(($kpz->getPowtorzenie() == 1 ? $kpz->getZnakowanie()->getPowtorzenie() : $kpz->getZnakowanie()->getPrzygotowalnia()));
                        } else {
                            $kpz->setPrzygotowalnia(($kpz->getPowtorzenie() == 1 ? $kpz->getZnakowanie()->getPowtorzenie() : $kpz->getZnakowanie()->getPrzygotowalnia()) * $kpz->getIloscKolorow());
                            $kpz->setPrzygotowalniaMnozenie(1);
                        }


                        try {
                            $this->_em->getRepository('Entities\Koszyk_Produkt_Znakowanie')->zmien($kpz);
                        } catch (Doctrine\DBAL\DBALException $exc) {
                            Moyoki_Debug::debug($exc->getMessage(), __METHOD__);
                            exit;
                        } catch (Doctrine\ORM\ORMException $exc) {
                            Moyoki_Debug::debug($exc->getMessage(), __METHOD__);
                            exit;
                        } catch (Exception $exc) {
                            Moyoki_Debug::debug($exc->getMessage(), __METHOD__);
                            exit;
                        }
                    }
                }
                //Moyoki_Debug::debug(count($this->_post['znak']), $znak);
            }
        }
    }

}
