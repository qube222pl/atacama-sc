<?php

/**
 * Polecenie - logowanie czynnosci
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_Rejestracja {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        $config = Atacama_Config::getInstance();

        $firma = $this->_em->getRepository('Entities\Firma')->getByNip($this->_post['nip']);
        if ($firma instanceof Entities\Firma) {
            //Nic nie robimy
        } else {
            $phDomyslny = $this->_em->getRepository('Entities\Uzytkownik')->getById($config->domyslnyPh->id);
            if ($phDomyslny instanceof Entities\Uzytkownik) {
                $firma = new Entities\Firma();
                $firma->setNazwa($this->_post['nazwa'])
                        ->setNip(trim($this->_post['nip']))
                        ->setRegon($this->_post['regon'])
                        ->setUpust(0)
                        ->setPhId($phDomyslny->getId())
                        ->setPh($phDomyslny);

                $this->_em->persist($firma);
                $this->_em->flush();
                $result = array('nazwa' => $this->_post['nazwa'], 'nip' => $this->_post['nip']);
                Atacama_Powiadomienie::dodaj($this->_em, Atacama_Powiadomienie::NOWA_FIRMA_DODANA, json_encode($result));
            }
        }

        if (null !== $firma->getId() && $firma->getId() > 0) {

            $crypt = Moyoki_Crypt::getInstance();
            $uzytkownik = new Entities\Uzytkownik();

            $uzytkownik->setEmail($crypt->encrypt($this->_post['email']))
                    ->setHaslo(md5($this->_post['haslo1']))
                    ->setImie($this->_post['imie'])
                    ->setNazwisko($this->_post['nazwisko'])
                    ->setRola(Atacama_Acl::ROLA_KLIENT)
                    ->setFirmyId($firma->getId())
                    ->setFirmy($firma)
                    ->setActive(1);
            $this->_em->persist($uzytkownik);
            $this->_em->flush();

            $result = array('login' => $this->_post['email'], 'imie' => $this->_post['imie'], 'nazwisko' => $this->_post['nazwisko'], 'firma' => $firma->getNazwa());
            Atacama_Powiadomienie::dodaj($this->_em, Atacama_Powiadomienie::NOWY_UZYTKOWNIK_DODANY, json_encode($result));
        } else {
            throw new Exception('Problem rejestracji', 112);
        }
    }

}
