<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZdjecieUstawGlowne implements Moyoki_Command_Interface
{

	private $zdjecie_id;
	public $msg = null;
        public $em;

	/**
	 *
	 * @param array $_POST
	 */
	public function __construct(Doctrine\ORM\EntityManager $entityManager, $id)
	{
            $this->em = $entityManager;
		$this->zdjecie_id = (int) $id;
	}

	public function execute()
	{
		$glowne = $this->em->getRepository('Entities\Produkt_Zdjecie')->getById($this->zdjecie_id);

// Musi byc takie zdjecie, bo inaczej identyfikator nie bedzie sie zgadzal
		if ($glowne instanceof Entities\Produkt_Zdjecie) {
                    
			$zdjeciaProduktu = $this->em->getRepository('Entities\Produkt_Zdjecie')->findBy(array('produkty_id' => $glowne->getProduktyId()));
	//Zerujemy glowne dla produktu
			foreach ($zdjeciaProduktu as $zdjecie) {
				$update = $this->em->getRepository('Entities\Produkt_Zdjecie')->setZdjecieGlowne($zdjecie->getId());
			}

			//Ustawiamy to glowne
			$update = $this->em->getRepository('Entities\Produkt_Zdjecie')->setZdjecieGlowne($glowne->getId(), '1');
			return $update;
		}
	}

}