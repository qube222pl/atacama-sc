<?php

/**
 * Polecenie - import
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_Import {

    private $_formValues;
    private $_em;
    

    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $formValues) {
        $this->_formValues = $formValues;
        $this->_em = $entityManager;
    }

    function __destruct() {
        // skasowanie pliku przesłanego formularzem - jeśli istnieje
        if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_PLIK) {
            $config = Atacama_Config::getInstance();
            $plik = realpath($config->importTmp->path) . DIRECTORY_SEPARATOR . $this->_formValues['plik'];
            if(file_exists($plik)) {
                unlink($plik);
            }
        }
    }
   
    public function execute() {
        
        $id = (int)$this->_formValues['dystrybutor'];
        if(!($id > 0 )) {
            return 'Niepoprawny identyfikator dystrybutora.';
        }

        $importParams = $this->_em->getRepository('Entities\Import_Parametr')->filtrujPoDystrybutorze($id);
        if(count($importParams) == 0) {
            return 'Import danych od wybranego dystrybutora nie jest obsługiwany.';
        }

        if(!isset($importParams['plikLog'])) {
            return 'Błąd wewnętrzny: brak parametru "plik logu".';
        }
        
        if(!isset($importParams['rabat'])) {
            return 'Błąd wewnętrzny: brak parametru "rabat".';
        }
        
        $className = 'Application_Model_Import_' . $importParams['nazwaKlasy'];
        if(!class_exists($className)) {
            return 'Błąd wewnętrzny: brak klasy importu.';
        }

        try {
            Application_Model_Import_Log::setPrefix($importParams['plikLog']);
            $import = new $className($this->_em, $importParams, $this->_formValues);
            $res = $import->execute();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        
        return $res;
    }
    
    

}
