<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_UzytkownikPrzypomnijEmail implements Moyoki_Command_Interface
{

	private $_error = false;

	const BRAK_UZYTKOWNIKA = 1;

	private $_post = array();
        public $entityManager;

	/**
	 *
	 * @param array $_POST
	 */
	public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post)
	{
		$this->_post = $post;
                $this->entityManager = $entityManager;
	}

	public function execute()
	{
		$crypt = Moyoki_Crypt::getInstance();

		$uzytkownik = $this->entityManager->getRepository('Entities\Uzytkownik')->getByEmail($crypt->encrypt(trim($_POST['email'])));

		if ($uzytkownik instanceof Entities\Uzytkownik) {
			try {
				$mail = new Atacama_Mail($this->entityManager);
				$mail->przypomnijHaslo($uzytkownik);
			} catch (Exception $e) {
				Moyoki_Debug::debug($e->getMessage());
				exit;
			}
		} else {
			$this->_error = true;
			$this->_message = self::BRAK_UZYTKOWNIKA;
		}
	}

	public function isError()
	{
		return $this->_error;
	}

	public function getMessage()
	{
		return $this->_message;
	}

}