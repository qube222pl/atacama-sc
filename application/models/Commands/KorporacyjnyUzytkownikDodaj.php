<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyUzytkownikDodaj {

    public $firma;
    public $success = [];
    public $errors = [];
    private $_post;
    private $_em;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param array $post
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post, Entities\Firma $firma) {
        $this->_post = $post;
        $this->_em = $entityManager;
        $this->firma = $firma;
    }

    public function execute() {
        try {
            $uzytkownik = $user = $this->_getUzytkownik();
            if ($uzytkownik) {
                $user = new Entities\Korporacyjny_Uzytkownik();
                $user->setStatus(Atacama_Static::KORPORACYJNI_UZYTKOWNICY_STATUS_AKTYWNY)
                        ->setRola(Atacama_Static::KORPORACYJNI_UZYTKOWNICY_ROLA_KLIENT)
                        ->setFirmy($this->firma)
                        ->setFirmyId($this->firma->getId())
                        ->setUzytkownicy($uzytkownik)
                        ->setUzytkownicyId($uzytkownik->getId());
                
                $this->_em->persist($user);
                $this->_em->flush();
                $this->success[] = "Użytkownik dodany pomyślnie";
            } else {
                $this->errors[] = "Użytkownik nie istnieje";
            }
        } catch (DoctrineExtensions $e) {
            $this->errors[] = $e;
            return FALSE;
        } catch (Exception $e) {
            $this->errors[] = $e;
            return FALSE;
        }
    }

    private function _getUzytkownik() {
        $user = $this->_em->getRepository('Entities\Uzytkownik')->getById($this->_post['uzytkownik']);
        if ($user instanceof Entities\Uzytkownik) {
            return $user;
        }
        return FALSE;
    }

}
