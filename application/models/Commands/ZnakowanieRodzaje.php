<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_ZnakowanieRodzaje {

    private $_post;
    public $msg = null;
    private $_em;

    /**
     *
     * @param array $_POST
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, array $post) {
        $this->_post = $post;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Generowanie znakowania
         */
        $znakowanie = $this->_em->getRepository('Entities\Znakowanie')->getById($this->_post['id']);

        if ($znakowanie instanceof Entities\Znakowanie) {

            /**
             * Tworzna jest tablica, ktora pozwoli na obsluge pol formularza
             */
            $rodzajeTab = array();
            foreach ($this->_post as $k => $v) {
                if (stristr($k, '_')) {
                    list($detalID, $klucz) = explode('_', $k);
                    $rodzajeTab[$detalID][$klucz] = $v;
                }
            }
            /**
             * Teraz obslugujemy juz tablice i w zaleznosci czy dodac czy zmienic reagujemy
             */
            
            foreach ($rodzajeTab as $k => $v) {
                if (is_numeric($k)) {
                    /**
                     * Zmiana
                     */
                    $rodzaj = $this->_em->getRepository('Entities\Znakowanie_Rodzaj')->getById($k);
                    $rodzaj->setCena(\Atacama_Produkt_Kwota::zapis($v['cena']))
                            ->setCenaInny(\Atacama_Produkt_Kwota::zapis($v['cenainny']))
                            ->setMinimum($v['minimum'])
                            ->setMaximum($v['maximum']);

                    $this->_em->getRepository('Entities\Znakowanie_Rodzaj')->zmien($rodzaj);
                } else {
                    /**
                     * Dodanie nowej pozycji
                     */
                    
                    $rodzaj = new Entities\Znakowanie_Rodzaj();
                    $rodzaj->setCena(\Atacama_Produkt_Kwota::zapis($v['cena']))
                            ->setCenaInny(\Atacama_Produkt_Kwota::zapis($v['cenainny']))
                            ->setMinimum($v['minimum'])
                            ->setMaximum($v['maximum'])
                            ->setZnakowania($znakowanie)
                            ->setZnakowaniaId($znakowanie->getId());

                    $this->_em->persist($rodzaj);
                    $this->_em->flush();
                }
            }
        } else {
            return false;
        }
    }

}
