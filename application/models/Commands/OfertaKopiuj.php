<?php

/**
 *
 * Kopiowanie (klonowanie) oferty przez PH
 * @author Studio Moyoki
 */
class Application_Model_Commands_OfertaKopiuj {

    private $_oferta = null;
    public $msg = null;
    private $_em;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param Entities\Koszyk $oferta
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, Entities\Koszyk $oferta) {
        $this->_oferta = $oferta;
        $this->_em = $entityManager;
    }

    public function execute() {

        /**
         * Zmiana Koncepcji
          $nowaOferta = clone $this->_oferta;

          $nowaOferta->setStatus(Atacama_Acl::STATUS_OFERTA_TWORZENIE)
          ->setOfertaPlik(NULL)
          ->setIdZamowienia(NULL)
          ->setNrZamowienia(NULL);

          $this->_em->persist($nowaOferta);
          $this->_em->flush();

         */
        $user = Zend_Auth::getInstance()->getIdentity();
        $nowaOferta = $this->_em->getRepository('Entities\Koszyk')->getKoszykUzytkownika($user['id']);

        if (!$nowaOferta instanceof Entities\Koszyk) {
            //Nie ma koszyka - trzeba go stworzyc


            $uzytkownikObj = $this->_em->getRepository('Entities\Uzytkownik')->getById($user['id']);
            if ($uzytkownikObj instanceof Entities\Uzytkownik) {
                $nowaOferta = new Entities\Koszyk();
                $nowaOferta->setUzytkownicy($uzytkownikObj)
                        ->setUzytkownicyId($uzytkownikObj->getId())
                        ->setFirmyId($uzytkownikObj->getFirmyId())
                        ->setFirmy($uzytkownikObj->getFirmy())
                        ->setStatus('0'); // koszyk

                $this->_em->persist($nowaOferta);
                $this->_em->flush();
            }
        }


        $this->_produkty($nowaOferta);

        return $nowaOferta;
    }

    private function _produkty($nowaOferta) {

        foreach ($this->_oferta->getKoszykiProdukty() as $kp) {
            $nowyKp = clone $kp;
            $nowyKp->setKoszyki($nowaOferta)
                    ->setKoszykiId($nowaOferta->getId());

            $this->_em->persist($nowyKp);

            $this->_znakowania($kp, $nowyKp);
        }
        $this->_em->flush();
    }

    private function _znakowania($koszyk_produkt, $nowy_koszyk_produkt) {

        foreach ($koszyk_produkt->getKoszykiProduktyZnakowania() as $kpz) {
            //$nowyKpz = new Entities\Koszyk_Produkt_Znakowanie;
            //$kpz = new Entities\Koszyk_Produkt_Znakowanie;

            $nowyKpz = clone $kpz;
            $nowyKpz->setKoszykiDetaleId($nowy_koszyk_produkt->getId())
                    ->setKoszykiProdukty($nowy_koszyk_produkt);

            $this->_em->persist($nowyKpz);
        }
        $this->_em->flush();
    }

}
