<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Commands_KorporacyjnyKoszykPonadStock {

    public $errors = [];
    public $zamowienie_id = null;
    private $_em;
    private $_uzytkownik;
    private $_produkty;
    private $_kosztZamowienia = 0;
    private $_dataDodruku;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param Entities\Uzytkownik $uzytkownik
     * @param array $produkty
     */
    public function __construct(Doctrine\ORM\EntityManager $entityManager, Entities\Uzytkownik $uzytkownik, $produkty) {
        $this->_em = $entityManager;
        $this->_uzytkownik = $uzytkownik;
        $this->_produkty = $produkty;
    }

    public function execute() {
        $this->_getKosztZamowieniaCzasDodruku();
        $this->_tworzZamowienie();
        return $this->zamowienie_id;
    }

    private function _tworzZamowienie() {
        $zamowienie = new Entities\Korporacyjny_Koszyk();
        $zamowienie->setStatus(Atacama_Static::KORPORACYJNI_ZAMOWIENIE_NOWE)
                ->setUzytkownicy($this->_uzytkownik)
                ->setUzytkownicyId($this->_uzytkownik->getId())
                ->setNetto($this->_kosztZamowienia);


        $this->_em->persist($zamowienie);

        $this->_dodajProduktyDoZamowienia($zamowienie);

        $this->zamowienie_id = $zamowienie->getId();
    }

    private function _dodajProduktyDoZamowienia(Entities\Korporacyjny_Koszyk $zamowienie) {
        foreach ($this->_produkty as $prod) {
            $produkt = new Entities\Korporacyjny_Koszyk_Produkt();
            $produkt->setIlosc($prod['ilosc'])
                    ->setKorporacyjniKoszyki($zamowienie)
                    ->setKorporacyjniKoszykiId($zamowienie->getId())
                    ->setKorporacyjniProdukty($prod['produkt'])
                    ->setKorporacyjniProduktyId($prod['produkt']->getId())
                    ->setCenaZakupu($produkt->getKorporacyjniProdukty()->getCena())
                    ->setDataDodruku(DateTime::createFromFormat('Y-m-j', $this->_dataDodruku));

            $this->_em->persist($produkt);
        }
    }

    private function _getKosztZamowieniaCzasDodruku() {
        $koszt = 0;
        $iloscDni = 0;
        foreach ($this->_produkty as $produkt) {
            if ($produkt['produkt'] instanceof Entities\Korporacyjny_Produkt) {

                $koszt += $produkt['produkt']->getCena() * $produkt['ilosc'];

                /**
                 * Obliczanie najdalszej daty dodruku produktow
                 */
                if ($iloscDni < $produkt['produkt']->getCzasDodruku()) {
                    $iloscDni = $produkt['produkt']->getCzasDodruku();
                }
            }
        }
        $this->_kosztZamowienia = $koszt;
        $this->_dataDodruku = Atacama_Data::getDataZaDniRobocze($iloscDni);
    }

}
