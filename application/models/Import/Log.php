<?php

class Application_Model_Import_Log
{

    const LOG_ERROR = 1;
    const LOG_OK = 2;
    const LOG_INFO = 3;
    
    private static $_instance = null;
    private static $_filename = null;
    private static $_filePrefix = 'import_';
    private static $_type = array(
        self::LOG_ERROR => 'BŁĄD: ',
        self::LOG_OK    => 'OK:   ',
        self::LOG_INFO  => 'INFO: '
    );
    
	public function __construct($init = null)
	{
        
        $config = Atacama_Config::getInstance();
        $path = realpath($config->importLog->path);
        $file = $path . '/' . self::$_filePrefix . date('Y-m-d_H-i-s') . '.txt';
        self::$_filename = $file;
        
	}

    static public function setPrefix($prefix)
    {
        self::$_filePrefix = $prefix . '_';
    }
    
    static public function getFileName()
    {
        return self::$_filename;
    }
    
    static public function reset()
    {
        self::$_instance = null;
    }
    
    static public function dodaj($cat, $info)
    {
        if( !isset(self::$_instance) ) {
            self::$_instance = new self();
        }

        list($msec, $sec) = explode(' ', microtime());
        $date = date('H:i:s', $sec) . ',' . substr($msec, 2, 4);
        $header = $date . ' ' . self::$_type[$cat];
        file_put_contents(self::$_filename, $header . ' ' . $info . "\n", FILE_APPEND);
        
    }
    
    static public function dodajMapowanie($info, $what) {
        // $what może być = 'colors', 'categories', 'brands' albo 'markgroups'
        self::dodaj(self::LOG_INFO, 'Mapowanie ' . $what . ' ' . print_r($info, true));
    }
    
    static public function dodajIlosci($info, $what) {
        // $what może być = 'inserts' lub 'updates'
        self::dodaj(self::LOG_INFO, $what . ' ' . print_r($info, true));
    }
    
    static public function dodajRabaty($info) {
        self::dodaj(self::LOG_INFO, 'Obliczone mnożniki wg rabatów' . print_r($info, true));
    }
    
    static public function dodajNiewidoczne($info) {
        self::dodaj(self::LOG_INFO, 'Do ukrycia: ' . print_r($info, true));
    }
    
}

?>
