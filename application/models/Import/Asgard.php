<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Import_Asgard extends Application_Model_Import_Abstract {

    
    // potrzebne tylko do testów
    // private $rozmiary = Atacama_Produkt_Rozmiar::tablica();
    
    static private $opcjeImportu = array(
        Application_Model_Import_Abstract::ZAKRES_OFERTA => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_STANY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
    ); 

    static public function pobierzOpcje()
    {
        return self::$opcjeImportu;
    }

    protected function init() {
    }
        
    protected function _pobierzDane() {
        
        if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_OFERTA ) {
            return $this->_pobierzXml('produkt');
        } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_STANY ) {
            return $this->_pobierzXml('product');
        } else {
            return false;
        }
        
    }

    protected function _sprawdzDane() {
        
        //return "Brakuje kolorów";
        return true;
        
    }
        
    protected function _pobierzMapowanie() {

        $out = chr(239) . chr(187) . chr(191) . "indeks;code_short;color_id;color;nazwa\n";
        $out2 = chr(239) . chr(187) . chr(191) . "ZNAKOWANIE;1;2;3;4;5\n";
        
        // pobieramy niepowtarzalne wartości z wszystkich produktów
        // dodatkowo liczymy ich ilość
        foreach ($this->_data as $product) {
            
            // brak numeru -> następny produkt
            if(!isset($product['indeks'])) {
                continue;
            }
            
            $codeFull = $product['indeks'];
            $prodInfo = $this->_getColor($codeFull);
            
            // kolory
            if(isset($product['kolor'])) {
                $this->_count($this->_sourceMappings['colors'], $product['kolor']);
            } else {
                $this->_sourceMappings['colors'][-1]['count']++;
            }
            
            // kategorie
            if(isset($product['kategoria'])) {
                $cat = $product['kategoria'];
                if(isset($product['podkategoria'])) {
                    $cat .= ' - ' . $product['podkategoria'];
                }
                $this->_count($this->_sourceMappings['categories'], trim($cat));
            } else {
                $this->_sourceMappings['categories'][-1]['count']++;
            }
            
            // znakowania
            $found = false;
            if(isset($product['znakowanie_produktu'])) {
                $out2 .= $product['znakowanie_produktu'] . ';';
                $markgroups = $this->_getMarkgroups($product['znakowanie_produktu']);
                if($markgroups !== null) {
                    foreach($markgroups as $markgroup) {
                        $this->_count($this->_sourceMappings['markgroups'], $markgroup);
                        $found = true;
                        $out2 .= $markgroup . ';';
                    }
                }
                $out2 .= "\n";
            } else {
                $out2 .= 'brak znakowania';
            }

            if(!$found) {
                $this->_sourceMappings['markgroups'][-1]['count']++;
            }

            $out .= $codeFull . ";" . $prodInfo['code'] . ";" . $prodInfo['color'] . ";" . (isset($product['kolor']) ? $product['kolor'] : '') . ";" . $product['nazwa'] . "\n";
            continue;
            
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
//        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_dane.csv', $out);
//        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_znakowania.csv', $out2);
        //return false;

        return true;
        
    }
    
    protected function _importProdukty() {

        // licznik produktów w danych wejściowych <product></product>
        $prodCnt = 0;
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Obliczony mnożnik wg rabatu: ' . $this->_rabat);

        // pobieramy tylko widoczne detale
        $this->_detaleNiewidoczne = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktow($this->_dystrybutorId, 0, null, true);
//        Application_Model_Import_Log::dodajRabaty($this->_rabaty);

        $producentId = $this->_importParams['idProducenta'];
        $urlPhoto = $this->_importParams['urlZdjecia'];
        $producent = $this->_em->getRepository('Entities\Producent')->getById($producentId);
        if (!($producent instanceof Entities\Producent)) {
            throw new Exception("Niepoprawny parametr importu: 'idProducenta'!");
        }
        
        
        // główna pętla dla każdego produktu wejściowego
        foreach ($this->_data as $product) {
         
            $prodCnt++;
            $dodajKategorieZnakowania = true;
            
            if(!isset($product['indeks'])) {
                // brak indeksu == pełnego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez numeru');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - brak numeru.");
                continue;
            }
                
            if(!isset($product['nazwa'])) {
                // brak nazwy
                $this->_dodajDoRaportu('Ilość produktów/detali bez nazwy');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (NO = " . $product['indeks'] . ") - brak nazwy.");
                continue;
            }

            if(!isset($product['kolor'])) {
                $this->_dodajDoRaportu('Ilość produktów/detali z brakiem koloru');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (NO = " . $product['indeks'] . ") - brak koloru.");
                $productColor = null;
            } else {
                $productColor = $this->_importMappings['colors'][($product['kolor'])];
            }

            
            $codeFull = $product['indeks'];
            $prodInfo = $this->_getColor($codeFull);
            $codeShort = $prodInfo['code'];
            
            // cena regularna
            if(isset($product['cena_netto_katalogowa'])) {
                $cenaSrp = (float)str_replace(',', '.', $product['cena_netto_katalogowa']);
                $cena = (float)$cenaSrp * $this->_rabat;
            } else {
                $cena = 0.0;
                $cenaSrp = 0.0;
            }

            $cat = '';
            if(isset($product['kategoria'])) {
                $cat = $product['kategoria'];
                if(isset($product['podkategoria'])) {
                    $cat .= ' - ' . $product['podkategoria'];
                }
                $cat = trim($cat);
                $kategoriaGlowna = $this->_importMappings['categories'][$cat];
            } else {
                $kategoriaGlowna = null;
            }
            
            // wymiary, pojemności i opisy
            $rozmiarOpis = null;
            $pojemnoscOpis = null;
            $rozmiar = null; // brak aktualizacji, dla dodawania 'one size'
            
            // opis produktu do i18n
            $opis = (isset($product['opis_produktu']) ? '<p>' . $product['opis_produktu'] . '</p>' : '');
            $opis .= (isset($product['wymiary_produktu']) ? '<p>Wymiary: ' . $product['wymiary_produktu'] . '</p>' : '');
//            $opis .= (($pojemnoscOpis != null) ? '<p>Pojemność: ' . $pojemnoscOpis . '</p>' : '');
            $opis .= (isset($product['waga_jednostkowa_netto_w_kg']) ? '<p>Waga: ' . $product['waga_jednostkowa_netto_w_kg'] . ' kg</p>': '');

            $nazwa = strtr($product['nazwa'], array('“' => "'", '”' => "'", '"' => "'"));

            // DODAWANIE PRODUKTU
            if(!isset($this->_produktyIds[$codeShort])) {
                // nie ma w systemie code_short więc
                // dodajemy pozycję jako produkt

                // zapisujemy główny produkt
                $prodData = array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'producenci_id' => $producentId,
                    'nr_katalogowy' => $codeShort,
                    'kategoria_glowna' => $kategoriaGlowna,
                    'widoczny' => 1,
                    'nowosc' => ((isset($product['status']) && $product['status'] == 'Nowość') ? 1 : 0),
                    'promocja' => 0
                );
                
                try {
                    $res = 0;
                    $res = $this->_conn->insert('produkty', $prodData);
                } catch (Exception $e) {
                    if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie istniejącego już produktu nr $prodCnt");
                    } else {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania produktu nr $prodCnt");
                    }
                    continue;
                }

                if($res != 1) {
                    continue;
                }

                // id dodanego produktu
                $productId = (int)$this->_conn->lastInsertId();

                // zapisujemy i18n
                if($productId > 0) {
                    $this->_produktyIds[$codeShort] = $productId;
                    $this->_produktyNewIds[$codeShort] = $productId;
                    
                    $prodData = array(
                        'produkty_id' => $productId,
                        'jezyk' => 'pl',
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    try {
                        $res = $this->_conn->insert('produkty_i18n', $prodData);
                    } catch(Exception $e) {
                        if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie opisu istniejącego już produktu nr $prodCnt");
                        } else {
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania opisu produktu nr $prodCnt");
                        }
                    }
                } else {
                    continue;
                }

                $this->_rowCount['inserts']['products']++;
                
            } else {
                // UPDATE PRODUKTU

                $productId = $this->_produktyIds[$codeShort];
                
                // jeśli nie ma code_short w produktyNewIds
                // to oznacza, że produkt został dodany podczas poprzedniego
                // importu i trzeba produkt zaktualizować
                if(!isset($this->_produktyNewIds[$codeShort])) {
                
                    // aktualizujemy główny produkt
                    $prodData = array(
                    'producenci_id' => $producentId,
                    'nowosc' => ((isset($product['status']) && $product['status'] == 'Nowość') ? 1 : 0),
//                        'promocja' => 0
                    );

                    try {
                        $res = 0;
                        $res = $this->_conn->update('produkty', $prodData, array('id' => $productId));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania produktu nr $prodCnt, ID = $productId");
                    }
                    
                    // aktualizujemy i18n
                    $prodData = array(
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    
                    try {
                        $res2 = 0;
//                        $res2 = $this->_conn->update('produkty_i18n', $prodData, array('produkty_id' => $productId, 'jezyk' => 'pl'));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania opisu produktu (i18n) nr $prodCnt, ID = $productId");
                    }

                    if( $res == 1 || $res2 == 1) {
                        $this->_rowCount['updates']['products']++;
                    }

                } else {
                    $dodajKategorieZnakowania = false;
                }

                $this->_produktyNewIds[$codeShort] = $productId;
                
            }

            if($dodajKategorieZnakowania) {
                
                // zapisujemy kategorie
                if(!isset($this->_kategorieIds[$productId])) {
                    // jeśli przed importem produkt nie miał kategorii to dodajemy
                    if($cat != '') {
                        $this->_zapiszKategorie($cat, $productId);
                    }
                }

                // zapisujemy znakowania
                if(!isset($this->_znakowaniaIds[$productId])) {
                    // jeśli przed importem produkt nie miał znakowania to dodajemy
                    if(isset($product['znakowanie_produktu'])) {
                        $markgroups = $this->_getMarkgroups($product['znakowanie_produktu']);
                        if($markgroups !== null) {
                            foreach($markgroups as $markgroup) {
                                $this->_zapiszZnakowanie($markgroup, $productId);
                            }
                        }
                    }                    
                }
                
            }
            
            // dodajemy/aktualizujemy detal
            $this->_zapiszDetal(
                    $productId,
                    $codeFull,
                    $codeFull,
                    $cena,
                    null,
                    null,
                    $productColor,
                    $rozmiar,
                    ((isset($product['in_stock']))? $product['in_stock'] : 0),
                    0,
                    null,
                    null,
                    $cenaSrp
            );
            
            
            if(isset($this->_detaleNiewidoczne[$codeFull])) {
                unset($this->_detaleNiewidoczne[$codeFull]);
            }

            // dodajemy zdjęcia (zdjęcia nie są aktualizowane)
            if(isset($product['obraz'])) {
                $this->_zapiszZdjecie($productId, $urlPhoto . $product['obraz'], $productColor);
            }
                
        } // nstępny produkt/detal wejściowy
        
    }
    
    protected function _importStany() {
  
        $out = "index;quantity\n";
        $rows = 0;

        foreach ($this->_data as $product) {
            if(isset($product['index']) && isset($product['stock_pcs'])) {
                $codeFull = $product['index'];
                if(isset($this->_detaleIds[$codeFull])) {
                    $res = 0;
                    $res = $this->_conn->update('produkty_detale', array(
                        'stan_magazynowy_1' => (int)$product['stock_pcs'],
//                        'stan_magazynowy_2' => 0
                    ), array(
                        'id' => $this->_detaleIds[$codeFull]
                    ));
                    $out .= ($product['index'] . ';' . $product['stock_pcs'] . "\n");
                    if($res == 1) {
                        $rows++;
                    }
                }
            }
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_stocks.txt', $out);
        return $rows;
        
    }
    
    protected function _importTlumaczenia() {
    }

    
    private function _getColor($index) {
        
        $color = '';
        $kreska = strpos($index, '-');

        if($kreska !== false) {
            $code = substr($index, 0, $kreska);
            $color = substr($index, $kreska + 1);
        } else {
            $code = $index;
        }

        return(array(
            'code' => $code,
            'color' => $color,
        ));
        
    }
    
    private function _getMarkgroups($markgroups) {
        
        if(is_string($markgroups)) {
            $markgroups = preg_replace('/\((.*?)\)/', '', $markgroups);
            $res = explode(',', $markgroups);
            if( $res !== false) {
                foreach ($res as $markgroup) {
                    $out[] = trim($markgroup);
                }
                return($out);
            }
        }
        
        return(null);
        
    }
    

            
}
