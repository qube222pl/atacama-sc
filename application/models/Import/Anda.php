<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Import_Anda extends Application_Model_Import_Abstract {

    
    // potrzebne tylko do testów
    // private $rozmiary = Atacama_Produkt_Rozmiar::tablica();
    
    static private $opcjeImportu = array(
        Application_Model_Import_Abstract::ZAKRES_OFERTA => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_STANY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
    ); 

    static public function pobierzOpcje()
    {
        return self::$opcjeImportu;
    }

    protected function init() {
    }
        
    protected function _pobierzDane() {
        
        return $this->_pobierzXml('product', 'attribute');
        
    }

    protected function _sprawdzDane() {
        
        return true;
        
    }
        
    protected function _pobierzMapowanie() {

$out = "id;code;size;color\n";
        
        
        //to musi być gdyż _rozmiary jest używane przez _getColorSize()
        // a _rozmiary jest ładowane po _pobierzMapowanie()
        $this->_rozmiary = Atacama_Produkt_Rozmiar::tablicaImport();
        
        // pobieramy niepowtarzalne wartości z wszystkich produktów
        // dodatkowo liczymy ich ilość
        foreach ($this->_data as $product) {
            
            // brak numeru -> następny produkt
            if(!isset($product['product']['no'])) {
                continue;
            }
            
            $codeFull = $product['product']['no'];
            $prodInfo = $this->_getColorSize($codeFull);

            // $prodInfo === null oznacza, że są jakieś dane dodatkowe ale nie zostały rozpoznane
            if($prodInfo !== null) {
                $kolor = $prodInfo['color'];
$out .= ($codeFull . ';' . $prodInfo['code'] . ';' . $prodInfo['size'] . ';' . $prodInfo['color'] . "\n" );
            } else {
$out .= ($codeFull . ';;;' . "\n" );
                continue;
            }
            
            // kolory
            if($kolor != '') {
                $this->_count($this->_sourceMappings['colors'], $kolor);
            } else {
                $this->_sourceMappings['colors'][-1]['count']++;
            }
            
            // kategorie
            if(isset($product['folders']['folder'])) {
                foreach ($product['folders']['folder'] as $category) {
                    $this->_count($this->_sourceMappings['categories'], strip_tags($category['category'] . ' - ' . $category['subcategory']));
                }
            } else {
                $this->_sourceMappings['categories'][-1]['count']++;
            }
            
            // znakowania
            $found = false;
            $markgroups = $this->_getPropertyValue($product, 'METODA DRUKU');
            if($markgroups !== null) {
                $markgroups = $this->_getMarkgroups($markgroups);
                foreach($markgroups as $markgroup) {
                    $this->_count($this->_sourceMappings['markgroups'], $markgroup);
                    $found = true;
                }
            }

            if(!$found) {
                $this->_sourceMappings['markgroups'][-1]['count']++;
            }
            
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_dane.csv', $out);
//        return false;

        return true;
        
    }
    
    protected function _importProdukty() {

        // licznik produktów w danych wejściowych <product></product>
        $prodCnt = 0;
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Obliczony mnożnik wg rabatu: ' . $this->_rabat);

        // pobieramy tylko widoczne detale
        $this->_detaleNiewidoczne = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktow($this->_dystrybutorId, 0, null, true);
//        Application_Model_Import_Log::dodajRabaty($this->_rabaty);

        $producentId = $this->_importParams['idProducenta'];
        $producent = $this->_em->getRepository('Entities\Producent')->getById($producentId);
        if (!($producent instanceof Entities\Producent)) {
            throw new Exception("Niepoprawny parametr importu: 'idProducenta'!");
        }
        
        
        // główna pętla dla każdego produktu wejściowego
        foreach ($this->_data as $product) {
         
            $prodCnt++;
            $dodajKategorieZnakowania = true;
            
            if(!isset($product['product']['no'])) {
                // brak pełnego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez numeru');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - brak numeru.");
                continue;
            }
                
            if(!isset($product['product']['name'])) {
                // brak nazwy
                $this->_dodajDoRaportu('Ilość produktów/detali bez nazwy');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (NO = " . $product['product']['no'] . ") - brak nazwy.");
                continue;
            }

            $codeFull = $product['product']['no'];
            $prodInfo = $this->_getColorSize($codeFull);

            // $info === null oznacza, że są jakieś dane dodatkowe ale nie zostały rozpoznane
            if($prodInfo === null) {
                $this->_dodajDoRaportu('Ilość produktów/detali z nierozpoznanym kolorem/rozmiarem');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (NO = " . $product['product']['no'] . ") - nierozpoznany kolor/rozmiar.");
                continue;
            }
            
            $codeShort = $prodInfo['code'];
            $productColor = (($prodInfo['color'] != '') ? $this->_importMappings['colors'][($prodInfo['color'])] : null);
            
            // cena regularna
            if(isset($product['product']['price'])) {
                $cenaSrp = (float)$product['product']['price'];
                $cena = (float)$cenaSrp * $this->_rabat;
            } else {
                $cenaSrp = 0.0;
                $cena = 0.0;
            }

            // kategoria główna
            if(isset($product['folders']['folder'])) {
                $kategoria = strip_tags($product['folders']['folder'][0]['category'] . ' - ' . $product['folders']['folder'][0]['subcategory']);
                $kategoriaGlowna = $this->_importMappings['categories'][$kategoria];
            } else {
                $kategoriaGlowna = null;
            }                

            // wymiary, pojemności i opisy
            $rozmiarOpis = null;
            $pojemnoscOpis = null;
            
            if($prodInfo['size'] != '' && isset($this->_rozmiary[$prodInfo['size']])) {
                $rozmiar = $this->_rozmiary[$prodInfo['size']];
            } else {
                $rozmiar = null; // brak aktualizacji, dla dodawania 'one size'
            }
            
            // opis produktu do i18n
            $opis = (isset($product['description']['tagValue']) ? '<p>' . $product['description']['tagValue'] . '</p>' : '');
//            $opis .= (($rozmiarOpis != null) ? '<p>Wymiary: ' . $rozmiarOpis . '</p>' : '');
//            $opis .= (($pojemnoscOpis != null) ? '<p>Pojemność: ' . $pojemnoscOpis . '</p>' : '');
//            $opis .= (isset($product['attributes']['weight']) ? '<p>Waga: ' . $product['attributes']['weight'] . '</p>': '');

            $nazwa = strtr($product['product']['name'], array('“' => "'", '”' => "'", '"' => "'"));

            // DODAWANIE PRODUKTU
            if(!isset($this->_produktyIds[$codeShort])) {
                // nie ma w systemie code_short więc
                // dodajemy pozycję jako produkt

                // zapisujemy główny produkt
                $prodData = array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'producenci_id' => $producentId,
                    'nr_katalogowy' => $codeShort,
                    'kategoria_glowna' => $kategoriaGlowna,
                    'widoczny' => 1,
                    'nowosc' => 0,
                    'promocja' => 0
                );
                
                try {
                    $res = 0;
                    $res = $this->_conn->insert('produkty', $prodData);
                } catch (Exception $e) {
                    if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie istniejącego już produktu nr $prodCnt");
                    } else {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania produktu nr $prodCnt");
                    }
                    continue;
                }

                if($res != 1) {
                    continue;
                }

                // id dodanego produktu
                $productId = (int)$this->_conn->lastInsertId();

                // zapisujemy i18n
                if($productId > 0) {
                    $this->_produktyIds[$codeShort] = $productId;
                    $this->_produktyNewIds[$codeShort] = $productId;
                    
                    $prodData = array(
                        'produkty_id' => $productId,
                        'jezyk' => 'pl',
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    try {
                        $res = $this->_conn->insert('produkty_i18n', $prodData);
                    } catch(Exception $e) {
                        if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie opisu istniejącego już produktu nr $prodCnt");
                        } else {
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania opisu produktu nr $prodCnt");
                        }
                    }
                } else {
                    continue;
                }

                $this->_rowCount['inserts']['products']++;
                
            } else {
                // UPDATE PRODUKTU

                $productId = $this->_produktyIds[$codeShort];
                
                // jeśli nie ma code_short w produktyNewIds
                // to oznacza, że produkt został dodany podczas poprzedniego
                // importu i trzeba produkt zaktualizować
                if(!isset($this->_produktyNewIds[$codeShort])) {
                
                    // aktualizujemy główny produkt
                    $prodData = array(
                        'producenci_id' => $producentId,
//                        'nowosc' => 0,
//                        'promocja' => 0
                    );

                    try {
                        $res = 0;
//                        $res = $this->_conn->update('produkty', $prodData, array('id' => $productId));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania produktu nr $prodCnt, ID = $productId");
                    }
                    
                    // aktualizujemy i18n
                    $prodData = array(
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    
                    try {
                        $res2 = 0;
//                        $res2 = $this->_conn->update('produkty_i18n', $prodData, array('produkty_id' => $productId, 'jezyk' => 'pl'));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania opisu produktu (i18n) nr $prodCnt, ID = $productId");
                    }

                    if( $res == 1 || $res2 == 1) {
                        $this->_rowCount['updates']['products']++;
                    }

                } else {
                    $dodajKategorieZnakowania = false;
                }

                $this->_produktyNewIds[$codeShort] = $productId;
                
            }

            if($dodajKategorieZnakowania) {
                
                // zapisujemy kategorie
                if(!isset($this->_kategorieIds[$productId])) {
                    // jeśli przed importem produkt nie miał kategorii to dodajemy
                    if(isset($product['folders']['folder'])) {
                        foreach ($product['folders']['folder'] as $category) {
                            $kategoria = strip_tags($category['category'] . ' - ' . $category['subcategory']);
                            $this->_zapiszKategorie($kategoria, $productId);
                        }
                    }
                }

                // zapisujemy znakowania
                if(!isset($this->_znakowaniaIds[$productId])) {
                    // jeśli przed importem produkt nie miał znakowania to dodajemy
                    $markgroups = $this->_getPropertyValue($product, 'METODA DRUKU');
                    if($markgroups !== null) {
                        $markgroups = $this->_getMarkgroups($markgroups);
                        foreach($markgroups as $markgroup) {
                            $this->_zapiszZnakowanie($markgroup, $productId);
                        }
                    }
                }
                
            }
            
            // dodajemy/aktualizujemy detal
            $this->_zapiszDetal(
                    $productId,
                    $codeFull,
                    $codeFull,
                    $cena,
                    null,
                    null,
                    $productColor,
                    $rozmiar,
                    $this->_getStockValue($product, 'navi_central'),
                    $this->_getStockValue($product, 'navi_arrive'),
                    null,
                    null,
                    $cenaSrp
            );
            
            
            if(isset($this->_detaleNiewidoczne[$codeFull])) {
                unset($this->_detaleNiewidoczne[$codeFull]);
            }

            // dodajemy zdjęcia (zdjęcia nie są aktualizowane)
            if(isset($product['images']['image'])) {
                foreach($product['images']['image'] as $image) {
                    $this->_zapiszZdjecie($productId, $image['src'], $productColor);
                }
            }
                
        } // nstępny produkt/detal wejściowy
        
    }
    
    protected function _importStany() {
        
//        $out = "id;code_short;code_full;quantity\n";
        $rows = 0;
        
        foreach ($this->_data as $product) {
            if(isset($product['product']['no'])) {
                $codeFull = $product['product']['no'];
                if(isset($this->_detaleIds[$codeFull])) {
                    $res = 0;
                    $res = $this->_conn->update('produkty_detale', array(
                        'stan_magazynowy_1' => $this->_getStockValue($product, 'navi_central'),
                        'stan_magazynowy_2' => $this->_getStockValue($product, 'navi_arrive')
                    ), array(
                        'id' => $this->_detaleIds[$codeFull]
                    ));
//                    $out .= ($product['stock']['id'] . ';' . $product['stock']['code_short'] . ';' . $product['stock']['code_full'] . ';' . $product['stock']['quantity_24h'] . "\n");
                    if($res == 1) {
                        $rows++;
                    }
                }
            }
        }
//
//        $config = Atacama_Config::getInstance();
//        $path = realpath($config->importTmp->path);
//        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_stocks.txt', $out);
        return $rows;
        
    }
    
    protected function _importTlumaczenia() {
    }

    
    private function _getColorSize($sku) {
        
        $kolor = '';
        $rozmiar = '';
        $ok = '';

        $kreska = strpos($sku, '-');

        if($kreska !== false) {

            $product = substr($sku, 0, $kreska);
            $reszta = substr($sku, $kreska + 1);

            if(strlen($reszta) > 2) {
                $odcien = strtoupper($reszta[2]);
                if($odcien == 'A' || $odcien == 'V' || $odcien == 'T' || $odcien == 'F') {
                    $kolor = substr($reszta, 0, 2) . $odcien;
                    $rozmiar = substr($reszta, 3);
                } else {
                    $kolor = substr($reszta, 0, 2);
                    $rozmiar = substr($reszta, 2);
                }

                if($rozmiar[0] == '_' || $rozmiar[0] == '-') {
                    $rozmiar = substr($rozmiar, 1);
                }

            } else {
                $kolor = $reszta;
            }

        } else if( ($kreska = strpos($sku, '_')) !== false ) {

            $product = substr($sku, 0, $kreska);
            $rozmiar = substr($sku, $kreska + 1);

        } else {
            $product = $sku;
        }

        if($rozmiar != '' ) {
            if( isset($this->_rozmiary[$rozmiar]) ) {
//                $ok = $this->rozmiary[$this->_rozmiary[$rozmiar]];
                $ok = 'ok';
            } else {

                if(preg_match('/^([0-9]){2}[AVavT]?$/', $rozmiar)) {
                    $kolor .= ('_' . strtoupper($rozmiar));
                    $rozmiar = '';
                } else {
                    $ok = 'brak';
                    return null;
                }
            }
        } 
        
        return(array(
            'code' => $product,
            'size' => $rozmiar,
            'color' => $kolor,
//            'ok' => $ok
        ));
        
    }
    
    private function _getMarkgroups($markgroups) {
        
        if(is_string($markgroups)) {
            $res = preg_match_all("/(\w+) \(.+\)/U", $markgroups, $out);
            if( $res !== false && $res > 0) {
                return($out[1]);
            }
        }
        
        return(array());
        
    }
    
    private function _getPropertyValue(&$product, $propertyName) {

        if(isset($product['properties']['property'])) {
            foreach ($product['properties']['property'] as $property) {
                if($property['name'] == $propertyName) {
                    return $property['value'];
                }
            }
        } 
        
        return null;
        
    }
            
    private function _getStockValue(&$product, $propertyName) {

        if(isset($product['stocks']['stock'])) {
            foreach ($product['stocks']['stock'] as $stock) {
                if($stock['name'] == $propertyName) {
                    $val = trim($stock['value']);
                    return (strlen($val) ? $val : null);
                }
            }
        } 
        
        return null;
        
    }
            
}
