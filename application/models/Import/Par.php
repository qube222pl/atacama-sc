<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Import_Par extends Application_Model_Import_Abstract {

    static private $opcjeImportu = array(
        Application_Model_Import_Abstract::ZAKRES_OFERTA => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_STANY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
    ); 

    static public function pobierzOpcje()
    {
        return self::$opcjeImportu;
    }

    protected function _pobierzDane() {

        if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_OFERTA ) {
        
            if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_INTERNET ) {
                //return $this->_pobierzXmlAuth('produkt', 'urlLogin', 'urlHaslo', 'urlProdukty');
                return $this->_pobierzXmlAuth('product', 'urlLogin', 'urlHaslo');
            } else if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_PLIK ) {
                return $this->_pobierzXml('product');
            } else {
                return false;
            }
            
        } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_STANY ) {
            
            if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_INTERNET ) {
                return $this->_pobierzXmlAuth('produkt', 'urlLogin', 'urlHaslo');
            } else if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_PLIK ) {
                return $this->_pobierzXml('produkt');
            } else {
                return false;
            }
            
        } else {
            return false;
        }
        
    }

    protected function _sprawdzDane() {
        
        return true;
        
        /*      
        $out = chr(239) . chr(187) . chr(191) . "id;cena;kod;code;size;color;kolor_p;kolor_d;wymiary;nazwa\n";
        
        foreach ($this->_data as $product) {
            $info = $this->_getColorSize($product['kod']);
            $out .= ($product['id'] . ';' . $product['cena_pln'] . ';' . $product['kod'] . ';' . $info['code'] . ';' . $info['size'] . ';' . $info['color'] . ';' . $product['kolor_podstawowy'] . ';'. $product['kolor_dodatkowy'] . ';' . $product['wymiary'] . ';' . $product['nazwa'] .  "\n" );
        }
        
        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_dane2.csv', $out);

        return false;
        */      
    }
            
    protected function _pobierzMapowanie() {

        // pobieramy niepowtarzalne wartości z wszystkich produktów
        // dodatkowo liczymy ich ilość
        foreach ($this->_data as $product) {

            $info = $this->_getColorSize($product['kod']);
            $color = $info['color'];
            
            // kolory
            if(isset($product['kolor_podstawowy'])) {
                if(strlen($color)) {
                    $this->_count($this->_sourceMappings['colors'], $product['kolor_podstawowy'] . $color, $product['kolor_podstawowy'] . ' - ' . $color);
                } else {
                    $this->_count($this->_sourceMappings['colors'], $product['kolor_podstawowy'], $product['kolor_podstawowy']);
                }
            } else {
                $this->_sourceMappings['colors'][-1]['count']++;
            }
            
            // znakowania
            if(isset($product['techniki_zdobienia'])) {
                $markgroups = $this->_getMarkgroups($product['techniki_zdobienia']);
                foreach ($markgroups as $markgroup) {
                    $this->_count($this->_sourceMappings['markgroups'], $markgroup);
                }
            } else {
                $this->_sourceMappings['markgroups'][-1]['count']++;
            }

            // kategorie
            if(isset($product['kategorie'])) {
                $categories = $this->_getCategories($product['kategorie']);
                foreach ($categories as $category) {
                    $this->_count($this->_sourceMappings['categories'], $category);
                }
            } else {
                $this->_sourceMappings['categories'][-1]['count']++;
            }
            
        }

        return true;
        
    }
    
    protected function _importProdukty() {

        // licznik produktów w danych wejściowych <product></product>
        $prodCnt = 0;
        
        // pobieramy tylko widoczne detale
        $this->_detaleNiewidoczne = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktow($this->_dystrybutorId, 0, null, true);
//        Application_Model_Import_Log::dodajRabaty($this->_rabaty);
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Obliczony mnożnik wg rabatu: ' . $this->_rabat);

        $producentId = $this->_importParams['idProducenta'];
        $producent = $this->_em->getRepository('Entities\Producent')->getById($producentId);
        if (!($producent instanceof Entities\Producent)) {
            throw new Exception("Niepoprawny parametr importu: 'idProducenta'!");
        }

        // główna pętla dla każdego produktu wejściowego
        foreach ($this->_data as $product) {
         
            $prodCnt++;
            $dodajKategorieZnakowania = true;
            
//            if(!($prodCnt % 500)) {
//                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Produkt nr $prodCnt");
//            }
            
            if(!isset($product['id'])) {
                // brak id produktu
                $this->_dodajDoRaportu('Ilość produktów/detali bez identyfikatora');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - brak ID.");
                continue;
            }
            
            if(!isset($product['kod'])) {
                // brak pełnego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez kodu');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (ID = " . $product['id'] . ")- brak kodu.");
                continue;
            }
                
            if(!isset($product['nazwa'])) {
                // brak nazwy
                $this->_dodajDoRaportu('Ilość produktów/detali bez nazwy');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (KOD = " . $product['kod'] . ") - brak nazwy.");
                continue;
            }
            

            $codeFull = $product['kod'];
            $prodInfo = $this->_getColorSize($codeFull);
            $codeShort = $prodInfo['code'];
            $colorId = (strlen($prodInfo['color'])) ? $product['kolor_podstawowy'] . $prodInfo['color'] : $product['kolor_podstawowy'];
            $productColor = $this->_importMappings['colors'][$colorId];
            
            // cena regularna
            if(isset($product['cena_pln'])) {
                $cenaSrp = (float)$product['cena_pln'];
                $cena = $cenaSrp * $this->_rabat;
            } else {
                $cenaSrp = 0.0;
                $cena = 0.0;
            }


            // kategoria główna
             if(isset($product['kategorie'])) {
                $categories = $this->_getCategories($product['kategorie']);
                $kategoriaGlowna = $this->_importMappings['categories'][$categories[0]];
             } else {
                 $kategoriaGlowna = null;
             }

            // wymiary, pojemności i opisy
            $rozmiar = null; // brak aktualizacji, dla dodawania 'one size'
           
            // opis produktu do i18n
            $opis = (isset($product['opis']) ? '<p>' . $product['opis'] . '</p>' : '');
            $opis .= (isset($product['wymiary']) ? '<p>Wymiary: ' . $product['wymiary'] . '</p>' : '');

            // usuwamy z nazwy określenie koloru
            $compos = strrpos($product['nazwa'], ',');
            if($compos !== false) {
                $nazwa = substr($product['nazwa'], 0, $compos);
            } else {
                $nazwa = $product['nazwa'];
            }
            
            // DODAWANIE PRODUKTU
            if(!isset($this->_produktyIds[$codeShort])) {
                // nie ma w systemie code_short więc
                // dodajemy pozycję jako produkt

                // zapisujemy główny produkt
                $prodData = array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'producenci_id' => $producentId,
                    'nr_katalogowy' => $codeShort,
                    'kategoria_glowna' => $kategoriaGlowna,
                    'widoczny' => 1,
                    'nowosc' => ((isset($product['towar_nowosc']) && $product['towar_nowosc'] == 'true') ? 1 : 0),
//                    'promocja' => $jestPromo
                );
                
                try {
                    $res = 0;
                    $res = $this->_conn->insert('produkty', $prodData);
                } catch (Exception $e) {
                    if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie istniejącego już produktu nr $prodCnt");
                    } else {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania produktu nr $prodCnt");
                    }
                    continue;
                }

                if($res != 1) {
                    continue;
                }

                // id dodanego produktu
                $productId = (int)$this->_conn->lastInsertId();

                // zapisujemy i18n
                if($productId > 0) {
                    $this->_produktyIds[$codeShort] = $productId;
                    $this->_produktyNewIds[$codeShort] = $productId;
                    
                    $prodData = array(
                        'produkty_id' => $productId,
                        'jezyk' => 'pl',
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    try {
                        $res = $this->_conn->insert('produkty_i18n', $prodData);
                    } catch(Exception $e) {
                        if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie opisu istniejącego już produktu nr $prodCnt");
                        } else {
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania opisu produktu nr $prodCnt");
                        }
                    }
                } else {
                    continue;
                }

                $this->_rowCount['inserts']['products']++;
                
            } else {
                // UPDATE PRODUKTU

                $productId = $this->_produktyIds[$codeShort];
                
                // jeśli nie ma code_short w produktyNewIds
                // to oznacza, że produkt został dodany podczas poprzedniego
                // importu i trzeba produkt zaktualizować
                if(!isset($this->_produktyNewIds[$codeShort])) {
                
                    // aktualizujemy główny produkt
                    $prodData = array(
                        'producenci_id' => $producentId,
                        'nowosc' => ((isset($product['towar_nowosc']) && $product['towar_nowosc'] == 'true') ? 1 : 0),
//                        'promocja' => $jestPromo
                    );

                    try {
                        $res = 0;
                        $res = $this->_conn->update('produkty', $prodData, array('id' => $productId));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania produktu nr $prodCnt, ID = $productId");
                    }
                    
                    // aktualizujemy i18n
                    $prodData = array(
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    
                    try {
                        $res2 = 0;
//                        $res2 = $this->_conn->update('produkty_i18n', $prodData, array('produkty_id' => $productId, 'jezyk' => 'pl'));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania opisu produktu (i18n) nr $prodCnt, ID = $productId");
                    }

                    if( $res == 1 || $res2 == 1) {
                        $this->_rowCount['updates']['products']++;
                    }

                } else {
                    $dodajKategorieZnakowania = false;
                }

                $this->_produktyNewIds[$codeShort] = $productId;
                
            }

            if($dodajKategorieZnakowania) {
                
                // zapisujemy znakowania
                if(!isset($this->_znakowaniaIds[$productId])) {
                    // jeśli przed importem produkt nie miał znakowania to dodajemy
                    if(isset($product['techniki_zdobienia'])) {
                        $markgroups = $this->_getMarkgroups($product['techniki_zdobienia']);
                        foreach ($markgroups as $markgroup) {
                            $this->_zapiszZnakowanie($markgroup, $productId);
                        }
                    } 
                }
                
                // zapisujemy kategorie
                if(!isset($this->_kategorieIds[$productId])) {
                    // jeśli przed importem produkt nie miał kategorii to dodajemy
                    if(isset($product['kategorie'])) {
                        $categories = $this->_getCategories($product['kategorie']);
                        foreach ($categories as $category) {
                            $this->_zapiszKategorie($category, $productId);
                        }
                    } 
                }
                
            }
                
            // dodajemy/aktualizujemy detal
            $this->_zapiszDetal(
                    $productId,
                    $product['id'],
                    $codeFull,
                    $cena,
                    null,
                    null,
                    $productColor,
                    $rozmiar,
                    null,
                    null,
                    null,
                    null,
                    $cenaSrp
            );
            
            if(isset($this->_detaleNiewidoczne[($product['id'])])) {
                unset($this->_detaleNiewidoczne[($product['id'])]);
            }

            // dodajemy zdjęcia (zdjęcia nie są aktualizowane)
            if(isset($product['zdjecia']['zdjecie'])) {
                if(is_array($product['zdjecia']['zdjecie'])) {
                    // wiele zdjęć
                    foreach ($product['zdjecia']['zdjecie'] as $photo) {
                        $file =  $this->_importParams['hostZdjecia'] . $photo;
                        $this->_zapiszZdjecie($productId, $file, $productColor);
                    } 
                } else {
                    // jedno zdjęcie
                    $file =  $this->_importParams['hostZdjecia'] . $product['zdjecia']['zdjecie'];
                    $this->_zapiszZdjecie($productId, $file, $productColor);
                }
            }
                
        } // nstępny produkt/detal wejściowy

    }
    
    protected function _importStany() {
        
        $out = "id;code_full;quantity\n";
        $rows = 0;
        
        foreach ($this->_data as $product) {
            if(isset($product['id'])) {
                $dystrId = (int)$product['id'];
                if(isset($this->_detaleIds[$dystrId])) {
                    $res = 0;
                    $res = $this->_conn->update('produkty_detale', array(
                        'stan_magazynowy_1' => $product['stan_magazynowy'],
                    ), array(
                        'id' => $this->_detaleIds[$dystrId]
                    ));
                    $out .= ($product['id'] . ';' . $product['kod'] . ';' . $product['stan_magazynowy'] . "\n");
                    if($res == 1) {
                        $rows++;
                    }
                }
            }
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_stocks.txt', $out);
        return $rows;
        
    }
    
    protected function _importTlumaczenia() {
                
        return 0;
        
    }

    /*
    protected function _pobierzXmlAuth($tag, $loginParam, $passParam, $sourceParam, $iterator = 'default') {
        
        $authUrl = 'http://parstore.royaldesign.eu/sessions/create_xml';
        $cookieName = '_parstore_session';
        $domain = 'http://parstore.royaldesign.eu';

        $client = new Zend_Http_Client();
        $client->setUri($authUrl);
        $client->setParameterPost(array(
            'email'  => $this->_importParams[$loginParam],
            'password'   => $this->_importParams[$passParam]
        ));
        $response = $client->request('POST');
        
        $jar = Zend_Http_CookieJar::fromResponse($response, $domain);
        $cookie = $jar->getCookie($domain, $cookieName);

        unset($client);
        $client = new Zend_Http_Client();
        $client->setUri($this->_importParams[$sourceParam]);
        $client->setCookie($cookie);
        $client->setConfig(array(
            'timeout'      => 60
        ));
        $response = $client->request('GET');
        
        if(!$response->isSuccessful()) {
            throw new Exception($this->_messages['messUrlPobieranie']);
        }
        
        $config = Atacama_Config::getInstance();
        $tmpFile = realpath($config->importTmp->path) . '/' . $this->_importParams['plikLog'] . '_Urldata.txt';
        $res = file_put_contents($tmpFile, $response->getBody());
        if($res === false) {
            throw new Exception($this->_messages['messUrlZapisywanie']);
        }
        
        $this->_setDataSource($tmpFile);
        return $this->_pobierzXml($tag, $iterator);
        
    }
    */
    
    private function _getColorSize($kod) {
    
            $size = '';
            $color = '';
            
            $kropka = strpos($kod, '.');
            if($kropka !== false) {
                $code = substr($kod, 0, $kropka);
                $subkod = substr($kod, $kropka+1);
                
                $kropka = strpos($subkod, '.');
                if($kropka !== false) {
//                    $aaa = $subkod;
                    $color = substr($subkod, 0, $kropka);
                    $size = substr($subkod, $kropka+1);
                    if($size != 'L' && $size != 'XL') {
                        $size = '';
                    }
                } else if($subkod == 'L' || $subkod == 'XL') {
                    $size = $subkod;
                    $color = '';
                } else if($subkod == 'IIQ' || $subkod == 'A' || $subkod == 'O') {
                    $size = '';
                    $color = '';
                } else if(($iiq = strpos($subkod, 'IIQ')) !== false) {
                    $color = str_replace('IIQ', '', $subkod);
//                    $size = 'IIQ';
                    $size = '';
                } else {
                    $color = $subkod;
                }
            } else {
                $code = $kod;
            }
            
            return(array(
                'code' => $code,
                'size' => $size,
                'color' => $color,
            ));
            
    }    

    private function _getMarkgroups($markgroups) {

        $out = array();
        
        if(isset($markgroups['technika'][0]) && is_array($markgroups['technika'][0])) {
            // wiele znakowań
            foreach($markgroups['technika'] as $markgroup) {
                $out[] = $markgroup['technika_zdobienia'];
            }
        } else {
            // jedno znakowanie
            $out[] = $markgroups['technika']['technika_zdobienia'];
        }
        
        return array_unique($out);
        
    }

    private function _getCategories($categories) {

        $out = array();
        
        if(isset($categories['kategoria']) && is_array($categories['kategoria'])) {
            // wiele znakowań
            foreach($categories['kategoria'] as $category) {
                $out[] = $category;
            }
        } else {
            // jedno znakowanie
            $out[] = $categories['kategoria'];
        }
        
        return array_unique($out);
        
    }

}
