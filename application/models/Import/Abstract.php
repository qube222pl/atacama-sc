<?php

abstract class Application_Model_Import_Abstract {
    
    const ZRODLO_PLIK = 1;
    const ZRODLO_INTERNET = 2;
    const ZRODLO_PLIK_XML = 3;
    const ZRODLO_PLIK_CSV = 4;
    const ZRODLO_PLIK_ZIP = 5;
    const ZRODLO_PLIK_XLS = 6;
    const ZRODLO_PLIK_INNY = 7;
    
    const ZAKRES_OFERTA = 1;
    const ZAKRES_STANY = 2;
    const ZAKRES_ZDJECIA = 3;
    const ZAKRES_TLUMACZENIE_EN = 4;
    const ZAKRES_TLUMACZENIE_RU = 5;
    const ZAKRES_TLUMACZENIE_DE = 6;
    const ZAKRES_ZNAKOWANIE = 7;
    const ZAKRES_CENY = 8;
    const ZAKRES_TLUMACZENIE_PL = 9;
    
    // tablica z nazwami zakresów; dostęp przez _getZakresOpis();
    private $_zakresOpis = array(
        Application_Model_Import_Abstract::ZAKRES_OFERTA => 'oferta',
        Application_Model_Import_Abstract::ZAKRES_STANY => 'stany',
        Application_Model_Import_Abstract::ZAKRES_ZDJECIA => 'zdjęcia',
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN => 'tłumaczenie en',
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU => 'tłumaczenie ru',
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_DE => 'tłumaczenie de',
        Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE => 'znakowanie',
        Application_Model_Import_Abstract::ZAKRES_CENY => 'ceny',

    );

    protected $_formValues;
    protected $_importParams;
    protected $_em;
    
    // id dystrybutora
    protected $_dystrybutorId;
    
    // dystrybutor (encja z DB)
    protected $_dystrybutor;
    
    // dane wejściowe w postaci tablicy
    protected $_data = array();
    
    // mapowania: kolorów, znakowań, producentów i kategori z DB
    protected $_importMappings = array();
    
    // mapowania: kolorów, znakowań, producentów i kategori z danych wejściowych
    protected $_sourceMappings = array();
    
    // mnożnik cen dla całego importu np. rabat 60% = mnożnik 0.4
    protected $_rabat;
    
    // mnożniki cen dla poszczególnych producentów (po id producenta) np. rabat 60% = mnożnik 0.4
    protected $_rabaty;
    
    // mnożniki cen dla poszczególnych producentów (po nazwie producenta) np. rabat 60% = mnożnik 0.4
    protected $_rabatyPoNazwieProducenta;
    
    // tabela z nazwami i id rozmiarów
    protected $_rozmiary = array();
    
    // połączenie do DB
    protected $_conn;
    
    // tabela z id detali wraz z id_produktu_dystr 
    // istniejące w systemie przed importem
    protected $_detaleIds = array(); 
    
    // tabela z id produktu wraz z nr_katalogowy
    // istniejące w systemie przed importem
    protected $_produktyIds = array();
    
    // tabela przechowuje UNIKALNĄ wartość (nr_katalogowy, code, code_short) wspólną dla 
    // produktu i detali wraz z odpowiednikiem 'id produktu' dodanego w trakcie importu
    protected $_produktyNewIds = array();

    // przechowuje id (z DB) produktu, dla którego dodano zdjęcie z ustawionym polem 'glowne'
    protected $_zdjeciaGlowne = array();

    // unikalne produkty_id z tabeli 'produkty_has_kategorie' przed importem
    protected $_kategorieIds = array();
    
    // unikalne produkty_id z tabeli 'produkty_has_znakowania' przed importem
    protected $_znakowaniaIds = array();
    
    // stawka VAT z application.ini
    protected $_vat;

    // nazwa źródła danych (plik lub url)
    private $_dataSource;
    
    // podsumowanie importu
    protected $_raport = array();

    // komunikaty błędów
    protected $_messages = array(
        'messZlyZakres' => 'Nieobsługiwany zakres importu.',
        'messZleZrodlo' => 'Nieobsługiwane żródło importu.',
        'messZlyPlik' => 'Plik z danymi nie istnieje lub zła nazwa pliku.',
        'messZlyUrlOferta' => 'Konfiguracja - brak adresu URL oferty.',
        'messZlyUrlStany' => 'Konfiguracja - brak adresu URL stanów.',
        'messZlyUrlCeny' => 'Konfiguracja - brak adresu URL cen.',
        'messZlyUrlZnakowanie' => 'Konfiguracja - brak adresu URL znakowań.',
        'messZlyUrlTlumaczenie' => 'Konfiguracja - brak adresu URL tłumaczenia.',
        'messZlyJezyk' => 'Nieobsługiwany język tłumaczenia.',
        'messFtpPolaczenie' => 'Nie udało się połączyć z serwerem FTP.',
        'messFtpLogowanie' => 'Nie udało się zalogować do serwera FTP.',
        'messFtpPobieranie' => 'Nie udało się pobrać pliku z serwera FTP.',
        'messUrlPobieranie' => 'Nie udało się pobrać pliku (REST).',
        'messUrlZapisywanie' => 'Nie udał się zapis lokalnego pliku (REST).'
    );

    
    // tablica ze skrótami języków; dostęp przez _getJezyk();
    private $_jezyki = array(
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU => 'ru',
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN => 'en',
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_DE => 'de',
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_PL => 'pl'
    );

    // liczniki dodanych / zmienionych pozycji
    protected $_rowCount= array(
        'inserts' => array(
            'products' => 0,
            'detals' => 0,
            'categories' => 0,
            'markgroups' => 0,
            'photos' => 0
        ),
        'updates' => array(
            'products' => 0,
            'detals' => 0,
            'categories' => 0,
            'markgroups' => 0,
            'photos' => 0
        )
    );

    // id detali do ukrycia (tych co były w systemie a nie było w danych źródłowych)
    protected $_detaleNiewidoczne;

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $importParams - parametry odczytane z tabeli 'import_parametry' dla wybranego dystrybutora
     * @param type $formValues - parametry przekazane przez formularz importu
     * @param type $importOptions - paremtry określające jakie opcje importu są dostepne dla danego dystrybutora
     */

    public function __construct(Doctrine\ORM\EntityManager $entityManager, $importParams, $formValues) {
        $this->_em = $entityManager;
        $this->_formValues = $formValues;
        $this->_importParams = $importParams;
        $this->_dystrybutorId = (int)$formValues['dystrybutor'];
        $this->_dystrybutor = $this->_em->getRepository('Entities\Dystrybutor')->getById($this->_dystrybutorId);
        $this->_conn = $entityManager->getConnection();
        $this->_zrodlo();
        $this->init();
        $this->_vat = Atacama_Config::getInstance()->importVat;

    }

    public function __destruct() {
        
        // zapisujemy raport do bazy
        $importRaport = new Entities\Import_Raport();
        $this->_raport['Dystrybutor'] = $this->_dystrybutor->getNazwa();
        $importRaport->setOpis(json_encode($this->_raport))
                ->setPliklog(basename(Application_Model_Import_Log::getFileName()))
                ->setDystrybutorzyId($this->_dystrybutorId)
                ->setDystrybutorzy($this->_dystrybutor)
        ;
        $this->_em->persist($importRaport);
        $this->_em->flush();
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Koniec importu.');
        
    }

    // ustala źródło importu (nazwę pliku lub url)
    // i zapisuje do _dataSource
    private function _zrodlo() {

        if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_INTERNET) {

            if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_OFERTA) {
            
                if(!isset($this->_importParams['urlProdukty'])) {
                    throw new Exception($this->_messages['messZlyUrlOferta']);
                }
                $path = $this->_importParams['urlProdukty'];
                
            } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN) {

                if(!isset($this->_importParams['urlProduktyEn'])) {
                    throw new Exception($this->_messages['messZlyUrlTlumaczenie']);
                }
                $path = $this->_importParams['urlProduktyEn'];

            } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU) {

                if(!isset($this->_importParams['urlProduktyRu'])) {
                    throw new Exception($this->_messages['messZlyUrlTlumaczenie']);
                }
                $path = $this->_importParams['urlProduktyRu'];

            } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_STANY) {

                if(!isset($this->_importParams['urlStany'])) {
                    throw new Exception($this->_messages['messZlyUrlStany']);
                }
                $path = $this->_importParams['urlStany'];

            } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_CENY) {

                if(!isset($this->_importParams['urlCeny'])) {
                    throw new Exception($this->_messages['messZlyUrlCeny']);
                }
                $path = $this->_importParams['urlCeny'];

            } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE) {

                if(!isset($this->_importParams['urlZnakowanie'])) {
                    throw new Exception($this->_messages['messZlyUrlZnakowanie']);
                }
                $path = $this->_importParams['urlZnakowanie'];

            } else {
                throw new Exception($this->_messages['messZlyZakres']);
            }

            $zrodloOpis = 'internet';
            
        } else if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_PLIK) {

            $config = Atacama_Config::getInstance();
            $path = realpath($config->importTmp->path);
            $plik = $path . '/' . $this->_formValues['plik'];
            if(!file_exists($plik)) {
                throw new Exception($this->_messages['messZlyPlik']);
            }
            $path = $plik;
            $zrodloOpis = 'plik';
            
        } else {
            throw new Exception($this->_messages['messZleZrodlo']);
        }
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Początek importu.');
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Dystrybutor: id = ' . $this->_dystrybutorId . ', nazwa = ' . $this->_dystrybutor->getNazwa());
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Zakres importu: ' . $this->_getZakresOpis($this->_formValues['zakres']) . '.');
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Źródło danych: ' . $zrodloOpis . ' ' . $path);
        $this->_dataSource = $path;
        
    }

    protected function init() {
        // może - ale nie musi - być nadpisana w klasie dziedziczącej
        // służy do inicjalizacji niestandardowych parametrów
    }
    
    public function execute() {
        
        set_time_limit(600);

        // pobranie danych wejściowych do tablicy _data
        $res = $this->_pobierzDane();
        if($res !== true) {
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd pobierania danych wejściowych. ' . $res );
            return $res;
        } 
//        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo pobrano dane wejściowe.');
        
        // zapis wczytanych danych do pliku
        if(!(isset($this->_importParams['dumpInputdata']) && $this->_importParams['dumpInputdata'] == '0')) {
            $config = Atacama_Config::getInstance();
            $path = realpath($config->importTmp->path);
            file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_inputdata.txt', print_r($this->_data, true));
        }

        // sprawdzenie czy dane są prawidłowe
        $res = $this->_sprawdzDane();
        if($res !== true) {
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd weryfikacji danych wejściowych. ' . $res );
            return $res;
        } 
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo zweryfikowano dane wejściowe.');

        
        if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_OFERTA ||
           $this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE
        ) {
        
            // pobranie tablic mapowania z DB dla danego dystrybutora
            $res = $this->_wczytajMapowanie();
            if($res !== true) {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd wczytywania danych mapowania. ' . $res );
                return $res;
            } 
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo wczytano dane mapowania (z systemu).');

            // odczytanie różnych wartości dla mapowania z danych wejściowych
            $this->_przygotujMapowanie();
            $res = $this->_pobierzMapowanie();
            if($res !== true) {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd pobierania danych mapowania. ' . $res );
                return $res;
            } 
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo pobrano dane mapowania (z danych wejściowych).');

            // sprawdzenie czy mapowania zawierają niezbędne dane (użyte w danych wejściowych)
            $res = $this->_sprawdzMapowanie();
            if($res !== true) {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Niepoprawne mapowanie. ' . $res );
                return $res;
            } 
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowe mapowanie danych.');

            if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_OFERTA) {
                
                $this->_przygotujPobranie();
                $res = $this->_importProduktyWrapper();
                if($res !== true) {
                    Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd dodawania/aktualizacji produktów. ' . $res );
                    return $res;
                } 
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo dodano/zaktualizowano produkty.');
                
            } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE) {
        
                // nie wywołujemy _przygotujPobranie() gdyż potrzebne są tylko _znakowaniaIds i _produktyIds
                $this->_znakowaniaIds = $this->_em->getRepository('Entities\Produkt_Has_Znakowanie')->pobierzIdProduktow($this->_dystrybutorId);
                $this->_produktyIds = $this->_em->getRepository('Entities\Produkt')->pobierzNrKatalogowe($this->_dystrybutorId);
                $res = $this->_importZnakowaniaWrapper();
                if($res !== true) {
                    Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd dodawania znakowań. ' . $res );
                    return $res;
                } 
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo dodano znakowania.');
                
            }
            
        } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_STANY) {
            
            // import stanów
            $this->_przygotujPobranie();
            $res = $this->_importStanyWrapper();
            if($res !== true) {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd aktualizacji stanów magazynowych. ' . $res );
                return $res;
            } 
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo zaktualizowano stany magazynowe.');
            
        } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_CENY) {
            
            // import cen
            $this->_przygotujPobranie();
            $res = $this->_importCenyWrapper();
            if($res !== true) {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd aktualizacji cen. ' . $res );
                return $res;
            } 
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo zaktualizowano ceny.');
            
        } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN ||
                  $this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU ||
                  $this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_PL) {
            
            // import tłumaczeń
            $this->_przygotujPobranie();
            $res = $this->_importTlumaczeniaWrapper();
            if($res !== true) {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, 'Błąd importu tłumaczeń. ' . $res );
                return $res;
            } 
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, 'Prawidłowo zaimportowano tłumaczenia.');
            
        } 
        
        return true;
        
    }
    
    private function _wczytajMapowanie() {

        $this->_importMappings['colors'] = $this->_em->getRepository('Entities\Import_Kolor')->filtrujPoDystrybutorze($this->_dystrybutorId);
        $this->_importMappings['categories'] = $this->_em->getRepository('Entities\Import_Kategoria')->filtrujPoDystrybutorze($this->_dystrybutorId);
        $this->_importMappings['brands'] = $this->_em->getRepository('Entities\Import_Producent')->filtrujPoDystrybutorze($this->_dystrybutorId);
        $this->_importMappings['markgroups'] = $this->_em->getRepository('Entities\Import_Znakowanie')->filtrujPoDystrybutorze($this->_dystrybutorId);

        $info = "Znaleziono w systemie ilości mapowań:\n";
        $info .= 'Kolory - ' . count($this->_importMappings['colors']) . "\n";
        $info .= 'Kategorie - ' . count($this->_importMappings['categories']) . "\n";
        $info .= 'Producenci - ' . count($this->_importMappings['brands']) . "\n";
        $info .= 'Znakowania - ' . count($this->_importMappings['markgroups']);
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, $info);
        return true;
        
    }
    
    private function _przygotujMapowanie() {
        
        // -1 to liczniki produktów, które nie posiadają określonego atrybutu
        $this->_sourceMappings['colors'][-1]['count'] = 0;
        $this->_sourceMappings['categories'][-1]['count'] = 0;
        $this->_sourceMappings['brands'][-1]['count'] = 0;
        $this->_sourceMappings['markgroups'][-1]['count'] = 0;
        
    }
    
    private function _sprawdzMapowanie() {
        
//        // zapis ilości i id kodów do pliku
//        $out = "Kolory\n" . print_r($this->_sourceMappings['colors'], true);
//        $out .= "Kategorie\n" . print_r($this->_sourceMappings['categories'], true);
//        $out .= "Producenci\n" . print_r($this->_sourceMappings['brands'], true);
//        $out .= "Znakowanie\n" . print_r($this->_sourceMappings['markgroups'], true);
//        
//        $config = Atacama_Config::getInstance();
//        $path = realpath($config->importTmp->path);
//        file_put_contents($path . '/mapowanie.txt', $out);

        Application_Model_Import_Log::dodajMapowanie($this->_sourceMappings['colors'], 'colors');
        Application_Model_Import_Log::dodajMapowanie($this->_sourceMappings['categories'], 'categories');
        Application_Model_Import_Log::dodajMapowanie($this->_sourceMappings['brands'], 'brands');
        Application_Model_Import_Log::dodajMapowanie($this->_sourceMappings['markgroups'], 'markgroups');

        // usunięcie zbędnych indeksów
        unset($this->_sourceMappings['colors'][-1]);
        unset($this->_sourceMappings['categories'][-1]);
        unset($this->_sourceMappings['brands'][-1]);
        unset($this->_sourceMappings['markgroups'][-1]);

        
        // czy wszystkie kody w importownych danych
        // są w tabelach mapowania

        $notFound = array(
            'colors' => false,
            'categories' => false,
            'brands' => false,
            'markgroups' => false
        );

        // kolory
        $i = 0;
        foreach ($this->_sourceMappings['colors'] as $color => $info) {
            if(!isset($this->_importMappings['colors'][$color])) {
                $i += $this->_conn->insert('import_kolory', array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'kolory_id' => 1, // id koloru o nazwie 'do uzupełnienia'
                    'kolor_dystr_id' => $color,
                    'kolor_dystr_nazwa' => $info['name']
                ));
                $notFound['colors'] = true;
            } else if($this->_importMappings['colors'][$color] == 1) { // id koloru o nazwie 'do uzupełnienia'
                $notFound['colors'] = true;
            }
        }
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Dodano do mapowania ' . $i . ' kolorów.');

        // kategorie
        $i = 0;
        foreach ($this->_sourceMappings['categories'] as $category => $info) {
            if(!isset($this->_importMappings['categories'][$category])) {
                $i += $this->_conn->insert('import_kategoria', array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'kategorie_id' => 1, // id kategorii o nazwie 'do uzupełnienia'
                    'kategoria_dystr_id' => $category,
                    'kategoria_dystr_nazwa' => $info['name']
                ));
                $notFound['categories'] = true;
            } else if($this->_importMappings['categories'][$category] == 1) { // id kategorii o nazwie 'do uzupełnienia'
                $notFound['categories'] = true;
            }
        }
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Dodano do mapowania ' . $i . ' kategorii.');

        // producenci
        $i = 0;
        foreach ($this->_sourceMappings['brands'] as $brand => $info) {
            if(!isset($this->_importMappings['brands'][$brand])) {
                $i += $this->_conn->insert('import_producent', array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'producenci_id' => 1, // id producenta o nazwie 'do uzupełnienia'
                    'producent_dystr_id' => $brand,
                    'producent_dystr_nazwa' => $info['name']
                ));
                $notFound['brands'] = true;
            } else if($this->_importMappings['brands'][$brand] == 1) { // id producenta o nazwie 'do uzupełnienia'
                $notFound['brands'] = true;
            }
        }
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Dodano do mapowania ' . $i . ' producentów.');

        // znakowania
        $i = 0;
        foreach ($this->_sourceMappings['markgroups'] as $markgroup => $info) {
            if(!isset($this->_importMappings['markgroups'][$markgroup])) {
                $i += $this->_conn->insert('import_znakowanie', array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'znakowanie_id' => 1, // id znakowania o nazwie 'do uzupełnienia'
                    'znakowanie_dystr_id' => $markgroup,
                    'znakowanie_dystr_nazwa' => $info['name']
                ));
                $notFound['markgroups'] = true;
            } else if($this->_importMappings['markgroups'][$markgroup] == 1) { // id znakowania o nazwie 'do uzupełnienia'
                $notFound['markgroups'] = true;
            }
        }
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Dodano do mapowania ' . $i . ' znakowań.');
        

        $mess = '';
        
        if($notFound['colors']) {
            $mess .= '<br>Brak niektórych kolorów w tablicy mapowania. Proszę uzupełnić brakujące dane.';
        }
        
        if($notFound['categories']) {
            $mess .= '<br>Brak niektórych kategorii w tablicy mapowania. Proszę uzupełnić brakujące dane.';
        }

        if($notFound['brands']) {
            $mess .= '<br>Brak niektórych producentów w tablicy mapowania. Proszę uzupełnić brakujące dane.';
        }

        if($notFound['markgroups']) {
            $mess .= '<br>Brak niektórych znakowań w tablicy mapowania. Proszę uzupełnić brakujące dane.';
        }
//        return true;
        return (strlen($mess) ? $mess : true);
        
    }
    
    protected function _pobierzXmlAuth($tag, $loginParam, $passParam, $iterator = 'default') {
        
//        $opts = array(
//            'http'=>array(
//                'method' => 'GET',
//                'header' => sprintf('Authorization: Basic %s', base64_encode($this->_importParams[$loginParam] . ':' . $this->_importParams[$passParam]) )
//            )
//        );
//
//        $context = stream_context_create($opts);
//        $stream = file_get_contents($this->_dataSource, false, $context);
//        if($stream === false) {
//            throw new Exception($this->_messages['messUrlPobieranie']);
//        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_dataSource);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->_importParams[$loginParam] . ':' . $this->_importParams[$passParam]);
        $stream = curl_exec($ch);
        curl_close($ch);  
  
        if($stream === false) {
            throw new Exception($this->_messages['messUrlPobieranie']);
        }
        
        $config = Atacama_Config::getInstance();
        $tmpFile = realpath($config->importTmp->path) . '/' . $this->_importParams['plikLog'] . '_Urldata.txt';
        $res = file_put_contents($tmpFile, $stream);
        if($res === false) {
            throw new Exception($this->_messages['messUrlZapisywanie']);
        }
        
        $this->_dataSource = $tmpFile;
        return $this->_pobierzXml($tag, $iterator);
        
    }
    
    protected function _pobierzXml($tag, $iterator = 'default') {

        if($iterator == 'attribute') {
            $items = new System_XmlAttrIterator($this->_dataSource, $tag);
        } else {
            $items = new System_XmlIterator($this->_dataSource, $tag);
        }

        libxml_use_internal_errors(true);
        libxml_clear_errors();

        foreach($items as $item) {
            $this->_data[] = $item;
        }

        $errors = array();
        foreach(libxml_get_errors() as $error) {
            $errors[] =  $error->message;
        }

        if(count($errors)) {
            return 'Niepoprawne dane wejściowe: ' . implode('<br>', $errors); //$errors;
        } elseif(count($this->_data) == 0) {
            return 'Brak danych wejściowych.';
        } else {
            $count = count($this->_data);
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Odczytano z danych wejściowych ' . $count . ' produktów/detali.');
            $this->_raport['Ilość produktów/detali w danych wejściowych'] = $count;
            return true;
        }
        
    }
    
    // $columns - tablica intów z kolumnami, które zostaną pobrane z arkusza
    // 0 == A, 1 == B, 25 == Z, 26 == AA, itd.
    protected function _pobierzExcel($columns = null) {
        
        require_once 'PHPExcel/PHPExcel/IOFactory.php';
        
        try {
            $inputFileType = PHPExcel_IOFactory::identify($this->_dataSource);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            if($columns != null) {
                $filterSubset = new Application_Model_Import_PHPExcelFilter($columns);
                $objReader->setReadFilter($filterSubset); 
            }
            $objPHPExcel = $objReader->load($this->_dataSource);

            $worksheet = $objPHPExcel->getSheet(0);
            $maxRow = (int)$worksheet->getHighestRow();

            if($columns != null) {

                $tblmax = count($columns);
                for($row = 1; $row <= $maxRow; $row++) {
                    for($col = 0; $col < $tblmax; $col++) {
                        $dataRow[$col] = $worksheet->getCellByColumnAndRow($columns[$col], $row)->getValue();
                    }
                    $this->_data[] = $dataRow;
                }

            } else {

                $maxCol = PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn());
                for($row = 1; $row <= $maxRow; $row++) {
                    for($col = 0; $col < $maxCol; $col++) {
                        $dataRow[$col] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                    $this->_data[] = $dataRow;
                }

            }
        } catch(Exception $e) {
            return($e->getMessage());
        }
        
//    Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_OK, print_r($cells, true));
        $count = count($this->_data);
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Odczytano z danych wejściowych ' . $count . ' produktów/detali.');
        $this->_raport['Ilość produktów/detali w danych wejściowych'] = $count;
        return true;   
    
    }

    protected function _pobierzFtp($loginParam, $passParam) {
     
        $connInfo = parse_url($this->_dataSource);
        
        $conn = ftp_connect($connInfo['host']);
        if($conn === false) {
            throw new Exception($this->_messages['messFtpPolaczenie']);
        }
        
        $res = @ftp_login($conn, $this->_importParams[$loginParam], $this->_importParams[$passParam]);
        if($res === false) {
            ftp_close($conn);
            throw new Exception($this->_messages['messFtpLogowanie']);
        }
        
        ftp_pasv($conn, true);
        
        
        $config = Atacama_Config::getInstance();
        $tmpFile = realpath($config->importTmp->path) . '/' . $this->_importParams['plikLog'] . '_ftpdata.txt';
        
        $file = $connInfo['path'];
        if($file[0] == '/') {
            $file = substr($file, 1);
        }
        
        
        $res = ftp_get($conn, $tmpFile, $file, FTP_ASCII);
        if($res === false) {
            ftp_close($conn);
            throw new Exception($this->_messages['messFtpPobieranie']);
        }
        
        ftp_close($conn);
        $this->_dataSource = $tmpFile;
        return true;
        
    }
    
    protected function _pobierzCsv() {
        
        $rows = file($this->_dataSource, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        
        foreach($rows as $row) {
            $this->_data[] = explode(';', rtrim($row, ';\r\n'));
        }
        
        $count = count($this->_data);
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Odczytano z danych wejściowych ' . $count . ' produktów/detali.');
        $this->_raport['Ilość produktów/detali w danych wejściowych'] = $count;
        return true;
        
    }
    
    protected function _pobierzSystemCsv($delimiter = ';', $enclosure = '"') {
        
        if(($file = fopen($this->_dataSource,"r")) !== FALSE) {
            while(($data = fgetcsv($file, 0, $delimiter, $enclosure)) !== FALSE)  {
                // czy pusta linia?
                if(count($data) == 1 && $data[0] == null) {
                    continue;
                }
                $this->_data[] = $data;
            }
            fclose($file);
        }        
        
        $count = count($this->_data);
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Odczytano z danych wejściowych ' . $count . ' produktów/detali.');
        $this->_raport['Ilość produktów/detali w danych wejściowych'] = $count;
        return true;
        
    }
    
    protected function _unzip($file) {
        
        $zip = new ZipArchive();
        $result = $zip->open($this->_dataSource);
        if ($result === TRUE) {
            $config = Atacama_Config::getInstance();
            $path = realpath($config->importTmp->path);
            $zip->extractTo($path, $file);
            $zip->close();
            $this->_setDataSource($path . '/' . $file);
            return true;
        } else {
            return false;
        }        
        
    }

    private function _przygotujPobranie() {
        
        $this->_rabat = (float)(100 - $this->_importParams['rabat']) / 100;
        $this->_rozmiary = Atacama_Produkt_Rozmiar::tablicaImport();
        $this->_detaleIds = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktow($this->_dystrybutorId);
        $this->_produktyIds = $this->_em->getRepository('Entities\Produkt')->pobierzNrKatalogowe($this->_dystrybutorId);
        $this->_zdjeciaGlowne = $this->_em->getRepository('Entities\Produkt_Zdjecie')->pobierzIdGlownych($this->_dystrybutorId);
        $this->_kategorieIds = $this->_em->getRepository('Entities\Produkt_Has_Kategoria')->pobierzIdProduktow($this->_dystrybutorId);
        $this->_znakowaniaIds = $this->_em->getRepository('Entities\Produkt_Has_Znakowanie')->pobierzIdProduktow($this->_dystrybutorId);
        
    }
    
    protected function _count(&$what, $id, $name = null) {
        
        if(isset($what[$id])) {
            $what[$id]['count']++;
        } else {
            $what[$id]['count'] = 1;
            $what[$id]['name'] = (($name != null) ? $name : $id);
        }
        
    }

    protected function _dodajDoRaportu($id) {
        
        if(isset($this->_raport[$id])) {
            $this->_raport[$id]++;
        } else {
            $this->_raport[$id] = 1;
        }
        
    }

    protected function _zapiszKategorie($kategoria, $productId) {
        
        $res = 0;
        $prodData = array(
            'produkty_id' => $productId,
            'kategorie_id' => $this->_importMappings['categories'][$kategoria]
        );
        
        try {
            $res = $this->_conn->insert('produkty_has_kategorie', $prodData);
        } catch (Exception $err) {
        }

        if($res) {
            $this->_rowCount['inserts']['categories']++;
            
        }
        
    }
    
    protected function _zapiszZnakowanie($znakowanie, $productId) {

        $res = 0;
        $prodData = array(
            'produkty_id' => $productId,
            'znakowanie_id' => $this->_importMappings['markgroups'][$znakowanie]
        );
        
        try {
            $res = $this->_conn->insert('produkty_has_znakowania', $prodData);
        } catch (Exception $err) {
        }

        if($res) {
            $this->_rowCount['inserts']['markgroups']++;
        }
        
    }

    protected function _zapiszDetal($productId, $detalId, $detalKod, $cena, $cenaWyp, $cenaPromo, $kolor = null, $rozmiar = null, $ilosc1 = null, $ilosc2 = null, $minPromo = null, $widoczny = null, $cenaSrp = null) {
        
            $res = 0;
            
            if(isset($this->_detaleIds[$detalId])) {
                
                // detal istnieje -> aktualizujemy
                $prodData = array(
                    'promo' => $cenaPromo,
                    'wyp' => $cenaWyp,
                    'vat' => $this->_vat,
                    'id_produktu_dystr' => $detalId,
                );
                
                if($detalKod !== null) {
                    $prodData['kod_produktu_dystr'] = $detalKod;
                }
                
                if($cena !== null) {
                    $prodData['cena'] = $cena;
                }
                
                if($ilosc1 !== null) {
                    $prodData['stan_magazynowy_1'] = $ilosc1;
                }
                
                if($ilosc2 !== null) {
                    $prodData['stan_magazynowy_2'] = $ilosc2;
                }
                
                if($rozmiar !== null) {
                    $prodData['rozmiar'] = $rozmiar;
                }
                
                if($minPromo !== null) {
                    $prodData['promo_min'] = $minPromo;
                }
                
                if($widoczny !== null) {
                    $prodData['widoczny'] = $widoczny;
                }
                
                if($cenaSrp !== null) {
                    $prodData['srp'] = $cenaSrp;
                }
                
                $res = $this->_conn->update('produkty_detale', $prodData, array(
                    'id' => $this->_detaleIds[$detalId]
                ));
                
                if($res) {
                    $this->_rowCount['updates']['detals']++;
                }
                
            } else {
                
                // detal nie istnieje -> dodajemy
                $prodData = array(
                    'produkty_id' => $productId,
                    'kolory_id' => $kolor,
                    'rozmiar' => (($rozmiar != null) ? $rozmiar : 0),
                    'cena' => $cena,
                    'promo' => $cenaPromo,
                    'wyp' => $cenaWyp,
                    'vat' => $this->_vat,
                    'stan_magazynowy_1' => $ilosc1,
                    'stan_magazynowy_2' =>  $ilosc2,
                    'kod_produktu_dystr' => $detalKod,
                    'id_produktu_dystr' => $detalId,
                    'widoczny' => (($widoczny !== null) ? $widoczny : 1),
                    'promo_min' => $minPromo,
                    'srp' => $cenaSrp
                );
                
                $res = $this->_conn->insert('produkty_detale', $prodData);
                if($res) {
                    $this->_rowCount['inserts']['detals']++;
                }
                
            }
        return $res;
    }

    protected function _zapiszZdjecie($productId, $plikZrodlo, $kolor = null, $plikCel = null) {
        
        $res = 0;
        $prodData = array(
            'produkty_id' => $productId,
            'kolory_id' => $kolor,
            'glowne' => ((isset($this->_zdjeciaGlowne[$productId])) ? null : 1),
            'plik' => ($this->_dystrybutorId . '/' . (($plikCel != null) ? $plikCel : basename($plikZrodlo))),
            'plik_dystr' => $plikZrodlo
        );

        try {
            $res = $this->_conn->insert('produkty_zdjecia', $prodData);
        } catch (Exception $e) {
            if(strpos($e->getMessage(), '1062') !== false ){ // 1062 = duplicate entry
//                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Zdjęcie już istnieje - $plik - PlikCnt: $prodCnt - DB Id: $productId.");
            } else {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd dodawania zdjęcia - $plik - PlikCnt: $prodCnt - DB Id: $productId.");
            }
        }
        
        if($res) {
//         Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Zdjęcie dodano - $plik - PlikCnt: $prodCnt - DB Id: $productId.");
            $this->_rowCount['inserts']['photos']++;
            $this->_zdjeciaGlowne[$productId] = true;
        }
        
        return $res;
        
    }

    protected function _getJezyk() {
        
        if(isset($this->_formValues['zakres'])) {
            $zakres = $this->_formValues['zakres'];
            if(isset($this->_jezyki[$zakres])) {
                return $this->_jezyki[$zakres];
            }
        }
        throw new Exception($this->_messages['messZlyJezyk']);
        
    }

    private function _utworzRaport() {
        
        $this->_raport['Ilość dodanych produktów'] = $this->_rowCount['inserts']['products'];
        $this->_raport['Ilość dodanych detali'] = $this->_rowCount['inserts']['detals'];
        $this->_raport['Ilość dodanych kategorii'] = $this->_rowCount['inserts']['categories'];
        $this->_raport['Ilość dodanych znakowań'] = $this->_rowCount['inserts']['markgroups'];
        $this->_raport['Ilość dodanych zdjęć'] = $this->_rowCount['inserts']['photos'];

        $this->_raport['Ilość zaktualizowanych produktów'] = $this->_rowCount['updates']['products'];
        $this->_raport['Ilość zaktualizowanych detali'] = $this->_rowCount['updates']['detals'];
        // znakowania, kategorie i zdjęcie nie podlegają aktualizacji
        
    }
    
    private function _importProduktyWrapper() {

        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Początek dodawania produktów do bazy.');
        $this->_conn->beginTransaction();
        $rows = $this->_importProdukty();
        $this->_conn->commit();
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Koniec dodawania produktów do bazy.');
        Application_Model_Import_Log::dodajIlosci($this->_rowCount['inserts'], 'inserts');
        Application_Model_Import_Log::dodajIlosci($this->_rowCount['updates'], 'updates');
        
        $this->_utworzRaport();
        
        // ukrywamy niepotrzebne detale i produkty
        $this->_ukryjDetaleProdukty();
        
        return true;
        
    }
        
    private function _importZnakowaniaWrapper() {

        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Początek dodawania znakowań do bazy.');
        $this->_conn->beginTransaction();
        $rows = $this->_importZnakowania();
        $this->_conn->commit();
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Koniec dodawania znakowań do bazy.');
        Application_Model_Import_Log::dodajIlosci($this->_rowCount['inserts'], 'inserts');
        Application_Model_Import_Log::dodajIlosci($this->_rowCount['updates'], 'updates');
        
        $this->_utworzRaport();
        return true;
        
    }
        
    private function _importCenyWrapper() {

        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Początek aktualizacji cen.');
        $this->_conn->beginTransaction();
        $rows = $this->_importCeny();
        $this->_conn->commit();
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Koniec aktualizacji cen.');
        Application_Model_Import_Log::dodajIlosci($this->_rowCount['inserts'], 'inserts');
        Application_Model_Import_Log::dodajIlosci($this->_rowCount['updates'], 'updates');
        
        $this->_utworzRaport();
        return true;
        
    }
        
    private function _importStanyWrapper() {
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Znaleziono ' . count($this->_detaleIds) . ' detali dystrybutora w systemie.');
//        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Znaleziono ' . count($this->_data) . ' produktów w danych wejściowych.');
        $this->_conn->beginTransaction();
        $rows = $this->_importStany();
        $this->_conn->commit();
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Zaktualizowano ' . $rows . ' detali.');
        $this->_raport['Ilość zaktualizowanych detali'] = $rows;
        return true;
        
    }
        
    private function _importTlumaczeniaWrapper() {
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Znaleziono ' . count($this->_produktyIds) . ' produktów dystrybutora w systemie.');
        $this->_conn->beginTransaction();
        $rows = $this->_importTlumaczenia();
        $this->_conn->commit();
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Zaktualizowano ' . $rows . ' detali.');
        return true;
        
    }

    protected function _ukryjDetaleProdukty() {

        if(count($this->_detaleNiewidoczne) == 0){
            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, 'Brak detali do ukrycia');
            return;
        }
        
        Application_Model_Import_Log::dodajNiewidoczne($this->_detaleNiewidoczne);
        
        $ids = implode(',', $this->_detaleNiewidoczne);
        $stmt = $this->_conn->prepare("UPDATE produkty_detale SET widoczny = 0 WHERE id IN ($ids)");
        $stmt->execute();
        $rows = $stmt->rowCount();
        
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Ustawiono niewidocznych $rows detali.");
        $this->_raport['Ilość detali ustawionych jako niewidoczne'] = $rows;

        // Ustawienie widoczny = 0 dla tych produktów, których wszystkie detale mają widoczny = 0
        $stmt = $this->_conn->prepare("UPDATE produkty p INNER JOIN (SELECT produkty_id FROM produkty_detale GROUP BY produkty_id HAVING SUM( widoczny ) = 0) pd ON p.id = pd.produkty_id SET p.widoczny = 0 WHERE p.dystrybutorzy_id = " . $this->_dystrybutorId);
        $stmt->execute();
        $rows = $stmt->rowCount();
        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Ustawiono niewidocznych $rows produktów.");
        $this->_raport['Ilość produktów ustawionych jako niewidoczne'] = $rows;
        
        
    }

    protected function _policzRabaty() {
        
        $producenci = $this->_em->getRepository('Entities\Import_Producent')->filtrujPoDystrybutorze($this->_dystrybutorId, 1);
        foreach ($producenci as $producent) {
            $rabatStawka = (float)$producent->getRabat();
            if($rabatStawka < 0.0 || $rabatStawka > 100.0) {
                throw new Exception("Rabat dla producenta '" . $producent->getProducentDystrNazwa() . "' jest nieprawidłowy!");
            }
            $this->_rabaty[$producent->getProducentDystrId()] = array(
                'mnoznik' => ((float)(100.0 - $rabatStawka) / 100.0),
                'rabat' => $rabatStawka,
                'producent' => $producent->getProducentDystrNazwa()
            );
            $this->_rabatyPoNazwieProducenta[$producent->getProducentDystrNazwa()] = array(
                'mnoznik' => ((float)(100.0 - $rabatStawka) / 100.0),
                'rabat' => $rabatStawka,
                'producent' => (int)$producent->getProducentDystrId()
            );
        }
        
    }

    // poniższe funkcje muszą być zaimplementowane w klasie dziedziczonej
    // nawet gdyby miały być puste
    
    abstract protected function _pobierzDane();
    
    abstract protected function _sprawdzDane();

    abstract protected function _pobierzMapowanie();
        
    abstract protected function _importProdukty();
        
    abstract protected function _importStany();

    abstract protected function _importTlumaczenia();
    
//    abstract protected function _importZnakowania();

    protected function _getZakresOpis($zakres)
    {
        if(isset($this->_zakresOpis[$zakres])) {
            return $this->_zakresOpis[$zakres];
        } else {
            return '';
        }
    }

    protected function _setDataSource($dataSource) {
        
        $this->_dataSource = $dataSource;
        
    }
    
}
