<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Import_Texet extends Application_Model_Import_Abstract {

    static private $opcjeImportu = array(
        Application_Model_Import_Abstract::ZAKRES_OFERTA => array(Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_STANY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET),
    ); 

    static public function pobierzOpcje()
    {
        return self::$opcjeImportu;
    }

    protected function init() {
        
        $this->_policzRabaty();
        
    }

    protected function _pobierzDane() {
        
        return $this->_pobierzXml('produkt');
        
    }

    protected function _sprawdzDane() {
        
        return true;
        
    }
        
    protected function _pobierzMapowanie() {
        
        // pobieramy niepowtarzalne wartości z wszystkich produktów
        // dodatkowo liczymy ich ilość
        foreach ($this->_data as $product) {

            // kolory w detalu
            if(isset($product['detale']['detal']['id'])) {
                // pojedynczy detal
                if(isset($product['detale']['detal']['kolor'])) {
                    $this->_count($this->_sourceMappings['colors'], $product['detale']['detal']['kolor']);
                } else {
                    $this->_sourceMappings['colors'][-1]['count']++;
                }
            } else if(isset($product['detale']['detal'])) {
                foreach ($product['detale']['detal'] as $detal) {
                    $found = false;
                    if(isset($detal['kolor'])) {
                        $this->_count($this->_sourceMappings['colors'], $detal['kolor']);
                        $found = true;
                    }
                }
                if(!$found) {
                    $this->_sourceMappings['colors'][-1]['count']++;
                }
            } else {
                $this->_sourceMappings['colors'][-1]['count']++;
            }
            
            // kolory w zdjęciach
            if(isset($product['zdjecia']['zdjecie']['url'])) {
                // pojedyncze zdjecie
                if(isset($product['zdjecia']['zdjecie']['kolor'])) {
                    $this->_count($this->_sourceMappings['colors'], $product['zdjecia']['zdjecie']['kolor']);
                } else {
                    $this->_sourceMappings['colors'][-1]['count']++;
                }
            } else if(isset($product['zdjecia']['zdjecie'])) {
                // wiele detali
                foreach ($product['zdjecia']['zdjecie'] as $zdjecie) {
                    if(isset($zdjecie['kolor'])) {
                        $this->_count($this->_sourceMappings['colors'], $zdjecie['kolor']);
                    } else {
                        $this->_sourceMappings['colors'][-1]['count']++;
                    }
                }
            }            
            
            // kategorie
            if(isset($product['kategorie']['kategoria'])) {
                if(is_array($product['kategorie']['kategoria'])) {
                    foreach ($product['kategorie']['kategoria'] as $val) {
                        $this->_count($this->_sourceMappings['categories'], $val);
                    }
                } else {
                    $this->_count($this->_sourceMappings['categories'], $product['kategorie']['kategoria']);
                } 
            } else if(isset($product['kategoria'])) {
                $this->_count($this->_sourceMappings['categories'], $product['kategoria']);
            } else {
                $this->_sourceMappings['categories'][-1]['count']++;
            }
            
            // producenci
            if(isset($product['producent'])) {
                $this->_count($this->_sourceMappings['brands'], $product['producent']);
            } else {
                $this->_sourceMappings['brands'][-1]['count']++;
            }
            
            // znakowania
            if(isset($product['metody_znakowania']['znakowanie'])) {
                if(is_array($product['metody_znakowania']['znakowanie'])) {
                    foreach ($product['metody_znakowania']['znakowanie'] as $val) {
                        $this->_count($this->_sourceMappings['markgroups'], $val);
                    }
                } else {
                    $this->_count($this->_sourceMappings['markgroups'], $product['metody_znakowania']['znakowanie']);
                }
            } else {
                $this->_sourceMappings['markgroups'][-1]['count']++;
            }
        }

        return true;
    }
    
    protected function _importProdukty() {
        
        // licznik produktów w danych wejściowych <produkt></produkt>
        $prodCnt = 0;
        
        $product = $this->_data[0];
        if(isset($product['producent'])) {
            $masterProducentId = $this->_importMappings['brands'][($product['producent'])];
        } else {
            throw new Exception('Brak producenta w pierwszym produkcie.');
        }
        
        // pobieramy tylko widoczne detale
        $this->_detaleNiewidoczne = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktow($this->_dystrybutorId, 1, $masterProducentId, true);
        Application_Model_Import_Log::dodajRabaty($this->_rabaty);

        // główna pętla dla każdego produktu wejściowego
        foreach ($this->_data as $product) {
         
            $prodCnt++;
            
            if(!isset($product['nr_katalogowy'])) {
                // brak nr katalogowego
                $this->_dodajDoRaportu('Ilość produktów/detali bez nr katalogowego');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - nr_katalogowy.");
                continue;
            }
                            
            if(!isset($product['nazwa'])) {
                // brak nazwy
                $this->_dodajDoRaportu('Ilość produktów/detali bez nazwy');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - brak nazwy.");
                continue;
            }
            
            if(!isset($product['producent'])) {
                // brak producenta
                $this->_dodajDoRaportu('Ilość produktów/detali bez producenta');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Produkt nr $prodCnt - brak producenta.");
                continue;
            }
            
            $nrKatalogowy = $product['nr_katalogowy'];
            $producentId = $this->_importMappings['brands'][($product['producent'])];
           
            if($producentId != $masterProducentId) {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - niezgodny producent.");
                continue;
            }
            
            if(isset($product['kategorie']['kategoria'])) {
                if(is_array($product['kategorie']['kategoria'])) {
                    $kategoriaGlowna = $this->_importMappings['categories'][($product['kategorie']['kategoria'][0])];
                } else {
                    $kategoriaGlowna = $this->_importMappings['categories'][($product['kategorie']['kategoria'])];
                } 
            } else if(isset($product['kategoria'])) {
                $kategoriaGlowna = $this->_importMappings['categories'][($product['kategoria'])];
            } else {
                $kategoriaGlowna = null;
            }

            $opis = (isset($product['opis']) ? $product['opis'] : '');
            $nazwa = $product['nazwa'];
            $jestPromo = ((isset($product['promocja']) && $product['promocja'] == 'promocja') ? 1 : 0);
            $jestNowosc = ((isset($product['nowosc']) && $product['nowosc'] == 'nowość') ? 1 : 0);
            
            // DODAWANIE PRODUKTU
            if(!isset($this->_produktyIds[$nrKatalogowy])) {
                // nie ma w systemie nr_katalogowy więc
                // dodajemy nowy produkt
            
            
                // zapisujemy główny produkt
                $prodData = array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'producenci_id' => $producentId,
                    'nr_katalogowy' => $nrKatalogowy,
                    'kategoria_glowna' => $kategoriaGlowna,
                    'widoczny' => 1,
                    'nowosc' => $jestNowosc,
                    'promocja' => $jestPromo
                );

                try {
                    $res = 0;
                    $res = $this->_conn->insert('produkty', $prodData);
                } catch (Exception $e) {
                    if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie istniejącego już produktu nr $prodCnt");
                    } else {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania produktu nr $prodCnt");
                    }
                    continue;
                }

                if($res != 1) {
                    continue;
                }

                // id dodanego produktu
                $productId = (int)$this->_conn->lastInsertId();
                
                // zapisujemy i18n
                if($productId > 0) {
                    $this->_produktyIds[$nrKatalogowy] = $productId;
                    $this->_produktyNewIds[$nrKatalogowy] = $productId;

                    $prodData = array(
                        'produkty_id' => $productId,
                        'jezyk' => 'pl',
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );

                    try {
                        $res = $this->_conn->insert('produkty_i18n', $prodData);
                    } catch(Exception $e) {
                        if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie opisu istniejącego już produktu nr $prodCnt");
                        } else {
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania opisu produktu nr $prodCnt");
                        }
                    }
                } else {
                    continue;
                }

                $this->_rowCount['inserts']['products']++;
                
            } else {
                
                // UPDATE PRODUKTU

                $productId = $this->_produktyIds[$nrKatalogowy];
                
                // aktualizujemy główny produkt
                $prodData = array(
                    'producenci_id' => $producentId,
                    'nowosc' => $jestNowosc,
                    'promocja' => $jestPromo
                );

                try {
                    $res = 0;
                    $res = $this->_conn->update('produkty', $prodData, array('id' => $productId));
                } catch (Exception $e) {
                    Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania produktu nr $prodCnt, ID = $productId");
                }

                // aktualizujemy i18n
                $prodData = array(
                    'nazwa' => $nazwa,
                    'opis' => $opis,
                );

                try {
                    $res2 = 0;
//                    $res2 = $this->_conn->update('produkty_i18n', $prodData, array('produkty_id' => $productId, 'jezyk' => 'pl'));
                } catch (Exception $e) {
                    Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania opisu produktu (i18n) nr $prodCnt, ID = $productId");
                }

                if( $res == 1 || $res2 == 1) {
                    $this->_rowCount['updates']['products']++;
                }

            }

            // zapisujemy kategorie

            if(!isset($this->_kategorieIds[$productId])) {
                if(isset($product['kategorie']['kategoria'])) {
                    if(is_array($product['kategorie']['kategoria'])) {
                        // produkt ma wiele kategorii
                        foreach ($product['kategorie']['kategoria'] as $kategoria) {
                            $this->_zapiszKategorie($kategoria, $productId);
                        }
                    } else {
                        // produkt ma tylko jedną kategorię
                        $this->_zapiszKategorie($product['kategorie']['kategoria'], $productId);
                    } 
                } else if(isset($product['kategoria'])) {
                    // produkt ma tylko jedną kategorię
                    $this->_zapiszKategorie($product['kategoria'], $productId);
                }
            }

            // zapisujemy znakowania
            if(!isset($this->_znakowaniaIds[$productId])) {
                if(isset($product['metody_znakowania']['znakowanie'])) {
                    if(is_array($product['metody_znakowania']['znakowanie'])) {
                        foreach ($product['metody_znakowania']['znakowanie'] as $znakowanie) {
                            $this->_zapiszZnakowanie($znakowanie, $productId);
                        }
                    } else {
                        $this->_zapiszZnakowanie($product['metody_znakowania']['znakowanie'], $productId);
                    }
                }
            }
                
            
            // zapisujemy detale
            if(isset($product['detale']['detal']['id'])) {
                // pojedynczy detal
                $this->_zapiszDetal($product['detale']['detal'], $productId, $product['producent']);
            } else if(isset($product['detale']['detal'])) {
                // wiele detali
                foreach ($product['detale']['detal'] as $detal) {
                    $this->_zapiszDetal($detal, $productId, $product['producent']);
                }
            }            
            

            // zapisujemy zdjęcia
            if(isset($product['zdjecia']['zdjecie']['url'])) {
                // pojedyncze zdjecie
                $this->_zapiszZdjecie($product['zdjecia']['zdjecie'], $productId);
            } else if(isset($product['zdjecia']['zdjecie'])) {
                // wiele detali
                foreach ($product['zdjecia']['zdjecie'] as $zdjecie) {
                    $this->_zapiszZdjecie($zdjecie, $productId);
                }
            }            
                
        }

    }

    protected function _importStany() {
        
        $out = "id;quantity\n";
        $rows = 0;
        
        foreach ($this->_data as $product) {
            if(isset($product['id'])) {
                $dystrId = (int)$product['id'];
                if(isset($this->_detaleIds[$dystrId])) {
                    $res = 0;
                    $res = $this->_conn->update('produkty_detale', array(
                        'stan_magazynowy_1' => $product['ilosc']
                    ), array(
                        'id' => $this->_detaleIds[$dystrId]
                    ));
                    $out .= ($product['id'] . ';' . $product['ilosc'] . "\n");
                    if($res == 1) {
                        $rows++;
                    }
                }
            }
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_stocks.txt', $out);
        return $rows;
        
    }
        
    protected function _importTlumaczenia() {
        
    }

    protected function _zapiszDetal(&$detal, $productId, $producentId) {
        
        
        $cena = 0.0;
        $cenaPromo = 0.0;
        $cenaWyp = 0.0;
        $cenaSrp = 0.0;
        $mnoznik = $this->_rabaty[$producentId]['mnoznik'];
        $rozmiar = null;
        $size = null;
        $widoczny = null;
        $index = null;
        
        
        // cena regularna
        if(isset($detal['cena'])) {
            $cena = (float)str_replace(',', '.', $detal['cena']) * $mnoznik;
        }
        
        // cena promocyjna
        if(isset($detal['cena_promo'])) {
            $cenaPromo = (float)str_replace(',', '.', $detal['cena_promo']) * $mnoznik;
        }
        
        // cena wyprzedaży
        if(isset($detal['cena_wyp'])) {
            $cenaWyp = (float)str_replace(',', '.', $detal['cena_wyp']) * $mnoznik;
        }
  
        // cena SRP (katalogowa)
        if(isset($this->_importParams['narzutCenaKatalogowa'])) {
            $narzutSrp = (float)(100 + $this->_importParams['narzutCenaKatalogowa']) / 100;
            $cenaSrp = $cena * $narzutSrp;
        }
        
        // indeks
        if(isset($detal['indeks'])) {
            if($detal['indeks'] == 'produkt wycofany') {
                $widoczny = 0;
            } else {
                $index = $detal['indeks'];                
            }
        }
        
        // rozmiar
        if(isset($detal['rozmiar'])) {
            $size = $detal['rozmiar'];
            
            // usuwamy 'cm' gdy zawiera 'x'
            if(strpos($size, 'x') !== false) {
                $size = trim(str_ireplace('cm', '', $size));
            }
            
            if(isset($this->_rozmiary[$size])) {
                $rozmiar = $this->_rozmiary[$size];
            } else {
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Brak mapowania rozmiaru dla: " . $size);
            }
        }
        
//        $rozmiar = ((isset($detal['rozmiar']) && isset($this->_rozmiary[($detal['rozmiar'])])) ? $this->_rozmiary[($detal['rozmiar'])] : null);
        
        // dodajemy/aktualizujemy detal
        // jeżeli detal nie istnieje a mamy go dodać z <indeks>produkt wycofany</indeks>
        // to go nie dodajemy
        
        //if(!($widoczny == 0 && !isset($this->_detaleIds[($detal['id'])]))) {
        if($widoczny === null || isset($this->_detaleIds[($detal['id'])])) {
            parent::_zapiszDetal(
                    $productId,
                    $detal['id'],
                    $index,
                    $cena,
                    $cenaWyp,
                    $cenaPromo,
                    ((isset($detal['kolor'])) ? $this->_importMappings['colors'][($detal['kolor'])] : null),
                    $rozmiar,
                    ((isset($detal['ilosc']) ? $detal['ilosc'] : 0)),
                    null,
                    null,
                    $widoczny,
                    $cenaSrp
            );
        }
        if(isset($this->_detaleNiewidoczne[($detal['id'])])) {
            unset($this->_detaleNiewidoczne[($detal['id'])]);
        }
    }
    
    protected function _zapiszZdjecie(&$zdjecie, $productId) {
        
        $plik = $zdjecie['url'];
        $kolor = ((isset($zdjecie['kolor'])) ? $this->_importMappings['colors'][($zdjecie['kolor'])] : null);
        parent::_zapiszZdjecie($productId, $plik, $kolor);
        
    }    
}
