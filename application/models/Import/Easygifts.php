<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Import_Easygifts extends Application_Model_Import_Abstract {

    static private $opcjeImportu = array(
        Application_Model_Import_Abstract::ZAKRES_OFERTA => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_STANY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_CENY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML)
    ); 

    static public function pobierzOpcje()
    {
        return self::$opcjeImportu;
    }

    protected function init() {

        $this->_policzRabaty();
        
    }
        
    protected function _pobierzDane() {
        
        return $this->_pobierzXml('product');
        
    }

    protected function _sprawdzDane() {
        
        return true;
        
    }
        
    protected function _pobierzMapowanie() {

/*        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        $file = $path . '/' . $this->_importParams['plikLog'] . '_photos.csv';
        file_put_contents($file,  chr(239) . chr(187) . chr(191) . "Photo\n");
*/
        // pobieramy niepowtarzalne wartości z wszystkich produktów
        // dodatkowo liczymy ich ilość
        foreach ($this->_data as $product) {

            // kolory
            if(isset($product['color']['id'])) {
                $this->_count($this->_sourceMappings['colors'], (int)$product['color']['id'], $product['color']['name']);
            } else {
                $this->_sourceMappings['colors'][-1]['count']++;
            }
            
            // kategorie
            if(isset($product['categories']['category']['id'])) {
                $this->_count($this->_sourceMappings['categories'], (int)$product['categories']['category']['id'], $product['categories']['category']['name']);
            } else if(isset($product['categories']['category'])) {
                foreach ($product['categories']['category'] as $category) {
                    $this->_count($this->_sourceMappings['categories'], (int)$category['id'], $category['name']);
                }
            } else {
                $this->_sourceMappings['categories'][-1]['count']++;
            }
            
            // producenci
            if(isset($product['brand']['id'])) {
                $this->_count($this->_sourceMappings['brands'], (int)$product['brand']['id'], $product['brand']['name']);
            } else {
                $this->_sourceMappings['brands'][-1]['count']++;
            }

            // znakowania
            if(isset($product['markgroups']['markgroup']['id'])) {
                $this->_count($this->_sourceMappings['markgroups'], (int)$product['markgroups']['markgroup']['id'], $product['markgroups']['markgroup']['name']);
            } else if(isset($product['markgroups']['markgroup'])) {
                foreach ($product['markgroups']['markgroup'] as $category) {
                    $this->_count($this->_sourceMappings['markgroups'], (int)$category['id'], $category['name']);
                }
            } else {
                $this->_sourceMappings['markgroups'][-1]['count']++;
            }
            
/*            if(isset($product['images']) && is_array($product['images'])) {
                foreach($product['images'] as $image) {
                    file_put_contents($file,  ( $image .  "\n" ), FILE_APPEND);
                }
            }
*/
            
            
        }

        return true;
        
    }
    
    protected function _importProdukty() {

        // licznik produktów w danych wejściowych <product></product>
        $prodCnt = 0;
        
        // pobieramy tylko widoczne detale
        $this->_detaleNiewidoczne = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktow($this->_dystrybutorId, 0, null, true);
        Application_Model_Import_Log::dodajRabaty($this->_rabaty);

        $defaultProducentId = $this->_importParams['idProducenta'];
        $prodTmp = $this->_em->getRepository('Entities\Producent')->getById($defaultProducentId);
        if (!($prodTmp instanceof Entities\Producent)) {
            throw new Exception("Niepoprawny parametr importu: 'idProducenta'!");
        }
        
        // główna pętla dla każdego produktu wejściowego
        foreach ($this->_data as $product) {
         
            $prodCnt++;
            $dodajKategorieZnakowania = true;
            
//            if(!($prodCnt % 500)) {
//                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Produkt nr $prodCnt");
//            }
            
            if(!isset($product['baseinfo']['id'])) {
                // brak id produktu
                $this->_dodajDoRaportu('Ilość produktów/detali bez identyfikatora');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - brak ID.");
                continue;
            }
            
            if(!isset($product['baseinfo']['code_full'])) {
                // brak pełnego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez CODE_FULL');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (ID = " . $product['baseinfo']['id'] . ")- brak code_full.");
                continue;
            }
                
            if(!isset($product['baseinfo']['code_short'])) {
                // brak krótkiego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez CODE_SHORT');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (CODE_FULL = " . $product['baseinfo']['code_full'] . ", ID = " . $product['baseinfo']['id']  . ") - brak code_short.");
                continue;
            }
                
            if(!isset($product['baseinfo']['name'])) {
                // brak nazwy
                $this->_dodajDoRaportu('Ilość produktów/detali bez nazwy');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (CODE_FULL = " . $product['baseinfo']['code_full'] . ") - brak nazwy.");
                continue;
            }
            
            if(!isset($product['brand']['id'])) {
                // brak producenta
                $this->_dodajDoRaportu('Ilość produktów/detali bez producenta');
//                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (CODE_FULL = " . $product['baseinfo']['code_full'] . ") - brak producenta. Zapisano id = " . $defaultProducentId);
//                continue;
                $producent = "";
                $producentId = $defaultProducentId;
            } else {
                $producent = $product['brand']['id'];
                $producentId = $this->_importMappings['brands'][($product['brand']['id'])]; 
            }

            $codeFull = $product['baseinfo']['code_full'];
            $codeShort = $product['baseinfo']['code_short'];
            $productColor = (isset($product['color']['id']) ? $this->_importMappings['colors'][($product['color']['id'])] : null);
            
            // kategoria główna
            if(isset($product['categories']['category']['id'])) {
                $kategoriaGlowna = $this->_importMappings['categories'][($product['categories']['category']['id'])];
            } else if(isset($product['categories']['category'])) {
                $kategoriaGlowna = $this->_importMappings['categories'][($product['categories']['category'][0]['id'])];
            } else {
                $kategoriaGlowna = null;
            }                

            // wymiary, pojemności i opisy
            
            $rozmiarOpis = null;
            $pojemnoscOpis = null;
            $introOpis = null;
            $rozmiar = null; // brak aktualizacji, dla dodawania 'one size'
            $size = ((isset($product['attributes']['size'])) ? $product['attributes']['size'] : null);
            
            if(isset($product['attributes']['capacity'])) {
                
                // jest CAPACITY
                $capacity = str_replace(' ', '', $product['attributes']['capacity']);
                if(isset($this->_rozmiary[$capacity])) {
                    $rozmiar = $this->_rozmiary[$capacity];
                } else {
                    Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (CODE_FULL = " . $product['baseinfo']['code_full'] . ") - jest CAPACITY = '" . $capacity . "' ale brak mapowania.");
                    $pojemnoscOpis = $capacity;
                }
                $rozmiarOpis = $size;
                
            } else {
                
                // nie ma CAPACITY
                if($size != null) {
                    // jest SIZE
                    if(isset($this->_rozmiary[$size])) {
                        $rozmiar = $this->_rozmiary[$size];
                    } else {
                        $rozmiarOpis = $size;
                    }
                } 
                
            }
            
            if(isset($product['baseinfo']['intro'])) {
                if(is_array($product['baseinfo']['intro'])) {
                    $introOpis = $product['baseinfo']['intro'][0];
                } else {
                    $introOpis = $product['baseinfo']['intro'];
                }
            }
            
            // opis produktu do i18n
            $opis = (($introOpis != null) ? '<p>' . $introOpis . '</p>' : '');
            $opis .= (($rozmiarOpis != null) ? '<p>Wymiary: ' . $rozmiarOpis . '</p>' : '');
            $opis .= (($pojemnoscOpis != null) ? '<p>Pojemność: ' . $pojemnoscOpis . '</p>' : '');
            $opis .= (isset($product['attributes']['weight']) ? '<p>Waga: ' . $product['attributes']['weight'] . '</p>': '');

            $nazwa = strtr($product['baseinfo']['name'], array('“' => "'", '”' => "'", '"' => "'"));
            
            // DODAWANIE PRODUKTU
            if(!isset($this->_produktyIds[$codeShort])) {
                // nie ma w systemie code_short więc
                // dodajemy pozycję jako produkt

                // zapisujemy główny produkt
                $prodData = array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'producenci_id' => $producentId,
                    'nr_katalogowy' => $codeShort,
                    'kategoria_glowna' => $kategoriaGlowna,
                    'widoczny' => 1,
                    'nowosc' => ((isset($product['attributes']['new']) && $product['attributes']['new'] == '1') ? 1 : 0)
//                    'promocja' => 0
                );
                
                try {
                    $res = 0;
                    $res = $this->_conn->insert('produkty', $prodData);
                } catch (Exception $e) {
                    if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie istniejącego już produktu CODEFULL $codeFull");
                    } else {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania produktu CODEFULL =  $codeFull");
                    }
                    continue;
                }

                if($res != 1) {
                    continue;
                }

                // id dodanego produktu
                $productId = (int)$this->_conn->lastInsertId();

                // zapisujemy i18n
                if($productId > 0) {
                    $this->_produktyIds[$codeShort] = $productId;
                    $this->_produktyNewIds[$codeShort] = $productId;
                    
                    $prodData = array(
                        'produkty_id' => $productId,
                        'jezyk' => 'pl',
                        'nazwa' => $nazwa,
                        'opis' => $opis
                    );
                    try {
                        $res = $this->_conn->insert('produkty_i18n', $prodData);
                    } catch(Exception $e) {
                        if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie opisu istniejącego już produktu CODEFULL =  $codeFull");
                        } else {
                            if(($epos = strpos($e->getMessage(), "SQLSTATE"))!== false) {
                                $errMess = substr($e->getMessage(), $epos);
                            } else {
                                $errMess = $e->getMessage();
                            }
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania opisu produktu CODEFULL $codeFull" . " Błąd SQL: " . $errMess);
                        }
                    }
                } else {
                    continue;
                }

                $this->_rowCount['inserts']['products']++;
                
            } else {
                // UPDATE PRODUKTU

                $productId = $this->_produktyIds[$codeShort];
                
                // jeśli nie ma code_short w produktyNewIds
                // to oznacza, że produkt został dodany podczas poprzedniego
                // importu i trzeba produkt zaktualizować
                if(!isset($this->_produktyNewIds[$codeShort])) {
                
                    // aktualizujemy główny produkt
                    $prodData = array(
                        'producenci_id' => $producentId,
                        'nowosc' => ((isset($product['attributes']['new']) && $product['attributes']['new'] == '1') ? 1 : 0)
//                        'promocja' => $jestPromo
                    );

                    try {
                        $res = 0;
                        $res = $this->_conn->update('produkty', $prodData, array('id' => $productId));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania produktu nr $prodCnt, ID = $productId");
                    }
                    
                    // aktualizujemy i18n
                    $prodData = array(
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    
                    try {
                        $res2 = 0;
                        //$res2 = $this->_conn->update('produkty_i18n', $prodData, array('produkty_id' => $productId, 'jezyk' => 'pl'));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania opisu produktu (i18n) nr $prodCnt, ID = $productId");
                    }

                    if( $res == 1 || $res2 == 1) {
                        $this->_rowCount['updates']['products']++;
                    }

                } else {
                    $dodajKategorieZnakowania = false;
                }

                $this->_produktyNewIds[$codeShort] = $productId;
                
            }

            if($dodajKategorieZnakowania) {
                
                // zapisujemy kategorie
                if(!isset($this->_kategorieIds[$productId])) {
                    // jeśli przed importem produkt nie miał kategorii to dodajemy
                    if(isset($product['categories']['category']['id'])) {
                        // produkt ma tylko jedną kategorię
                        $this->_zapiszKategorie((int)$product['categories']['category']['id'], $productId);
                    } else if(isset($product['categories']['category'])) {
                        // produkt ma wiele kategorii
                        foreach ($product['categories']['category'] as $category) {
                            $this->_zapiszKategorie((int)$category['id'], $productId);
                        }
                    }
                }

                // zapisujemy znakowania
                if(!isset($this->_znakowaniaIds[$productId])) {
                    // jeśli przed importem produkt nie miał znakowania to dodajemy
                    if(isset($product['markgroups']['markgroup']['id'])) {
                        // produkt ma jedno znakowanie
                        $this->_zapiszZnakowanie((int)$product['markgroups']['markgroup']['id'], $productId);
                    } else if(isset($product['markgroups']['markgroup'])) {
                        // produkt ma wiele znakowań
                        foreach ($product['markgroups']['markgroup'] as $category) {
                            $this->_zapiszZnakowanie((int)$category['id'], $productId);
                        }
                    }
                }
                
            }
                
            // dodajemy/aktualizujemy detal
            $this->_zapiszDetal(
                    $productId,
                    $product['baseinfo']['id'],
                    $codeFull,
                    0,
                    0,
                    0,
                    $productColor,
                    $rozmiar
            );
            
//            $this->_zapiszDetal(
//                    $productId,
//                    $product['baseinfo']['id'],
//                    $codeFull,
//                    $cena,
//                    $cenaWyp,
//                    $cenaPromo,
//                    $productColor,
//                    $rozmiar,
//                    ((isset($product['quantity']['quantity_24h']) ? $product['quantity']['quantity_24h'] : null)),
//                    ((isset($product['quantity']['quantity_37days']) ? $product['quantity']['quantity_37days'] : null)),
//                    $minPromo
//            );
//            
            if(isset($this->_detaleNiewidoczne[($product['baseinfo']['id'])])) {
                unset($this->_detaleNiewidoczne[($product['baseinfo']['id'])]);
            }

            // dodajemy zdjęcia (zdjęcia nie są aktualizowane)
            if(isset($product['images']) && is_array($product['images'])) {
                foreach($product['images'] as $plik) {
                    
                    if($plik != 'NULL') {
                        $plik = str_replace('large-', '', $plik);
                        $plik = $this->_checkPhotoFile($plik);
                        $this->_zapiszZdjecie($productId, $plik, $productColor);
                    }
                }
            }
            
                
        } // nstępny produkt/detal wejściowy

    }
    
    protected function _importStany() {
        
        $out = "id;code_short;code_full;quantity\n";
        $rows = 0;
        
        foreach ($this->_data as $product) {
            if(isset($product['id'])) {
                $dystrId = (int)$product['id'];
                if(isset($this->_detaleIds[$dystrId])) {
                    $res = 0;
                    $res = $this->_conn->update('produkty_detale', array(
                        'stan_magazynowy_1' => $product['quantity_24h'],
                        'stan_magazynowy_2' => $product['quantity_37days']
                    ), array(
                        'id' => $this->_detaleIds[$dystrId]
                    ));
                    $out .= ($product['id'] . ';' . $product['code_short'] . ';' . $product['code_full'] . ';' . $product['quantity_24h'] . "\n");
                    if($res == 1) {
                        $rows++;
                    }
                }
            }
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_stocks.txt', $out);
        return $rows;
        
    }
    
    protected function _importCeny() {

        
        //$out = print_r($this->_detaleIds, true);
        
        $out = "id;code_short;code_full;srp;price;price_promo;price_sellout;jest_promo;promo_min\n";
        $rows = 0;

        // zaktualizowane produkty - by ich wielokrotnie nie aktualizować
        $updated = array();
        
        // rabat dla pendrive i powerbank
        if(isset($this->_importParams['rabatUSB'])) {
            $rabatUSB = (float)(100 - $this->_importParams['rabatUSB']) / 100;
        } else {
            $rabatUSB = 1.0;
        }

        foreach ($this->_data as $product) {
            if(isset($product['id']) && isset($product['price'])) {
                
            // cena regularna
                if(isset($product['price']) && $product['price'] != 'NULL') {
                    $cenaSrp = (float)str_replace(',', '.', $product['price']);
                    $cena = $cenaSrp;
                    if(isset($product['additional_offer']) && $product['additional_offer'] == '1') {
                        $cena = $cena * $rabatUSB;
                    } else {
                        $producent = $product['brand'];
                        if(isset($this->_rabatyPoNazwieProducenta[$producent])) {
                            $cena = $cena * $this->_rabatyPoNazwieProducenta[$producent]['mnoznik'];
                        } 
                    }
                } else {
                    $cenaSrp = 0.0;
                    $cena = 0.0;
                }

                // obliczamy cenę promocyjną by przy okazji sprawdzić czy jest promocja
                $cenaPromo = 0.0;
                $jestPromo = 0;
                $minPromo = 0;

                if(isset($product['price_promotion']) && $product['price_promotion'] != 'NULL') {
                    $cenaPromo = (float)str_replace(',', '.', $product['price_promotion']);
                    if($cenaPromo > 0) {
                        $jestPromo = 1;
                        $minPromo = (isset($product['promotion_min']) ? $product['promotion_min'] : 1);
                    }
                } else if(isset($product['price_promotion_from']) ) {
                    // szukamy najniższej ceny
                    $lastPrice = 999999.0;
                    $currentPrice = 0.0;
                    foreach($product['price_promotion_from']['level'] as $level) {
                        $currentPrice = (float)str_replace(',', '.', $level['price']);
                        if($currentPrice > 0) {
                            if($currentPrice < $lastPrice) {
                                $cenaPromo = $currentPrice;
                                $lastPrice = $currentPrice;
                                $jestPromo = 1;
                                $minPromo = (int)$level['from'];
                            }
                        }
                    }
                }

                // cena wyprzedaży
                if(isset($product['price_sellout']) && $product['price_sellout'] != 'NULL') {
                    $cenaWyp = (float)str_replace(',', '.', $product['price_sellout']);
                } else {
                    $cenaWyp = 0.0;
                }
                
                $dystrId = (int)$product['id'];
                if(isset($this->_detaleIds[$dystrId])) {
                    $res = $this->_conn->update(
                        'produkty_detale', array(
                            'cena' => $cena,
                            'wyp' => $cenaWyp,
                            'promo' => $cenaPromo,
                            'promo_min' => $minPromo,
                            'srp' => $cenaSrp
                        ), array('id' => $this->_detaleIds[$dystrId])
                    );
                    if($res) {
                        $this->_rowCount['updates']['detals']++;
                        $rows++;
                    }
                    
                    // aktualizujemy główny produkt ale tylko wtedy gdy jeszcze nie był aktualizowany
                    $productId = $this->_produktyIds[$product['code_short']];

                    if(!isset($updated[$productId])) {

                        $prodData = array(
                            'promocja' => $jestPromo
                        );
                    
                        try {
                            $res = 0;
                            $res = $this->_conn->update('produkty', $prodData, array('id' => $productId));
                            $updated[$productId] = true;
                        } catch (Exception $e) {
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania produktu nr $prodCnt, ID = $productId");
                        }
                        if($res) {
                            $this->_rowCount['updates']['products']++;
                        }                        
                    }
                    
                    $out .= ($product['id'] . ';' . $product['code_short'] . ';' . $product['code_full'] . ';' . $cena . ';' . $cenaPromo . ';' . $cenaWyp . ';' . $jestPromo . ';' . $minPromo . "\n");
                    
                }
//                    $out .= ($dystrId . ';' . $product['code_short'] . ';' . $product['code_full'] . ';' . $cena . ';' . $cenaPromo . ';' . $cenaWyp . ';' . $jestPromo . ';' . $minPromo . "\n");

            }                
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_prices.txt', $out);
        //return $rows;
    }

    protected function _importTlumaczenia() {
        
        
        $jezyk = $this->_getJezyk();
        $prodCnt = 0;
        $rows = 0;
        
        foreach ($this->_data as $product) {

            $prodCnt++;
            $codeFull = isset($product['baseinfo']['code_full']) ? ' (CODE_FULL = ' . $product['baseinfo']['code_full'] . ')' : '';

            if(!isset($product['baseinfo']['id'])) {
                // brak id produktu
                $this->_dodajDoRaportu('Ilość produktów/detali bez identyfikatora');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt $codeFull- brak ID.");
                continue;
            }

            if(!isset($product['baseinfo']['code_short'])) {
                // brak któtkiego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez CODE_SHORT');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (ID = " . $product['baseinfo']['id']  . ")$codeFull - brak code_short.");
                continue;
            }
            
            if(!isset($product['baseinfo']['name'])) {
                // brak nazwy
                $this->_dodajDoRaportu('Ilość produktów/detali bez nazwy');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (ID = " . $product['baseinfo']['id'] . ")$codeFull - brak nazwy.");
                continue;
            }

            $nazwa = strtr($product['baseinfo']['name'], array('“' => "'", '”' => "'", '"' => "'"));
            $codeShort = $product['baseinfo']['code_short'];
            $opis = (isset($product['baseinfo']['intro']) ? '<p>' . $product['baseinfo']['intro'] . '</p>' : '');
            $opis .= (isset($product['attributes']['size']) ? '<p>Size: ' . $product['attributes']['size'] . '</p>' : '');
            $opis .= (isset($product['attributes']['weight']) ? '<p>Weight: ' . $product['attributes']['weight'] . '</p>': '');
            
            if(isset($this->_produktyIds[$codeShort]) && !isset($this->_produktyNewIds[$codeShort])) {
                $prodData = array(
                    'produkty_id' => $this->_produktyIds[$codeShort],
                    'jezyk' => $jezyk,
                    'nazwa' => $nazwa,
                    'opis' => $opis,
                );
                
                try {
                    $res = 0;
                    $res = $this->_conn->insert('produkty_i18n', $prodData);
                } catch(Exception $e) {
                    if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
//                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie opisu istniejącego już produktu nr $prodCnt");
                    } else {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania opisu produktu nr $prodCnt");
                    }
                }
                
                if($res == 1) {
                    $rows++;
                    $this->_produktyNewIds[$codeShort] = true;
                }
            }
            
        }
        
        return $rows;
    }

    private function _checkPhotoFile($plik)
    {
        // a.jpg 5 1 = 4
        // a.jpeg 6 1 = 5
        
        $len = strlen($plik);
        
        if($len > 5) {
            
            if($plik[$len-4] != '.' && $plik[$len-5] != '.') {
                $plik .= '.jpg';
            }
        } 
    
        return $plik;
    }
}
