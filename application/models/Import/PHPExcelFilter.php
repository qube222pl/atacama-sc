<?php

class Application_Model_Import_PHPExcelFilter implements PHPExcel_Reader_IReadFilter 
{ 

    private $_columns;
    
    
    public function __construct($columns) { 
        
        foreach($columns as $column) {
            $strCol = PHPExcel_Cell::stringFromColumnIndex($column);
            $this->_columns[$strCol]  = true; 
        }
    } 

    public function readCell($column, $row, $worksheetName = '') { 
        if (isset($this->_columns[$column])) { 
            return true; 
        } 
        return false; 
    } 
} 


?>
