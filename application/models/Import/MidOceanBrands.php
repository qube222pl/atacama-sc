<?php

/**
 *
 * @author Studio Moyoki
 */
class Application_Model_Import_MidOceanBrands extends Application_Model_Import_Abstract {

    static private $opcjeImportu = array(
        Application_Model_Import_Abstract::ZAKRES_OFERTA => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_STANY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
        Application_Model_Import_Abstract::ZAKRES_CENY => array(Application_Model_Import_Abstract::ZRODLO_INTERNET),
        Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE => array(Application_Model_Import_Abstract::ZRODLO_INTERNET, Application_Model_Import_Abstract::ZRODLO_PLIK_XML),
    ); 

    static public function pobierzOpcje()
    {
        return self::$opcjeImportu;
    }

    protected function init() {

//        $this->_policzRabaty();
        
    }
        
    protected function _pobierzDane() {

        if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_CENY) {
            $this->_pobierzCeny();
        } else if($this->_formValues['zrodlo'] == Application_Model_Import_Abstract::ZRODLO_INTERNET ) {
            $this->_pobierzFtp('ftpLogin', 'ftpHaslo');
        }
        return $this->_pobierzXml('PRODUCT');

    }

    protected function _sprawdzDane() {
        
        return true;
        
    }
        
    protected function _pobierzMapowanie() {

        // pobieramy niepowtarzalne wartości z wszystkich produktów
        // dodatkowo liczymy ich ilość
        
        if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_OFERTA) {
        
            foreach ($this->_data as $product) {

                // kolory
                if(isset($product['COLOR_CODE']) && isset($product['COLOR_DESCRIPTION'])) {
                    $this->_count($this->_sourceMappings['colors'], (int)$product['COLOR_CODE'], $product['COLOR_DESCRIPTION']);
                } else {
                    $this->_sourceMappings['colors'][-1]['count']++;
                }

                // kategorie
                $found = false;
                for($i = 1; $i <= 4; $i++) {
                    $name = 'CATEGORY_LEVEL_' . $i;
                    if(isset($product[$name])) {
                        $this->_count($this->_sourceMappings['categories'], $product[$name]);
                        $found = true;
                    }
                } 
                if(!$found) {
                    $this->_sourceMappings['categories'][-1]['count']++;
                }

                // producenci
                if(isset($product['PRODUCT_BASE_NUMBER'])) {
                    $brand = substr($product['PRODUCT_BASE_NUMBER'], 0, 2);
                    $this->_count($this->_sourceMappings['brands'], $brand, $brand);
                } else {
                    $this->_sourceMappings['brands'][-1]['count']++;
                }

            }

        } else if($this->_formValues['zakres'] == Application_Model_Import_Abstract::ZAKRES_ZNAKOWANIE) {
            
            foreach ($this->_data as $product) {
                
                // znakowania
                $markgroupExists = false;
                if(isset($product['PRINTING_POSITIONS']['PRINTING_POSITION']['ID'])) {
                    // jedno miejsce
                    $markgroupExists = $this->_checkPrintingPosition($product['PRINTING_POSITIONS']['PRINTING_POSITION']);
                } else if(isset($product['PRINTING_POSITIONS']['PRINTING_POSITION'])) {
                    // wiele miejsc
                    foreach ($product['PRINTING_POSITIONS']['PRINTING_POSITION'] as $position) {
                        $markgroupExists += $this->_checkPrintingPosition($position);
                    }
                } 
                
                if($markgroupExists == false) {
                    $this->_sourceMappings['markgroups'][-1]['count']++;
                }
                
            }
            
        }
        
        return true;
        
    }
    
    private function _checkPrintingPosition($position)
    {
        
        $return = false;
        if(isset($position['PRINTING_TECHNIQUE']['ID'])) {
            // jedna metoda
            $this->_count($this->_sourceMappings['markgroups'], $position['PRINTING_TECHNIQUE']['ID']);
            $return = true;
        } else if(isset($position['PRINTING_TECHNIQUE'])) {
            // wiele metod
            foreach ($position['PRINTING_TECHNIQUE'] as $technique) {
                $this->_count($this->_sourceMappings['markgroups'], $technique['ID']);
            }
            $return = true;
        }
            
        return $return;

    }


    protected function _importProdukty() {

        // licznik produktów w danych wejściowych <product></product>
        $prodCnt = 0;
        
        // pobieramy tylko widoczne detale
        $this->_detaleNiewidoczne = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktow($this->_dystrybutorId, 0, null, true);

        // główna pętla dla każdego produktu wejściowego
        foreach ($this->_data as $product) {
         
            $prodCnt++;
            $dodajKategorieZnakowania = true;
            
//            if(!($prodCnt % 500)) {
//                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_INFO, "Produkt nr $prodCnt");
//            }
            
            if(!isset($product['PRODUCT_ID'])) {
                // brak id produktu
                $this->_dodajDoRaportu('Ilość produktów/detali bez identyfikatora');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt - brak ID.");
                continue;
            }
            
            if(!isset($product['PRODUCT_NUMBER'])) {
                // brak pełnego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez PRODUCT_NUMBER');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (ID = " . $product['PRODUCT_ID'] . ")- brak PRODUCT_NUMBER.");
                continue;
            }
                
            if(!isset($product['PRODUCT_BASE_NUMBER'])) {
                // brak któtkiego kodu
                $this->_dodajDoRaportu('Ilość produktów/detali bez PRODUCT_BASE_NUMBER');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (PRODUCT_NUMBER = " . $product['PRODUCT_NUMBER'] . ", ID = " . $product['PRODUCT_ID']  . ") - brak PRODUCT_BASE_NUMBER.");
                continue;
            }
                
            if(!isset($product['PRODUCT_NAME'])) {
                // brak nazwy
                $this->_dodajDoRaportu('Ilość produktów/detali bez nazwy');
                Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Produkt nr $prodCnt (PRODUCT_NUMBER = " . $product['PRODUCT_NUMBER'] . ") - brak nazwy.");
                continue;
            }
            
            $codeFull = $product['PRODUCT_NUMBER'];
            $codeShort = $product['PRODUCT_BASE_NUMBER'];
            $productColor = (isset($product['COLOR_CODE']) ? $this->_importMappings['colors'][((int)$product['COLOR_CODE'])] : null);
            
            
            // producent
            $producent = $this->_importMappings['brands'][substr($codeShort, 0, 2)];
            
            // kategoria główna
            if(isset($product['CATEGORY_LEVEL_2'])) {
                $kategoriaGlowna = $this->_importMappings['categories'][($product['CATEGORY_LEVEL_2'])];
            } else {
                $kategoriaGlowna = null;
            }                

            // wymiary, pojemności i opisy
            
            $rozmiar = null; // brak aktualizacji, dla dodawania 'one size'
            
            
            // opis produktu do i18n
            $wagaJednostka = (isset($product['GROSS_WEIGHT_UNIT'])) ? ' ' . strtolower($product['GROSS_WEIGHT_UNIT']) : '';
            $opis = (isset($product['LONG_DESCRIPTION']) ? '<p>' . $product['LONG_DESCRIPTION'] . '</p>' : '');
            $opis .= (isset($product['DIMENSIONS']) ? '<p>Wymiary: ' . $product['DIMENSIONS'] . '</p>' : '');
            $opis .= (isset($product['NET_WEIGHT']) ? '<p>Waga netto: ' . $product['NET_WEIGHT'] . $wagaJednostka . '</p>': '');
            $opis .= (isset($product['GROSS_WEIGHT']) ? '<p>Waga brutto: ' . $product['GROSS_WEIGHT'] . $wagaJednostka . '</p>': '');
            $opis .= (isset($product['MATERIAL_TYPE']) ? '<p>Materiał: ' . $product['MATERIAL_TYPE'] . '</p>' : '');

//            $nazwa = strtr($product['baseinfo']['name'], array('“' => "'", '”' => "'", '"' => "'"));
            if(isset($product['SHORT_DESCRIPTION'])) {
                $typ = $product['SHORT_DESCRIPTION'];
//                $typ = substr($typ, 0, strpos($typ, '   '));
                $typ = trim(str_replace($codeFull, '', $typ));
                if($typ[strlen($typ)-1] == '.') {
                    $typ = substr($typ, 0, -1);
                }
            } else {
                $typ = '';
            }
            $nazwa = $typ . " '" . $product['PRODUCT_NAME'] . "'";
            
            $jestNowosc = ((isset($product['CAMPAIGN_NEWPRODUCT']) && $product['CAMPAIGN_NEWPRODUCT'] == 'Y')) ? 1 : 0;
            $jestPromo = ((isset($product['CAMPAIGN_PROMOTION']) && $product['CAMPAIGN_PROMOTION'] == 'Y')) ? 1 : 0;
            
            // DODAWANIE PRODUKTU
            if(!isset($this->_produktyIds[$codeShort])) {
                // nie ma w systemie code_short więc
                // dodajemy pozycję jako produkt

                // zapisujemy główny produkt
                $prodData = array(
                    'dystrybutorzy_id' => $this->_dystrybutorId,
                    'producenci_id' => $producent,
                    'nr_katalogowy' => $codeShort,
                    'kategoria_glowna' => $kategoriaGlowna,
                    'widoczny' => 1,
                    'nowosc' => $jestNowosc,
                    'promocja' => $jestPromo
                );
                
                try {
                    $res = 0;
                    $res = $this->_conn->insert('produkty', $prodData);
                } catch (Exception $e) {
                    if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie istniejącego już produktu nr $prodCnt");
                    } else {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania produktu nr $prodCnt");
                    }
                    continue;
                }

                if($res != 1) {
                    continue;
                }

                // id dodanego produktu
                $productId = (int)$this->_conn->lastInsertId();

                // zapisujemy i18n
                if($productId > 0) {
                    $this->_produktyIds[$codeShort] = $productId;
                    $this->_produktyNewIds[$codeShort] = $productId;
                    
                    $prodData = array(
                        'produkty_id' => $productId,
                        'jezyk' => 'pl',
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    try {
                        $res = $this->_conn->insert('produkty_i18n', $prodData);
                    } catch(Exception $e) {
                        if((bool)strpos($e->getMessage(), '1062')) { // 1062 = duplicate entry
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Dodawanie opisu istniejącego już produktu nr $prodCnt");
                        } else {
                            Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas dodawania opisu produktu nr $prodCnt");
                        }
                    }
                } else {
                    continue;
                }

                $this->_rowCount['inserts']['products']++;
                
            } else {
                // UPDATE PRODUKTU

                $productId = $this->_produktyIds[$codeShort];
                
                // jeśli nie ma code_short w produktyNewIds
                // to oznacza, że produkt został dodany podczas poprzedniego
                // importu i trzeba produkt zaktualizować
                if(!isset($this->_produktyNewIds[$codeShort])) {
                
                    // aktualizujemy główny produkt
                    $prodData = array(
                        'producenci_id' => $producent,
                        'nowosc' => $jestNowosc,
                        'promocja' => $jestPromo
                    );

                    try {
                        $res = 0;
                        $res = $this->_conn->update('produkty', $prodData, array('id' => $productId));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania produktu nr $prodCnt, ID = $productId");
                    }
                    
                    // aktualizujemy i18n
                    $prodData = array(
                        'nazwa' => $nazwa,
                        'opis' => $opis,
                    );
                    
                    try {
                        $res2 = 0;
//                        $res2 = $this->_conn->update('produkty_i18n', $prodData, array('produkty_id' => $productId, 'jezyk' => 'pl'));
                    } catch (Exception $e) {
                        Application_Model_Import_Log::dodaj(Application_Model_Import_Log::LOG_ERROR, "Błąd podczas aktualizowania opisu produktu (i18n) nr $prodCnt, ID = $productId");
                    }

                    if( $res == 1 || $res2 == 1) {
                        $this->_rowCount['updates']['products']++;
                    }

                } else {
                    $dodajKategorieZnakowania = false;
                }

                $this->_produktyNewIds[$codeShort] = $productId;
                
            }

            if($dodajKategorieZnakowania) {
                
                // zapisujemy kategorie
                if(!isset($this->_kategorieIds[$productId])) {
                    for($i = 1; $i <= 4; $i++) {
                        $name = 'CATEGORY_LEVEL_' . $i;
                        if(isset($product[$name])) {
                            $this->_zapiszKategorie($product[$name], $productId);
                        }
                    } 
                }
                
                // znakowania dopisywane są innym zakresem importu
            }
                
            // dodajemy/aktualizujemy detal
            // ceny dopisywanie są innym zakresem importu
            $this->_zapiszDetal(
                    $productId,
                    $product['PRODUCT_ID'],
                    $codeFull,
                    null,
                    null,
                    null,
                    $productColor
            );
            
            if(isset($this->_detaleNiewidoczne[($product['PRODUCT_ID'])])) {
                unset($this->_detaleNiewidoczne[($product['PRODUCT_ID'])]);
            }

            // dodajemy zdjęcia (zdjęcia nie są aktualizowane)
            $plik = $this->_importParams['urlZdjecia'] . str_replace('-', '_', $codeFull) . '/700/700';
            $this->_zapiszZdjecie($productId, $plik, $productColor, $codeFull . '.png');
                
        } // nstępny produkt/detal wejściowy

    }
    
    protected function _importStany() {
        
        $out = "PRODUCT_NUMBER;quantity\n";
        $rows = 0;
        
        // ponieważ stany zawierają w rzeczywistości PRODUCT_NUMBER (mimo, że tag opisany jest jako ID!!!)
        // to do tablicy _detaleIds pobieramy dane z Produkt_Detal\kod_produktu_dystr zamiast z id_produktu_dystr
        $this->_detaleIds = $this->_em->getRepository('Entities\Produkt_Detal')->pobierzIdProduktowPoKodzie($this->_dystrybutorId);
        
        foreach ($this->_data as $product) {
            if(isset($product['ID'])) {
                $dystrKod = $product['ID'];
                if(isset($this->_detaleIds[$dystrKod])) {
                    $res = 0;
                    $res = $this->_conn->update('produkty_detale', array(
                        'stan_magazynowy_1' => $product['QUANTITY'],
                        'stan_magazynowy_2' => ((isset($product['ARRIVAL']['QUANTITY']) ? $product['ARRIVAL']['QUANTITY'] : 0))
                    ), array(
                        'id' => $this->_detaleIds[$dystrKod]
                    ));
                    $out .= ($dystrKod . ';' . $product['QUANTITY'] . "\n");
                    if($res == 1) {
                        $rows++;
                    }
                }
            }
        }

        $config = Atacama_Config::getInstance();
        $path = realpath($config->importTmp->path);
        file_put_contents($path . '/' . $this->_importParams['plikLog'] . '_stocks.txt', $out);
        return $rows;
        
    }
    
    protected function _importZnakowania() {
        
        foreach ($this->_data as $product) {
            if(isset($product['PRODUCT_BASE_NUMBER'])) {
                $shortCode = $product['PRODUCT_BASE_NUMBER'];
                if(isset($this->_produktyIds[$shortCode])) {
                    $productId = $this->_produktyIds[$shortCode];
                    if(!isset($this->_kategorieIds[$productId])) {

                        // gdy produkt nie ma żadnego znakowania to je dodajemy
                        if(isset($product['PRINTING_POSITIONS']['PRINTING_POSITION']['ID'])) {
                            // jedno miejsce
                            $this->_savePrintingPosition($product['PRINTING_POSITIONS']['PRINTING_POSITION'], $productId);
                        } else if(isset($product['PRINTING_POSITIONS']['PRINTING_POSITION'])) {
                            // wiele miejsc
                            foreach ($product['PRINTING_POSITIONS']['PRINTING_POSITION'] as $position) {
                                $this->_savePrintingPosition($position, $productId);
                            }
                        } 
                        
                    }
                }
            }
        }

    }

    private function _savePrintingPosition($position, $productId)
    {
        
        if(isset($position['PRINTING_TECHNIQUE']['ID'])) {
            // jedna metoda
            $this->_zapiszZnakowanie($position['PRINTING_TECHNIQUE']['ID'], $productId);
        } else if(isset($position['PRINTING_TECHNIQUE'])) {
            // wiele metod
            foreach ($position['PRINTING_TECHNIQUE'] as $technique) {
                $this->_zapiszZnakowanie($technique['ID'], $productId);
            }
        }
            
    }
    
    protected function _importCeny() {

        // narzut SRP
        if(isset($this->_importParams['narzutCenaKatalogowa'])) {
            $narzutSrp = (float)(100 + $this->_importParams['narzutCenaKatalogowa']) / 100;
        } else {
            $narzutSrp = 1.0;
        }
        
        foreach ($this->_data as $product) {
            if(isset($product['PRODUCT_ID']) && isset($product['PRICE'])) {  
                $codeFull = $product['PRODUCT_ID'];
                if(isset($this->_detaleIds[$codeFull])) {
                    $cena = (float)str_replace(',', '.', $product['PRICE']);
                    $res = $this->_conn->update(
                        'produkty_detale',
                        array('cena' => $cena, 'srp' => $cena * $narzutSrp), 
                        array('id' => $this->_detaleIds[$codeFull])
                    );
                    if($res) {
                        $this->_rowCount['updates']['detals']++;
                    }
                    
                }
            }                
        }
        
    }
    
    private function _pobierzCeny() {
        
        $httpClient = new Zend_Http_Client();
        $requestData = '<?xml version="1.0" encoding="utf-8"?>
            <PRICELIST_REQUEST>
            <CUSTOMER_NUMBER>' . $this->_importParams['bcNrKlienta'] . '</CUSTOMER_NUMBER>
            <LOGIN>' . $this->_importParams['bcLogin'] . '</LOGIN>
            <PASSWORD>' . $this->_importParams['bcHaslo'] . '</PASSWORD>
            <TIMESTAMP>' . date('YmdHis') . '</TIMESTAMP>
            </PRICELIST_REQUEST>';
        
        $httpClient->resetParameters(true);
        $httpClient->setUri($this->_importParams['urlCeny']);
        $httpClient->setRawData($requestData, 'text/xml');
        $response = $httpClient->request('POST');
        
        $config = Atacama_Config::getInstance();
        $tmpFile = realpath($config->importTmp->path) . '/' . $this->_importParams['plikLog'] . '_httpdata.txt';
        $body = $response->getBody();
        file_put_contents($tmpFile, $body);
        
        if($response->isSuccessful()) {
            if(strpos($body, '<RETURN_STATUS>OK</RETURN_STATUS>') !== false) {
                $this->_setDataSource($tmpFile);
                return;
            }
        } 
        
        throw new Exception('Błąd podczas pobierania cennika.');
        
    }
    
    protected function _importTlumaczenia() {
    }
    
}
