<?php

/*
 * To musi byc zawsze
 */
define("DONT_RUN_APP", true);

$dir = dirname(__FILE__);
$index = realpath($dir . '/../../public/index.php');
require($index);

/*
 * Teraz tresc cron
 */


$dystrId = 10; // Macma
$zakresy = array(
    Application_Model_Import_Abstract::ZAKRES_OFERTA,
    Application_Model_Import_Abstract::ZAKRES_CENY,
    Application_Model_Import_Abstract::ZAKRES_STANY,
    Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_EN,
    Application_Model_Import_Abstract::ZAKRES_TLUMACZENIE_RU
);

$entityManager = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Entitymanagerfactory');

foreach($zakresy as $zakres) {
    
    $cmd = new Application_Model_Commands_Import($entityManager, array(
        'dystrybutor' => $dystrId,
        'zakres' => $zakres,
        'zrodlo' => Application_Model_Import_Abstract::ZRODLO_INTERNET,
    ));
    $res = $cmd->execute();
    Application_Model_Import_Log::reset();
    if($res === true) {
        echo "Dystrybutor Id=$dystrId OK\n";
    } else {
        echo "Dystrybutor Id=$dystrId BŁĄD: $res\n";            
    }
    
}
