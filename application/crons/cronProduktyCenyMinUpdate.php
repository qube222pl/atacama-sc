<?php

/*
 * To musi byc zawsze
 */
define("DONT_RUN_APP", true);
// to chyba musi być by cron działał lokalnie
define('APPLICATION_ENV', 'development');

$dir = dirname(__FILE__);
$index = realpath($dir . '/../../public/index.php');
require($index);

/*
 * Teraz tresc cron
 */

$start = microtime(TRUE);

$cmd = new Application_Model_Commands_ProduktUstawCenyMin();
$cmd->execute();

$total_time = round((microtime(TRUE) - $start), 4);

$entityManager = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Entitymanagerfactory');
Atacama_Log::dodaj($entityManager, Atacama_Log::AKTUALIZACJA_CEN_MINIMALNYCH, $total_time);
