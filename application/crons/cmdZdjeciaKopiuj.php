<?php

/*
 * To musi byc zawsze
 */
define("DONT_RUN_APP", true);
// to chyba musi być by cron działał lokalnie
//define('APPLICATION_ENV', 'dev_michal');

$dir = dirname(__FILE__);
$index = realpath($dir . '/../../public/index.php');
require($index);

/*
 * Teraz tresc polecenia wywoływanego z linii poleceń
 */

if($argc != 3) {
    echo "Niepoprawna liczba argumentow.\n";
    echo "cmdZdjeciaKopiuj.php idDystrybutora katalogZNowymiZdjeciami\n";
    exit;
}

try {
    $cmd = new Application_Model_Commands_ZdjeciaKopiuj($argv);
    $cmd->execute();
} catch(Exception $e) {
    echo $e->getMessage() . "\n";
}

            
