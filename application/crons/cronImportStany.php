<?php

/*
 * To musi byc zawsze
 */
define("DONT_RUN_APP", true);
//define('APPLICATION_ENV', 'dev_michal');

$dir = dirname(__FILE__);
$index = realpath($dir . '/../../public/index.php');
require($index);

/*
 * Teraz tresc cron
 */

$dystrIds = array(
    5,  // Texet
    9,  // EasyGifts
    10, // Macma
    4,  // Mid Ocean
    1,  // Norwood
    18, // Asgard
);
$entityManager = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Entitymanagerfactory');

foreach($dystrIds as $dystrId) {
    $cmd = new Application_Model_Commands_Import($entityManager, array(
        'dystrybutor' => $dystrId,
        'zakres' => Application_Model_Import_Abstract::ZAKRES_STANY,
        'zrodlo' => Application_Model_Import_Abstract::ZRODLO_INTERNET,
    ));
    $res = $cmd->execute();
    Application_Model_Import_Log::reset();
    if($res === true) {
        echo "Dystrybutor Id=$dystrId OK\n";
    } else {
        echo "Dystrybutor Id=$dystrId BŁĄD: $res\n";            
    }
}
