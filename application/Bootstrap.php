<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    public function __construct($application) {
        parent::__construct($application);
        $this->bootstrap('frontController');
        $fc = Zend_Controller_Front::getInstance();
    }

    protected function _initPlaceholders() {
        $this->bootstrap('View');
        $view = $this->getResource('View');

        $view->description = 'Upominki reklamowe oraz tekstylia z nadrukiem lub znakowanie. Oryginalne gadżety reklamowe z logo Warszawa. ';
        $view->keywords = \Moyoki_Seo_Keywords::generate(10);

        $view->headTitle('Atacama Warszaw. Gadżety reklamowe z logo i koszulki z nadrukiem')
                ->setSeparator(' - ');


        $this->view->headLink()
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css',
                    'href' => 'https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => 'https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700&amp;subset=latin-ext', 'media' => 'all'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => $view->baseUrl('css/minify/hover-min.css'), 'media' => 'all'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => 'https://use.fontawesome.com/releases/v5.3.1/css/all.css', 'media' => 'all'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', 'media' => 'all'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => 'https://kenwheeler.github.io/slick/slick/slick-theme.css', 'media' => 'all'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/164071/drift-basic.css', 'media' => 'all'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/164071/luminous-basic.css', 'media' => 'all'))
                ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => $view->baseUrl('css/styles.css'), 'media' => 'all'))

        ;

        $view->headScript()
                ->appendFile('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js')
                ->appendFile('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js')
                ->appendFile('https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js')
                ->appendFile('https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js')
                ->appendFile('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js')
                ->appendFile('https://cdn.jsdelivr.net/npm/lazyload@2.0.0-beta.2/lazyload.js')
                ->appendFile('https://s3-us-west-2.amazonaws.com/s.cdpn.io/164071/Drift.min.js')
                ->appendFile('https://s3-us-west-2.amazonaws.com/s.cdpn.io/164071/Luminous.min.js')
                ->appendFile($view->baseUrl('js/libs/hogan-2.0.0.js'))
                ->appendFile($view->baseUrl('js/libs/typeahead.min.js'))
                ->appendFile($view->baseUrl('js/libs/jquery.numeric.js'))
                ->appendFile($view->baseUrl('js/minifyjs/ajax-min.js'))
                ->appendFile($view->baseUrl('js/libs/jquery.cookie.js'))
                ->appendFile($view->baseUrl('js/minifyjs/cookie-min.js'))
                ->appendFile($view->baseUrl('js/global180523.js'));

        ;
    }

    public function _initRouter() {

        $this->bootstrap('FrontController');

        $fc = $this->getResource('FrontController');

        $router = $fc->getRouter();
        $langRoute = new \Zend_Controller_Router_Route(':language', array(
            'language' => 'pl'
                ), array('DEFAULT_REGEX' => '^(pl|en|de)$')
        );

        $defaultRoute = new Zend_Controller_Router_Route('/:controller/:action/*', array(
            'controller' => 'index',
            'action' => 'index',
            'nazwa' => ':nazwa'
        ));

        $routeProducent = new \Zend_Controller_Router_Route('/marka/:nazwa/:prod/:s', array(
            'controller' => 'produkt',
            'action' => 'lista',
            'prod' => ':prod',
            'nazwa' => ':nazwa',
            '' => ':s'
                )
        );

        $routeKategoria = new \Zend_Controller_Router_Route('/:nazwa/:kat/strona/:s', array(
            'controller' => 'produkty',
            'action' => 'lista',
            'kat' => ':kat',
            'nazwa' => ':nazwa',
            's' => ':s'
                )
        );

        $routeGrupa = new \Zend_Controller_Router_Route('/g/:grupa/:nazwa/s/:s', array(
            'controller' => 'produkty',
            'action' => 'grupa',
            'grupa' => ':grupa',
            'nazwa' => ':nazwa',
            's' => ':s'
                )
        );

        $routeProdukt = new \Zend_Controller_Router_Route('/art/:id/:nazwa/', array(
            'controller' => 'produkty',
            'action' => 'podglad',
            'id' => ':id',
            'nazwa' => ':nazwa'
                )
        );

        $routeWyprzedaz = new \Zend_Controller_Router_Route('/art/:id/:nazwa/:sell', array(
            'controller' => 'produkty',
            'action' => 'podglad',
            'id' => ':id',
            'nazwa' => ':nazwa'
                )
        );

        $routeStrona = new \Zend_Controller_Router_Route('/p/:kod/:nazwa', array(
            'controller' => 'strona',
            'action' => 'podglad',
            'kat' => ':kod',
            'nazwa' => ':nazwa'
                )
        );

        $routeMarka = new \Zend_Controller_Router_Route('/b/:id/:nazwa', array(
            'controller' => 'Marka',
            'action' => 'widok',
            'id' => ':id',
            'nazwa' => ':nazwa'
                )
        );

        $defaultRouteChain = $langRoute->chain($defaultRoute);


        $routeProducentChain = $langRoute->chain($routeProducent);
        $routeKategoriaChain = $langRoute->chain($routeKategoria);
        $routeGrupaChain = $langRoute->chain($routeGrupa);
        $routeProduktChain = $langRoute->chain($routeProdukt);
        $routeWyprzedazChain = $langRoute->chain($routeWyprzedaz);
        $routeStronaChain = $langRoute->chain($routeStrona);
        $routeMarkaChain = $langRoute->chain($routeMarka);

        $router->addRoute('langRoute', $langRoute);
        $router->addRoute('default', $defaultRouteChain);
        $router->addRoute('producenci', $routeProducentChain);
        $router->addRoute('kategorie', $routeKategoriaChain);
        $router->addRoute('grupy', $routeGrupaChain);
        $router->addRoute('produkty', $routeProduktChain);
        $router->addRoute('wyprzedaze', $routeWyprzedazChain);
        $router->addRoute('strony', $routeStronaChain);
        $router->addRoute('marki', $routeMarkaChain);
    }

    protected function _initMailTransport() {

        // to tylko do testów
        // nie wysyła maili a zapisuje do katalogu
//        $transport = new System_Mail_Transport_File();
//        $transport->setSavePath(APPLICATION_PATH . '/../mail');
//        Zend_Mail::setDefaultTransport($transport);
    }

}
