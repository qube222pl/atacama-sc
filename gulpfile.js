var gulp = require('gulp'),
    less = require("gulp-less"),
    watch = require("gulp-watch"),
    prefixer = require('gulp-autoprefixer'),
    minify = require('gulp-minify'),
    rename = require('gulp-rename'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    jasmine = require('gulp-jasmine'),
    karma = require('gulp-karma'),
    cleanCSS = require('gulp-clean-css');

gulp.task('less', function () {
    gulp
        .src('public/less/*.less')
        .pipe(less({
            paths: ['less']
        }))
         .pipe(prefixer({
             browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css'));
});

gulp.task('minifyjs', function() {
            gulp.src('public/js/*.js')
        .pipe(minify())
        .pipe(gulp.dest('public/js/minifyjs/'));
});

gulp.task('minify-css', () => {
    return gulp.src('public/css/*.css')
            .pipe(cleanCSS({debug: true}, (details) => {
                console.log(`${details.name}: ${details.stats.originalSize}`);
                console.log(`${details.name}: ${details.stats.minifiedSize}`);
            }))
            .pipe(gulp.dest('public/css/minify'));
});

// The default task (called when you run `gulp`)
gulp.task('default' , function () {
    
    // Watch files and run tasks if they change
    gulp.watch('public/js/*.js', function (event) {
        gulp.run('minifyjs');
    });
    gulp.watch('public/less/*.less', function (event) {
        gulp.run('less');
    });
    
    gulp.watch('public/css/*.css', function (event) {
        gulp.run('minify-css');
    });

});

