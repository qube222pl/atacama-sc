<?php

/**
 * Description of Moyoki_Log_Writer_Doctrine
 *
 * 
 */

class Moyoki_Log_Writer_Doctrine extends Zend_Log_Writer_Abstract {
    
    /**
     *
     * @var EntityManager 
     */
    private $em;
    
    /**
     *
     * @var integer 
     */
    private $logRecId = 0;
    
    private $rodzic = false;
    
    function __construct($em, $rodzic = false) {
        $this->em = $em;
        $this->rodzic = $rodzic;
    }
    
    protected function _write($event) {  
        $this->em->beginTransaction();
        $log = new Entities\Log();
        $log->setCzas(new DateTime($event['timestamp']));
        $log->setOpis($event['message']);        
        $log->setPriorytet($event['priority']);
        $log->setNsId($event['ns_id']);
        $log->setModul($event['module']);
        $log->setPriorytetNazwa($event['priorityName']);
        if ($this->logRecId > 0) {
            $log->setRodzic($this->em->find('Entities\Log', $this->logRecId));
        } 
        $this->em->persist($log);
        $this->em->flush();
        
        if ($this->logRecId < 1 && $this->rodzic === true) {
            $this->logRecId = $log->getId();
        }
        $this->em->commit();
    }
    
    public static function factory($config) {
        
    }
}

?>
