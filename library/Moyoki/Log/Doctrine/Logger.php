<?php



/**
 * Description of Logger
 *
 *
 */

class Moyoki_Log_Doctrine_Logger {
    
    /**
     * 
     * @var Zend_Log 
     */
    protected $logger = null;
    
    /**
     *
     */
    static $instance = null;
    
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self();                        
        }        
        return self::$instance;
    }
    
    /**
     * return Zend_Log
     */
    public function getLog() {
        return $this->logger;
    }
    
    protected function __construct() {
        $this->logger = Zend_Registry::get('logger');        
    }
    
    /**
     * log a message
     * @param string $message
     */
    public static function info($message, $module, $ns_id) {        
        self::getInstance()->getLog()->setEventItem('module', $module);
        self::getInstance()->getLog()->setEventItem('ns_id', $ns_id);
        self::getInstance()->getLog()->info($message);
    }
    
    /**
     *
     * @param string $message 
     */
    public static function crit($message, $module, $ns_id) {
        self::getInstance()->getLog()->setEventItem('module', $module);
        self::getInstance()->getLog()->setEventItem('ns_id', $ns_id);
        self::getInstance()->getLog()->crit($message);
    }
	
}

?>
