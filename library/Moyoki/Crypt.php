<?php

/**
 * Szyfrowanie danych
 * Wymaga zewnetrznej bilbioteki Crypt:
 * @link        http://code.google.com/p/cryptclass
 * @author Studio Moyoki
 */
final class Moyoki_Crypt
{

	protected static $_instance = null;
	protected static $_crypt = null;
	private static $_crypt_key = '!@#$^&*()_+?:';

	/**
	 * Singleton - nie mozna utworzyc obiektu
	 *
	 * @return void
	 */
	protected function __construct()
	{

	}

	/**
	 * Singleton - nie mozna klonowac
	 *
	 * @return void
	 */
	protected function __clone()
	{

	}

	/**
	 * Zwraca model_Crypt
	 * Singleton
	 *
	 * @return Moyoki_Crypt
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
			
			require_once 'Crypt_lib.php';
			self::$_crypt = new Crypt(Crypt::MODE_HEX, self::$_crypt_key);
		}

		return self::$_instance;
	}

	/**
	 * Szyfruje dane
	 *
	 * @param string $data
	 * @return crypted_string
	 */
	public function encrypt($data)
	{
		return self::$_crypt->encrypt($data);
	}

	/**
	 * Zwraca odszyfrowane dane
	 * 
	 * @param crypted_string $data
	 * @return string
	 */
	public function decrypt($data)
	{
		return self::$_crypt->decrypt($data);
	}

}
