<?php

/**
 * Przycina ciagi znakow do okreslonej wartosci
 *
 * @author Studio Moyoki
 */
class Moyoki_StringTruncate {

    static function cut($string) {
        $config = Atacama_Config::getInstance();

        if (strlen($string) > $config->iloscZnakowWnazwie) {
            $string = wordwrap($string, $config->iloscZnakowWnazwie);
            $string = substr($string, 0, strpos($string, "\n")) . '[...]';
        }
        return $string;
    }

}
