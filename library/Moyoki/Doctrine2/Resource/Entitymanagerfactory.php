<?php

class Moyoki_Doctrine2_Resource_Entitymanagerfactory extends Zend_Application_Resource_ResourceAbstract {
    
    public function init() {
        $options = $this->getOptions();
        
        
        $emf = new Moyoki_Doctrine2_EntityManagerFactory($options);
        $emf->registerAutoload();
        
        if(isset($options['lazyLoad']) && $options['lazyLoad'] == true) {
            return $emf;
        } else {
            return $emf->createEntityManager();
        }
    }   
}
