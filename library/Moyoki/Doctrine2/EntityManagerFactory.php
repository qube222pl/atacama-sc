<?php

require_once 'Doctrine/Common/ClassLoader.php';

class Moyoki_Doctrine2_EntityManagerFactory 
{
    
    protected $_options = array();
    
    public function __construct($options)
    {
        if ($options instanceof Zend_Config)
        {
            $this->_options = $options->toArray();
        } 
        else if(!is_array($options))
        {
            throw new Moyoki_Doctrine2_Exception("Błędna konfiguracja dla Doctrine2");
        }
        $this->_options = $options;
    }
    
    
    public function registerAutoload()
    {
  
		$loader = new \Doctrine\Common\ClassLoader('Doctrine', APPLICATION_PATH . '/../library');
		$loader->register();

        $loader = new \Doctrine\Common\ClassLoader('Symfony', APPLICATION_PATH . '/../library/Doctrine');
		$loader->register();

        $loader = new \Doctrine\Common\ClassLoader('Entities', $this->_options['metadata']['classDirectory']);
        $loader->register();
        
        $loader = new \Doctrine\Common\ClassLoader('Repositories', $this->_options['metadata']['classDirectory']);
        $loader->register();
        
        $loader = new \Doctrine\Common\ClassLoader($this->_options['proxyDirName'], $this->_options['proxyDir']);
        $loader->register();
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
    */
    public function createEntityManager()
    {
        $cache = $this->_setupCache($this->_options);
        
        if(!isset($this->_options['proxyDir'])) 
        {
            throw new Moyoki_Doctrine2_Exception("Brak definicji wymaganej opcji 'proxyDir' dla Doctrine 2 ");
        }
        
        if (!file_exists($this->_options['proxyDir']))
        {
            throw new Moyoki_Doctrine2_Exception("Katalog 'proxyDir' ".$this->_options['proxyDir']." nie istnieje ");
        }
        
        if (!isset($this->_options['proxyNamespace']))
        {
            $this->_options['proxyNamespace'] = 'MyProject/Proxies';
        }
        
        if (!isset($this->_options['autoGenerateProxyClasses'])) 
        {
            $this->_options['autoGenerateProxyClasses'] = false;
        }
        
        
        $eventManager = new \Doctrine\Common\EventManager();
        
        $config = new \Doctrine\ORM\Configuration();
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir($this->_options['proxyDir']);
        $config->setProxyNamespace($this->_options['proxyNamespace']);
        $config->setAutoGenerateProxyClasses($this->_options['autoGenerateProxyClasses']);
        if (isset($this->_options['sqllogger']))
        {
            if (is_string($this->_options['sqllogger']) && class_exists($this->_options['sqllogger']))
            {
                $logger = new $this->_options['sqllogger'];
                if (!($logger instanceof \Doctrine\DBAL\Logging\SQLLogger))
                {
                    throw new Moyoki_Doctrine2_Exception('sqllogger musi być implementacją \Doctrine\DBAL\Logging\SqlLogger');
                }
                $config->setSQLLogger($logger);
            }
            else 
            {
                throw new Moyoki_Doctrine2_Exception('Błędna konfiguracja sqllogger');
            }
        }
        
        if (!isset($this->_options['connectionOptions']) || !is_array($this->_options['connectionOptions']))
        {
            throw new Moyoki_Doctrine2_Exception('Niepoprawna konfiguracja DBAL Connection!');
        }
        
        $connectionOptions = $this->_options['connectionOptions'];
        
        $connection = \Doctrine\DBAL\DriverManager::getConnection($connectionOptions, $config, $eventManager);
        
        $this->_setupMetadataDriver($this->_options, $config, $cache, $connection);
        
        $em = \Doctrine\ORM\EntityManager::create($connection, $config, $eventManager);
        
        $em->getEventManager()->addEventSubscriber(new \Doctrine\DBAL\Event\Listeners\MysqlSessionInit('utf8', 'utf8_unicode_ci'));
        
        return $em;
    }
    
    /**
     * Setup the metadata driver if necessary options are set. Otherwise Doctrine defaults are used (AnnotationReader).
     *
     * @param array $options
     * @param Doctrine\ORM\Configuration $config
     * @param Doctrine\Common\Cache\AbstractCache $cache
     * @param Doctrine\DBAL\Connection $conn
     */
    protected function _setupMetadataDriver($options, $config, $cache, $conn)
    {
        $driver = false;
        
        if (isset($options['metadata']))
        {
            if (isset($options['metadata']['driver']))
            {
                $driverName = $options['metadata']['driver'];
                switch(strtolower($driverName))
                {
                    case 'yaml':
                        $driverName = 'Doctrine\ORM\Mapping\Driver\YamlDriver';
                        break;
                    case 'xml':
                        $driverName = 'Doctrine\ORM\Mapping\Driver\XmlDriver';
                        break;
                    default:
                        throw new Moyoki_Doctrine2_Exception('MetadataDriver '.$driverName.' niezaimplementowany');
                }
                
                if (!class_exists($driverName))
                {
                    throw new Moyoki_Doctrine2_Exception('MetadataDriver '.$driverName.' nie istnieje');
                }
                
                if (in_array('Doctrine\Common\Persistence\Mapping\Driver\FileDriver', class_parents($driverName)))
                {
                    if (!isset($options['metadata']['paths']))
                    {
                        throw new Moyoki_Doctrine2_Exception('MetadataDriver bazuje na plikach, ale nie podano prawidłowej ścieżki do plików '.$driverName);
                    }
                    $driver = new $driverName($options['metadata']['paths']);
                }
                
                $config->setMetadataDriverImpl($driver);
            }
        }
    }
    /**
     * 
     * @param array $options
     * @return Doctrine\Common\Cache\Cache
     */
    protected function _setupCache(array $options)
    {
        if(!isset($options['cache'])) {
            throw new Moyoki_Doctrine2_Exception("Nie zdefiniowano klasy dla Doctrine2 Cache.");
        }
        if(!class_exists($options['cache'], true)) {
            throw new Moyoki_Doctrine2_Exception("Klasa '".$options['cache']."' nie istnieje!");
        }
        $cache = new $options['cache'];
        return $cache;   
    }
}
