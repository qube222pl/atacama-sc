<?php

/**
 * @package Moyoki_lib
 *
 * @author Studio Moyoki
 */
class Moyoki_EmailCheck
{

	/**
	 * Sprawdza poprawnosc adresu e-mail
	 * @param string $adres
	 * @return boolean
	 */
	static function check($adres)
	{

		$validator = new Zend_Validate_EmailAddress();
		if ($validator->isValid($adres)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

