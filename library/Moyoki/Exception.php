<?php

/**
 * Exception
 *
 * @author Studio Moyoki
 */
class Moyoki_Exception extends Exception
{

	/**
	 *
	 * @var Moyoki_Exception_Log2Db
	 */
	private static $_TABLE = NULL;

	public static function handle(Exception $e)
	{
		Moyoki_Debug::debug($e);
		if (NULL !== self::$_TABLE) {
			if (is_string(self::$_TABLE) && class_exists(self::$_TABLE)) {
				self::$_TABLE = new self::$_TABLE;
			}
			self::$_TABLE->log($e);
		}
	}

	public static function setLogTable($table)
	{
		if ($table instanceof Moyoki_Exception_Log2Db) {
			self::$_TABLE = $table;
		} else if (is_string($table) && class_exists($table)) {
			self::$_TABLE = $table;
		} else {
			throw new Moyoki_Exception('Nie mozna ustawic tabeli do logowania wyjatkow', E_USER_ERROR);
		}
	}

}
