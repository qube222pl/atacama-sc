<?php

/**
 * Obsluga plikow wszelkiego typu
 *
 * @author Studio Moyoki
 */
class Moyoki_File {

    protected $_name;
    protected $_tmp_name;
    protected $_extension = '';
    protected $_path;
    protected $_dirPath = '.';
    protected $_type;
    protected $_size = 0;
//	protected $_isResizable = false;
    protected $_dozwoloneFormaty = array(
        'application/pdf' => 'pdf',
        'image/png' => 'png',
        'image/jpeg' => 'jpg',
        'image/gif' => 'gif',
        'application/msword' => 'doc',
        'application/vnd.ms-office' => 'xls',
        'application/x-shockwave-flash' => 'swf'
    );

    public function __construct($path = NULL) {
        if ($path) {
            $this->loadFromPath($path);
        }
    }

    public function loadFromPath($path) {
        if (file_exists($path)) {
            $this->_path = $path;
            $this->_name = basename($path);
            $this->_size = filesize($path);
            $this->_dirPath = dirname($path) . DIRECTORY_SEPARATOR; //preg_replace('/' . $this->_name . '$/', '', $path);

            $ext = explode('.', $this->_name);
            if (count($ext) > 1) {
                $this->_extension = $ext[count($ext) - 1];
            }

            switch (strtolower($this->_extension)) {
                case 'pdf' : $this->_type = 'application/pdf';
                    break;
                case 'gif' : $this->_type = 'image/gif';
                    break;
                case 'png' : $this->_type = 'image/png';
                    break;
                case 'jpg' :
                case 'jpeg' : $this->_type = 'image/jpeg';
                    break;
                case 'doc' : $this->_type = 'application/msword';
                    break;
                case 'xls' : $this->_type = 'application/vnd.ms-office';
                    break;
                case 'swf' : $this->_type = 'application/x-shockwave-flash';
                    break;
                default: $this->_type = 'unknown';
            }
        } else {
            throw new Moyoki_Exception("Plik [" . basename($path) . "] nie istnieje", E_USER_ERROR);
        }
    }

    public function getName($withExtension = TRUE) {
        return $withExtension ? $this->_name : preg_replace('/\.' . $this->_extension . '$/', '', $this->_name);
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function getTmpName() {
        return $this->_tmp_name;
    }

    public function setTmpName($_tmp_name) {
        $this->_tmp_name = $_tmp_name;
        return $this;
    }

    public function getPath() {
        return $this->_path;
    }

    public function setPath($path) {
        $this->_path = $path;
        return $this;
    }

    public function getType() {
        return $this->_type;
    }

    /**
     *
     * @param string $type moze przyjac np. image/jpeg, application/msword itd
     * @return Moyoki_File
     */
    public function setType($type) {
        $this->_type = $type;
        return $this;
    }

    public function getSize() {
        return $this->_size;
    }

    public function setSize($size) {
        $this->_size = $size;
        return $this;
    }

    public function getExtension() {
        return $this->_dozwoloneFormaty[$this->getType()];
    }

    function DozwolonyFormat() {
        foreach ($this->_dozwoloneFormaty as $format => $rozsz) {
            if ($format == $this->getType()) {
                return true;
            }
        }
        return false;
    }

    function setFromArray($array) {
        if (is_array($array) && !empty($array)) {
            foreach ($array AS $tab) {
                foreach ($tab as $k => $v) {
                    switch ($k) {
                        case 'name': $this->setName($v);
                            break;
                        case 'type': $this->setType($v);
                            break;
                        case 'size': $this->setSize($v);
                            break;
                        case 'tmp_name': $this->setTmpName($v);
                            break;
                        case 'type': $this->setType($v);
                            break;
                    }
                }
            }
        }
    }

    /**
     * 	Sprawdza czy dodawany plik jest obrazkiem
     * @return bool true - obrazek
     */
    public function isImage() {
        $img = (bool) (isset($this->_dozwoloneFormaty[$this->_type]) && in_array(
                        strtolower($this->_dozwoloneFormaty[$this->_type])
                        , array('jpg', 'png', 'gif'))
                );
        return $img;
    }

    /**
     * Zwraca plik miniatury. Nie moze zwracac sciezki, poniewaz upload jest sciezka serwerowa,
     * natomiast Www jest sciezka do przegladarki
     *
     * UWAGA: wymiary definiuja max szerokosc i wysokosc,
     * czyli miniatura_150x100.jpg moze byc obrazkiem o faktycznych wymiarach 150x20, 100x100, 150x100
     * lub nawet 30x30 jesli zrodlowy plik mial mniejsze rozmiary niz wymagana miniatura
     * Jesli miniatura nie istnieje zostanie utworzona z pliku zrodlowego z zachowaniem proporcji
     * Miniatura jest tworzona w tym samym katalogu co plik zrodlowy z rozszerzeniem nazwaZrodlowa_150x100.jpg/gif/png
     *
     * @param int $width	maksymalna szerokosc miniatury
     * @param int $height	maksymalna wysokosc miniatury
     * @param bool $overwrite nadpisz obecna miniature jesli TRUE
     * @return string nazwa_miniaturki (trzeba dolozyc wczesniej sciezke katalogWww)
     */
    public function getThumbnailFile($width = 100, $height = 100, $overwrite = FALSE) {
        if (!$this->isImage()) {
            throw new Moyoki_Exception('Plik nie jest obrazem', E_USER_ERROR);
        }
        if (!file_exists($this->_path)) {
            throw new Moyoki_Exception('Brak sciezki do pliku', E_USER_ERROR);
        }
        if (!is_numeric($width) || !is_numeric($height) || round($width) <= 0 || round($height) <= 0) {
            throw new Moyoki_Exception('Przekazano bledne parametry', E_USER_ERROR);
        }
        $w = round($width);
        $h = round($height);
        if ($w <= 0 || $h <= 0) {
            throw new Moyoki_Exception('Przekazano bledne parametry', E_USER_ERROR);
        }

        $fileName = $this->getName(FALSE) . '_' . $width . 'x' . $height . '.' . $this->_extension;
        $thumbPath = $this->_dirPath . $fileName;

        if (!file_exists($thumbPath) || $overwrite) {
            try {
                self::resize($this->_path, $thumbPath, $w, $h);
            } catch (Exception $e) {
                Moyoki_Exception::handle($e);
                $thumbPath = '';
                $fileName = '';
            }
        }

        return $fileName;
    }

    /**
     * 	tworzy miniature mieszczaca sie w boxie o wymiarach $wMax x $hMax z zachowaniem proporcji
     * @param string $pathIn sciezka pliku wejsciowego
     * @param string $pathOut sciezka miniatury
     * @param int $wMax maksymalna szerokosc
     * @param int $hMax maksymalna wysokosc
     */
    public static function resize($pathIn, $pathOut, $wMax, $hMax) {
        if (!file_exists($pathIn)) {
            throw new Moyoki_Exception('Brak sciezki do pliku', E_USER_ERROR);
        }
        if (!is_numeric($wMax) || !is_numeric($hMax) || round($wMax) <= 0 || round($hMax) <= 0) {
            throw new Moyoki_Exception('Przekazano bledne parametry', E_USER_ERROR);
        }
        $copyInToOut = FALSE; //miniatura o wymiarach wiekszych niz plik wejsciowy -> powinno byc skopiowanie pliku we na nowa nazwe, a nie przeliczenie
        $w = round($wMax);
        $h = round($hMax);
        if ($w <= 0 || $h <= 0) {
            throw new Moyoki_Exception('Przekazano bledne parametry', E_USER_ERROR);
        }
        $ext = explode('.', basename($pathIn));
        $extension = 'unknown';
        if (count($ext) > 1) {
            $extension = $ext[count($ext) - 1];
        }
        switch (strtolower($extension)) {
            case 'jpeg':
            case 'jpg': $img = @imagecreatefromjpeg($pathIn);
                break;
            case 'gif': $img = @imagecreatefromgif($pathIn);
                break;
            case 'png': $img = @imagecreatefrompng($pathIn);
                break;
            default : throw new Moyoki_Exception('Nieobslugiwany format pliku [' . $extension . ']');
        }

        $imgX = imagesx($img);
        $imgY = imagesy($img);

        $ratioX = $imgX / $w;
        $ratioY = $imgY / $h;

        $ratio = max(array($ratioX, $ratioY));

        if ($ratio > 1) {
            $newX = max(array(round($imgX / $ratio), 1));
            $newY = max(array(round($imgY / $ratio), 1));
        } else {
            $newX = $imgX;
            $newY = $imgY;
            $copyInToOut = TRUE;
        }
        if ($copyInToOut) {
            // jesli wymiary obrazu wejsciowego sa mniejsze niz to wynika ze skalowania to nie skaluj tylko po prostu skopiuj plik wejsciowy w lokalizacje docelowa
            if (file_exists($pathOut)) {
                unlink($pathOut);
            }
            copy($pathIn, $pathOut);
        } else {
            // stworz nowy obraz przeskalowany do wymiarow docelowych
            $thumb = imagecreatetruecolor($newX, $newY);
            //$bg = imagecolorallocate($thumb, 255, 255, 255);
            //imagefilledrectangle($thumb, 0, 0, $newX, $newY, $bg);
            imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newX, $newY, $imgX, $imgY);

            if (file_exists($pathOut)) {
                unlink($pathOut);
            }

            switch (strtolower($extension)) {
                case 'jpeg':
                case 'jpg': imagejpeg($thumb, $pathOut, 90);
                    break;
                case 'gif': imagegif($thumb, $pathOut);
                    break;
                case 'png': imagepng($thumb, $pathOut);
                    break;
            }
        }
    }

    /**
     * Kasuje plik glowny i miniatury, jesli plik posiada miniatury
     */
    public function delete() {
        unlink($this->_path);
        $this->deleteThumbnails();
    }

    /**
     * Kasuje miniatury do pliku
     */
    public function deleteThumbnails() {
        if ($this->isImage()) {
            $files = new DirectoryIterator($this->_dirPath);
            foreach ($files as $k => $f) {
                if (preg_match('/' . $this->getName(FALSE) . '_[0-9]+x[0-9]+/', $f->getFilename())) {
                    unlink($this->_dirPath . $f->getFilename());
                }
            }
        }
    }

}
