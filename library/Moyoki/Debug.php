<?php

/**
 * Zastepuje wyswietlanie wszytkich operacji debugowych
 *
 * @author Studio Moyoki
 */
class Moyoki_Debug {

    public static $ENABLED = FALSE;

    /**
     * Robi print_r lub echo w zaleznosci od typu
     * @param string|array|object co dostanie to wyswietli
     * @param string $msg -> dodatkowy komunikat wyswietlony przed debugiem wlasciwym.
     */
    static function debug($value, $msg = '') {
        if ($msg) {
            echo "<p style='background-color:black;color:white;text-align:center; clear=both;'>$msg</p>";
        }
        if (is_string($value) || is_numeric($value) || is_bool($value) || is_null($value)) {
            echo "<div style='background-color: yellow; color: red; clear=both; '>" . (is_bool($value) ? ($value ? 'TRUE' : 'FALSE') : (is_null($value) ? 'NULL' : $value)) . "</div>";
        } else {
            echo "<pre>";
            print_r($value);
            echo "</pre>";
        }
    }

    /**
     * Robi var_dump
     * @param string|array|object co dostanie to wyswietli
     */
    static function getType($value) {
        if (is_bool($value)) {
            echo "<pre>";
            var_dump($value);
            echo "</pre>";
        } elseif (is_array($value)) {
            echo "<pre>";
            var_dump($value);
            echo "</pre>";
        } elseif (is_numeric($value)) {
            echo 'numeric: ' . $value;
        } elseif (is_string($value)) {
            echo 'string: ' . $value;
        } elseif (strlen(get_class($value)) > 1) {
            echo 'class name: ' . get_class($value);
        }
    }

}
