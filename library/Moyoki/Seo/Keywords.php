<?php

/**
 * Do przyjaznych linkow
 *
 * @author Studio Moyoki
 */
class Moyoki_Seo_Keywords {

    private static $list = array();

    static function generate($limit = 6) {
        self::lista();

        $list = shuffle(self::$list);
        $result = '';

        for ($i = 0; $i < $limit; $i++) {
            $result .= self::$list[$i] . ', ';
        }
        return $result;
    }

    static function lista() {
        self::$list = [
            'atacama',
            'atacama warszawa',
            'agencja reklamowa',
            'gadżety z logo',
            'gadżety reklamowe warszawa',
            'koszulki z nadrukiem',
            'nadruk na miejscu',
            'produkty reklamowe',
            'odzież reklamowa',
            'odzież reklamowa warszawa',
            'odzież promocyjna',
            'odzież promocyjna warszawa',
            'oryginalne gadgety reklamowe',
            'tekstylia reklamowe',
            'upominki personalizowane',
            'upominki reklamowe',
            'upominki reklamowe z nadrukiem',
            'kubki reklamowe',
            'znakowanie',
            'znakowanie odzieży',
            'smycze reklamowe'
        ];
    }

}
