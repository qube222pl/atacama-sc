<?php

/**
 * Nadformularz
 * @author Studio Moyoki
 */
class Moyoki_Form extends Zend_Form {

    public $divElementDecorators = array(
        'ViewHelper',
        array('Errors', array('tag' => 'div', 'class' => 'form-error label label-warning col-12')),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array('Label', array('placement' => 'prepand', 'class' => 'control-label')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group col-12'))
    );
    public $inlineDivElementDecorators = array(
        'ViewHelper',
        array('Errors', array('class' => 'alert alert-danger')),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array('Label', array('placement' => 'prepand', 'class' => 'control-label')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    public $checkboxElementDecorators = array(
        'ViewHelper',
        array('Label', array('class' =>'dupa', 'placement' => 'append', 'escape' => false)),
        'Errors',
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkbox'))
    );
    
    public $checkboxcol4ElementDecorators = array(
        'ViewHelper',
        array('Label', array('class' =>'dupa', 'placement' => 'append', 'escape' => false)),
        'Errors',
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkbox col-4'))
    );
    
    public $divTextareaDecorators = array(
        'ViewHelper',
        array('Errors', array('class' => 'alert alert-danger')),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array('Label', array('placement' => 'prepand', 'class' => 'control-label')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group textarea'))
    );

    /**
     * Uzywany przy ajaxowych importach
     */
    public $fileDecorators = array(
        'File',
        'Errors',
        array('Label', array('placement' => 'prepand', 'class' => 'control-label')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'file-input'))
    );

    /**
     * Uzywany np. przy producentach
     * Pojawia sie nazwa pliku
     */
    public $divElementFileDecorators = array(
        'File',
        array('Errors', array('class' => 'alert alert-danger')),
        array('Description', array('class' => 'help-block', 'placement' => 'appand')),
        array('Label', array('placement' => 'prepand', 'class' => 'control-label')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );

    /*     * public $fileDecorators = array(
      'File',
      'Errors',
      array(array('data' => 'HtmlTag'), array('tag' => 'td')),
      array('Label', array('tag' => 'th')),
      array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
      );
     * 
     */
    public $submitDivElementDecorators = array(
        'ViewHelper',
        'Description',
        'Errors',
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'clearfix submit col-12 text-right'))
    );
    public $inlineSubmitDivElementDecorators = array(
        'ViewHelper',
        'Description',
        'Errors',
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    public $formDecorator = array(
        'FormElements',
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row')),
        'Form'
    );

    public function __construct($options = null) {
        parent::__construct($options);
    }

    public function init() {
        parent::init();
    }

}
