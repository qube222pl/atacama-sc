<?php

/**
 * Interface
 *
 * @author Studio Moyoki
 */
interface Moyoki_Command_Interface
{
	public function execute();
}
