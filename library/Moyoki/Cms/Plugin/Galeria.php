<?php

/**
 * Description of Galeria
 *
 * @author Studio Moyoki
 */
class Moyoki_Cms_Plugin_Galeria implements Moyoki_Cms_Plugin_IPlugin {

    private $_entityManager;
    private $_ID = null;
    private $_ilosc_zdjec = null;
    private $_przycisk = null;
    private $_params = array();

    public function __construct(Doctrine\ORM\EntityManager $entityManager) {
        $this->_entityManager = $entityManager;
    }

    public function params($params) {
        $this->_params = $params['galeria'];
        $this->_ID = $this->_params['id'];
        if (isset($this->_params['ilosc_zdjec']))
            $this->_ilosc_zdjec = $this->_params['ilosc_zdjec'];

        if (isset($this->_params['przycisk_wiecej']))
            $this->_przycisk = $this->_params['przycisk_wiecej'];
    }

    public function output() {
        try {
            $galeria = $galeria = $this->_entityManager->getRepository('Entities\Galeria')->getById($this->_ID);
            if (NULL === $galeria) {
                throw new Exception('Nie ma galerii');
            }

            $zdjecia = $galeria->getGalerieZdjecia();

            $przyciskWiecej = '';
            if (null != $this->_przycisk) {
                $przyciskWiecej = '<a href="link" class="wiecej">--></a>';
            }
            $return = "<div class='galeria'>" . $this->getZdjecia($zdjecia) . " " . $przyciskWiecej . "</div>";
            $return .= "\n<div class='clear'></div>";
            return $return;
        } catch (Exception $e) {
            Moyoki_Debug::debug($e->getMessage());
        }
    }

    private function getZdjecia($zdjecia) {
        $config = Atacama_Config::getInstance();
        $mWidth = $config->zdjecia->rozmiary->m->w;
        $mHeight = $config->zdjecia->rozmiary->m->h;
        $xlWidth = $config->zdjecia->rozmiary->xl->w;
        $xlHeight = $config->zdjecia->rozmiary->xl->h;
        $baseUrl = $config->resources->frontController->baseUrl;

        $zdjeciaString = '';
        $zdjecDodanych = 0;
        foreach ($zdjecia as $z) :
            
            // Jezeli jest ustawiona ilosc zdjec, to sprawdzamy i pokazujemy tylko tyle
            if (NULL === $this->_ilosc_zdjec || (NULL != $this->_ilosc_zdjec && $this->_ilosc_zdjec > $zdjecDodanych)) {
                $zdjeciaString .= "\n<div class=\"col-xs-6 col-md-2\">\n";
                $zdjecDodanych++;

                try {
                    if ($z instanceof \Entities\Galeria_Zdjecie && strlen($z->getPlik()) > 4) {
                        $foto = new Moyoki_File($config->images->path . $z->getPlik());
                        $thumbSrc = $config->images->urlbase . $foto->getThumbnailFile($mWidth, $mHeight);
                        $fullImage = $config->images->urlbase . $foto->getThumbnailFile($xlWidth, $xlHeight);
                    } else {
                        $thumbSrc =  $baseUrl. 'images/nophoto_' . $mWidth . 'x' . $mHeight . '.png';
                    }
                } catch (Exception $e) {
                    $thumbSrc = $baseUrl . 'images/nophoto_' . $mWidth . 'x' . $mHeight . '.png';
                }
                $zdjeciaString .= "<a href='" . $baseUrl . $fullImage . "' "
                        . " class='thumbnail' data-toggle='lightbox' data-lightbox='lightbox'
                        ><img src='" . $baseUrl . $thumbSrc . "' "
                        . "alt='' /></a>";
                $zdjeciaString .= "\n</div>\n";
            }
        endforeach;

        return $zdjeciaString;
    }

}
