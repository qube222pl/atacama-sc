<?php

/**
 * Description of Plugins
 *
 * @author Studio Moyoki
 */
class Moyoki_Cms_Parser {


    public static function init(Doctrine\ORM\EntityManager $entityManager, $content) {
        
        try {
            $pattern = "/\{.*?\}/";
            if (preg_match_all($pattern, $content, $found, PREG_PATTERN_ORDER)) {

                $znalezionePluginy = $found[0];

                for ($i = 0; $i < count($znalezionePluginy); $i++) {
                    $p = json_decode(html_entity_decode($znalezionePluginy[$i] . '}'), TRUE);
                    $nazwaPluginu = ucfirst(key($p));

                    $pluginClass = 'Moyoki_Cms_Plugin_' . $nazwaPluginu;
                    $plugin = new $pluginClass($entityManager);
                    $plugin->params($p);

                    $content = str_ireplace($znalezionePluginy[$i] . '}', $plugin->output(), $content);
                }
            }
        } catch (Exception $e) {
            Moyoki_Debug::debug($e->getMessage(), $e->getFile() . '-' . $e->getCode());
        }
        return $content;
    }

}
