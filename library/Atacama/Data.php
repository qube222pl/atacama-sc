<?php

/**
 * Funkcje pomocne do obslugi dat
 *
 * @author Studio Moyoki
 */
class Atacama_Data {

    static function getDataZaDniRobocze($iloscDni) {
        return date('Y-m-d', strtotime(date('Y-m-d') . ' +' . $iloscDni . ' Weekday'));
    }

}
