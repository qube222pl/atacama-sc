<?php

/**
 * Zapisywanie w tabeli powiadomienia
 *
 * @author Studio Moyoki
 */
class Atacama_Powiadomienie {

    const BRAK_PRZEDZIALU_W_KATEGORII = 10;
    const PROBLEM_Z_DODANIEM_DO_KOSZYKA = 20;
    const NOWA_FIRMA_DODANA = 30;
    const NOWY_UZYTKOWNIK_DODANY = 40;
    const GENEROWANIE_PDF = 50;
    const PRODUKT_BEZ_KOLOROW = 60;
    const BLAD_SYSTEMOWY = 100;

    static function dodaj(Doctrine\ORM\EntityManager $entityManager, $typ, $opis = NULL) {

        try {

            $powiadomienie = new Entities\Powiadomienie();
            $powiadomienie->setPrzeczytane(0)
                    ->setWidoczne(1)
                    ->setTyp($typ);

            if (NULL != $opis) {
                $powiadomienie->setOpis($opis);
            }
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $auth = Zend_Auth::getInstance()->getIdentity();
                $uzytkownik = $entityManager->getRepository('Entities\Uzytkownik')->getById($auth['id']);
                if ($uzytkownik instanceof Entities\Uzytkownik) {
                    $powiadomienie->setUzytkownicy($uzytkownik)
                            ->setUzytkownicyId($uzytkownik->getId());
                }
            }


            $entityManager->persist($powiadomienie);
            $entityManager->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    static function typy() {
        $typy = array(
            self::BRAK_PRZEDZIALU_W_KATEGORII =>
            array('nazwa' => 'Brak przedziału dla ilości',
                'opis' => 'Użytkownik chce dodać produkty do koszyka lub aktulizuje koszyk. Prawdopodobnie brakuje przedziału dla tej kategorii przy takiej ilości.'),
            self::PROBLEM_Z_DODANIEM_DO_KOSZYKA =>
            array('nazwa' => 'Nie można dodać produktu do koszyka',
                'opis' => 'Produkt ma cenę 0 zł, więc nie może zostać dodany'),
            self::PRODUKT_BEZ_KOLOROW =>
            array('nazwa' => 'Produkt bez wskazanych kolorów',
                'opis' => 'Ktoś trafił na produkt, którego nie można dodać do koszyka, bo nie ma wskazanych kolorów.'),
            self::NOWA_FIRMA_DODANA =>
            array('nazwa' => 'Nowa firma dodana przez użytkownika',
                'opis' => 'Ktoś się rejestruje i dodał firmę, która jeszcze nie istnieje'),
            self::NOWY_UZYTKOWNIK_DODANY =>
            array('nazwa' => 'Nowy użytkownik w systemie',
                'opis' => 'Do systemu został dodany nowy użytkownik'),
            self::BLAD_SYSTEMOWY =>
            array('nazwa' => 'Błąd systemowy',
                'opis' => 'Wymaga pilnej reakcji'),
        );

        return $typy;
    }

}
