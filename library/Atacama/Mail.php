<?php

/**
 * Wysylanie e-maili systemowych
 *
 * @author Studio Moyoki
 */
class Atacama_Mail {

    private $_mail;
    private $_lang;
    private $_crypt;
    private $_entityManager;

    public function __construct(Doctrine\ORM\EntityManager $entityManager, $lang = 'pl') {
        $config = Atacama_Config::getInstance();


        $this->_mail = new Zend_Mail('utf-8');
        $this->_mail->setFrom($config->outMail->address)
                ->setReplyTo($config->outMail->address);

        if (APPLICATION_ENV == 'development') {
            $options = array(
                'auth' => 'Crammd5',
                'username' => '8dcd9ab91f16ad',
                'password' => 'b52607b2e647ad',
                'port' => 2525
            );
            $mailTransport = new Zend_Mail_Transport_Smtp('smtp.mailtrap.io', $options);
            $this->_mail->setDefaultTransport($mailTransport);
        }

        $this->_crypt = Moyoki_Crypt::getInstance();

        $this->_entityManager = $entityManager;

        //TODO: Zrobic dobieranie jezyka
        $this->_lang = $lang;
    }

    public function przypomnijHaslo($uzytkownik) {
        if ($uzytkownik instanceof Entities\Uzytkownik) {
            $haslo = new Atacama_Uzytkownik_Haslo($uzytkownik);
            $params = $haslo->generujLink();

            $tresc = '<p>Witaj ' . $uzytkownik->getImie() . ' ' . $uzytkownik->getNazwisko() . ',</p>';
            $tresc .= '<p>Aby nadać sobie nowe hasło kliknij w poniższy link:</p><a href="' . $params['url'] . '">' . $params['url'] . '</a>';
            $tresc .= '<p>Po otwarciu strony wpisz hasło tymczasowe: <b>' . $params['haslo'] . '</b> ';
            $tresc .= 'oraz nadaj sobie nowe hasło.</p>';

            $this->_mail
                    ->addTo($this->_crypt->decrypt($uzytkownik->getEmail()))
                    ->setSubject('www.Atacama.com.pl - hasło')
                    ->setBodyHtml($this->_obudujTresc($tresc))
                    ->send();
        } else {
            throw new Exception('Przekazany zły parametr', 112);
        }
    }

    public function klientNoweZamowienie($adres, $temat, $tresc, $plik = null) {

        $this->_mail
                ->addTo($adres)
                ->setSubject($temat)
                ->setBodyHtml($this->_obudujTresc($tresc));

        if ($plik != null) {
            $zalacznik = $this->_utworzZalacznik($plik);
            if ($zalacznik != null) {
                $this->_mail->addAttachment($zalacznik);
            }
        }

        try {
            $this->_mail->send();
        } catch (Exception $ex) {
            Atacama_Log::dodaj($this->_entityManager, Atacama_Log::BLAD_SYSTEMU, $ex->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    public function klientZamowienieZrealizowane($adres, $temat, $tresc, $plik = null) {

        $this->_mail
                ->addTo($adres)
                ->setSubject($temat)
                ->setBodyHtml($this->_obudujTresc($tresc));

        if ($plik != null) {
            $zalacznik = $this->_utworzZalacznik($plik);
            if ($zalacznik != null) {
                $this->_mail->addAttachment($zalacznik);
            }
        }

        try {
            $this->_mail->send();
        } catch (Exception $ex) {
            Atacama_Log::dodaj($this->_entityManager, Atacama_Log::BLAD_SYSTEMU, $ex->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    public function phNoweZamowienie($adres, $temat, $tresc) {

        $this->_mail
                ->addTo($adres)
                ->setSubject($temat)
                ->setBodyHtml($this->_obudujTresc($tresc));

        if ($plik != null) {
            $zalacznik = $this->_utworzZalacznik($plik);
            if ($zalacznik != null) {
                $this->_mail->addAttachment($zalacznik);
            }
        }

        try {
            $this->_mail->send();
        } catch (Exception $ex) {
            Atacama_Log::dodaj($this->_entityManager, Atacama_Log::BLAD_SYSTEMU, $ex->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    public function ofertaDlaKlienta($adres, $temat, $tresc, $plik = null) {
        $this->_mail
                ->addTo($adres)
                ->setSubject($temat)
                ->setBodyHtml($this->_obudujTresc($tresc));

        if ($plik != null) {
            $zalacznik = $this->_utworzZalacznik($plik);
            if ($zalacznik != null) {
                $this->_mail->addAttachment($zalacznik);
            }
        }

        try {
            $this->_mail->send();
        } catch (Exception $ex) {
            Atacama_Log::dodaj($this->_entityManager, Atacama_Log::BLAD_SYSTEMU, $ex->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    public function ZamowienieSpecjalneMailDoPH($adres, $temat, $tresc) {
        $this->_mail
                ->addTo($adres)
                ->setSubject($temat)
                ->setBodyHtml($this->_obudujTresc($tresc));

        try {
            $this->_mail->send();
        } catch (Exception $ex) {
            Atacama_Log::dodaj($this->_entityManager, Atacama_Log::BLAD_SYSTEMU, $ex->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    public function ZamowienieSpecjalneMailDoKlienta($adres, $temat, $tresc) {
        $this->_mail
                ->addTo($adres)
                ->setSubject($temat)
                ->setBodyHtml($this->_obudujTresc($tresc));

        try {
            $this->_mail->send();
        } catch (Exception $ex) {
            Atacama_Log::dodaj($this->_entityManager, Atacama_Log::BLAD_SYSTEMU, $ex->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    private function _obudujTresc($tresc) {
        $conf = Atacama_Config::getInstance();
        $wynik = '<html>
					<head>
					<meta content = "text/html;charset=UTF-8" http-equiv = "Content-Type">
					<title></title>
                                        <link href="http://fonts.googleapis.com/css?family=Monda:400,700&amp;subset=latin,latin-ext" media="screen" rel="stylesheet" type="text/css">
					<style type = "text/css">
					body { background-color: #FFFFFF;}
					table.layout { background-color: #FFF;}
					table.layout td { padding: 8px;
					}
                                        h1 {
                                        font-size: 18px;
                                        font-weight: 400;
                                        }
                                        h2 {
                                        font-size: 16px;
                                        font-weight: 400;
                                        }
					p, div, td, span { font-size: 12px;
					font-family: Monda,Lucida Sans Unicode,Lucida Grande,Lucida Sans,Lucida,sans-serif;
					color: #333333; }
					a { font-size: 12px;
					color: #000000; font-style: normal; 
                                        font-family: Monda,Lucida Sans Unicode,Lucida Grande,Lucida Sans,Lucida,sans-serif; 
                                        }
					a:visited { color: #666666; }
					a:hover { text-decoration: underline;
					}
                                        .text-right {
                                            text-align: right;
                                        }
                                        .text-center {
                                            text-align: center;
                                        }
					table.tabela {border-collapse: collapse;
					}
					table.tabela td { border: 1px solid #e6e6e6; padding: 5px;}
					table.tabela td.noborder { border: 0;
					}
                                        table#koszyk-lista, table#koszyk-podsumowanie {
                                            border-collapse: collapse;
                                            width: 100%;
                                        }
                                        #koszyk-lista th, #koszyk-podsumowanie th {
                                            border: 1px solid #FFF;
                                            background-color: #F9F9F9;
                                            color: #000;
                                            font-size: 12px;
                                            font-weight: 400;
                                            padding: 4px;
                                        }
                                        #koszyk-lista td, #koszyk-podsumowanie td {
                                            border-bottom: 1px solid #DDD;
                                            color: #333;
                                            padding: 4px;
                                            
                                        }
                                        #koszyk-adresy {
                                            margin: 20px 0px;
                                        }
                                        #koszyk-adresy .adres {
                                            float: left;
                                            margin: 3px;
                                            width: 31%;
                                        }
                                        #koszyk-adresy h3 {
                                            border-bottom: 1px solid #00878F;
                                            font-weight: 400;
                                            font-size: 18px;
                                        }
                                        
                                        #termin-dostawy .data {
                                            border: 1px dotted #333;
                                            font-size: 24px;
                                            font-weight: bold;
                                            padding: 5px;
                                            width: 350px;
                                        }

                                        #termin-dostawy .data-opis {
                                            border: 1px dotted #333;
                                            font-weight: bold;
                                            padding: 5px;
                                            width: 350px;
                                        }
                                        
					</style>
					</head><body>
					<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="layout">
					<tr>
					<td><img src = "' . $conf->appUrl . 'images/mail-naglowek.png" alt="ATACAMA S.C." /></td>
					</tr>
					<tr>
					<td>&nbsp;
					</td>
					</tr>
					<tr>
					<td>' . $tresc . '</td>
					</tr>
					<tr>
					<td>&nbsp;
					</td>
					</tr>
					</table>';
        return $wynik;
    }

    private function _utworzZalacznik($plik) {

        if (!file_exists($plik)) {
            return null;
        }

        $content = file_get_contents($plik);
        if ($content === false) {
            return null;
        }

        $attachment = new Zend_Mime_Part($content);
        $attachment->type = 'application/pdf';
        $attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
        $attachment->encoding = Zend_Mime::ENCODING_BASE64;
        $attachment->filename = basename($plik);
        return($attachment);
    }

}
