<?php

/**
 * Description of Adapter
 *
 * @author Studio Moyoki
 */
class Atacama_Auth_Adapter implements Zend_Auth_Adapter_Interface {

    const BRAK_UZYTKOWNIKA = 1;
    const ZLE_HASLO = 2;

    public $entityManager;
    protected $login = null;
    protected $haslo = null;
    public $uzytkownik = null;

    public function __construct(Doctrine\ORM\EntityManager $entityManager, $login, $haslo) {
        $this->entityManager = $entityManager;
        $this->login = $login;
        $this->haslo = $haslo;
    }

    public function authenticate() {
        try {
            $this->uzytkownik = $this->entityManager->getRepository('Entities\Uzytkownik')->logowanie($this->login);
            if ($this->uzytkownik instanceof Entities\Uzytkownik) {

                if (md5($this->haslo) === $this->uzytkownik->gethaslo()) {
                    $sukces = 1;
                } else {
                    throw new \Exception(self::ZLE_HASLO);
                }
            } else {
                throw new \Exception(self::BRAK_UZYTKOWNIKA);
            }
            return $this->resultTworzymy(Zend_Auth_Result::SUCCESS);
        } catch (Exception $e) {

            if ($e->getMessage() == self::BRAK_UZYTKOWNIKA)
                return $this->resultTworzymy(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND, array('Brak użytkownika lub złe hasło'));

            if ($e->getMessage() == self::ZLE_HASLO)
                return $this->resultTworzymy(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID, array('Brak użytkownika lub złe hasło.'));
        }
    }

    private function resultTworzymy($kod, $wiadomosci = array()) {

        return new Zend_Auth_Result($kod, $this->uzytkownik, $wiadomosci);
    }

}
