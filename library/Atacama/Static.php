<?php

/**
 * Description of Static
 */
class Atacama_Static {

    /**
     * Korporacyjni firma typ budzetu 
     */
    
    const KORPORACYJNI_FIRMA_BUDZET_MIESIECZNY = 1;
    const KORPORACYJNI_FIRMA_BUDZET_KWARTALNY = 2;
    
    /**
     * Korporacyjni produkt powiadomienia 
     */
    
    const KORPORACYJNI_POWIADOMIENIA_NIEWYSLANE = 0;
    const KORPORACYJNI_POWIADOMIENIA_WYSLANE = 1;
    
    
    /**
     * Korporacyjni zamowienie 
     */
    const KORPORACYJNI_ZAMOWIENIE_NOWE = 1;
    const KORPORACYJNI_ZAMOWIENIE_ZREALIZOWANE = 2;
    const KORPORACYJNI_ZAMOWIENIE_ANULOWANE = 5;

    /**
     * Korporacyjni zamowienie 
     */
    const KORPORACYJNI_POWIADOMIENIE_TYP_NOWE_ZAMOWIENIE = 1;
    const KORPORACYJNI_POWIADOMIENIE_TYP_BRAK_PRODUKTU = 2;
    

    /**
     * Korporacyjni uzytkownicy
     */
    const KORPORACYJNI_UZYTKOWNICY_STATUS_AKTYWNY = 1;
    const KORPORACYJNI_UZYTKOWNICY_STATUS_USUNIETY = 0;
    
    const KORPORACYJNI_UZYTKOWNICY_ROLA_KLIENT = 1;
    const KORPORACYJNI_UZYTKOWNICY_ROLA_KOORDYNATOR = 5;
    

}
