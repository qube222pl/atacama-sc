<?php

/**
 * Zapisywanie do loga
 *
 * @author Studio Moyoki
 */
class Atacama_Log {

    const LOGOWANIE = 10;
    const REJESTRACJA = 20;
    const PRZYPOMNIENIE_HASLA = 30;
    const ZAMOWIENIE_ZLOZONE = 40;
    const GENEROWANIE_PDF = 50;
    const ZAMOWIENIE_EDYCJA = 60;
    const AKTUALIZACJA_CEN_MINIMALNYCH = 70;
    const WYSZUKIWANIE_ZAAWANSOWANE = 80;
    const WYDAJNOSC = 90;
    const WYDAJNOSC_POBIERANIE_NASTEPNEGO_PRODUKTU_W_KATEGORII = 91;
    const BLAD_SYSTEMU = 999;

    static function dodaj(Doctrine\ORM\EntityManager $entityManager, $typ, $opis = NULL, $rodzic_id = NULL) {

        try {
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $auth = Zend_Auth::getInstance()->getIdentity();
                $user_id = $auth['id'];
            } else {
                $user_id = NULL;
            }
            $log = new Entities\Log();
            $log->setUserId($user_id)
                    ->setTyp($typ);
            if (NULL != $opis) {
                $log->setOpis($opis);
            }


            $entityManager->persist($log);
            $entityManager->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
