<?php

/**
 * Acl dla calosci
 * Zawiera tablice wszystkich zasobow i uprawnien
 *
 * @author Studio Moyoki
 * Aby poprawnie dodac zasob trzeba zrobic jest CONST odpowiednik, aby poprawnie sie do niego odwolac
 * i dopisac, do _zasobyArray
 */
class Atacama_Acl extends Zend_Acl {

    const ROLA_NIEZALOGOWANY = 0;
    const ROLA_KLIENT = 5;
    const ROLA_PH = 10;
    const ROLA_REDAKTOR = 15;
    const ROLA_ADMINISTRATOR = 20;
    const ZAS_ADMINISTRACJA_OGOLNA = 'administracja';
    const ZAS_ADMINISTRACJA_PRODUKTY = 'produkty';
    const ZAS_ADMINISTRACJA_STRONY = 'administracja stronami';
    const ZAS_ADMINISTRACJA_FIRMY = 'administracja firmami';
    const ZAS_ADMINISTRACJA_UZYTKOWNICY = 'administracja uzytkownikami';
    const ZAS_ADMINISTRACJA_ZAMOWIENIA_WSZYSTKIE = 'podglad wszystkich zamowien';
    const ZAS_ADMINISTRACJA_MARZA_HANDLOWCOW = 'marza wszystkich handlowcow';
    const ZAS_PRODUKT_CENNA = 'cennik_klienta';
    const ZAS_STANY_MAGAZYNOWE = 'stany_magazykowe';
    const ZAS_KLIENT_DOSTEP = 'dostep dla klienta';
    const ZAS_KORPORACYJNY_KOSZYK = 'korp. koszyk';
    const ZAS_KORPORACYJNY_KOSZYK_PH = 'korp. zarzadzanie';
    const ZAS_KOSZYK = 'koszyk';
    const ZAS_KOSZYK_KLIENT = 'koszyk za klienta';
    const ZAS_KOSZYK_MARZA = 'koszyk marza dla PH';
    const ZAS_GENERUJ_OFERTE = 'ph moze generowac oferte';
    const ZAS_RAPORTY = 'raporty';
    const ZAS_ZAMOWIENIA_PODGLAD = 'podglad zamowien';
    const ZAS_ZAMOWIENIA_EDYCJA = 'edycja zamowien';
    const ZAS_ZAMOWIENIA_MARZA = 'widok marzy';
    const ZAS_WIDOK_DLA_PH = 'to widzi ph i wyzej';
    const PARAM_KOSZYK_ID = 1;
    const PARAM_USER_ID = 2;
    const STATUS_KOSZYK = 0;
    const STATUS_WYSLANE = 1; // Wyslana oferta do Klienta
    const STATUS_PRZYJETE = 2; // Przyjete do realizacji - zatwierdzone przez Klienta
    const STATUS_POTWIERDZONE = 3;
    const STATUS_ZREALIZOWANE = 4;
    const STATUS_ANULOWANE = 5;
    const STATUS_OFERTA_TWORZENIE = 10;
    const STATUS_OFERTA = 11; // To chyba nie jest ustawiane
    const STATUS_PRZETARG = 15;

    public static $_wszystkieRole = array(
        self::ROLA_NIEZALOGOWANY => 'Niezalogowany',
        self::ROLA_KLIENT => 'Klient',
        self::ROLA_PH => 'Opiekun klienta',
        self::ROLA_REDAKTOR => 'Redaktor',
        self::ROLA_ADMINISTRATOR => 'Administrator systemu'
    );
    private $_zasobyArray = array(
        array(
            'zasob' => self::ZAS_PRODUKT_CENNA,
            'rola' => self::ROLA_KLIENT),
        array(
            'zasob' => self::ZAS_STANY_MAGAZYNOWE,
            'rola' => self::ROLA_KLIENT),
        array(
            'zasob' => self::ZAS_KOSZYK,
            'rola' => self::ROLA_KLIENT),
        array(
            'zasob' => self::ZAS_KLIENT_DOSTEP,
            'rola' => self::ROLA_KLIENT),
        array(
            'zasob' => self::ZAS_KORPORACYJNY_KOSZYK,
            'rola' => self::ROLA_KLIENT),
        array(
            'zasob' => self::ZAS_WIDOK_DLA_PH,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_KOSZYK_KLIENT,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_KOSZYK_MARZA,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_KORPORACYJNY_KOSZYK_PH,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_ZAMOWIENIA_PODGLAD,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_ZAMOWIENIA_EDYCJA,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_ZAMOWIENIA_MARZA,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_GENERUJ_OFERTE,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_ADMINISTRACJA_OGOLNA,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_ADMINISTRACJA_FIRMY,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_ADMINISTRACJA_UZYTKOWNICY,
            'rola' => self::ROLA_PH),
        array(
            'zasob' => self::ZAS_ADMINISTRACJA_PRODUKTY,
            'rola' => self::ROLA_PH), //'rola' => self::ROLA_ADMINISTRATOR),
        array(
            'zasob' => self::ZAS_ADMINISTRACJA_STRONY,
            'rola' => self::ROLA_PH), //'rola' => self::ROLA_ADMINISTRATOR),
        array(
            'zasob' => self::ZAS_RAPORTY,
            'rola' => self::ROLA_ADMINISTRATOR),
        array(
            'zasob' => self::ZAS_ADMINISTRACJA_ZAMOWIENIA_WSZYSTKIE,
            'rola' => self::ROLA_ADMINISTRATOR),
        array(
            'zasob' => self::ZAS_ADMINISTRACJA_MARZA_HANDLOWCOW,
            'rola' => self::ROLA_ADMINISTRATOR),
    );
    protected static $_instance = null;
    protected static $_user = null;

    public function __construct() {
        $this->pobierzUzytkownika();

        $this->addRole(new Zend_Acl_Role(self::ROLA_NIEZALOGOWANY));
        $this->addRole(new Zend_Acl_Role(self::ROLA_KLIENT), self::ROLA_NIEZALOGOWANY);
        $this->addRole(new Zend_Acl_Role(self::ROLA_PH), self::ROLA_KLIENT);
        $this->addRole(new Zend_Acl_Role(self::ROLA_REDAKTOR), self::ROLA_PH);
        $this->addRole(new Zend_Acl_Role(self::ROLA_ADMINISTRATOR), self::ROLA_REDAKTOR);

        $this->ustawWszystkieZasobyIUprawnienia();
    }

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function sprawdzDostep($zasobNazwa) {

        $zasobNazwa = strtolower(trim($zasobNazwa));
        if (is_array(self::$_user)) {
            $rolaID = self::$_user['rola'];
        } else {
            $rolaID = 0;
        }

        if ($this->isAllowed($rolaID, $zasobNazwa) == TRUE)
            return true;

        return false;
    }

    private function pobierzUzytkownika() {
        if (null === self::$_user) {
            self::$_user = Zend_Auth::getInstance()->getIdentity();
        }
    }

    private function ustawWszystkieZasobyIUprawnienia() {
        for ($i = 0; $i < count($this->_zasobyArray); $i++) {
            if (!$this->has($this->_zasobyArray[$i]['zasob'])) {
                $this->addResource($this->_zasobyArray[$i]['zasob']);
            }
            $this->allow($this->_zasobyArray[$i]['rola'], $this->_zasobyArray[$i]['zasob']);
        }
    }

}
