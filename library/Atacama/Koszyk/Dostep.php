<?php

class Atacama_Koszyk_Dostep {

    const PARAM_KOSZYK_ID = 'koszID';
    const PARAM_USER_ID = 'userID';
    const PARAM_KOSZYK_OBJ = 'koszOBJ';

    public $em;
    protected $user_rola = null;
    protected $koszyk = null;
    protected static $_instance = null;

    public function __construct() {
        
    }

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function sprawdzDostep($params, \Doctrine\ORM\EntityManager $em) {
        //Moyoki_Debug::debug($params, __METHOD__);

        if (is_array($params) && (key_exists(self::PARAM_KOSZYK_ID, $params) || key_exists(self::PARAM_KOSZYK_OBJ, $params))) {

            if (key_exists(self::PARAM_KOSZYK_ID, $params)) {
                $this->koszyk = $em->getRepository('Entities\Koszyk')->getById($params[self::PARAM_KOSZYK_ID]);
            }
            if (key_exists(self::PARAM_KOSZYK_OBJ, $params)) {
                $this->koszyk = $params[self::PARAM_KOSZYK_OBJ];
            }

            if ($this->koszyk instanceof Entities\Koszyk) {

                $user = Zend_Auth::getInstance()->getIdentity();

                $this->user_rola = $user['rola'];
                //Moyoki_Debug::debug($user, __METHOD__);
                /**
                 * W zaleznosci od roli mozemy sprawdzic o co chodzi z tym konkretnym koszykiem
                 */
                /**
                 * User jako KLIENT
                 */
                if ($user['rola'] == Atacama_Acl::ROLA_KLIENT) {
                    if ($this->koszyk->getUzytkownicyId() == $user['id'] && ($this->koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA || $this->koszyk->getStatus() == Atacama_Acl::STATUS_PRZETARG || $this->koszyk->getStatus() == Atacama_Acl::STATUS_KOSZYK || $this->koszyk->getStatus() == Atacama_Acl::STATUS_WYSLANE)) {
                        //Moyoki_Debug::debug('UID:' . $this->koszyk->getUzytkownicyId() . ', stat.: ' . $this->koszyk->getStatus(), __METHOD__ . ': Jest dostep');exit;
                        return TRUE;
                    } else {
                        //Moyoki_Debug::debug('UID:' . $this->koszyk->getUzytkownicyId() . ', stat.: ' . $this->koszyk->getStatus(), __METHOD__ . ': Odmowa');exit;
                        return FALSE;
                    }
                }

                /**
                 * User jako PH
                 */
                if ($user['rola'] == Atacama_Acl::ROLA_PH) {
                    //$koszyk = new Entities\Koszyk;
                    /**
                     * 1. koszyk i PH jest userem
                     * 2. Oferta i PH jest PH firmy
                     */
                    if (($this->koszyk->getStatus() == Atacama_Acl::STATUS_KOSZYK && $this->koszyk->getUzytkownicyId() == $user['id']) || ($this->koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA_TWORZENIE && $this->koszyk->getFirmy()->getPhId() == $user['id']) || ($this->koszyk->getStatus() == Atacama_Acl::STATUS_OFERTA && $this->koszyk->getFirmy()->getPhId() == $user['id'])) {
                        //Moyoki_Debug::debug('UID:' . $this->koszyk->getUzytkownicyId() . ', stat.: ' . $this->koszyk->getStatus(), __METHOD__ . ': Jest dostep');
                        return TRUE;
                    } else {
                        Moyoki_Debug::debug('KoszykUserID:' . $this->koszyk->getUzytkownicyId()
                                . ', stat.: ' . $this->koszyk->getStatus()
                                . ', id: ' . $this->koszyk->getId()
                                . ', FirmaId: ' . $this->koszyk->getFirmy()->getId()
                                . ', FirmaNazwa: ' . $this->koszyk->getFirmy()->getNazwa()
                                . ', FirmaPhId: ' . $this->koszyk->getFirmy()->getPhId()
                                . ', loggedUserId: ' . $user['id'], __METHOD__ . ': Odmowa');
                        return FALSE;
                    }
                }
                if ($user['rola'] == Atacama_Acl::ROLA_ADMINISTRATOR) {
                    return TRUE;
                }
            } else {
                /**
                 * Nie ma takiego koszyka
                 */
            }
        } else {
            /**
             * Niepoprawny parametr
             */
        }

        return TRUE;
    }

    public
            function getRola() {
        if ($this->user_rola != null) {
            return $this->user_rola;
        }
    }

    public
            function getKoszyk() {
        if ($this->koszyk instanceof Entities\Koszyk) {
            return $this->koszyk;
        }
    }

}
