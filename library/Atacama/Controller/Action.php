<?php

/**
 * @author Studio Moyoki
 */
abstract class Atacama_Controller_Action extends Zend_Controller_Action {

    protected $_entityManager = null;
    protected $_acl = null;
    protected $_lang = null;
    protected $_user = null;
    protected $_messages = null;
    protected $_errors = null;
    protected $_success = null;
    protected $_infos = null;
    public function init() {
        parent::init();
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->_entityManager = $bootstrap->getResource('Entitymanagerfactory');

        $this->_acl = Atacama_Acl::getInstance();
        $this->_lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('language');

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_user = Zend_Auth::getInstance()->getIdentity();
        }
        
        $this->view->displayContainer = TRUE;
    }

    public function preDispatch() {
        parent::preDispatch();
        $this->_messages = $this->_helper->getHelper('FlashMessenger');
        $this->_errors = $this->_messages->setNamespace('error')->getMessages();
        $this->_success = $this->_messages->setNamespace('success')->getMessages();
        $this->_infos = $this->_messages->setNamespace('info')->getMessages();
        
    }

    public function postDispatch() {
        parent::postDispatch();

        foreach ($this->_errors as $m) {
            $this->addMessageError($m);
        }
        foreach ($this->_success as $m) {
            $this->addMessageSuccess($m);
        }
        foreach ($this->_infos as $m) {
            $this->addMessageInfo($m);
        }
    }

    protected function addMessageSuccess($m, $delayed = false) {
        $this->addMessage($m, 'success', $delayed);
    }

    /**
     *
     * @param string Tresc do wyświetlenia
     * @param bool $delayed
     */
    protected function addMessageInfo($m, $delayed = false) {
        $this->addMessage($m, 'info', $delayed);
    }

    /**
     *
     * @param string Tresc do wyświetlenia
     * @param bool $delayed
     */
    protected function addMessageError($m, $delayed = false) {
        $this->addMessage($m, 'error', $delayed);
    }

    protected function addMessage($m, $type = 'info', $delayed = false) {

        if ($delayed) {
            $this->_messages->setNamespace($type)->addMessage($m);
        } else {
            if (!is_array($this->view->messages)) {
                $this->view->messages = array();
            }
            array_push($this->view->messages, array('status' => $type, 'message' => $m));
        }
    }

}
