<?php

/**
 * Description of Atacama_Controller_Plugin_Multilanguage
 */

class Atacama_Controller_Plugin_Multilanguage extends Zend_Controller_Plugin_Abstract 
{
    
    public function routeShutdown (Zend_Controller_Request_Abstract $request)
    {
        /*
         * In the routeStartup method, we add a quick fix to be sure the language is alway in each RequestUri. If the language parameter is not there, we force it.
         */
        
        $config = Atacama_Config::getInstance();
        
        if (substr($request->getRequestUri(), 0, -1) == $request->getBaseUrl()){
            $request->setRequestUri($request->getRequestUri()."pl"."/");
            $request->setParam("language", $config->resources->translate->locale);
        }
        
        $language = $request->getParam('language');
                
        $translate = new Zend_Translate(array(
            'adapter' => $config->resources->translate->adapter,
            'content' => $config->resources->translate->content,
            'scan' => $config->resources->translate->scan,
            'disableNotices' => $config->resources->translate->disableNotices,
            'logUntranslated' => $config->resources->translate->logUntranslated,
            'locale'  => $language,
        ));
       
        Zend_Registry::set($config->resources->translate->registry_key, $translate);
    }
}