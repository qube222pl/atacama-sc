<?php

/**
 * Metoda statyczna do pobierana określonego widgeta
 *
 * @author Studio Moyoki
 */
class Atacama_Widget
{

	static function wyswietl(Doctrine\ORM\EntityManager $entityManager, $kod, $lang = 'pl')
	{
		$widget = $entityManager->getRepository('Entities\Widget')->pobierzPoKodzie($kod, $lang);

		if ($widget instanceof Entities\Widget) {
			echo $widget->getTresc();
		} else {
			echo 'Brak widgeta !!!!';
		}
	}

}
