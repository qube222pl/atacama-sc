<?php

/**
 * Description of ZFDebugDoctrineProfiler
 *
 */
namespace Atacama\Doctrine;

class ZFDebugDoctrineProfiler implements \Doctrine\DBAL\Logging\SQLLogger 
 {
    public $totalTime = 0.0;
    public $queries = array();

    protected $_curQuery = null;

    public function startQuery($sql, array $params = null, array $types = null)
    {
        $this->_curQuery = new \stdClass();
        $this->_curQuery->sql = $sql;
        $this->_curQuery->params = $params;
        $this->_curQuery->time = \microtime(true);
    }

    public function stopQuery()
    {
        $executionTime = \microtime(true) - $this->_curQuery->time;
        $this->totalTime += $executionTime;
        $this->queries[] = array(
            'time'   => $executionTime,
            'sql'    => $this->_curQuery->sql,
            'params' => $this->_curQuery->params
        );
    }
}

?>
