<?php

/**
 * Klasa pomocnicza do prezentacji modeli (damski, meski, unisex)
 *
 * @author Studio Moyoki
 */
class Atacama_Produkt_Rodzaj
{

	static function pokaz($rodzaj)
	{

		switch ($rodzaj) {
			case '1': $wynik = 'damski';
				break;
			case '2': $wynik = 'meski';
				break;
			case '3': $wynik = 'unisex';
				break;
		}
		return $wynik;
	}

	static function toArray()
	{
		$tab = array('1' => 'damski',
			'2' => 'męski',
			'3' => 'unisex');

		return $tab;
	}

}

