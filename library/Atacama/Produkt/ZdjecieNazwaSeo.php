<?php

/**
 * Klasa pomocnicza do zmiany nazwy zdjecia
 *
 * @author Studio Moyoki
 */
class Atacama_Produkt_ZdjecieNazwaSeo {

    static function zmien($em, Entities\Produkt_Zdjecie $zdjecie, $nazwaProduktu) {

        if ($zdjecie->getNazwaZmieniona() != 1) {
            $config = Atacama_Config::getInstance();
            $path = $config->zdjecia->path;

            self::usunMiniatury($path . $zdjecie->getPlik());

            $basenameAndExtension = explode('.', $zdjecie->getPlik());
            $ext = strtolower(end($basenameAndExtension));
            $nowaNazwa = $zdjecie->getProdukty()->getDystrybutorzyId() . '/' . strtolower(Moyoki_Friendly::name('atacama-' . $nazwaProduktu) . '-' . $zdjecie->getId() . '-' . rand(40, 299) . '-' . rand(0, 99) . '.' . $ext);

            if (file_exists($path . '/' . $zdjecie->getPlik()) && rename($path . '/' . $zdjecie->getPlik(), $path . '/' . $nowaNazwa)) {
                $zdjecie->setPlik($nowaNazwa)
                        ->setNazwaZmieniona(1);

                $em->persist($zdjecie);
                $em->flush();
            }
        }
        return $zdjecie;
    }

    static private function usunMiniatury($path) {
        if (file_exists($path)) {
            $moyoki_file = new Moyoki_File($path);
            $moyoki_file->deleteThumbnails();
        }
    }

}
