<?php

/**
 * Klasa pomocnicza prezentacji i zapisu kwot
 *
 * @author Studio Moyoki
 */
class Atacama_Produkt_Kwota {

    static function zapis($string) {

        $kwota = str_replace(',', '.', $string);

        return $kwota;
    }

    static function odczyt($kwota, $waluta = null) {

        $kwota = str_replace(',', '.', $kwota);
        $string = number_format(doubleval($kwota), 2, ',', ' ');

        if (null != $waluta)
            $string = $string . '&nbsp;' . $waluta;

        return $string;
    }

}
