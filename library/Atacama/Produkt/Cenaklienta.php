<?php

/**
 * Klasa pomocnicza do prezentacji ceny dla klienta z upustami, promocjami itd
 *
 * @author Studio Moyoki
 */
class Atacama_Produkt_Cenaklienta {

    public $em = null;
    public $user = NULL;
    public $kategoriaID = null;
    public $detalObj = null;
    public $ilosc = null;

    public function __construct(Doctrine\ORM\EntityManager $em, Entities\Produkt_Detal $detal_id, $kategoria_id, $ilosc) {

        $this->em = $em;
        $this->user = \Zend_Auth::getInstance()->getIdentity();
        $this->kategoriaID = (int) $kategoria_id;
        $this->detalObj = $detal_id;
        $this->ilosc = (int) $ilosc;
    }

    public function wylicz() {

        //$zakupCena = NULL; // Cena zakupu
        $cena = NULL;
        $promoCena = NULL;
        $wypCena = NULL;
        $error = NULL;

        if ($this->detalObj->getWyp() === NULL || $this->detalObj->getWyp() == '0.00') {
            /**
             * Wyprzedazy brak
             * Pobieram firme i procenty w kategorii
             */
            $kategoriaID = $this->kategoriaID;
            $jestWyprzedaz = FALSE;
        } else {
            /**
             * Wyprzedaz
             * Pobieram ID kategorii domyslnej dla wyprzedazy
             */
            $conf = Atacama_Config::getInstance();
            $kategoriaID = $conf->kategoriaWyprzedaz->id;
            $jestWyprzedaz = TRUE;
        }
        $kategoria_cenaOBJ = $this->em->getRepository('Entities\Kategoria_Cena')->getProcentByKategoriaIdAndIlosc($kategoriaID, $this->ilosc);
        $firmaOBJ = $this->em->getRepository('Entities\Firma')->getById($this->user['firma_id']);

        //$kategoria_cenaOBJ = new Entities\Kategoria_Cena;
        //$firmaOBJ = new Entities\Firma;

        if ($kategoria_cenaOBJ instanceof Entities\Kategoria_Cena && $firmaOBJ instanceof Entities\Firma) {

            if ($jestWyprzedaz) {
                $procentWyprzedaz = $kategoria_cenaOBJ->getProcentRegularna();
                $wypCena = $this->detalObj->getWyp() + ($this->detalObj->getWyp() * ($procentWyprzedaz / 100));
            } else {
                $procentRegularna = $kategoria_cenaOBJ->getProcentRegularna() - $firmaOBJ->getUpust();
                $procentPromocja = $kategoria_cenaOBJ->getProcentPromocja() - $firmaOBJ->getUpust();

                $cena = $this->detalObj->getCena() + ($this->detalObj->getCena() * ($procentRegularna / 100));

                if ($this->detalObj->getPromo() != NULL && $this->detalObj->getPromo() != '0.00') {
                    $promoCena = $this->detalObj->getPromo() + ($this->detalObj->getPromo() * ($procentPromocja / 100));
                }
            }
        } else {
            if (!$kategoria_cenaOBJ instanceof Entities\Kategoria_Cena) {
                $kategoria = $this->em->getRepository('Entities\Kategoria')->getById($this->kategoriaID);
                $result = array(
                    'Ilość sztuk dodawanych do koszyka' => $this->ilosc,
                    'Produkt w kategorii' => $kategoria->getKategorieI18n()->first()->getNazwa()
                );
                Atacama_Powiadomienie::dodaj($this->em, Atacama_Powiadomienie::BRAK_PRZEDZIALU_W_KATEGORII, json_encode($result));
            }
            $error = 'kategoria lub firma error';
        }

        /**
         * Zwrotka wyniku
         */
        if ($error) {
            return $error;
        } else {
            return array('cenaRegularna' => number_format($cena, 2, '.', ''),
                'cenaPromocyjna' => number_format($promoCena, 2, '.', ''),
                'cenaWyprzedaz' => number_format($wypCena, 2, '.', '')
            );
        }
    }

    /**
     * Najnizsza mozliwa cena dla produktu w ogolnym podgladzie produktu
     */
    public function najnizszaCena($upust = null) {
        
        if ($this->detalObj->getWyp() != NULL && $this->detalObj->getWyp() != '0.00') {
            $conf = Atacama_Config::getInstance();
            $kat = $conf->kategoriaWyprzedaz->id;
        } else if (0 === $this->kategoriaID) {
            $kategorieRodzice = $this->em->getRepository('Entities\Kategoria')->pobierzRodzicow($this->detalObj->getProdukty()->getKategoriaGlowna());
            if(!empty($kategorieRodzice)) {
            $kat = (int) $kategorieRodzice[0]->getId();
            } else {
                $kat = 1;
            }
        } else {
            $kat = (int) $this->kategoriaID;
        }
        try {
            $kategoria_cenaOBJ = $this->em->getRepository('Entities\Kategoria_Cena')->getNajnizszyProcentByKategoriaId($kat);
            if (!$kategoria_cenaOBJ instanceof Entities\Kategoria_Cena) {
                Atacama_Log::dodaj($this->em, Atacama_Log::BLAD_SYSTEMU, __METHOD__ . ': Nie mozna pobrać najniższej ceny dla przedziału kategorii: ' . $kat);
                return false;
            }
        } catch (Exception $exc) {
            Moyoki_Debug::debug($exc->getMessage(), __METHOD__);
            return false;
        }



        /**
         * Mamy przedzil upustu wg kategorii, wiec mozemy wyliczyc najnizsza mozliwa cene produktu
         */
        if ($this->detalObj->getWyp() != NULL && $this->detalObj->getWyp() != '0.00') {
            /**
             * Jest w wyprzedazy
             */
            $procentWyprzedaz = $kategoria_cenaOBJ->getProcentRegularna();
            $cena = $this->detalObj->getWyp() + ($this->detalObj->getWyp() * ($procentWyprzedaz / 100));
        } else {
            /**
             * Produkt nie jest w wyprzedazy
             */
            if (NULL === $upust) {
                $firmaOBJ = $this->em->getRepository('Entities\Firma')->getById($this->user['firma_id']);

                $upust = $firmaOBJ->getUpust();
            } else {
                $upust = 0;
            }
            if ($this->detalObj->getPromo() != NULL && $this->detalObj->getPromo() != '0.00') {
                /**
                 * Produkt w promocji
                 */
                $procentPromocja = $kategoria_cenaOBJ->getProcentPromocja() - $upust;
                $cena = $this->detalObj->getPromo() + ($this->detalObj->getPromo() * ($procentPromocja / 100));
            } else {
                /**
                 * Produkt w cenie regularnej
                 */
                $procentRegularna = $kategoria_cenaOBJ->getProcentRegularna() - $upust;
                $cena = $this->detalObj->getCena() + ($this->detalObj->getCena() * ($procentRegularna / 100));
            }
        }

        return $cena;
    }

}
