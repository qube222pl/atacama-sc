<?php

/**
 * Budzet uzytkownika
 *
 * @author Studio Moyoki
 */
class Atacama_Uzytkownik_Budzet {

    public $em;
    private $_user;
    private $_budzet = 0;

    public function __construct(Doctrine\ORM\EntityManager $entityManager, Entities\Korporacyjny_Uzytkownik $uzytkownik) {
        $this->em = $entityManager;
        $this->_user = $uzytkownik;
    }

    public function getDostepnyBudzetMiesieczny() {
        if (null == $this->_budzet) {
            $this->_budzet = $this->_user->getBudzet();
        }
        return $this->_budzet - $this->_budzetMiesieczny(date('y'), date('m'));
    }

    public function getWykorzystanyBudzetMiesiecznyBiezacy() {
        return $this->_budzetMiesieczny(date('y'), date('m'));
    }
    private function _budzetMiesieczny($rok, $miesiac) {
        $wydatki = $this->em->getRepository('Entities\Korporacyjny_Koszyk')->getWydanyBudzetUzytkownikaMiesieczny($this->_user, $rok, $miesiac);
        return (null != $wydatki ? $wydatki['suma'] : 0);
    }

}
