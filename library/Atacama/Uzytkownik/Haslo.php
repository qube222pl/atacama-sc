<?php

/**
 * Wysylanie e-maili systemowych
 *
 * @author Studio Moyoki
 */
class Atacama_Uzytkownik_Haslo
{
	private $_user;
	private $_crypt;
	private $_solt = 'atacama';
	
	public function  __construct($uzytkownik)
	{
		$this->_user = $uzytkownik;
		$this->_crypt = Moyoki_Crypt::getInstance();
	}


	/**
	 * Generuje link potrzebny do przywrocenia hasla
	 * @return string
	 */
	public function generujLink()
	{
		$url = Atacama_Config::getInstance()->appUrl;
		$haslo = $this->_generujHaslo();
		return array('url' => $url . 'pl/Haslo/zmien/a/' . $this->_user->getId() . '/b/' . md5($this->_user->getEmail(). $this->_solt . $haslo),
				'haslo' => $haslo);
		
	}
	
	/**
	 * Sprawdza czy przekazany w e-mailu parametr (B) jest zgodny z haslem
	 * @param string $haslo
	 * @param string $parametr 
	 * @return boolean
	 */
	public function sprawdzLink($haslo, $parametr)
	{
		$parametrGenerowany = md5($this->_user->getEmail(). $this->_solt . $haslo);

		if ($parametr == $parametrGenerowany)
			return true;
		
		return false;
	}

	private function _generujHaslo($length = 8)
	{
		$uppercase = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'W', 'Y', 'Z');
		$lowercase = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'w', 'y', 'z');
		$number = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

		$password = NULL;
		for ($i = 0; $i < $length; $i++) {
			$password .= $uppercase[rand(0, count($uppercase) - 1)];
			$password .= $lowercase[rand(0, count($lowercase) - 1)];
			$password .= $number[rand(0, count($number) - 1)];
		}
		return substr($password, 0, $length);
	}
}
