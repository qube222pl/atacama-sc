<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config
 */
class Atacama_Config {
    protected static $instance = null;    
    
            
    
    public static function getInstance() {
        if (!isset(self::$instance)) {
            if (is_file(APPLICATION_PATH . '/configs/application.ini'))                            
                self::$instance = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
            else if (is_file(APPLICATION_PATH . '/../application/configs/application.ini'))
                self::$instance = new Zend_Config_Ini(APPLICATION_PATH . '/../application/configs/application.ini', APPLICATION_ENV);
            else
                throw new Zend_Exception('Nie można zlokalizować pliku konfiguracyjnego application.ini');
        }
        
        return self::$instance;
    }
}
