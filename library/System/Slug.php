<?php

abstract class System_Slug
{

    /*
     *
     *  $options = array(
     *      'separator' => '-',
     *      'case'      => 'lower'
     *      'maxlength' => 100,
     *   );
     */

    public static function string2slug($string = '', $options = array())
	{

        $ReplacePolishSign = array(  
         'ą' => 'a', 'Ą' => 'A', 'ę' => 'e', 'Ę' => 'E',  
         'ć' => 'c', 'Ć' => 'C', 'ń' => 'n', 'Ń' => 'N', 'ł' => 'l',  
         'Ł' => 'L', 'ś' => 's', 'Ś' => 'S', 'ż' => 'z',  
         'Ż' => 'Z', 'ź' => 'z', 'Ź' => 'Z', 'ó' => 'o', 'Ó' => 'O'  
         );  

        if (!isset($options['separator'])) {
            $options['separator'] = '-';
        }

        if (!isset($options['case'])) {
            $options['case'] = 'lower';
        }

       
        $string = str_replace(array_keys($ReplacePolishSign), array_values($ReplacePolishSign), $string);

        $string = preg_replace('/[^A-Za-z0-9]/', $options['separator'], $string);

        if (isset($options['case'])) {
            if ($options['case'] == 'lower') {
                $string = strtolower($string);
            } else if ($options['case'] == 'upper') {
                $string = strtoupper($string);
            }
        }

        $string = preg_replace('/' . preg_quote($options['separator'], '/') . '{2,}/', $options['separator'], $string);
        $string = trim($string, $options['separator']);

        if (isset($options['maxlength'])) {
            
            $string = substr($string, 0, $options['maxlength']);
        }

        return $string;


	}

    
}