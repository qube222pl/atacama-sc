<?php

class System_XmlAttrIterator implements Iterator
{
    /**
     * XML file path
     *
     * @var string
     */
    protected $strFile = null;

    /**
     * XML reader object
     *
     * @var XMLReader
     */
    protected $reader = null;

    /**
     * Current item
     *
     * @var array
     */
    protected $item = null;

    /**
     * Dummy-key for iteration.
     * Has no real value except maybe act as a counter
     *
     * @var integer
     */
    protected $nKey = null;

    protected $strObjectTagname = 'xxxxxxxx';
    
    protected $valueTagName = 'tagValue';
    protected $attrTagName = 'attributes';

    function __construct($strFile, $tag)
    {
        $this->strFile = $strFile;
        $this->strObjectTagname = $tag;
    }

    public function current() {
        return $this->item;
    }

    public function key() {
        return $this->nKey;
    }

    public function next() {
        $this->item = null;
    }

    public function rewind() {
        $this->reader = new XMLReader();
        @$this->reader->open($this->strFile, null, LIBXML_NOERROR | LIBXML_NOWARNING | 1);
        $this->item = null;
        $this->nKey = null;
    }

    public function valid() {
        if ($this->item === null) {
            $this->loadNext();
        }

        return $this->item !== null;
    }

    /**
     * Loads the next item
     *
     * @return void
     */
    protected function loadNext()
    {
        $strElementName = null;
        $bCaptureValues = false;

        while(@$this->reader->read() !== false) {
            
//            $node_nazwa = $this->reader->name;
//            $node_typ = $this->reader->nodeType;
//            $node_has_attributes = $this->reader->hasAttributes;
//            if($node_has_attributes) {
//                $a = $this->reader->getAttributeNo(0);
//                $b = $this->reader->getAttributeNo(1);
//                $c = $this->reader->getAttribute('name1');
//                $c = $this->reader->getAttribute('name2');
//                
//            }
            
            switch ($this->reader->nodeType) {
                case XMLReader::ELEMENT:
                    $strElementName = $this->reader->name;
                    if ($bCaptureValues) {
                        
                        $empty = $this->reader->isEmptyElement;
                        
//                        if (!$empty) {
//                            $out[$strElementName] = $this->_processNode($strElementName);
//                        }
                        
                        $attr = array();
                        if($this->reader->hasAttributes) {
                             while ($this->reader->moveToNextAttribute()) {
                                $attr[$this->reader->name] = $this->reader->value;
                             }
                        }

                        if (!$empty) {
                            $node = $this->_processNode($strElementName);
                            if(is_array($node)) {
                                $out[$strElementName] = $node;
                                if(count($attr)) {
                                    $out[$strElementName][$this->attrTagName] = $attr;
                                }
                            } else {
                                if(count($attr)) {
                                    $out[$strElementName] = $attr;
                                }
                                $out[$strElementName][$this->valueTagName] = $node;
                            }
                        } else {

                            if(count($attr)) {
                                $out[$strElementName][$this->attrTagName] = $attr;
                            }
                        }

                    }
                    
                    if ($strElementName == $this->strObjectTagname) {
                        $bCaptureValues = true;
                        
                        // jeśli $this->strObjectTagname ma atrybuty to je dodajemy
                        $attr = array();
                        if($this->reader->hasAttributes) {
                             while ($this->reader->moveToNextAttribute()) {
                                $out[$this->strObjectTagname][$this->reader->name] = $this->reader->value;
                             }
                        }
                    }
                    break;

                case XMLReader::END_ELEMENT:
                    if ($this->reader->name == $this->strObjectTagname) {
                        $this->item = $out;
                        ++$this->nKey;
                        break 2;
                    }
                    break;
                    
            } // koniec switch
        } // koniec read()
    }
    
    private function _processNode($strEndTag)
    {
        
        $strElementName = null;
        $out = null;
//        $duplicates = false;
        
        while(@$this->reader->read() !== false) {
            
            $node_nazwa = $this->reader->name;
            $node_typ = $this->reader->nodeType;
        
            switch ($this->reader->nodeType) {
                case XMLReader::ELEMENT:
                    $strElementName = $this->reader->name;
                    
                    $empty = $this->reader->isEmptyElement;
                    
                    $attr = array();
                    if($this->reader->hasAttributes) {
                         while ($this->reader->moveToNextAttribute()) {
                            $attr[$this->reader->name] = $this->reader->value;
                         }
                    }
                    
                    if (!$empty) {
//                        $attr[0] = $this->_processNode($strElementName);
                        $attr[$this->valueTagName] = $this->_processNode($strElementName);
                    }
                    
                    $out[$strElementName][] = $attr;
                    break;

                case XMLReader::END_ELEMENT:
                    if ($this->reader->name == $strEndTag) {
                        break 2;
                    }
                    break;

                case XMLReader::TEXT:
                case XMLReader::CDATA:
                    $out = $this->reader->value;
                    break;
                
            } // koniec switch
        } // koniec read()
     
        return $out;
    }
    
}
