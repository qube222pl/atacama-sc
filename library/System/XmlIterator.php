<?php

class System_XmlIterator implements Iterator
{
    /**
     * XML file path
     *
     * @var string
     */
    protected $strFile = null;

    /**
     * XML reader object
     *
     * @var XMLReader
     */
    protected $reader = null;

    /**
     * Current item
     *
     * @var array
     */
    protected $item = null;

    /**
     * Dummy-key for iteration.
     * Has no real value except maybe act as a counter
     *
     * @var integer
     */
    protected $nKey = null;

    protected $strObjectTagname = 'xxxxxxxx';

    function __construct($strFile, $tag)
    {
        $this->strFile = $strFile;
        $this->strObjectTagname = $tag;
    }

    public function current() {
        return $this->item;
    }

    public function key() {
        return $this->nKey;
    }

    public function next() {
        $this->item = null;
    }

    public function rewind() {
        $this->reader = new XMLReader();
        @$this->reader->open($this->strFile, null, LIBXML_NOERROR | LIBXML_NOWARNING | 1);
        $this->item = null;
        $this->nKey = null;
    }

    public function valid() {
        if ($this->item === null) {
            $this->loadNext();
        }

        return $this->item !== null;
    }

    /**
     * Loads the next item
     *
     * @return void
     */
    protected function loadNext()
    {
        $strElementName = null;
        $bCaptureValues = false;

        while(@$this->reader->read() !== false) {
            
//            $node_nazwa = $this->reader->name;
//            $node_typ = $this->reader->nodeType;
//            $node_has_attributes = $this->reader->hasAttributes;
//            if($node_has_attributes) {
//                $a = $this->reader->getAttributeNo(0);
//                $b = $this->reader->getAttributeNo(1);
//                $c = $this->reader->getAttribute('name1');
//                $c = $this->reader->getAttribute('name2');
//                
//            }
            
            switch ($this->reader->nodeType) {
                case XMLReader::ELEMENT:
                    $strElementName = $this->reader->name;
                    $strIndexName = $strElementName;
                    if($this->reader->hasAttributes) {
                        for($i = 0; $i < $this->reader->attributeCount; $i++ ) {
                            $strIndexName .= ('_' . $this->reader->getAttributeNo($i));
                        }
                    }
                    if ($bCaptureValues) {
                        if (!$this->reader->isEmptyElement) {
                            $out[$strIndexName] = $this->_processNode($strElementName);
                        }
                    }
                    if ($strElementName == $this->strObjectTagname) {
                        $bCaptureValues = true;
                    }
                    break;

                case XMLReader::END_ELEMENT:
                    if ($this->reader->name == $this->strObjectTagname) {
                        $this->item = $out;
                        ++$this->nKey;
                        break 2;
                    }
                    break;
                    
            } // koniec switch
        } // koniec read()
    }
    
    private function _processNode($strEndTag)
    {
        
        $strElementName = null;
        $out = null;
        $duplicates = false;
        
        while(@$this->reader->read() !== false) {
            
//            $node_nazwa = $this->reader->name;
//            $node_typ = $this->reader->nodeType;
        
            switch ($this->reader->nodeType) {
                case XMLReader::ELEMENT:
                    $strElementName = $this->reader->name;
                    if (!$this->reader->isEmptyElement) {
                        if(!isset($out[$strElementName])) {
                            $out[$strElementName] = $this->_processNode($strElementName);
                        } else {
                            if($duplicates == false) {
                                $tmp = $out[$strElementName];
                                unset($out[$strElementName]);
                                $out[$strElementName][] = $tmp;
                                $duplicates = true;
                            }
                            $out[$strElementName][] = $this->_processNode($strElementName);
                        }
                    }
                    break;

                case XMLReader::END_ELEMENT:
                    if ($this->reader->name == $strEndTag) {
                        break 2;
                    }
                    break;

                case XMLReader::TEXT:
                case XMLReader::CDATA:
                    $out = $this->reader->value;
                    break;
                
            } // koniec switch
        } // koniec read()
     
        return $out;
    }
    
}
