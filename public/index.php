<?php

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
        APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini'
);

if (!defined('DONT_RUN_APP') || DONT_RUN_APP == false) {
    $application->bootstrap()
            ->run();
} else {
    //Zadania cron maja wylaczony layout
    //poprzez wywołanie index/cron

    $za = $application->bootstrap();
    $fc = Zend_Controller_Front::getInstance();
    $router = $fc->getRouter();
    foreach ($router->getRoutes() as $name => $route) {
            $router->removeRoute($name);
    }
    
    $router->addRoutes(array(
        'default' =>  new Zend_Controller_Router_Route('*', array('controller'=>'index', 'module'=>'default', 'action'=>'cron'))
    ));
            
    $za->run();
    
}