$(function () {
    $("select#select_kolor").change(function () {
        eval($(this).val());
    });
    //doAjax($('#index-stan').attr('url'), '#stan', null);

    sliders();
});


function pokazZdjecie(zdjecie, powiekszenie, url) {
    $("#zdjecie-big img").attr("src", zdjecie);
    $("#zdjecie-big a").attr("href", powiekszenie);
    if ('null' == url) {
        $("#do-koszyka").html('');
    } else {
        doAjax(url, '#do-koszyka', null);
    }
}

function przeliczCeny() {

    var ilosc = $("#ilosc").val();
    var kategoria = $("#kategoria").val();
    var produkt_id = $("#produkt_id").val();
    var kolor_id = $("#kolor_id").val();
    var rozmiar = $("#rozmiary").val();
    var cena1szt = $("#cena1szt");
    var netto = $("#netto");
    if (kategoria > 0) {
        $("#koszyk-darmo").hide();
        $("#modal-footer-menu").show();
        $.ajax({
            type: 'GET',
            data: {'ilosc': ilosc, 'kategoria': kategoria, 'produkt_id': produkt_id, 'rozmiar': rozmiar, 'kolor_id': kolor_id},
            url: _BASEURL + '/pl/Ajax/przelicz-produkt-detal/',
            success: function (msg) {
                var json = jQuery.parseJSON(msg);
                if (json.detal_id == 'error') {
                    alert('Przepraszamy blad aplikacji. Nie mozna dodac produktu do koszyka');
                } else if (json.cena == 'error') {
                    alert('Przepraszamy blad aplikacji. System nie moze przeliczyc ceny');
                } else {
                    $('#produkt_detal_id').val(json.detal_id);
                    var wynikCena;
                    if (json.cenaRegularna == '0.00' && json.cenaWyprzedaz == '0.00' && json.cenaPromocyjna == '0.00') {
                        $("#koszyk-darmo").show();
                        $("#koszyk-promocja-wartosc").html(0);
                        $("#modal-footer-menu").slideUp('slow');
                    } else if (json.cenaPromocyjna != '0.00') {
                        wynikCena = json.cenaPromocyjna;
                        $("#koszyk-promocja").show();
                        $("#koszyk-promocja-wartosc").html(json.cenaRegularna);

                    } else if (json.cenaWyprzedaz != '0.00') {
                        wynikCena = json.cenaWyprzedaz;
                        $("#koszyk-wyprzedaz").show();
                    } else {
                        wynikCena = json.cenaRegularna;
                    }

                    cena1szt.val(parseFloat(wynikCena).toFixed(2));
                    var wynikNetto = wynikCena * ilosc;
                    netto.val(parseFloat(wynikNetto).toFixed(2));
                }
            },
            error: function (msg, jqXHR, ajaxSettings, thrownError) {
                $('#netto').val('Błąd pobierania');
                console.log('msg', msg);
                console.log('jqXHR', jqXHR);
                console.log('ajaxSettings', ajaxSettings);
                console.log('thrownError', thrownError);
            }
        });
    }


}

function dodajZnakowanie(tablica) {
    var pozycjaznakowania = parseInt($("#pozycja-znakowania").val());
    $("#pozycja-znakowania").val(++pozycjaznakowania);
    var row = "";
    row += "<tr><td>";
    row += "<select name='rodzajznakowania_" + pozycjaznakowania + "' id='rodzajznakowania_" + pozycjaznakowania + "'>";
    row += "<option value =\"0\"> ----- </option>";
    for (i = 0; i < tablica.length; i++) {
        row += '<option value="' + tablica[i].id + '">' + tablica[i].nazwa + '</option>';
    }

    row += '</select></td>';
    row += '<td>';
    row += "<select name='koloryznakowania_" + pozycjaznakowania + "' id='koloryznakowania_" + pozycjaznakowania + "'>";
    row += "<option value =\"0\"> ----- </option>";
    row += '</select></td>';
    row += '</td>';
    row += '< td >';
    row += 'Button';
    row += '</td>';
    row += '</tr>';
    $('#tabela-koszyk-znakowanie tr:last').after(row);
}

function sliders() {

    // Product View Sliders
    $('#prod-slider')
            .slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                lazyLoad: 'ondemand',
                centerMode: false,
                vertical: true,
                adaptiveHeight: true,
                verticalSwiping: true,
                infinite: true,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }

                    }, {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                        }
                    }
                ]
            });


    $('#color-slider')
            .slick({
                centerMode: false,
                lazyLoad: 'ondemand',
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1
            });

    $('#color-slider .slick-slide, #prod-slider .slick-slide').click(function (e) {
        $("#img-zoom img").attr("src", $(this).data('image'));
        $("#img-zoom").attr("href", $(this).data('zoom-image'));
    });

    var prodTrigger = document.querySelector('.slider-single');
    /*
     new Drift(prodTrigger, {
     paneContainer: document.querySelector('.detail'),
     inlinePane: 900,
     inlineOffsetY: -85,
     zoomFactor: 1.5,
     containInline: true,
     sourceAttribute: 'href'
     });
     */

    new Luminous(prodTrigger);
}


function showPicture(thumb, pull, url) {
    $("#zdjecie-big img").attr("src", thumb);
    $("#zdjecie-big a").attr("href", pull);
    if ('null' == url) {
        $("#do-koszyka").html('');
    } else {
        doAjax(url, '#do-koszyka', null);
    }
}
