$(function() {

    // Przerobinie na ladny wyglad input file
    //$(":file").filestyle({input: false});

    $('.kolor-zmien').click(function() {
        $('#produkty_zdjecia_id').val($(this).attr('id'));


    });
    $('#progressbar').hide();
    $('.progress-bar').css('width', 0);

    $('#produktzdjecie').fileupload({
        dataType: 'json',
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
                error('Tylko pliki graficzne: jpg, gif, png');
                goUpload = false;
            }
            if (uploadFile.size > 2000000) { // 2mb
                error('Przekroczony dozwolony rozmiar pliku (2 MB)');
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }
        },
        progress: function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);

            $('#progressbar').show();
            $('.progress-bar').css('width', progress);

        },
        done: function(e, data) {
            $('#progressbar').hide();
            console.log(data.result);
            ProduktPobierzZdjecie(data.result);
        },
        fail: function(e, data) {
            $('#progressbar').hide();
            console.log(data.result);
        }
    });

    // Zmiana koloru
    $("#form-zmien-kolor").submit(function(event) {
        /* zatrzmujemy normalne dzialanie formularza */
        event.preventDefault();
        var postData = $(this).serializeArray();
        var target = $("#produkty_zdjecia_id").val() +'-kolor';
        
        $('#ModalKolor').modal('hide');
        $("#"+target).html("---");
        
        $.ajax({
            type: 'POST',
            data: postData,
            url: _BASEURL + '/pl/Ajax/produktzdjeciekolor/',
            statusCode: {
                500: function() {
                    $("#"+target).html("Application error");
                },
                404: function() {
                    $("#"+target).html("Page not found");
                }
            },
            success: function(msg) {
                
                $("#" + target).html(msg);
            },
            error: function(msg) {
                 $("#" + target).html(msg);
            }
        });
    });

});

function ProduktPobierzZdjecie(id) {
    $.ajax({
        type: 'GET',
        url: _BASEURL + '/pl/Ajax/produktpobierzzdjecie/id/' + id,
        success: function(msg) {
            $('#zdjecia').append(msg);

        },
        error: function(msg) {
            $('#zdjecia').append('<p>Błąd ładowania</p>');
        }
    });
}

function error(msg) {

    $("#error-alert")
            .fadeIn('slow')
            .html(msg)
            .delay(2000)
            .fadeOut('slow');
}