$(document).ready(function() {

    $("#koszyk-lista input").bind('propertychange input paste', function() {
        $("#zamawiam-button").fadeOut('slow');
        $("#przelicz-button").fadeIn('slow');
    });

    $("#koszyk-lista select").change(function() {
        $("#zamawiam-button").fadeOut('slow');
        $("#przelicz-button").fadeIn('slow');
    });


    $(".metoda-button").click(function(e) {
        e.preventDefault();
        obj = JSON.parse($(this).val());
        dodajMetodeZnakowania(obj.prod_id, obj.detal_id, obj.zam_id);
    });

});

function dodajMetodeZnakowania(prod_id, detal_id, zam_id) {
    $.ajax({
        type: 'GET',
        url: _BASEURL + '/pl/Koszyk/ajax-dodaj-znakowanie/prod_id/' + prod_id + "/detal_id/" + detal_id + "/edycja/" + zam_id,
        success: function(data) {
            $("#ajax-form-value").html(data);
        },
        error: function(data) {
            $("#ajax-form-value").html(data);
        }
    });
}

function usunprodukt(url, target) {

    var answer = confirm("Na pewno usunąć produkt ?")
    if (answer) {
        $(target).css('border', '1px solid #EEE');
        $.ajax({
            url: url,
            success: function(data) {
                var json = jQuery.parseJSON(data);
                if (json.error == '') {
                    $(target).fadeOut('slow');
                    $("#razemProduktow").html(json.razemProduktow);
                    $("#razemNetto").html(json.razemNetto);
                    $("#razemBrutto").html(json.razemBrutto);
                } else {
                    alert(json.error);
                }
            },
            error: function(data) {
                $(target).html(data).addClass('ajaxerror');
            }
        });
    }
}