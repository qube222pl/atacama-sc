$(function () {
//Search-box expanding
    $('.search-bar').blur();
    $('.search-bar').focus(
            function () {
                $("#szukane").animate({width: '80%'});
            }).blur(
            function () {
                $("#szukane").animate({width: '50%'});
            });

    $("input.numeric").numeric({decimal: ",", negative: false});
    $('span.email').each(function () {
        var subject = $(this).attr('subject');
        var at = / at /;
        var dot = / dot /g;
        var addr = $(this).text().replace(at, "@").replace(dot, ".");
        if (null != subject) {
            $(this).after('<a href="mailto:' + addr + '?subject=' + subject + '" title="' + subject + '">' + addr + '</a>');
        } else {
            $(this).after('<a href="mailto:' + addr + '" title="' + addr + '">' + addr + '</a>');
        }
        $(this).remove();
    });
    $('#szukane').typeahead({
        name: 'wyszukiwarka',
        prefetch: _BASEURL + '/pl/Ajax/szukaj',
        limit: 10
    });
    $("#menu-produkty-click").click(function () {
        $('#menu-produkty .row').toggle("fade");
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
// scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('[data-toggle="tooltip"]').tooltip();


    // Home: Add class on hover
    $('.product-card-box').hover(
            function () {
                $(this).addClass('hover')
            },
            function () {
                $(this).removeClass('hover')
            }
    );

    /**
     * RODO
     */
    if ($("#modal-rodo").length > 0) {
        $("#modal-rodo").show();
        $("#rodo-potwiedzam").click(function () {
            var url = _BASEURL + '/pl/Ajax/rodo-akceptacja/';
            $("#modal-rodo").hide();
        });

        $("#modal-rodo .close").click(function () {
            $("#modal-rodo").hide();
        });
    }
});


