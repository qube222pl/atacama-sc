$(document).ready(function() {

    init();

    $('#adv-search-przelacznik').click(function() {

        if ($('#adv-search-form').is(':visible')) {
            $('#adv-search-form').slideUp('slow');
            $(this).removeClass('glyphicon-circle-arrow-up').addClass('glyphicon-circle-arrow-down');
        } else {
            $('#adv-search-form').slideDown('slow');
            $(this).removeClass('glyphicon-circle-arrow-down').addClass('glyphicon-circle-arrow-up');
        }
    });

    $('.kolory .kolor-element').click(function() {
        $('.kolory .thumbnail').each(function() {
            $(this).removeClass('kolor-wybrany');
        });
        var p = $(this).parent();
        p.addClass('kolor-wybrany');

    });
});


function init() {
    var radio = $('input.radio:radio:checked').val();
    if (radio != "undefined") {
        $("label[for='kolory-" + radio + "'] .thumbnail").addClass('kolor-wybrany');
        console.log(radio);
    }
}