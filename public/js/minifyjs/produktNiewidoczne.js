$(document).ready(function () {

});

function pokazModal(href) {
    $('#podgladModal').modal('show');

    $.ajax({
        type: 'GET',
        url: href,
        beforeSend: function () {
            $('#podgladModal .modal-content').html('<div class="animated infinite flash">Pobieranie danych ...</div>');
        },
        success: function (msg) {
            $('#podgladModal .modal-content').html(msg);

        },
        error: function (msg) {
            $('#podgladModal .modal-content').html(msg);
        }
    });
}


function zmienStatus(id, status) {
    $.ajax({
        type: 'GET',
        data: {'id': id, 'status': status},
        url: _BASEURL + '/pl/Ajax/niewidoczne-produkt-zmien/',
        success: function (msg) {
            console.log('success: ' + msg);
            $('#podgladModal').modal('hide');
        },
        complete: function () {
            $('#row-' + id).addClass('animated fadeOutDown').fadeOut('slow');
        },

        error: function (msg) {
            $('#podgladModal .modal-content').html(msg);
        }
    });
}