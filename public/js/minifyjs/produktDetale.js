$(function() {

    $("#dodaj-detal").click(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'GET',
            url: _BASEURL + '/pl/Ajax/produktdodajdetal/',
            success: function(msg) {
                $('#produkt-detale-tabela tr:last').after(msg);

            },
            error: function(msg) {
                $('#zdjecia').append('<p>Błąd ładowania</p>');
            }
        });

    });
});
