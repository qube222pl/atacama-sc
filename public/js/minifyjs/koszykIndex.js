$(document).ready(function () {

    $("#koszyk-lista input").bind('propertychange input paste', function () {
        $("#zamawiam-button").fadeOut('slow');
        $("#przelicz-button").fadeIn('slow');
    });

    $("#koszyk-lista select").change(function () {
        $("#zamawiam-button").fadeOut('slow');
        $("#przelicz-button").fadeIn('slow');
    });


    $("#klient").change(function () {
        $("#zamawiam-button").fadeOut('slow');
        $("#przelicz-button").fadeIn('slow');
    });


    $(".metoda-button").click(function (e) {
        e.preventDefault();
        obj = JSON.parse($(this).val());
        dodajMetodeZnakowania(obj.prod_id, obj.detal_id);
    });

});

function dodajMetodeZnakowania(prod_id, detal_id) {
    $.ajax({
        type: 'GET',
        url: _BASEURL + '/pl/Koszyk/ajax-dodaj-znakowanie/prod_id/' + prod_id + "/detal_id/" + detal_id,
        success: function (data) {
            $("#ajax-form-value").html(data);
        },
        error: function (data) {
            $("#ajax-form-value").html(data);
        }
    });
}