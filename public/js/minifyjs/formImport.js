$(document).ready(function() {
    /*
     * Na poczatek wylaczymy submit, bo nie ma czego zatwierdzac
     */
    $("#form-import .submit").hide();
    $(".file-input").hide();
    
    $("#form-import #dystrybutor").change(function() {
         $(".file-input").hide();
         $("#form-import .submit").hide();
        var val = $("#form-import #dystrybutor").find(":selected").val();
        $.ajax({
            type: 'POST',
            data: 'dystrybutor=' + val,
            url: _BASEURL + '/pl/Import/ajax-form/',
            success: function(msg) {
                $('#form-import #placer').html(msg);
            },
            error: function(msg) {
                $('#form-import #placer').append('<p>Błąd ładowania</p>');
            }
        });
    });
});
