var budzet = 0, suma;

$(function () {
    budzet = $("#budzet").data("budzet");
    $("#zamowienia-specjalne-koszyk .ilosc").change(function () {
        przeliczKoszyk();
    });

});
function przeliczKoszyk() {
    var ilosc = 0, cena, ilosc_min, stock, alert;
    suma = 0;
    $('#zamowienia-specjalne-koszyk .pozycja').each(function () {
        ilosc = $(this).find('.ilosc').val();
        if (undefined != ilosc && ilosc > 0) {
            cena = $(this).find('.cena').data('cena');
            ilosc_min = $(this).find('.ilosc_min').data('ilosc_min');
            alert = $(this).find('.alert');
            suma += cena * ilosc;
        }

    });
    sprawdzBudzet();
    console.log('suma', suma);
    $("#suma-netto").html(suma.toFixed(2));
}
function potwierdzUsuniecie(id) {
return confirm('Czy na pewno chcesz usunąć produkt z oferty?');
    
}

function sprawdzBudzet() {
    var submit = $("#zamow-submit");
    submit.prop("disabled", false);
    if (suma > budzet) {
        alert("Budżet został przekroczony");
        submit.prop("disabled", true);
    }
}

function usunElement(el) {
    $("#el-" + el).addClass('animated flipOutY');

    setTimeout(function () {
        $("#el-" + el).remove();
    }, 1000);

    setTimeout(function () {
        przeliczZnakowanie();
    }, 1000);
}

function pokazBlad(el, val) {
    console.log('blad', el);
    $(el).show().html(val);
}
function ukryjBlad(el) {
    $(el).hide().html('');
}