$(function() {

    $("#dodaj-rodzaj").click(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'GET',
            url: _BASEURL + '/pl/Ajax/kategoriecenydodaj/',
            success: function(msg) {
                $('#kategoria-ceny-tabela tr:last').after(msg);
                
            },
            error: function(msg) {
                $('#zdjecia').append('<p>Błąd ładowania</p>');
            }
        });

    });
});
