$(document).ready(function () {

    $('#data_dostawy').datepicker({
            uiLibrary: 'bootstrap4',
            //locale: 'pl-pl',
            disableDaysOfWeek: [0, 6],
            format: 'yyyy-mm-dd',
            minDate: $('#data_dostawy').val(),
            weekStartDay: 1
        });
});
