$(function() {

    $("#dodaj-rodzaj").click(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'GET',
            url: _BASEURL + '/pl/Ajax/znakowanierodzajdodaj/',
            success: function(msg) {
                $('#znakowanie-rodzaje-tabela tr:last').after(msg);
                
            },
            error: function(msg) {
                $('#zdjecia').append('<p>Błąd ładowania</p>');
            }
        });

    });
});
