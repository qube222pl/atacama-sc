$(function() {
    $('#progressbar').hide();
    $('.progress-bar').css('width', 0);

    $('#galeriazdjecie').fileupload({
        dataType: 'json',
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
                error('Tylko pliki graficzne: jpg, gif, png');
                goUpload = false;
            }
            if (uploadFile.size > 2000000) { // 2mb
                error('Przekroczony dozwolony rozmiar pliku (2 MB)');
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }
        },
        progress: function(e, data) {
            console.log(data);
            var progress = parseInt(data.loaded / data.total * 100, 10);

            $('#progressbar').show();
            $('.progress-bar').css('width', progress);

        },
        done: function(e, data) {
            $('#progressbar').hide();
            console.log("done: " + data.result);
            if ($.isNumeric(data.result)) {
                GaleriaPobierzZdjecie(data.result);
            }
        },
        fail: function(e, data) {
            $('#progressbar').hide();
            console.log("error: " + data.result);
        }
    });
});

function GaleriaPobierzZdjecie(id) {
    $.ajax({
        type: 'GET',
        url: _BASEURL + '/pl/Ajax/galeriapobierzzdjecie/id/' + id,
        success: function(msg) {
            $('#zdjecia').append(msg);

        },
        error: function(msg) {
            $('#zdjecia').append('<p>Błąd ładowania</p>');
        }
    });
}

function error(msg) {

    $("#error-alert")
            .fadeIn('slow')
            .html(msg)
            .delay(2000)
            .fadeOut('slow');
}